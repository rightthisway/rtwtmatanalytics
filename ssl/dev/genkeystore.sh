#!/bin/sh

# import key, cert and root cert into keystore
# we need to convert the key and cert into DER format before importing

# concatenate yoonew cert and digicert
cat ../star_yoonew_com.crt ../DigiCertCA.crt  > mix_cert.crt
openssl pkcs8 -topk8 -nocrypt -in ../server.key -inform PEM -out server.der -outform DER
openssl x509 -in mix_cert.crt -inform PEM -out mix_cert.der -outform DER

javac ImportKey.java
java -Dkeystore=keystore.jks ImportKey server.der mix_cert.der 
rm -f *.der
rm *.class
rm mix_cert.*
