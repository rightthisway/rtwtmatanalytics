To deploy the war copy the file target/tmat-1.war to your Tomcat webapps
directory.

This will deploy the web application as well as one crawler.

If you want to run an additional crawler, you can run 
go to target/tmat-1-full and run crawler.bat or crawler.sh

Start tomcat before the additional crawler.
Also if you kill a crawler, then you need to restart the whole application
(we haven't implemented all the hotplug and fault tolerance features yet)