--
-- demo/demo
-- admin/admin
-- editor/editor
--

INSERT INTO user (username, password, email, first_name, last_name, phone, locked)
VALUES
('demo', sha('demo'), 'francois@yoonew.com', 'Demo', 'demo', '', false),
('admin', sha('admin'), 'admin@yoonew.com', 'Administrator', 'administrator', '', false),
('editor', sha('editor'), 'admin@yoonew.com', 'Editor', 'editor', '', false);

INSERT INTO role (id, description)
VALUES
('ROLE_USER', 'Any user'),
('ROLE_EDITOR', 'Editor'),
('ROLE_ADMIN', 'Administrator');

INSERT INTO user_role (username, role_id)
VALUES
('demo', 'ROLE_USER'),
('editor', 'ROLE_USER'),
('editor', 'ROLE_EDITOR'),
('admin', 'ROLE_ADMIN'),
('admin', 'ROLE_EDITOR'),
('admin', 'ROLE_USER');
