--
-- Jan 11 2009 and Jan 18  2009
--

INSERT INTO artist (id, name) VALUES(2, 'New York Giants');
INSERT INTO tour(id, artist_id, name, location, start_date, end_date) VALUES (2, 2, 'NY Giants Divisional Playoff Game', 'East Rutherford, NJ, United States', '2009-01-11', '2009-01-18');
INSERT INTO venue (id,  building, city, state, country) VALUES
	(40, 'Giants Stadium ', 'New Jersey ', 'NJ', 'US');
	
INSERT INTO event (id, tour_id, event_date, venue_id) VALUES
(50, 2, '2009-01-11', 40),
(51, 2, '2009-01-18', 40);


INSERT INTO ticket_listing_crawl (id, name, site_id, crawl_frequency, tour_id, event_id, enabled, extra_parameters) VALUES (2, 'ebay-playoff-ny-giants-2009-01-10', 'ebay', 3600, 2, 50, true, "queryYear=2009\nqueryMonth=1\nqueryDay=10\nqueryString=NY Giants");
INSERT INTO ticket_listing_crawl (id, name, site_id, crawl_frequency, tour_id, event_id, enabled, extra_parameters) VALUES (3, 'ebay-playoff-ny-giants-2009-01-11', 'ebay', 3600, 2, 50, true, "queryYear=2009\nqueryMonth=1\nqueryDay=11\nqueryString=NY Giants");
INSERT INTO ticket_listing_crawl (id, name, site_id, crawl_frequency, tour_id, event_id, enabled, extra_parameters) VALUES (4, 'ebay-playoff-ny-giants-2009-01-18', 'ebay', 3600, 2, 51, true, "queryYear=2009\nqueryMonth=1\nqueryDay=18\nqueryString=NY Giants");
