package com.admitone.tmat.ticketfetcher;

import java.util.Collection;

import org.testng.annotations.Test;

import com.admitone.tmat.crawler.InterruptedTicketListingCrawlException;
import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.ticketfetcher.FanSnapTicketListingFetcher;
import com.admitone.tmat.ticketfetcher.TicketHit;
import com.admitone.tmat.ticketfetcher.TicketListingFetcher;

/**
 * Class to test the FanSnap Ticket Listing Fetcher.
 */
public class FanSnapTicketListingFetcherTest {

	@Test(groups="ticketfetcher")
	public void testTicketListingFetcher() {
		TestTicketHitIndexer ticketHitIndexer = new TestTicketHitIndexer(null);
		TicketListingFetcher ticketListingFetcher = new FanSnapTicketListingFetcher();
				
		String[] urls = {
				"http://www.fansnap.com/boise-state-broncos-tickets/san-jose-state-spartans-vs-boise-state-broncos/october-31-2009-136416",
				"http://www.fansnap.com/metallica-tickets/december-12-2009-134425",
				"http://www.fansnap.com/billy-elliot-tickets/88498/87472/88533/january-03-2010-88499"
		};

		String[] wrongUrls = {
				"http://www.fansnap.com/metallica-tickets/december-12-2009-123456",
				"http://www.fansnap.com/boise-state-broncos-tickets/san-jose-state-spartans-vs-boise-state-broncos/october-31-2009-100000"
		};		
		
		for (String url: urls) {
			System.out.println("URL=" + url);
			TicketListingCrawl ticketListingCrawl = new TicketListingCrawl();
			ticketListingCrawl.setSiteId(Site.FAN_SNAP);
			ticketListingCrawl.setQueryUrl(url);
			ticketListingCrawl.resetStats();
			
			boolean error = false;
			try {
				ticketListingFetcher.fetchTicketListing(ticketHitIndexer, ticketListingCrawl);
			} catch (InterruptedTicketListingCrawlException e) {
				// do nothing
			} catch (Exception e) {
				e.printStackTrace();
				error = true;
			}
			assert error == false;
			
			Collection<TicketHit> ticketHits = ticketHitIndexer.getFetchedTicketHits();
			assert ticketHits.size() > 0;
			
			System.out.println("FETCHED=" + ticketHits.size());
			
			ticketHitIndexer.reset();			
		}		

		for (String url: wrongUrls) {
			System.out.println("URL=" + url);
			TicketListingCrawl ticketListingCrawl = new TicketListingCrawl();
			ticketListingCrawl.setSiteId(Site.FAN_SNAP);
			ticketListingCrawl.setQueryUrl(url);
			ticketListingCrawl.resetStats();
			
			boolean error = false;
			try {
				ticketListingFetcher.fetchTicketListing(ticketHitIndexer, ticketListingCrawl);
			} catch (InterruptedTicketListingCrawlException e) {
				// do nothing
			} catch (Exception e) {
				e.printStackTrace();
				error = true;
			}
			assert error == false;
			
			Collection<TicketHit> ticketHits = ticketHitIndexer.getFetchedTicketHits();
			assert ticketHits.size() == 0;
			
			ticketHitIndexer.reset();			
		}		

	}
}
