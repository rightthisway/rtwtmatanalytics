package com.admitone.tmat.ticketfetcher;

import java.util.Collection;

import org.testng.annotations.Test;

import com.admitone.tmat.crawler.InterruptedTicketListingCrawlException;
import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.ticketfetcher.OldEIMarketPlaceTicketListingFetcher;
import com.admitone.tmat.ticketfetcher.TicketHit;
import com.admitone.tmat.utils.eimarketplace.EIMPSimpleCredential;

/**
 * Class to test the EiMarketPlace Ticket Listing Fetcher.
 */
public class OldEiMarketPlaceTicketListingFetcherTest {

	@Test(groups="ticketfetcher")
	public void testTicketListingFetcher() throws Exception{
		TestTicketHitIndexer ticketHitIndexer = new TestTicketHitIndexer(null);
		OldEIMarketPlaceTicketListingFetcher ticketListingFetcher = new OldEIMarketPlaceTicketListingFetcher();
		ticketListingFetcher.setCredential(new EIMPSimpleCredential("admit1", "shortit09"));
				
		String[] queryEventIds = {
				"819991",
				"740906",
				"808877", //u2
				"766481", // Billy Elliot
				"817184" // mettalica
		};

		String[] wrongQueryEventIds = {
				"100000", 
				"234567" 
		};

		for (String queryEventId: queryEventIds) {
			System.out.println("EVENT INVENTORY EVENTID=" + queryEventId);
			TicketListingCrawl ticketListingCrawl = new TicketListingCrawl();
			ticketListingCrawl.setSiteId(Site.EI_MARKETPLACE);
			ticketListingCrawl.setExtraParameter("queryEventId", queryEventId);
			ticketListingCrawl.resetStats();
			
			boolean error = false;
			try {
				ticketListingFetcher.fetchTicketListing(ticketHitIndexer, ticketListingCrawl);
			} catch (InterruptedTicketListingCrawlException e) {
				// do nothing
			} catch (Exception e) {
				e.printStackTrace();
				error = true;
			}
			assert error == false;
			
			Collection<TicketHit> ticketHits = ticketHitIndexer.getFetchedTicketHits();
			assert ticketHits.size() > 0;
			
			ticketHitIndexer.reset();
		}		

		for (String queryEventId: wrongQueryEventIds) {
			System.out.println("EVENT INVENTORY EVENTID=" + queryEventId);
			TicketListingCrawl ticketListingCrawl = new TicketListingCrawl();
			ticketListingCrawl.setSiteId(Site.EI_MARKETPLACE);
			ticketListingCrawl.setExtraParameter("queryEventId", queryEventId);
			ticketListingCrawl.resetStats();
			
			boolean error = false;
			try {
				ticketListingFetcher.fetchTicketListing(ticketHitIndexer, ticketListingCrawl);
			} catch (InterruptedTicketListingCrawlException e) {
				// do nothing
			} catch (Exception e) {
				e.printStackTrace();
				error = true;
			}
			assert error == false;
			
			Collection<TicketHit> ticketHits = ticketHitIndexer.getFetchedTicketHits();
			assert ticketHits.size() == 0;
			
			ticketHitIndexer.reset();
		}		

	}
}
