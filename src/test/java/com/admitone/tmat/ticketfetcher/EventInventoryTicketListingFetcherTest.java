package com.admitone.tmat.ticketfetcher;

import java.util.Collection;

import org.testng.annotations.Test;

import com.admitone.tmat.crawler.InterruptedTicketListingCrawlException;
import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.ticketfetcher.EventInventoryTicketListingFetcher;
import com.admitone.tmat.ticketfetcher.TicketHit;
import com.admitone.tmat.ticketfetcher.TicketListingFetcher;

/**
 * Class to test the Event Inventory Ticket Listing Fetcher.
 */
public class EventInventoryTicketListingFetcherTest {

	@Test(groups="ticketfetcher")
	public void testTicketListingFetcher() {
		TestTicketHitIndexer ticketHitIndexer = new TestTicketHitIndexer(10);
		TicketListingFetcher ticketListingFetcher = new EventInventoryTicketListingFetcher();
				
		String[] urls = {
				"http://www.eventinventory.com/search/results.cfm?cfid=133612010&cftoken=f02f02-07691964-bdef-4474-86fe-62f8cec0a245&cfuser=B3D3E02D-727E-447E-9A843E59DF6C5731&client=1337&d=8/9/2009&e=349&id=10&restart=yes&s=1&v=2486&cfid=133612010&cftoken=f02f02-07691964-bdef-4474-86fe-62f8cec0a245&cfuser=B3D3E02D-727E-447E-9A843E59DF6C5731&RefList=",
				"http://www.eventinventory.com/search/results.cfm?Client=1337&restart=yes&e=901&v=93&d=9/13/2009&s=1",
				"http://www.eventinventory.com/search/results.cfm?restart=yes&client=1337&e=560&v=5597&s=1&month=8&day=5&year=2009&p=735251&cfid=133612010&cftoken=f02f02-07691964-bdef-4474-86fe-62f8cec0a245&cfuser=B3D3E02D-727E-447E-9A843E59DF6C5731&RefList=#search_top"
		};

		String[] wrongUrls = {
				"http://www.eventinventory.com/search/results.cfm?cfid=133612010&cftoken=f02f02-07691964-bdef-4474-86fe-62f8cec0a245&cfuser=B3D3E02D-727E-447E-9A843E59DF6C5731&client=1337&d=8/9/2009&e=1&id=10&restart=yes&s=1&v=2486&cfid=133612010&cftoken=f02f02-07691964-bdef-4474-86fe-62f8cec0a245&cfuser=B3D3E02D-727E-447E-9A843E59DF6C5731&RefList=",
				"http://www.eventinventory.com/search/results.cfm?Client=1337&restart=yes&e=2&v=93&d=9/13/2009&s=1",
		};

		for (String url: urls) {
			System.out.println("EVENT INVENTORY URL=" + url);
			TicketListingCrawl ticketListingCrawl = new TicketListingCrawl();
			ticketListingCrawl.setSiteId(Site.EVENT_INVENTORY);
			ticketListingCrawl.setQueryUrl(url);
			ticketListingCrawl.resetStats();
			
			boolean error = false;
			try {
				ticketListingFetcher.fetchTicketListing(ticketHitIndexer, ticketListingCrawl);
			} catch (InterruptedTicketListingCrawlException e) {
				// do nothing
			} catch (Exception e) {
				e.printStackTrace();
				error = true;
			}
			assert error == false;
			
			Collection<TicketHit> ticketHits = ticketHitIndexer.getFetchedTicketHits();
			assert ticketHits.size() > 0;
			
			ticketHitIndexer.reset();
		}		

		for (String url: wrongUrls) {
			System.out.println("EVENT INVENTORY URL=" + url);
			TicketListingCrawl ticketListingCrawl = new TicketListingCrawl();
			ticketListingCrawl.setSiteId(Site.EVENT_INVENTORY);
			ticketListingCrawl.setQueryUrl(url);
			ticketListingCrawl.resetStats();
			
			boolean error = false;
			try {
				ticketListingFetcher.fetchTicketListing(ticketHitIndexer, ticketListingCrawl);
			} catch (InterruptedTicketListingCrawlException e) {
				// do nothing
			} catch (Exception e) {
				e.printStackTrace();
				error = true;
			}
			assert error == false;
			
			Collection<TicketHit> ticketHits = ticketHitIndexer.getFetchedTicketHits();
			assert ticketHits.size() == 0;
			
			ticketHitIndexer.reset();
		}		

	}
}
