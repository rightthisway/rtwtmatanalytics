package com.admitone.tmat.ticketfetcher;

import java.util.Collection;

import org.testng.annotations.Test;

import com.admitone.tmat.crawler.InterruptedTicketListingCrawlException;
import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.ticketfetcher.TicketHit;
import com.admitone.tmat.ticketfetcher.TicketListingFetcher;
import com.admitone.tmat.ticketfetcher.TicketMasterTicketListingFetcher;

/**
 * Class to test the TicketMaster Ticket Listing Fetcher.
 */
public class TicketMasterTicketListingFetcherTest {

	@Test(groups="ticketfetcher")
	public void testTicketListingFetcher() {
		TestTicketHitIndexer ticketHitIndexer = new TestTicketHitIndexer(10);
		TicketListingFetcher ticketListingFetcher = new TicketMasterTicketListingFetcher();
				
		String[] urls = {
				// team exchange
				"https://teamexchange.ticketmaster.com/html/postinglist.htmI?l=EN&team=iaspeed&EVNT=R0919&CNTX=",
				"https://teamexchange.ticketmaster.com/html/seatlist.htmI?l=EN&team=seattlemls&EVNT=QF1024&CNTX=",
				"https://teamexchange.ticketmaster.com/html/seatlist.htmI?l=EN&team=galaxy&EVNT=G102409&CNTX="
		};

		String[] wrongUrls = {
				// team exchange
				"https://teamexchange.ticketmaster.com/html/postinglist.htmI?l=EN&team=iaspeed&EVNT=R0007&CNTX=",
				"https://teamexchange.ticketmaster.com/html/seatlist.htmI?l=EN&team=galaxy&EVNT=G000009&CNTX=",
				"https://teamexchange.ticketmaster.com/html/seatlist.htmI?l=EN&team=myteam&EVNT=G102409&CNTX="				
		};

		for (String url: urls) {			
			System.out.println("TICKETMASTER URL=" + url) ;
			TicketListingCrawl ticketListingCrawl = new TicketListingCrawl();
			ticketListingCrawl.setSiteId(Site.TICKET_MASTER);
			ticketListingCrawl.setQueryUrl(url);
			ticketListingCrawl.resetStats();
			
			boolean error = false;
			try {
				ticketListingFetcher.fetchTicketListing(ticketHitIndexer, ticketListingCrawl);
			} catch (InterruptedTicketListingCrawlException e) {
				// do nothing
			} catch (Exception e) {
				e.printStackTrace();
				error = true;
			}
			assert error == false;
			
			Collection<TicketHit> ticketHits = ticketHitIndexer.getFetchedTicketHits();
			assert ticketHits.size() > 0;
			
			ticketHitIndexer.reset();
		}		

		for (String url: wrongUrls) {			
			System.out.println("TICKETMASTER URL=" + url) ;
			TicketListingCrawl ticketListingCrawl = new TicketListingCrawl();
			ticketListingCrawl.setSiteId(Site.TICKET_MASTER);
			ticketListingCrawl.setQueryUrl(url);
			ticketListingCrawl.resetStats();
			
			boolean error = false;
			try {
				ticketListingFetcher.fetchTicketListing(ticketHitIndexer, ticketListingCrawl);
			} catch (InterruptedTicketListingCrawlException e) {
				// do nothing
			} catch (Exception e) {
				e.printStackTrace();
				error = true;
			}
			assert error == false;
			
			Collection<TicketHit> ticketHits = ticketHitIndexer.getFetchedTicketHits();
			assert ticketHits.size() == 0;
			
			ticketHitIndexer.reset();
		}		

	}
}
