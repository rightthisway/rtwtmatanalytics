package com.admitone.tmat.ticketfetcher;

import java.io.FileOutputStream;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.testng.annotations.Test;

import com.admitone.tmat.crawler.InterruptedTicketListingCrawlException;
import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.ticketfetcher.TicketNetworkDirectTicketListingFetcher;
import com.admitone.tmat.ticketfetcher.TicketNetworkTicketListingFetcher;
import com.admitone.tmat.ticketfetcher.TicketHit;
import com.admitone.tmat.ticketfetcher.TicketListingFetcher;

/**
 * Class to test the StubHub Ticket Listing Fetcher.
 * mvn clean -Dtest=com.admitone.tmat.ticketfetcher.TicketNetworkWebAndWSTicketListingFetcherTest test
 */
public class TicketNetworkWebAndWSTicketListingFetcherTest {
	
	public static void main(String args[]) throws Exception {
		TicketNetworkWebAndWSTicketListingFetcherTest test = new TicketNetworkWebAndWSTicketListingFetcherTest();
		test.testTicket();
	}

	@Test
	public void testTicket() throws Exception{
	
		TestTicketHitIndexer wsTicketHitIndexer = new TestTicketHitIndexer(10000);
		TestTicketHitIndexer webTicketHitIndexer = new TestTicketHitIndexer(10000);
		TicketListingFetcher wsTicketListingFetcher = new TicketNetworkDirectTicketListingFetcher();
		TicketListingFetcher webTicketListingFetcher = new TicketNetworkTicketListingFetcher();
				
		String[] urls = {
				/*
				"http://zeromarkup.com/ResultsTicket.aspx?evtid=1318864&event=Vampire+Weekend",
				"http://zeromarkup.com/ResultsTicket.aspx?evtid=1329665&event=Black+Eyed+Peas",
				"http://zeromarkup.com/ResultsTicket.aspx?evtid=1347024&event=Rihanna+%26+Kesha"
				*/
				"http://zeromarkup.com/ResultsTicket.aspx?evtid=1185758&event=Cleveland+Indians+vs.+New+York+Yankees",
				"http://zeromarkup.com/ResultsTicket.aspx?evtid=1186428&event=New+York+Yankees+vs.+Toronto+Blue+Jays",
				"http://zeromarkup.com/ResultsTicket.aspx?evtid=1185628&event=Boston+Red+Sox+vs.+New+York+Yankees",
				
				"http://zeromarkup.com/ResultsTicket.aspx?evtid=1237526&event=Ohio+State+Buckeyes+vs.+Miami+Hurricanes",
				"http://zeromarkup.com/ResultsTicket.aspx?evtid=1229608&event=Wisconsin+Badgers+vs.+Ohio+State+Buckeyes",
				"http://zeromarkup.com/ResultsTicket.aspx?evtid=1237532&event=Ohio+State+Buckeyes+vs.+Michigan+Wolverines",
				
				"http://zeromarkup.com/ResultsTicket.aspx?evtid=1214108&event=Notre+Dame+Fighting+Irish+vs.+Stanford+Cardinal",
				"http://zeromarkup.com/ResultsTicket.aspx?evtid=1214110&event=Notre+Dame+Fighting+Irish+vs.+Utah+Utes",
				"http://zeromarkup.com/ResultsTicket.aspx?evtid=1237522&event=USC+Trojans+vs.+Notre+Dame+Fighting+Irish"
		};

		for (String url: urls) {
			
			Set<String> wsTicketIds = new HashSet<String>();
			Set<String> webTicketIds = new HashSet<String>();
			
			TicketListingCrawl webTicketListingCrawl = new TicketListingCrawl();
			webTicketListingCrawl.setSiteId(Site.TICKET_NETWORK);
			webTicketListingCrawl.setQueryUrl(url);
			webTicketListingCrawl.resetStats();
	
			TicketListingCrawl wsTicketListingCrawl = new TicketListingCrawl();
			wsTicketListingCrawl.setSiteId(Site.TICKET_NETWORK_DIRECT);
			wsTicketListingCrawl.setQueryUrl(url);
			wsTicketListingCrawl.resetStats();
	
			boolean error = false;
			try {
				wsTicketListingFetcher.fetchTicketListing(wsTicketHitIndexer, wsTicketListingCrawl);
				webTicketListingFetcher.fetchTicketListing(webTicketHitIndexer, webTicketListingCrawl);
			} catch (InterruptedTicketListingCrawlException e) {
				// do nothing
			} catch (Exception e) {
				e.printStackTrace();
				error = true;
			}
			assert error == false;
			
			Collection<TicketHit> wsTicketHits = wsTicketHitIndexer.getFetchedTicketHits();
			Map<String, TicketHit> wsTicketById = new HashMap<String, TicketHit>();
			for(TicketHit wsTicketHit: wsTicketHits) {
				// we are not using ticket id for the comparison as they are different on zeromarkup
				// and TND
				// we use <SECTION>-<ROW> combination instead
				wsTicketHit.setItemID(wsTicketHit.getSection() + "-" + wsTicketHit.getRow());
				wsTicketIds.add(wsTicketHit.getItemId());
				wsTicketById.put(wsTicketHit.getItemId(), wsTicketHit);
			}
			
			Collection<TicketHit> webTicketHits = webTicketHitIndexer.getFetchedTicketHits();
			Map<String, TicketHit> webTicketById = new HashMap<String, TicketHit>();
			for(TicketHit webTicketHit: webTicketHits) {
				// we are not using ticket id for the comparison as they are different on zeromarkup
				// and TND
				// we use <SECTION>-<ROW> combination instead
				webTicketHit.setItemID(webTicketHit.getSection() + "-" + webTicketHit.getRow());
				webTicketIds.add(webTicketHit.getItemId());
				webTicketById.put(webTicketHit.getItemId(), webTicketHit);
			}
			
			Set<String> diffWebTicketIds = new HashSet<String>(webTicketIds);
			Set<String> diffWsTicketIds = new HashSet<String>(wsTicketIds);
			
			Pattern eventIdPattern = Pattern.compile("evtid=(\\d+)&");
			Matcher matcher = eventIdPattern.matcher(url);
			matcher.find();
			String eventId = matcher.group(1);
			FileWriter writer = new FileWriter("result_" + eventId + ".csv");

			writer.write("Section, Row, Seat, Quantity, Price\n");
			writer.write("\n");

			writer.write("TICKET INDEXED FROM TND=" + wsTicketHits.size() + "\n");
			writer.write("TICKET INDEXED FROM ZEROMARKUP=" + webTicketHits.size() + "\n");
	
			// see which ids appears in feed but not in web
			for(String ticketId: wsTicketIds) {
				diffWebTicketIds.remove(ticketId);
			}
			
			// see which ids appears in web but not in ws
			for(String ticketId: webTicketIds) {
				diffWsTicketIds.remove(ticketId);
			}
	
			writer.write("\n");
			writer.write("EXTRA TICKET IN TND\n");
			for(String ticketId: diffWsTicketIds) {
				TicketHit ticketHit = wsTicketById.get(ticketId);
	//				System.out.println("TICKET HIT=" + ticketHit);
				writer.write(String.format("%1$s,%2$s,=\"%3$s\",%4$s,%5$f,%6$s\n", 
						ticketHit.getSection(),
						ticketHit.getRow(),
						ticketHit.getSeat(),
						ticketHit.getQuantity(),
						ticketHit.getBuyItNowPrice(),
						"TND"));
			}
			
			writer.write("\n");
			writer.write("EXTRA TICKET IN ZEROMARKUP\n");
			for(String ticketId: diffWebTicketIds) {
				TicketHit ticketHit = webTicketById.get(ticketId);
	//				System.out.println("TICKET HIT=" + ticketHit);
				writer.write(String.format("%1$s,%2$s,=\"%3$s\",%4$s,%5$f,%6$s\n", 
						ticketHit.getSection(),
						ticketHit.getRow(),
						ticketHit.getSeat(),
						ticketHit.getQuantity(),
						ticketHit.getBuyItNowPrice(),
						"ZeroMarkup"));
			}
			
			writer.write("\n");
			writer.write("TICKETS APPEARING IN BOTH\n");
			for(TicketHit webTicketHit: webTicketHits) {
				if (wsTicketById.get(webTicketHit.getItemId()) != null) {
	//					System.out.println("TICKET HIT=" + webTicketHit);					
					writer.write(String.format("%1$s,%2$s,=\"%3$s\",%4$s,%5$f,%6$s\n", 
							webTicketHit.getSection(),
							webTicketHit.getRow(),
							webTicketHit.getSeat(),
							webTicketHit.getQuantity(),
							webTicketHit.getBuyItNowPrice(),
							"ZeroMarkup+TND"));
				}
			}
			
			writer.close();
			
			System.out.println("Wrote file: result_" + eventId + ".csv");
			
			wsTicketHitIndexer.reset();
			webTicketHitIndexer.reset();
		}
		
	}
		
}
