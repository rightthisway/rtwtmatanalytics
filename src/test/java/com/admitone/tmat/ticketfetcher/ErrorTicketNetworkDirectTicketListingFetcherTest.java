package com.admitone.tmat.ticketfetcher;

import java.util.Collection;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import org.testng.annotations.Test;

import com.admitone.tmat.crawler.InterruptedTicketListingCrawlException;
import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.ticketfetcher.TicketHit;
import com.admitone.tmat.ticketfetcher.TicketListingFetcher;
import com.admitone.tmat.ticketfetcher.TicketNetworkDirectTicketListingFetcher;


/**
 * Class to test the TicketNetworkDirect(WS) Ticket Listing Fetcher.
 */
public class ErrorTicketNetworkDirectTicketListingFetcherTest {
	@Test(groups="ticketfetcher")
	public void testTicketListingFetcher() throws Exception {
	
		// eBay queries
		String[] queryEventIds = {
				"1373468",
				"1385035",
				"1297677",
				"1308618",
				"1283540",
				"1321233",
				"1321363"
		};


		Executor executor = Executors.newFixedThreadPool(10); 

		for (int i = 0 ; i < 100 ; i++) {
			for (final String queryEventId: queryEventIds) {
				executor.execute(new TNDTask(queryEventId));
			}
		}
		Thread.sleep(60L * 60L * 1000L);
	}
		
	public static class TNDTask implements Runnable {
		private String queryEventId;
		
		public TNDTask(String queryEventId) {
			this.queryEventId = queryEventId;
		}
			
		public void run() {
			TestTicketHitIndexer ticketHitIndexer = new TestTicketHitIndexer(10);
			TicketListingFetcher ticketListingFetcher = new TicketNetworkDirectTicketListingFetcher();

			System.out.println("QUERY EVENT ID=" + queryEventId);
			TicketListingCrawl ticketListingCrawl = new TicketListingCrawl();
			ticketListingCrawl.setSiteId(Site.TICKET_NETWORK_DIRECT);
			ticketListingCrawl.setExtraParameter("queryEventId", queryEventId);
			ticketListingCrawl.resetStats();
			
			boolean error = false;
			try {
				ticketListingFetcher.fetchTicketListing(ticketHitIndexer, ticketListingCrawl);
			} catch (InterruptedTicketListingCrawlException e) {
				// do nothing
			} catch (Exception e) {
				e.printStackTrace();
				error = true;
			}
			assert error == false;
			
			Collection<TicketHit> ticketHits = ticketHitIndexer.getFetchedTicketHits();
			assert ticketHits.size() > 0;
			
			for(TicketHit ticketHit: ticketHits) {
				System.out.println("TICKET HIT=" + ticketHit);
			}
			
			ticketHitIndexer.reset();
			
		}
	}		
}
