package com.admitone.tmat.ticketfetcher;

import java.util.Collection;

import org.testng.annotations.Test;

import com.admitone.tmat.crawler.InterruptedTicketListingCrawlException;
import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.ticketfetcher.TicketHit;
import com.admitone.tmat.ticketfetcher.TicketListingFetcher;
import com.admitone.tmat.ticketfetcher.TicketNetworkTicketListingFetcher;

/**
 * Class to test the TicketNetwork Ticket Listing Fetcher.
 */
public class TicketNetworkTicketListingFetcherTest {

	@Test(groups="ticketfetcher")
	public void testTicketListingFetcher() {
		TestTicketHitIndexer ticketHitIndexer = new TestTicketHitIndexer(null);
		TicketListingFetcher ticketListingFetcher = new TicketNetworkTicketListingFetcher();
				
		String[] urls = {
				// zero markup website
				"http://zeromarkup.com/ResultsTicket.aspx?evtid=1045894&event=U2",
				"http://zeromarkup.com/ResultsTicket.aspx?evtid=1040361&event=Metallica",
				"http://zeromarkup.com/ResultsTicket.aspx?evtid=988051&event=Billy+Elliot",
				"http://zeromarkup.com/ResultsTicket.aspx?evtid=930391&event=St.+Louis+Cardinals+Vs.+Chicago+Cubs",
				// ticket network website
				// this page contains starred ticket
				"http://www.ticketnetwork.com/tix/boise-state-broncos-vs-san-jose-state-spartans-saturday-10-31-2009-tickets-1053168.aspx#tixListing",
				"http://www.ticketnetwork.com/tix/metallica-saturday-12-12-2009-tickets-1040361.aspx#tixListing",
				"http://www.ticketnetwork.com/tix/billy-elliot-sunday-1-3-2010-tickets-988051.aspx#tixListing"
		};

		String[] wrongUrls = {
				// zero markup website
				"http://zeromarkup.com/ResultsTicket.aspx?evtid=1000000&event=U2",
				// ticket network website
				// this page contains starred ticket
				"http://www.ticketnetwork.com/tix/boise-state-broncos-vs-san-jose-state-spartans-saturday-10-31-2009-tickets-1000000.aspx#tixListing",
		};
		
		for (String url: urls) {
			System.out.println("URL=" + url);
			TicketListingCrawl ticketListingCrawl = new TicketListingCrawl();
			ticketListingCrawl.setSiteId(Site.TICKET_NETWORK);
			ticketListingCrawl.setQueryUrl(url);
			ticketListingCrawl.resetStats();
			
			boolean error = false;
			try {
				ticketListingFetcher.fetchTicketListing(ticketHitIndexer, ticketListingCrawl);
			} catch (InterruptedTicketListingCrawlException e) {
				// do nothing
			} catch (Exception e) {
				e.printStackTrace();
				error = true;
			}
			assert error == false;
			
			Collection<TicketHit> ticketHits = ticketHitIndexer.getFetchedTicketHits();
			assert ticketHits.size() > 0;
			
			System.out.println("Number of tickets extracted=" + ticketHits.size());
			ticketHitIndexer.reset();			
		}	
		
		for (String url: wrongUrls) {
			System.out.println("URL=" + url);
			TicketListingCrawl ticketListingCrawl = new TicketListingCrawl();
			ticketListingCrawl.setSiteId(Site.TICKET_NETWORK);
			ticketListingCrawl.setQueryUrl(url);
			ticketListingCrawl.resetStats();
			
			boolean error = false;
			try {
				ticketListingFetcher.fetchTicketListing(ticketHitIndexer, ticketListingCrawl);
			} catch (InterruptedTicketListingCrawlException e) {
				// do nothing
			} catch (Exception e) {
				e.printStackTrace();
				error = true;
			}
			assert error == false;
			
			Collection<TicketHit> ticketHits = ticketHitIndexer.getFetchedTicketHits();
			assert ticketHits.size() == 0;
			
			ticketHitIndexer.reset();			
		}		
		
	}
}
