package com.admitone.tmat.ticketfetcher;

import java.util.Collection;

import org.testng.annotations.Test;

import com.admitone.tmat.crawler.InterruptedTicketListingCrawlException;
import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.ticketfetcher.RazorGatorTicketListingFetcher;
import com.admitone.tmat.ticketfetcher.TicketHit;
import com.admitone.tmat.ticketfetcher.TicketListingFetcher;

/**
 * Class to test the RazorGator Ticket Listing Fetcher.
 */
public class RazorGatorTicketListingFetcherTest {

	@Test(groups="ticketfetcher")
	public void testTicketListingFetcher() {
		TestTicketHitIndexer ticketHitIndexer = new TestTicketHitIndexer(null);
		TicketListingFetcher ticketListingFetcher = new RazorGatorTicketListingFetcher();
				
		String[] urls = {
				"http://www.razorgator.com/tickets/sports/football/nfl/minnesota-vikings-tickets/?performance=7331183|1",
				"http://www.razorgator.com/tickets/concerts/rock-pop/american-idols-live-tickets/?performance=7400181|1",
				"http://www.razorgator.com/tickets/sports/football/ncaa-football/boise-state-broncos-football-tickets/?performance=7372653|1",
				"http://www.razorgator.com/tickets/concerts/rock-pop/metallica-tickets/?performance=7333717|1",
				"http://www.razorgator.com/tickets/theater/musical/billy-elliot-the-musical-tickets/?performance=7126929|1"
		};
		String[] wrongUrls = {
				"http://www.razorgator.com/tickets/concerts/rock-pop/u2-tickets/?performance=1124859|1",
				"http://www.razorgator.com/tickets/sports/basketball/nba/orlando-magic-tickets/?performance=7473|1"
		};

		for (String url: urls) {
			System.out.println("URL=" + url);
			TicketListingCrawl ticketListingCrawl = new TicketListingCrawl();
			ticketListingCrawl.setSiteId(Site.RAZOR_GATOR);
			ticketListingCrawl.setQueryUrl(url);
			ticketListingCrawl.resetStats();
			
			boolean error = false;
			try {
				ticketListingFetcher.fetchTicketListing(ticketHitIndexer, ticketListingCrawl);				
			} catch (InterruptedTicketListingCrawlException e) {
				// do nothing
			} catch (Exception e) {
				e.printStackTrace();
				error = true;
			}
			assert error == false;
			
			Collection<TicketHit> ticketHits = ticketHitIndexer.getFetchedTicketHits();
			
			System.out.println("TICKET INDEXED=" + ticketHits.size());
			assert ticketHits.size() > 0;
			
			ticketHitIndexer.reset();			
		}		

		for (String url: wrongUrls) {
			System.out.println("URL=" + url);
			TicketListingCrawl ticketListingCrawl = new TicketListingCrawl();
			ticketListingCrawl.setSiteId(Site.RAZOR_GATOR);
			ticketListingCrawl.setQueryUrl(url);
			ticketListingCrawl.resetStats();
			
			boolean error = false;
			try {
				ticketListingFetcher.fetchTicketListing(ticketHitIndexer, ticketListingCrawl);				
			} catch (InterruptedTicketListingCrawlException e) {
				// do nothing
			} catch (Exception e) {
				e.printStackTrace();
				error = true;
			}
			assert error == false;
			
			Collection<TicketHit> ticketHits = ticketHitIndexer.getFetchedTicketHits();
			
			assert ticketHits.size() == 0;
			
			ticketHitIndexer.reset();			
		}		

	}
}
