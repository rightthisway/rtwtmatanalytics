package com.admitone.tmat.ticketfetcher;

import java.util.Collection;

import org.testng.annotations.Test;

import com.admitone.tmat.crawler.InterruptedTicketListingCrawlException;
import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.ticketfetcher.TicketHit;
import com.admitone.tmat.ticketfetcher.TicketListingFetcher;
import com.admitone.tmat.ticketfetcher.TicketsNowTicketListingFetcher;

/**
 * Class to test the TicketsNow Ticket Listing Fetcher.
 */
public class TicketsNowTicketListingFetcherTest {

	@Test(groups="ticketfetcher")
	public void testTicketListingFetcher() {
		TestTicketHitIndexer ticketHitIndexer = new TestTicketHitIndexer(10);
		TicketListingFetcher ticketListingFetcher = new TicketsNowTicketListingFetcher();
				
		String[] urls = {
				"http://www.ticketsnow.com/InventoryBrowse/San-Jose-State-Spartans-Football-at-Boise-State-Broncos-Football-Tickets-at-Bronco-Stadium-in-Boise?PID=763909",
				"http://www.ticketsnow.com/InventoryBrowse/Metallica-Tickets-at-HP-Pavilion-at-San-Jose-in-San-Jose?PID=817189",
				"http://www.ticketsnow.com/InventoryBrowse/Billy-Elliot-Tickets-at-Imperial-Theatre---NY-in-New-York?PID=766481"				
		};

		String[] wrongUrls = {
				"http://www.ticketsnow.com/InventoryBrowse/U2-Tickets-at-Croke-Park-Stadium-in-?PID=81000"				
		};

		for (String url: urls) {
			TicketListingCrawl ticketListingCrawl = new TicketListingCrawl();
			ticketListingCrawl.setSiteId(Site.TICKETS_NOW);
			ticketListingCrawl.setQueryUrl(url);
			ticketListingCrawl.resetStats();
			
			boolean error = false;
			try {
				ticketListingFetcher.fetchTicketListing(ticketHitIndexer, ticketListingCrawl);
			} catch (InterruptedTicketListingCrawlException e) {
				// do nothing
			} catch (Exception e) {
				e.printStackTrace();
				error = true;
			}
			assert error == false;
			
			Collection<TicketHit> ticketHits = ticketHitIndexer.getFetchedTicketHits();
			assert ticketHits.size() > 0;
			
			ticketHitIndexer.reset();			
		}		

		for (String url: wrongUrls) {
			TicketListingCrawl ticketListingCrawl = new TicketListingCrawl();
			ticketListingCrawl.setSiteId(Site.TICKETS_NOW);
			ticketListingCrawl.setQueryUrl(url);
			ticketListingCrawl.resetStats();
			
			boolean error = false;
			try {
				ticketListingFetcher.fetchTicketListing(ticketHitIndexer, ticketListingCrawl);
			} catch (InterruptedTicketListingCrawlException e) {
				// do nothing
			} catch (Exception e) {
				e.printStackTrace();
				error = true;
			}
			assert error == false;
			
			Collection<TicketHit> ticketHits = ticketHitIndexer.getFetchedTicketHits();
			assert ticketHits.size() == 0;
			
			ticketHitIndexer.reset();			
		}		
		
	}
}
