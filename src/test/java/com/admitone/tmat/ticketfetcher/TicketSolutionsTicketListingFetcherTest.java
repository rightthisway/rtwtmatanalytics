package com.admitone.tmat.ticketfetcher;

import java.util.Collection;

import org.testng.annotations.Test;

import com.admitone.tmat.crawler.InterruptedTicketListingCrawlException;
import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.ticketfetcher.TicketHit;
import com.admitone.tmat.ticketfetcher.TicketListingFetcher;
import com.admitone.tmat.ticketfetcher.TicketSolutionsTicketListingFetcher;

/**
 * Class to test the TicketSolutions Ticket Listing Fetcher.
 */
public class TicketSolutionsTicketListingFetcherTest {

	@Test(groups="ticketfetcher")
	public void testTicketListingFetcher() {
		TestTicketHitIndexer ticketHitIndexer = new TestTicketHitIndexer(100);
		TicketListingFetcher ticketListingFetcher = new TicketSolutionsTicketListingFetcher();
				
		String[] urls = {
				"http://www.ticketsolutions.com/tickets.aspx?eventid=744723"
				//"http://www.ticketsolutions.com/tickets.aspx?eventid=932462"
				//"http://www.ticketsolutions.com/tickets.aspx?eventid=819301",
				//"http://www.ticketsolutions.com/tickets.aspx?eventid=764152"				
		};

		String[] wrongUrls = {
				//"http://go.ticketsolutions.com/tickets.aspx?eventid=81111",				
				//"http://go.ticketsolutions.com/tickets.aspx?eventid=10"				
		};

		for (String url: urls) {
			System.out.println("FETCHING LISTING=" + url);
			TicketListingCrawl ticketListingCrawl = new TicketListingCrawl();
			ticketListingCrawl.setSiteId(Site.TICKET_SOLUTIONS);
			ticketListingCrawl.setQueryUrl(url);
			ticketListingCrawl.resetStats();
			
			boolean error = false;
			try {
				ticketListingFetcher.fetchTicketListing(ticketHitIndexer, ticketListingCrawl);
			} catch (InterruptedTicketListingCrawlException e) {
				// do nothing
			} catch (Exception e) {
				e.printStackTrace();
				error = true;
			}
			assert error == false;
			
			Collection<TicketHit> ticketHits = ticketHitIndexer.getFetchedTicketHits();
			assert ticketHits.size() > 0;
			
			ticketHitIndexer.reset();
		}		

		for (String url: wrongUrls) {
			TicketListingCrawl ticketListingCrawl = new TicketListingCrawl();
			ticketListingCrawl.setSiteId(Site.TICKET_SOLUTIONS);
			ticketListingCrawl.setQueryUrl(url);
			ticketListingCrawl.resetStats();
			
			boolean error = false;
			try {
				ticketListingFetcher.fetchTicketListing(ticketHitIndexer, ticketListingCrawl);
			} catch (InterruptedTicketListingCrawlException e) {
				// do nothing
			} catch (Exception e) {
				e.printStackTrace();
				error = true;
			}
			assert error == false;
			
			Collection<TicketHit> ticketHits = ticketHitIndexer.getFetchedTicketHits();
			assert ticketHits.size() == 0;
			
			ticketHitIndexer.reset();
		}		

	}
}
