package com.admitone.tmat.eventfetcher;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import org.testng.annotations.Test;

import com.admitone.tmat.eventfetcher.EventHit;
import com.admitone.tmat.utils.eimarketplace.EIMPSimpleCredential;

/**
 * Class to test the EiMarketPlace Event Listing Fetcher.
 */
public class EventInventoryEventListingFetcherTest {	
	private static DateFormat queryDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

	@Test(groups="eventfetcher")
	public void testEventListingFetcher()  throws Exception {
		EiMarketPlaceEventListingFetcher eimpEventListingFetcher = new EiMarketPlaceEventListingFetcher();
		eimpEventListingFetcher.setCredential(new EIMPSimpleCredential("admit1", "shortit09"));

		EventInventoryEventListingFetcher eventListingFetcher = new EventInventoryEventListingFetcher();
		eventListingFetcher.setEiMarketPlaceEventListingFetcher(eimpEventListingFetcher);
				
		Object[][] queries = {
				// jersey boys
				{"jersey boys", null, queryDateFormat.parse("2009/10/02 00:00:00"), queryDateFormat.parse("2009/10/02 23:59:59")},
				
				// dallas cowboys vs. seattle seahawks at 2009/11/01
				{"seattle", null, queryDateFormat.parse("2009/11/01 00:00:00"), queryDateFormat.parse("2009/11/01 23:59:59")},
				{"dallas", null, queryDateFormat.parse("2009/11/01 00:00:00"), queryDateFormat.parse("2009/11/01 23:59:59")},
				{"dallas cowboys vs. seattle seahawks", null, queryDateFormat.parse("2009/11/01 00:00:00"), queryDateFormat.parse("2009/11/01 23:59:59")},

				// jacksonville jaguars vs. indianapolis colts at 2009/12/17
				{"jacksonville jaguars vs. indianapolis colts", null, queryDateFormat.parse("2009/12/17 00:00:00"), queryDateFormat.parse("2009/12/17 23:59:59")},

				// fetch dallas cowboys at 2009/09/2008
				{"dallas", null, queryDateFormat.parse("2009/09/20 00:00:00"), queryDateFormat.parse("2009/09/20 23:59:59")},

				// fetch new york yankees at 2009/09/2008
				{"new york", null, queryDateFormat.parse("2009/09/20 00:00:00"), queryDateFormat.parse("2009/09/20 23:59:59")},
				{"dallas cowboys", null, queryDateFormat.parse("2008/01/01 00:00:00"), queryDateFormat.parse("2010/01/01 00:00:00")},
				{"lion king", null, queryDateFormat.parse("2009/11/08 00:00:00"), queryDateFormat.parse("2009/11/08 23:59:59")},
				{"jvc jazz festival", null, queryDateFormat.parse("2008/01/01 00:00:00"), queryDateFormat.parse("2010/01/01 00:00:00")},
				{"metallica", "madison", queryDateFormat.parse("2008/01/01 00:00:00"), queryDateFormat.parse("2010/01/01 00:00:00")},
				{"u2", null, queryDateFormat.parse("2008/01/01 00:00:00"), queryDateFormat.parse("2010/01/01 00:00:00")}
		};
		
		for (Object[] query: queries) {
			Date startDate = new Date();
			System.out.println("EIMARKETPLACE URL=" + query[0]);
			
			Collection<EventHit> eventHits = null;
			boolean error = false;
			try {
				eventHits = eventListingFetcher.getEventList((String)query[0], (String)query[1], (Date)query[2], (Date)query[3]);
			} catch (Exception e) {
				e.printStackTrace();
				error = true;
			}
			
			assert error == false;
			assert eventHits.size() > 0;
			
			for(EventHit eventHit: eventHits) {
				System.out.println("EVENTHIT=" + eventHit);
			}

			Date endDate = new Date();
			System.out.println("************** Test " + query[0] + " took " + (((endDate.getTime() - startDate.getTime()) / 1000)) + " s");
		}		
	}
}
