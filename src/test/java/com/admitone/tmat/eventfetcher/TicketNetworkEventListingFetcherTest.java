package com.admitone.tmat.eventfetcher;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import org.testng.annotations.Test;

import com.admitone.tmat.eventfetcher.EventHit;
import com.admitone.tmat.utils.eimarketplace.EIMPSimpleCredential;

/**
 * Class to test the Ticket Network Event Listing Fetcher.
 */
public class TicketNetworkEventListingFetcherTest {	
	private static DateFormat queryDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

	@Test(groups="eventfetcher")
	public void testEventListingFetcher()  throws Exception {
		TicketNetworkEventListingFetcher eventListingFetcher = new TicketNetworkEventListingFetcher();
				
		Object[][] queries = {
				{"giant", null, queryDateFormat.parse("2010/01/13 00:00:00"), queryDateFormat.parse("2011/01/14 23:59:59")}
		};
		
		for (Object[] query: queries) {
			Date startDate = new Date();
			System.out.println("EIMARKETPLACE URL=" + query[0]);
			
			Collection<EventHit> eventHits = null;
			boolean error = false;
			try {
				eventHits = eventListingFetcher.getEventList((String)query[0], (String)query[1], (Date)query[2], (Date)query[3],null,false,false);
			} catch (Exception e) {
				e.printStackTrace();
				error = true;
			}
			
			assert error == false;
			assert eventHits.size() > 0;
			
			for(EventHit eventHit: eventHits) {
				System.out.println("EVENTHIT=" + eventHit);
			}

			Date endDate = new Date();
			System.out.println("************** Test " + query[0] + " took " + (((endDate.getTime() - startDate.getTime()) / 1000)) + " s");
		}		
	}
}
