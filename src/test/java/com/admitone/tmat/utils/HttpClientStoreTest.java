package com.admitone.tmat.utils;

import java.util.Map;

import org.testng.annotations.Test;

import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;

public class HttpClientStoreTest {
	
	@Test
	// TODO: write more complicate tests
	public HttpClientStoreTest() throws Exception {
		SimpleHttpClient httpClient = HttpClientStore.createHttpClient();
		Map<String, Integer> freeHttpClientCountBySiteId = HttpClientStore.getFreeHttpClientCountBySiteId();
		Map<String, Integer> allocatedHttpClientCountBySiteId = HttpClientStore.getAllocatedHttpClientCountBySiteId();
		assert freeHttpClientCountBySiteId.get(SimpleHttpClient.DEFAULT) == 1;
		assert allocatedHttpClientCountBySiteId.get(SimpleHttpClient.DEFAULT) == 1;
		HttpClientStore.releaseHttpClient(httpClient);
	}
	
}
