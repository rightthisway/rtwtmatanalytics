package com.admitone.tmat.utils;

import org.testng.annotations.Test;

public class SeatWaveEventPopulatorTest {
	
	@Test(groups="seatwave")
	public void startTest() throws Exception{
		System.out.println("START TEST");
		SeatWaveEventPopulator populator = new SeatWaveEventPopulator();
		
		populator.start();
		
		do {
			Thread.sleep(5000);
		} while(populator.isRunning());
	}
}
