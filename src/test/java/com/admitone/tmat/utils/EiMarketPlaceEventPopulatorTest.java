package com.admitone.tmat.utils;

import org.testng.annotations.Test;

import com.admitone.tmat.utils.eimarketplace.EIMPSimpleCredential;

public class EiMarketPlaceEventPopulatorTest {
	
	@Test(groups="eimp")
	public void startTest() throws Exception{
		System.out.println("START TEST");
		(new EiMarketPlaceEventPopulator()).setCredential(new EIMPSimpleCredential("admit1", "super2010"));
		EiMarketPlaceEventPopulator.start();
		
		do {
			Thread.sleep(5000);
		} while(EiMarketPlaceEventPopulator.isRunning());
	}
}
