package com.admitone.tmat.utils;

import org.testng.annotations.Test;

public class TextUtilTest {

	@Test(groups="util")
	public void testRemoveExtraWhitespaces() {
		assert TextUtil.removeExtraWhitespaces("one    test").equals("one test");
		assert TextUtil.removeExtraWhitespaces("      one      \n\n   \t test \n\n\n\t ").equals(" one test ");
		assert TextUtil.removeExtraWhitespaces(" \n\tone\n\t\ntest\n\t").equals(" one test ");
	}	

	@Test(groups="util")
	public void testRemoveNonWordCharacters() {
		assert TextUtil.removeNonWordCharacters("one   test").equals("onetest");
		assert TextUtil.removeNonWordCharacters("****&&&&####one---()%%%%      \n\n   \t test ***** $$ $ $$\n\n\n\t ").equals("onetest");
	}	

	@Test(groups="util")
	public void testRemoveAllWhitespaces() {
		assert TextUtil.removeAllWhitespaces("one    test").equals("onetest");
		assert TextUtil.removeAllWhitespaces("      one      \n\n   \t test \n\n\n\t ").equals("onetest");
		assert TextUtil.removeAllWhitespaces(" \n\tone\n\t\ntest\n\t").equals("onetest");
	}	

	@Test(groups="util")
	public void testTrimQuotes() {
		assert TextUtil.trimQuotes("test").equals("test");
		assert TextUtil.trimQuotes("\"test\"").equals("test");
		assert TextUtil.trimQuotes("'test'").equals("test");
		assert TextUtil.trimQuotes("'test\"").equals("'test\"");
		assert TextUtil.trimQuotes("\"test'").equals("\"test'");
	}	
}
