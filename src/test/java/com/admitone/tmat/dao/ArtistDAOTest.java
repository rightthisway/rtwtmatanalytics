package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.Date;
import java.util.ArrayList;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.testng.annotations.Test;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Artist;

public class ArtistDAOTest {
	public final Integer ENTITY_COUNT = 500;
	private DAORegistry daoRegistry;
	
	public ArtistDAOTest(DAORegistry daoRegistry) {
		this.daoRegistry = daoRegistry;
	}
	
	public void testGetAll() {
		Collection<Artist> artists = new ArrayList<Artist>(); 
		
		Date start = new Date();
		int offset = 1000000;
		for (int i = 0; i < ENTITY_COUNT; i++) {
			Artist artist = new Artist();
			artist.setId(i + offset);
			artist.setName("Artist-" + i);
			daoRegistry.getArtistDAO().save(artist);
			artists.add(artist);
		}
		Date end = new Date();
		System.out.println("Creating " + ENTITY_COUNT + " artists: " + (end.getTime() - start.getTime()) + " ms");
		
		for (int j = 0; j < 5; j++) {
			start = new Date();
			for (int i = 0; i < ENTITY_COUNT; i++) {
				daoRegistry.getArtistDAO().get(offset + i);
			}
			end = new Date();
			System.out.println("Reading " + ENTITY_COUNT + " artists (iteration " + j + "): " + (end.getTime() - start.getTime()) + " ms");
		}
		
		start = new Date();
		int j = 0;
		for (Artist artist: artists) {
			artist.setName("Artist-" + (j++ + 1));
		}
		daoRegistry.getArtistDAO().saveOrUpdateAll(artists);
		end = new Date();
		System.out.println("Updating " + ENTITY_COUNT + " artists: " + (end.getTime() - start.getTime()) + " ms");

		start = new Date();
		for (int i = 0; i < ENTITY_COUNT; i++) {
			daoRegistry.getArtistDAO().deleteById(i + offset);
		}
		end = new Date();
		System.out.println("Deleting " + ENTITY_COUNT + " artists: " + (end.getTime() - start.getTime()) + " ms");

	}
}
