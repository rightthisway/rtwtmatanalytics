package com.admitone.tmat.dao.hibernate;

import java.util.Collection;
import java.util.Date;
import java.util.ArrayList;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.testng.annotations.Test;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Artist;

public class ArtistDAOTest {
//	@Test(groups={"dao", "hibernate"})
	public void testGetAll() {
		new ClassPathXmlApplicationContext(new String[]{
			"spring-dao-database.xml"
		});
		com.admitone.tmat.dao.ArtistDAOTest test = new com.admitone.tmat.dao.ArtistDAOTest(new DAORegistry());
		test.testGetAll();
	}
}