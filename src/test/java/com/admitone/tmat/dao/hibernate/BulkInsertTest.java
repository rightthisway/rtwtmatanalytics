package com.admitone.tmat.dao.hibernate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Artist;
import com.admitone.tmat.data.Tour1;
import com.admitone.tmat.enums.TourType;

public class BulkInsertTest {
	/*public final static Integer NUM_TOURS = 500; 
	
	private Collection<Tour> generateTours(Integer artistId) {
		Collection<Tour> tours = new ArrayList<Tour>();
		for (int i = 0; i < NUM_TOURS; i++) {
			Tour tour = new Tour();
			tour.setName("TOUR " + i);
			tour.setTourType(TourType.SPORT);
			tour.setArtist(DAORegistry.getArtistDAO().get(artistId));
			tours.add(tour);
		}
		return tours;
	}
	
//	@BeforeClass(groups={"dao", "hibernate", "bulk"})
	public void init() {
		new ClassPathXmlApplicationContext(new String[]{
				"spring-dao-database.xml",
			});		
	}
	
//	@Test(groups={"dao", "hibernate", "bulk"})
	public void simpleInsertTest() {		
		Artist artist = new Artist();
		artist.setName("TEST_BULK_ARTIST");
		DAORegistry.getArtistDAO().save(artist);
		
		Collection<Tour> tours = generateTours(artist.getId());
		Date start = new Date();
		for (Tour tour: tours) {
			DAORegistry.getTourDAO().save(tour);
		}
		Date end = new Date();
		
		System.out.println(NUM_TOURS + " saved: " + (end.getTime() - start.getTime()) + " ms");

		start = new Date();
		for (Tour tour: tours) {
			DAORegistry.getTourDAO().delete(tour);
		}
		end = new Date();
		System.out.println(NUM_TOURS + " deleted: " + (end.getTime() - start.getTime()) + " ms");

		DAORegistry.getArtistDAO().delete(artist);		
	}
	
//	@Test(groups={"dao", "hibernate", "bulk"})
	public void bulkInsertTest() {

		Artist artist = new Artist();
		artist.setName("TEST_BULK_ARTIST");
		DAORegistry.getArtistDAO().save(artist);
		
		Collection<Tour> tours = generateTours(artist.getId());
		Date start = new Date();
		DAORegistry.getTourDAO().saveAll(tours);
		Date end = new Date();
		System.out.println(NUM_TOURS + " saved (BULK): " + (end.getTime() - start.getTime()) + " ms");

		start = new Date();
		DAORegistry.getTourDAO().deleteAll(tours);
		end = new Date();
		System.out.println(NUM_TOURS + " deleted (BULK): " + (end.getTime() - start.getTime()) + " ms");
		
		DAORegistry.getArtistDAO().delete(artist);
	}

//	@Test(groups={"dao", "hibernate", "bulk"})
	public void bulkSaveOrUpdateTest() {

		Artist artist = new Artist();
		artist.setName("TEST_BULK_ARTIST");
		DAORegistry.getArtistDAO().save(artist);
		
		Collection<Tour> tours = generateTours(artist.getId());
		Date start = new Date();
		DAORegistry.getTourDAO().saveAll(tours);
		Date end = new Date();
		System.out.println(NUM_TOURS + " saved (SaveOrUpdate BULK): " + (end.getTime() - start.getTime()) + " ms");

		start = new Date();
		DAORegistry.getTourDAO().deleteAll(tours);
		end = new Date();
		System.out.println(NUM_TOURS + " deleted (BULK): " + (end.getTime() - start.getTime()) + " ms");
		
		DAORegistry.getArtistDAO().delete(artist);
	}*/

}
