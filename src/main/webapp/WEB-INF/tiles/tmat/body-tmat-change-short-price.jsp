<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
  <a href="..">Home</a> 
  &gt; <a href="TMATBrowseShorts">Browse Shorts</a>
  <c:if test="${not empty eventId}">
    &gt; <a href="TMATShortOverview?eventId=${eventId}">Browse Tour Shorts</a>
  	&gt; <a href="TMATBrowseShorts?eventId=${eventId}">Browse Event Shorts</a> 
  </c:if>
  &gt; Change Short Price
</div>

<h1>Change Short Ticket Price</h1>

<div class="info">
        <div class="infoText">
                Please note that if an Event Price Adjustment exists,
                <br/>
                then the Tour Price Adjustment will not effect the determination of the short price.
        </div>
</div>
<h2>Current Settings</h2>
<table>
  <tr>
    <td>User Name: </td>
    <td>${username}</td>
  </tr>
  <tr>
    <td>Current Price(fees included): </td>
    <td>${ticket.price}</td>
  </tr>
  <tr>
    <td>Current Site: </td>
    <td>${ticket.siteId}</td>
  </tr>
  <tr>
    <td>Current Tour: </td>
    <td>${tourName}</td>
  </tr>
  <tr>
    <td>Current Event: </td>
    <td>${event.name}</td>
  </tr>
</table>
<h2>Tour Settings</h2>
<h3>Current Tour Adjustments:</h3>

<c:choose>
	<c:when test="${not empty tAdjs}">	
		<display:table class="list" name="${tAdjs}" id="tAdj"  requestURI="TMATChangeShortPrice">
			<display:column title="Tour"  >${tourName}</display:column >
		    <display:column title="Site" sortable="false" property="siteId" />
		    <display:column title="Short % Fee" sortable="false" property="shortPercent" format="{0,number,###,###.##}" />
		    <display:column title="Short Flat Fee" sortable="false" property="shortFlatFee" format="{0,number,###,###.##}"/>
		</display:table>
	</c:when>
	<c:otherwise>
		No Tour Adjustment Found
	</c:otherwise>
</c:choose>

<h3>Change Tour Adjustments:</h3>
<table>
  <tr>
    <td>Set Flat Fee: </td>
    <td>$<input id="tourFlatFee" type="text"/></td>
  </tr>
  <tr>
    <td>Set Percent Fee: </td>
    <td><input id="tourPercentFee" type="text"/>%</td>
  </tr>
  </table>
<br/>
<input id="addTourNewPriceButton" type="button" class="medButton" onclick="addTourFees()"  value="Add Tour Fees"/>
<h2>Event Settings</h2>
<h3>Current Event Adjustments:</h3>

<c:choose>
	<c:when test="${not empty eAdjs}">	
		<display:table class="list" name="${eAdjs}" id="eAdj"  requestURI="TMATChangeShortPrice">
			<display:column title="Event" sortable="false" >${event.name}</display:column >
    		<display:column title="Site" sortable="false" property="siteId" />
    		<display:column title="Short % Fee" sortable="false" property="shortPercent" format="{0,number,###,###.##}" />
    		<display:column title="Short Flat Fee" sortable="false" property="shortFlatFee" format="{0,number,###,###.##}"/>
		</display:table>
	</c:when>
	<c:otherwise>
		No Event Adjustment Found
	</c:otherwise>
</c:choose>

<h3>Change Event Adjustments:</h3>
<table>
  <tr>
    <td>Set Flat Fee: </td>
    <td>$<input id="eventFlatFee" type="text"/></td>
  </tr>
  <tr>
    <td>Set Percent Fee: </td>
    <td><input id="eventPercentFee" type="text"/>%</td>
  </tr>
</table>
<br/>
<input id="addEventNewPriceButton" type="button" class="medButton" onclick="addEventFees()"  value="Add Event Fees"/>
<br/>
<br/>

  <c:if test="${not empty eventId}">
	<a href="TMATBrowseShorts?eventId=${eventId}">Return to Browse Shorts</a> 
  </c:if>

<script type="text/javascript">
<!--//--><![CDATA[//><!--

  function addEventFees() {
    var id = "${ticket.ticketId}";
    var eventId = "${eventId}";
    var flat = $.trim($('#eventFlatFee').val());
    var per = $.trim($('#eventPercentFee').val());
        
	if (flat.length == 0) {
	  flat = "0.00";
	}
	    
	if (per.length == 0) {
	  per = "0.00";
	}

    ShortTransactionDwr.changeEventShortPrice(id, flat, per, function(response) {                
             alert(response);
             document.location.href = "TMATBrowseShorts?eventId=" + eventId;
     });
  }
  
    function addTourFees() {
    var id = "${ticket.ticketId}";
    var eventId = "${eventId}";
    var flat = $.trim($('#tourFlatFee').val());
    var per = $.trim($('#tourPercentFee').val());
        
	if (flat.length == 0) {
	  flat = "0.00";
	}
	    
	if (per.length == 0) {
	  per = "0.00";
	}

    ShortTransactionDwr.changeTourShortPrice(id, flat, per, function(response) {                
             alert(response);
             document.location.href = "TMATBrowseShorts?eventId=" + eventId;
     });
  }
//--><!]]>
</script>
