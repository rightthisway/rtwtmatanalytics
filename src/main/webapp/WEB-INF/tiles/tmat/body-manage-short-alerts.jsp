<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<c:choose>
	<c:when test="${mode == 'editAll'}">
		<div id="breadCrumbPath" class="breadCrumbPathEditor">
		  <a href="..">Home</a> 
		  &gt; Manage Short Alerts 
		</div>
		
		<c:if test="${not empty param.info}">
			<div class="info"><div class="infoText">${param.info}</div></div>
		</c:if>
		
		<c:if test="${not empty param.error}">
			<div class="error"><div class="errorText">${param.error}</div></div>
		</c:if>

		
		<h2>Manage User Short Alerts</h2>

		<c:set var="alertForList" value="TMAT,CIRCLES,TOTAL" />
		<c:set var="alertStatusList" value="ACTIVE,DISABLED,EXPIRED,DELETED,TOTAL" />

		<img id="show-stats-image" src="../images/ico-expand.gif">
			<a id="show-stats-link" href="javascript:void(0)" onclick="toggleAlertStatsTable()">Show stats</a>
		<br />
		
		<table id="alert-stats-table" class="list" style="display:none">
			<thead>
				<tr>
					<th></th>
					<c:forTokens var="alertStatus" items="${alertStatusList}" delims=",">
						<th width="100">${alertStatus}</th>
					</c:forTokens>
				</tr>
			</thead>
			<tbody>
				<c:forTokens var="alertFor" items="${alertForList}" delims=",">
					<tr>
						<th>
							<c:choose>
								<c:when test="${alertFor == 'TMAT'}">Wholesale</c:when>
								<c:when test="${alertFor == 'CIRCLES_PORTAL'}">Retail</c:when>
								<c:otherwise>${alertFor}</c:otherwise>
							</c:choose>
						</th>
						<c:forTokens var="alertStatus" items="${alertStatusList}" delims=",">
							<c:set var="statKey" value="${alertStatus}-${alertFor}" />
							<td align="right" width="100">	
								<c:choose>
									<c:when test="${empty statByKey[statKey]}">-</c:when>
									<c:otherwise>${statByKey[statKey]}</c:otherwise>
								</c:choose>
							</td>
							
						</c:forTokens>
					</tr>
				</c:forTokens>
			</tbody>
		</table>		
		<br />
		
		<c:set var="pageUrl" value="EditorManageAlerts" />
		<c:set var="editUrl" value="EditorEditAlert" />
		
	</c:when>
	<c:otherwise>
		<div id="breadCrumbPath" class="breadCrumbPathUser">
		  <a href="..">Home</a> 
		  &gt; Manage Short Alerts 
		</div>
		
		<c:if test="${not empty param.info}">
			<div class="info"><div class="infoText">${param.info}</div></div>
		</c:if>
		
		<c:if test="${not empty param.error}">
			<div class="error"><div class="errorText">${param.error}</div></div>
		</c:if>

		

		<h2>Manage My Short Alerts</h2>

		<c:set var="pageUrl" value="MyAlerts" />
		<c:set var="editUrl" value="EditMyAlert" />

	</c:otherwise>
</c:choose>



Filter by tour
<select onchange="selectFilterTour(this.value)">
	<option value="">All</option>
	<c:forEach var="tour" items="${tours}">
		<option value="${tour.id}" <c:if test="${tour.id == filterTourId}">selected</c:if> >${tour.name}</option>
	</c:forEach>
</select>
by event
<select onchange="selectFilterEvent(this.value)">
	<option value="">All</option>
	<c:forEach var="event" items="${events}">
		<option value="${event.id}" <c:if test="${event.id == filterEventId}">selected</c:if> >${event.name} - ${event.formattedEventDate}</option>
	</c:forEach>
</select>

<c:if test="${mode == 'editAll'}">
	by username
	<select onchange="selectFilterUsername(this.value)">
		<option value="">All</option>
		<c:forEach var="user" items="${users}">
			<option value="${user.username}" <c:if test="${user.username == filterUsername}">selected</c:if> >${user.username}</option>
		</c:forEach>
	</select>
</c:if>


<br /><br />
<b>View: </b>
<c:choose>
	<c:when test="${view == 'created'}"><b>Created alerts</b></c:when>
	<c:otherwise><a href="${pageUrl}?view=created">Created alerts</a></c:otherwise>
</c:choose>
-
<c:choose>
	<c:when test="${view == 'marketmaker'}"><b>Market maker assigned alerts</b></c:when>
	<c:otherwise><a href="${pageUrl}?view=marketmaker">Market maker assigned alerts</a></c:otherwise>
</c:choose>
-
<c:choose>
	<c:when test="${empty view}"><b>Both</b></c:when>
	<c:otherwise><a href="${pageUrl}?view=">Both</a></c:otherwise>
</c:choose>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

<b>Show only Alerts For: </b>
<c:choose>
	<c:when test="${alertFor == 'TMAT'}"><b>Wholesale</b></c:when>
	<c:otherwise><a href="${pageUrl}?alertFor=TMAT">Wholesale</a></c:otherwise>
</c:choose>
|
<c:choose>
	<c:when test="${alertFor == 'CIRCLES_PORTAL'}"><b>Retail</b></c:when>
	<c:otherwise><a href="${pageUrl}?alertFor=CIRCLES_PORTAL">Retail</a></c:otherwise>
</c:choose>
|
<c:choose>
	<c:when test="${empty alertFor}"><b>Both</b></c:when>
	<c:otherwise><a href="${pageUrl}?alertFor=">Both</a></c:otherwise>
</c:choose>

<br />
<input id="includeDisabledAlerts" type="checkbox" <c:if test="${includeDisabledAlerts}">checked</c:if> onchange="selectFilterIncludeDisabledAlerts(this.checked)" />
<label for="includeDisabledAlerts">Include disabled alerts</label>


<div style="margin-top:16px;margin-bottom:8px;">
	<div style="float: left"><b>Legends:</b></div>

	<div style="margin-left:20px;width:10px;height:10px;background-color: #EB9E6F;border: 2px solid #EB9E6F;margin-right: 6px;float: left"></div>  
	<div style="float: left">Disabled alert</div>
	<div style="margin-left:20px;width:10px;height:10px;background-color: #c5d973;border: 2px solid #c5d973;margin-right: 6px;float: left"></div>  
	<div style="float: left">Wholesale alert</div>
	<div style="margin-left:20px;width:10px;height:10px;background-color: #bbbbff;border: 2px solid #bbbbff;margin-right: 6px;float: left"></div>  
	<div style="float: left">Retail alert</div>
	<div style="margin-left:20px;font-weight: bold;margin-right:6px;float: left">[bold]</div>  
	<div style="float: left">Fulfilled alert</div>
</div><br /><br />

<b>Select:</b>
<a href="javascript:void(0)" onclick="selectAllAlerts()">All</a> -
<a href="javascript:void(0)" onclick="unselectAllAlerts()">None</a>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

<b>Actions:</b>
<img src="../images/ico-enable.gif" align="absbottom" /> <a href="javascript:void(0)" onclick="enableSelectedAlerts()">Enable</a> -
<img src="../images/ico-disable.gif" align="absbottom" /> <a href="javascript:void(0)" onclick="disableSelectedAlerts()">Disable</a> -
<img src="../images/ico-delete.gif" align="absbottom" /> <a href="javascript:void(0)" onclick="deleteSelectedAlerts()">Delete</a> -
<img src="../images/ico-download.gif" align="absbottom" /> <a href="javascript:void(0)" onclick="shortSelectedAlerts()">Short</a>
<br /><br />

<display:table class="list" name="${alerts}" id="tableAlert" requestURI="${pageUrl}" decorator="userAlertTableRowDecorator" defaultsort="3">
	<display:column>
		<input type="checkbox" id="checkbox-alert-${tableAlert.id}" class="checkbox-alert" />
	</display:column>
	<display:column title="Check" sortable="false" >
    	<a href="${pageUrl}?action=check&alertId=${tableAlert.id}#tickets"><img src="../images/ico-browse.gif" align="absmiddle" /></a>
	</display:column>
    <display:column title="ID" sortable="true" property="id" />
    <display:column title="Type" sortable="true" property="userAlertType" />
    <display:column title="User" sortable="true">
	  <c:choose>
         <c:when test="${not empty tableAlert.username}">
		${tableAlert.username}
	    </c:when>
	    <c:otherwise>
		${tableAlert.orderedBy}
	    </c:otherwise>
       </c:choose>
    </display:column>
    <display:column title="Market Maker" sortable="true" property="marketMakerUsername" />
    <display:column title="Event Name" sortable="true"  >
		<a href="BrowseTickets?eventId=${tableAlert.eventId}" tooltip="${tableAlert.event.venue.location}">
			${tableAlert.event.name} -
			${tableAlert.event.formattedEventDate}
		</a>
    </display:column>
    <display:column title="Scheme" sortable="true" property="categoryGrpName" />
    <display:column title="Zone" sortable="true" property="categoryDescription" />
    <display:column title="Quantity" sortable="true" property="quantitiesDescription" />
    <display:column title="Whls Price" sortable="true" property="priceRangeDescription" />
    <display:column title="Retail Price" sortable="true" property="adjustedPriceRangeDescription" />
    <display:column title="Section" sortable="true" property="sectionRangeDescription" />
    <display:column title="Row" sortable="true" property="rowRangeDescription" />
    <%-- <display:column title="Freq" sortable="true">
        <fmt:formatNumber value="${frequencyByAlertId[tableAlert.id] / 60}" pattern="#" />mn
    </display:column>  --%>       
    <display:column title="Description" sortable="false" >
    	<c:choose>
    		<c:when test="${empty tableAlert.description || fn:length(tableAlert.description) < 20}">
    			${tableAlert.description}
    		</c:when>
    		<c:otherwise>
    			<span tooltip="${tableAlert.description}">${fn:substring(tableAlert.description, 0, 20)}...</span>
    		</c:otherwise>
    	</c:choose>
    </display:column>
    <display:column title="Created From" sortable="false" property="alertTransType" />	
    <display:column title="" sortable="false" style="width:70px">	
		<img src="../images/ico-edit.gif" align="absmiddle" />
		<%-- <a href="${editUrl}?alertId=${tableAlert.id}#editAlert"  >Edit</a> --%>
		<%-- <a href="#" onclick="callEditShortAlert('${tableAlert.id}','${tableAlert.priceLow}','${tableAlert.priceHigh}');"  >Edit</a>  --%>   
		<a href="javascript:void(0);"
		 onclick="callEditShortAlert('${tableAlert.id}','${tableAlert.priceLow}','${tableAlert.priceHigh}','${tableAlert.quantitiesDescription}');"  >Edit Alert</a> 
	</display:column>
</display:table>
<br />

<%-- <c:if test="${not empty alerts}">

	<b>Select:</b>
	<a href="javascript:void(0)" onclick="selectAllAlerts()">All</a> -
	<a href="javascript:void(0)" onclick="unselectAllAlerts()">None</a>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

	<b>Actions:</b>
	<img src="../images/ico-enable.gif" align="absbottom" /> <a href="javascript:void(0)" onclick="enableSelectedAlerts()">Enable</a> -
	<img src="../images/ico-disable.gif" align="absbottom" /> <a href="javascript:void(0)" onclick="disableSelectedAlerts()">Disable</a> -
	<img src="../images/ico-delete.gif" align="absbottom" /> <a href="javascript:void(0)" onclick="deleteSelectedAlerts()">Delete</a>
	<img src="../images/ico-download.gif" align="absbottom" /> <a href="javascript:void(0)" onclick="shortSelectedAlerts()">Short</a>
</c:if> --%>

<c:if test="${param.action == 'check'}">
	<a name="tickets"><h3>Tickets for Alert ID ${param.alertId}:</h3></a>
	<b>Event: </b><a href="BrowseTickets?eventId=${userAlert.eventId}" >${userAlert.event.name}</a>
	<br /><br />
	<display:table class="list" name="${alertTickets}" id="ticket"  pagesize="20" requestURI="${pageUrl}">
	    <display:column title="Rem. Qty" sortable="true" style="text-align:right">
	    	<span tooltip="Lot Size: ${ticket.lotSize}">${ticket.remainingQuantity}</span>
	    </display:column>
		<display:column title="Zone" sortable="true" property="category.symbol" />
	    <display:column title="Section" property="section" sortable="true" />
	    <display:column title="Row" sortable="true" property="row" />
	    <display:column title="Wholesale Price" sortable="true" property="adjustedCurrentPrice" format="{0,number,currency}" style="text-align:right" />
	   
		<display:column title="Online Price" sortable="true" style="text-align:right">
	        <c:if test="${ticket.buyItNowPrice > 0}">
	          <fmt:formatNumber value="${ticket.buyItNowPrice}"
	               type="currency" minFractionDigits="2"/>
	        </c:if>
	        <c:set var="timeLeft">
				<c:choose>
					<c:when test="${empty ticket.numSecondsBeforeEndDate}">
					</c:when>
					<c:when test="${ticket.numSecondsBeforeEndDate < 0}">
						(expired)
					</c:when>
					<c:when test="${ticket.numSecondsBeforeEndDate > 24 * 3600 * 1000}">
						Ending in <fmt:formatNumber type="number" maxFractionDigits="0" value="${(ticket.numSecondsBeforeEndDate / (24 * 3600 * 1000))}"/> days
					</c:when>
					<c:when test="${ticket.numSecondsBeforeEndDate > 3600 * 1000}">
						Ending in <fmt:formatNumber type="number" maxFractionDigits="0" value="${((ticket.numSecondsBeforeEndDate % (24 * 3600 * 1000)) / (3600 * 1000))}"/> hours
					</c:when>
					<c:otherwise>
						Ending in <fmt:formatNumber type="number" maxFractionDigits="0" value="${((ticket.numSecondsBeforeEndDate % (3600 * 1000)) / (60 * 1000))}"/> minutes
					</c:otherwise>
				</c:choose>	          
	        </c:set>
			<c:choose>
				<c:when test="${ticket.ticketType == 'AUCTION'}">
					<span tooltip="${timeLeft}">
						<c:choose>
							<c:when test="${ticket.numSecondsBeforeEndDate > 3600 * 24}">
								<img src="../images/ico-auction.gif" align="absbottom"/>
							</c:when>
							<c:otherwise>
								<img src="../images/ico-auction-ending.gif" align="absbottom"/>
							</c:otherwise>
						</c:choose>
					</span>
				</c:when>
			</c:choose>
		</display:column>
		
		<display:column media="html" title="Seller" sortProperty="seller" sortable="true" >
			<span style="white-space:nowrap">
				<a href="RedirectToItemPage?id=${ticket.id}" tooltip="${ticket.siteId} - ${ticket.itemId}"><img src="../images/ico-${ticket.siteId}.gif" border="0" align="absbottom"/>
					(Buy Ticket)
			    </a>
				<c:if test="${ticket.ticketType != 'AUCTION' and not empty ticket.numSecondsBeforeEndDate}">
					<span tooltip="${timeLeft}">
						<c:choose>
							<c:when test="${ticket.numSecondsBeforeEndDate > 3600 * 24}">
								<img src="../images/ico-clock.gif" align="absbottom"/>
							</c:when>
							<c:otherwise>
								<img src="../images/ico-clock-ending.gif" align="absbottom"/>
							</c:otherwise>
						</c:choose>
					</span>
				</c:if>
		
				<c:choose>
					<c:when test="${ticket.ticketDeliveryType == 'EDELIVERY'}">
						<img src="../images/ico-edelivery.gif" title="eDelivery" align="absbottom" />
					</c:when>
					<c:when test="${ticket.ticketDeliveryType == 'INSTANT'}">
						<img src="../images/ico-instant-edelivery.gif" title="Instant eDelivery" align="absbottom" />
					</c:when>
				</c:choose>			    	
				${ticket.seller}
			</span>
		</display:column>
	</display:table>

</c:if>

<script type="text/javascript">
	function selectAllAlerts() {
		$('.checkbox-alert').attr('checked', true);
	};

	function unselectAllAlerts() {
		$('.checkbox-alert').attr('checked', false);
	};
	
	function selectFilterTour(tourId) {
		document.location.href="${pageUrl}?filterTourId=" + tourId;
	};
	
	function selectFilterEvent(eventId) {
		document.location.href="${pageUrl}?filterEventId=" + eventId;
	};

	function selectFilterUsername(username) {
		document.location.href="${pageUrl}?filterUsername=" + username;
	};

	function selectFilterIncludeDisabledAlerts(includeDisabledAlerts) {
		document.location.href="${pageUrl}?filterIncludeDisabledAlerts=" + (includeDisabledAlerts?"1":"0");
	};

	function __getSelectedAlertIds() {
		var ids = "";
		$.each($('.checkbox-alert:checked'), function(i, elt) {
			var id = $(elt).attr('id').split("-")[2]; 
			ids += id + ",";
		});
		
		if (ids == "") {
			return null;
		}
				
		// remove the final comma
		ids = ids.substring(0, ids.length - 1)
		return ids;		
	};
	
	function enableSelectedAlerts() {
		var alertIds = __getSelectedAlertIds();
		if (alertIds == null) {
			alert("No alert selected");
			return;
		}		
		document.location.href = "${pageUrl}?action=enable&alertIds=" + alertIds;
	};
	
	function disableSelectedAlerts() {
		var alertIds = __getSelectedAlertIds();
		if (alertIds == null) {
			alert("No alert selected");
			return;
		}		
		document.location.href = "${pageUrl}?action=disable&alertIds=" + alertIds;
	};

	function deleteSelectedAlerts() {	
		var alertIds = __getSelectedAlertIds();
		if (alertIds == null) {
			alert("No alert selected");
			return;
		}		
		
		var r = confirm("Do you really want to delete the " + alertIds.split(",").length + " selected alerts ?");
		if (r) {
			document.location.href = "${pageUrl}?action=delete&alertIds=" + alertIds;
		}		
	};

	function shortSelectedAlerts() {	
		var alertIds = __getSelectedAlertIds();
		if (alertIds == null) {
			alert("No alert selected");
			return;
		}		
		
		var r = confirm("Do you really want to short the " + alertIds.split(",").length + " selected alerts ?");
		if (r) {
			document.location.href = "${pageUrl}?action=short&alertIds=" + alertIds;
		}		
	};
	
	function toggleAlertStatsTable() {
		if ($('#alert-stats-table').css('display') != 'none') {
			$('#alert-stats-table').hide();
			$('#show-stats-link').text('Show stats');	
			$('#show-stats-image').attr('src', '../images/ico-expand.gif');
		} else {
			$('#alert-stats-table').show();	
			$('#show-stats-link').text('Hide stats');	
			$('#show-stats-image').attr('src', '../images/ico-collapse.gif');
		}
	};
	
	function callEditShortAlert(alertId,priceLow,priceHigh,quantity) {
		   
		   if ($('#alertCreationDialog').length == 0) { 
				var alertCreationDialog = $.DIV({'id': 'alertCreationDialog', title: 'Edit Alert'},
					$.TABLE({'id': 'alertCreationTable'},
						$.COLGROUP({},
							$.COL({width: '60'}),
							$.COL({width: '200'})
						),				
						$.TR({},
								$.TD({}, "Min Price:"),
								$.TD({}, 
									$.INPUT({id: 'minPrice', 'name': 'minPrice', 'type': 'text','value' :priceLow })
								)
							),
							$.TR({},
								$.TD({}, "Max Price:"),
								$.TD({}, 
									$.INPUT({id: 'maxPrice', 'name': 'maxPrice', 'type': 'text' ,'value' :priceHigh })
								)
							),
							$.TR({},
									$.TD({}, "Quantites:"),
									$.TD({}, 
										$.INPUT({id: 'quantites', 'name': 'quantites', 'type': 'text' ,'value' :quantity })
									)
							)
					),
					$.DIV({'id': 'alertLoading'}, $.IMG({src: '../images/process-running.gif', 'align': 'absbottom'}), "Editing Alert...")
					
				);

				
				$('BODY').append(alertCreationDialog);
				$('#alertLoading').hide();

			    $("#alertCreationDialog").dialog({
			            bgiframe: true,
			            resizable: false,
			            width: 300,
				        height: 175,
				        minHeight: 175,
			            modal: true,
			            autoOpen: false,
			            overlay: {
			                    backgroundColor: '#000',
			                    opacity: 0.5
			            },
			            buttons: {
			                    'OK': function() {		
			                    	var minPrice = $('#minPrice').val();
			                    	var maxPrice = $('#maxPrice').val();
			                    	var quantites = $('#quantites').val();
			                    	var isEmpty = false;
			                    	
			            			
									if(minPrice.length == 0 && maxPrice.length == 0){
										isEmpty = true;
									}
									
									if(isEmpty){
										alert("Please Enter Min or Max Price");
										return;
									}  
									var updateAlertDetails ={shortAlertId: alertId,minPrice: minPrice,maxPrice: maxPrice,qty: quantites};
									editShortAlertDetailsAjax(updateAlertDetails);
									
									$('#alertLoading').show();
									$('#alertCreationTable').hide();
			                   		$("#alertCreationDialog").dialog('option', 'buttons', {});									

								},
								
								'Cancel': function() {
									$(this).dialog('close');
								}
			            },
			            close: function() {
							$("#alertCreationDialog").remove();	            	
			            }
			            
			    });
			}
			$("#alertCreationDialog").dialog("open");
			
		};
		
		function editShortAlertDetailsAjax(editAlertDetails){
			$.ajax({
				type: "POST",
				url: "EditShortAlert",
				dataType:"json",
				data: editAlertDetails,
				success: function(data){
			        $.each(data, function(index, element) {
						$("#alertCreationDialog").dialog('close');
						document.location.href = "ManageMyShortAlert";
			
			        });
				}
		    });		 
			
		}

</script>



