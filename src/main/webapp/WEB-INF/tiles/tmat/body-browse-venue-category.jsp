<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<script type="text/javascript">
var dateRangeStr1 = null;
var dateRangeStr2 = null;


</script>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
	<a href="..">Home</a> 
	&gt; <a href="TMATBrowseShorts">IRA Home</a>
	&gt; Browse Venue Category capacity
</div>

<div style="clear:both"></div>


Date: <input type="text" id="dateRange1" name="dateRange1" class="date-pick" val ="${fromDateRange}" style="width: 240px"/>
 to <input type="text" id="dateRange2" name="dateRange2" class="date-pick" val="${toDateRange}" style="width: 240px"/> 
<div style="float: right">
	<input id="nocrawl" name ="nocrawl" type="hidden" value="${noCrawl}">
	<input id="reload" name ="reload" type="hidden" value="${reload}">
	<input id="tourId" name ="tourId" type="hidden" value="${tourId}">
	<input id="eventId" name ="eventId" type="hidden" value="${eventId}">
	<input id="iraEventStatusId" name ="iraEventStatusId" type="hidden" value="${iraEventStatusId}">
	<span class="refreshLink" style="display:none">
	
		<img src="../images/ico-reload.gif" align="absbottom"/>
		Page updated <span id="numMinsSinceLastUpdate"></span>
	</span>
	<span class="refreshedRecently" style="display:none">
		<img src="../images/ico-reload.gif" align="absbottom"/>
		<i>(updated less than <fmt:formatNumber pattern="0" value="${ticketListingCrawler.minCrawlPeriod / 60}"/>mn ago)</i>
	</span>
	<span class="refreshCrawlInfoSpan" style="display:none">
		<img src="../images/process-running.gif" align="absbottom"/>
		<span class="refreshCrawlInfo" style="color:green"></span>
	</span>
</div>
<br/>
<br/>

<div id="tmatHeaderGoTo">
	<span id="goToTextFieldTMATContainer">
		Go to <input type="text" id="goToTextTMATField" style="width: 240px" />
		<span id="goToTextTMATFieldStatus" style="color:#888888;font-style:italic"></span>
	</span>
	<span id="tmatRedirectingContainer" style="display:none">
  		<div>
			<div style="float:left"><img src="../images/process-running.gif" align="absbottom" /> Redirecting to &nbsp;</div>
			<div id="tmatRedirectingPage" style="float:left"></div>
		</div>
	</span>

	or <!--<a href="javascript: return null;" onclick="browseAllEvents();">Browse ALL</a> -->
	Parent Category:
	<select id="parentType" onchange="getChild()">
		<option value="">--Select--</option>
		<option value="1" <c:if test='${parentType=="1"}'>selected </c:if>>Browse All Sports</option>
		<option value="2" <c:if test='${parentType=="2"}'>selected </c:if>>Browse All Concerts</option>
		<option value="4" <c:if test='${parentType=="4"}'>selected </c:if>>Browse All Theaters</option>
	</select>
	Child Category:
	<select id="childType" onchange="getGrandChild()">
		<option value="">--Select--</option>
		<c:forEach items="${childTypes}" var="child">
			<option value="${child.id}" <c:if test='${childType==child.id}'>selected </c:if>>${child.name}</option>
		</c:forEach>
	</select>
	GrandChild Category:
	<select id="grandChildType" >
		<option value="">--Select--</option>
		<c:forEach items="${grandChildTypes}" var="grandChild">
			<option value="${grandChild.id}" <c:if test='${grandChildType==grandChild.id}'>selected </c:if>>${grandChild.name}</option>
		</c:forEach>
	</select>
	<input id="changeGroupButton" type="button" class="medButton" onclick="browseAllEvents();"  value="GET Status"/><br/><br/>
</div>        
<br/>
<br/>

<a href="javascript:expandAll()"><img src="../images/ico-expand.gif" />Expand All</a> -
<a href="javascript:collapseAll()"><img src="../images/ico-collapse.gif" />Collapse All</a>
<br /><br />
<table id="statusTable" class="list">
	<thead>
		<tr>
			<th>Event Name</th>
			<th>Event Date</th>
			<th>Event Time</th>
			<th>Venue</th>		
			<th>Zone</th>
			<th>Zone Capacity</th>
			<th>Zone InHand</th>
			<th>Zone Shorts</th>
			<th>Total Available</th>
			<th>InHand(%)</th>
			<th>Shorts(%)</th>
			<th>Total Risk</th>
			<th>Total Risk(%)</th>
			<th>Total Not In Play</th>
			<th>Total Not In Play(%)</th>
		</tr>	
	</thead>
<c:if test="${not empty categoriesStatus}">
	<c:forEach var="categoryStatus" items="${categoriesStatus}">
		<%--
			EVENT STATUS LEVEL
		--%>
		<tbody>
			<tr class="eventRow">
				<td style='text-align:left'>
					<a onclick="toggleStatusTBody(${categoryStatus.eventId})" style="cursor: pointer">
						<img id="event-expand-img-${categoryStatus.eventId}" class="event-expand-img" src="../images/ico-expand.gif" />
						${categoryStatus.eventName}
					</a>  					
				</td>
				<td align="right">${categoryStatus.eventDate}</td>
				<td align="right">${categoryStatus.eventTime}</td>
				<td style='text-align:center'><c:if test="${categoryStatus.venueName != null}">
					<img title="${categoryStatus.venueName}" id="show-venue-image" src="../images/question.jpg"></c:if>
				</td>
				<td align="right">${categoryStatus.zone}</td>
				<td align="right">${categoryStatus.zoneCapacity}</td>
				<td align="right">${categoryStatus.zoneInHand}</td>
				<td align="right">${categoryStatus.zoneShort}</td>
				<td align="right">${categoryStatus.totalAvailable}</td>
				<td align="right">${categoryStatus.inHandPercentage}%</td>
				<td align="right">${categoryStatus.shortsPercentage}%</td>
				<td align="right">${categoryStatus.totalRisk}</td>
				<td align="right">${categoryStatus.totalRiskPercentage}%</td>
				<td align="right">${categoryStatus.totalNotInPlay}</td>
				<td align="right">${categoryStatus.totalNotInPlayPercentage}%</td>
			</tr>
			</tbody>				
			
		<c:forEach var="child" items="${categoryStatus.children}">		
			<%--
				CATEGORY STATUS LEVEL
			--%>
			<tbody id="status-tbody-${categoryStatus.eventId}" class="status-child status-child-tbody-${categoryStatus.eventId}" style="display:none" >	
				<tr class="catRow">			
					<td> 
						<img src="../images/ico-branch.gif" />
					</td>			
					<%-- Empty Date , Time and Venue--%>
					<td> &nbsp; </td>
					<td> &nbsp; </td>
					<td> &nbsp; </td>
					<td >${child.zone}</td>
					<td align="right">${child.zoneCapacity}</td>
					<td align="right">${child.zoneInHand}</td>
					<td align="right">${child.zoneShort}</td>
					<td align="right">${child.totalAvailable}</td>
					<td align="right">${child.inHandPercentage}%</td>
					<td align="right">${child.shortsPercentage}%</td>
					<td align="right">${child.totalRisk}</td>
					<td align="right">${child.totalRiskPercentage}%</td>
					<td align="right">${child.totalNotInPlay}</td>
					<td align="right">${child.totalNotInPlayPercentage}%</td>
				</tr>
			</tbody>				
			
		</c:forEach> 
	</c:forEach>
</c:if>
</table>
<br /><br />

<div style="border-top: 1px dashed black; border-bottom: 1px dashed black;padding-top: 5px; padding-bottom: 5px;">
    Show tickets for the following sites: <a href="javascript:checkAllSites()" style="font-size:11px">check all</a> - <a href="javascript:uncheckAllSites()" style="font-size:11px">uncheck all</a> 
    <br />
	<div style="float:left">
		<c:forEach var="sid" items="${constants.siteIds}">
			<div style="float: left; width: 280px;height: 24px;">
				<c:set var="safeSid" value="[${sid}]" />
				
				<input type="hidden" id="${sid}-checkbox-input" name="${sid}_checkbox" value=""/> 
				<input type="checkbox" id="${sid}-checkbox" class="site-checkbox"
					<c:if test="${not fn:contains(constants.disabledSiteIdsString, safeSid)}">checked</c:if>
					<c:if test="${fn:contains(constants.disabledSiteIdsString, safeSid)}">disabled</c:if>
				>
				<label for="${sid}_checkbox">
			    	<img src="../images/ico-${sid}.gif" border="0" align="absmiddle"/>						
					<c:choose>
					  <c:when test="${fn:contains(constants.disabledSiteIdsString, safeSid)}">
					    <span style="color: #888888; text-decoration: line-through" tooltip="${sid} is disabled">
						  ${sid}
					    </span>
					  </c:when>
					  <c:otherwise>
					    ${sid}
					  </c:otherwise>
					</c:choose>
				</label>
			</div>
		</c:forEach>
	</div>
	<div style="clear:both"></div>
</div>

<br /><br />

<div id="statsDiv"></div>

<script type="text/javascript">
	
	var lastUpdated=new Date(parseInt($('#reload').val()));
	
	function showRefreshedRecently() {
		refreshRecentlySpanFirstTime();
	/*
	$('.refreshLink').hide();
	$('.refreshedRecently').show();
	var now = new Date();
	var numMins = parseInt((now.getTime() - lastUpdated.getTime()) / 60000);
	if (numMins >= ${1000 * ticketListingCrawler.minCrawlPeriod}) {
		refreshRecentlySpanFirstTime();
	} else {
	  setTimeout(refreshRecentlySpanFirstTime, ${1000 * ticketListingCrawler.minCrawlPeriod} - numMins);
	}
	*/
	}

	function refreshRecentlySpanFirstTime() {
		$('.refreshedRecently').hide();
		$('.refreshLink').show();
		refreshRecentlySpan();
		setInterval("refreshRecentlySpan()", 60000);
	}

	function refreshRecentlySpan() {
		var now = new Date();
		var numMins = parseInt((now.getTime() - lastUpdated.getTime()) / 60000);
		if (numMins == 0) {
		  $('#numMinsSinceLastUpdate').text("just now");
		} else {
		  $('#numMinsSinceLastUpdate').text(numMins + " mn ago");
		}
	}

	function expandAll() {
		$('#statusTable').find('.event-expand-img').attr('src', '../images/ico-collapse.gif');
		$('#statusTable').find('.category-expand-img').attr('src', '../images/ico-collapse.gif');
		$('#statusTable').find('.status-child').show();
	};

	function collapseAll() {
		$('#statusTable').find('.event-expand-img').attr('src', '../images/ico-expand.gif');
		$('#statusTable').find('.category-expand-img').attr('src', '../images/ico-expand.gif');
		$('#statusTable').find('.status-child').hide();
	};

	function toggleStatusTBody(eId) {
		if ($('#event-expand-img-' + eId).attr('src').indexOf('expand') >= 0) {
			$('#status-tbody-' + eId).show();
			$.each($('.status-child-tbody-' + eId), function(i, elt) {
				if ($(elt).hasClass("status-child")) {
					$(elt).show();
					$(elt).find('.category-expand-img').attr('src', '../images/ico-expand.gif');
				} else {
					$(elt).hide();
				}
				
			});
			$('#event-expand-img-' + eId).attr('src', '../images/ico-collapse.gif');
		} else {
			$('#status-tbody-' + eId).hide();
			$('.status-child-tbody-' + eId).hide();
			$('#event-expand-img-' + eId).attr('src', '../images/ico-expand.gif');
		}
	};
	    
	
  //
  // Autocomplete
  //
  
  $('#goToTextTMATField').val('Search Shorts Sold');
  $('#goToTextTMATField').css({color: '#aaaaaa', 'font-style': 'italic'});
  
  $('#goToTextTMATField').focus(function() {
	$('#goToTextTMATField').val("");
	$('#goToTextTMATField').css({color: '#000000', 'font-style': 'normal'});
	$('#goToTextTMATFieldStatus').empty();
  });

  $('#goToTextTMATField').blur(function() {
	$('#goToTextTMATField').val('Search Shorts Sold');
	$('#goToTextTMATField').css({color: '#aaaaaa', 'font-style': 'italic'});
	$('#goToTextTMATFieldStatus').empty();
  });
  
  $(function() {
		var date1 = new Date(); 
		$('#dateRange1').datepicker({changeMonth:true,changeYear:true,minDate:new Date(),maxDate:'+1y',showMonthAfterYear: true,onSelect: function(dateText,inst){dateRangeStr1 = dateText;}});
		$('#dateRange2').datepicker({changeMonth:true,changeYear:true,minDate:new Date(),maxDate:'+1y',showMonthAfterYear: true,onSelect: function(dateText,inst){dateRangeStr2 = dateText;}});
		
		if('${fromDateRange}'==''){
			var prettyDate =(date1.getMonth()+1) + '/' + date1.getDate() + '/' +date1.getFullYear();
			$('#dateRange1').val(prettyDate);
		}else{
			$('#dateRange1').val('${fromDateRange}');
		}
		
		if('${toDateRange}'==''){
			var prettyDate =(date1.getMonth()+2) + '/' + date1.getDate() + '/' +date1.getFullYear();
			$('#dateRange2').val(prettyDate);
		}else{
			$('#dateRange2').val('${toDateRange}');
		}
		
		var noCrawlVal= $('#nocrawl').val();
		if(noCrawlVal=='true'){
			getCrawlerStatus();
		}else{
			refreshRecentlySpanFirstTime();
		}
  });
  
  var tempFlag;
	var totalCrawls="${totalCrawl}";
	var eventIdstr = "${totalEvetnStr}";
	function getCrawlerStatus(){
		$.ajax({
			url:"GetCrawlsExecutedForEventsCount?eventIds=" + eventIdstr ,
			success: function(res){
				var temp = res.split(":");
				var count = parseInt(temp[0]);
				var eventString = temp[1];
				$('.refreshCrawlInfo').html(parseInt(100 * (count / (totalCrawls))) + "% updated");
				$('.refreshCrawlInfoSpan').show();
				if(count < totalCrawls) {
					setTimeout("getCrawlerStatus()", 10000);
				}else {
					// force reload (avoid caching by adding a dummy parameter)
					$('.refreshCrawlInfo').html("100% updated! Reloading page...");
					
					var cat = $('#selectCat').val();
					var inventoryType = $('#selectInventory').val();
					var view = $('#selectView').val();
					if(view == 'csv'){
						$('.refreshCrawlInfoSpan').hide();
					}
					var noCrawl = $('#nocrawl').val();
					var iraEventStatusId = $('#iraEventStatusId').val();
					dateRangeStr1= $('#dateRange1').val();
					dateRangeStr2= $('#dateRange2').val();
					if('${tourId}' != '' && '${tourId}' != 'ALL'){
						location.href = "TMATBrowseVenueCategory?tourId=${tourId}&dateRange1="+dateRangeStr1+"&dateRange2="+dateRangeStr2+ "&nocrawl=" + noCrawl + "&iraEventStatusId=" + iraEventStatusId;
					} else  if('${eventId}' != '' && '${eventId}' != 'ALL'){
						location.href = "TMATBrowseVenueCategory?eventId=${eventId}&dateRange1="+dateRangeStr1+"&dateRange2="+dateRangeStr2+ "&nocrawl=" + noCrawl + "&iraEventStatusId=" + iraEventStatusId;
					}else if('${venueId}' != '' && '${venueId}' != 'ALL'){
						location.href = "TMATBrowseVenueCategory?eventId=${eventId}&dateRange1="+dateRangeStr1+"&dateRange2="+dateRangeStr2+ "&nocrawl=" + noCrawl + "&iraEventStatusId=" + iraEventStatusId;
					}else{
						browseAllEvents();
					}
				}
			}
		});
	}
	
  
  $('#goToTextTMATField').autocomplete("AutoCompleteSearch", {
  		width: 550,
		max: 1000,
		minChars: 2,		
		formatItem: function(row, i, max) {
			if (row[0] == "ARTIST") {			
				return "<div class='searchArtistTag'>ARTIST</div>" + row[1];
			} else if (row[0] == "TOUR") {
				var tourRow = "<div  class='searchTourTag'>TOUR</div>" + row[1];
				if (!isNaN(row[3]*1) && !isNaN(row[4] * 1)) {
				 	tourRow += " - <font color='#943400'>" +  formatShortDate(new Date(row[3]*1)) + "-" + formatShortDate(new Date(row[4]*1)) + "</font>";
				 }
				 return tourRow;
			} else if (row[0] == "EVENT") {
				return "<div  class='searchEventTag'>EVENT</div>" + row[1] + " - <font color='#943400'>" +  formatDate(new Date(row[3]*1)) + "</font>";
			}else if(row[0] == "VENUE"){
				return "<div  class='searchVenueTag'>VENUE</div>" + row[1];
			}
		},
		formatResult: function(row) {
			return row[1];
		},
		startSearch: function() {
			$('#goToTextTMATFieldStatus').empty();
			$('#goToTextTMATFieldStatus').append($.IMG({src: '../images/process-running.gif', align: 'absbottom'}));
		},
		stopSearch: function(rows) {
			$('#goToTextTMATFieldStatus').empty();
			if (rows.length == 0) {
				$('#goToTextTMATFieldStatus').text("No results");
			} else {
				$('#goToTextTMATFieldStatus').text("Found " + rows.length + " results");
			}
		}
  });
  
  function checkAllSites() {
  	$('.site-checkbox').attr('checked', true);
  };

  function uncheckAllSites() {
  	$('.site-checkbox').attr('checked', false);
  };
  
  function browseAllEvents() {
	dateRangeStr1 = $('#dateRange1').val();
	dateRangeStr2 = $('#dateRange2').val();
	var parentType = $('#parentType').val();
	var childType = $('#childType').val();
	var grandChildType = $('#grandChildType').val();
	
	var flag=true;
	if(parentType==''){
		flag=false;
	}
	if(childType==''){
		flag=false;
	}
	if(grandChildType==''){
		flag=false;
	}
	var noCrawl = $('#nocrawl').val();
	var iraEventStatusId = $('#iraEventStatusId').val();
	if(flag){
		if(noCrawl=='true'){
			document.location.href = "TMATBrowseVenueCategory?dateRange1="+dateRangeStr1+"&dateRange2="+dateRangeStr2+"&parentType="+parentType+"&childType="+childType+"&grandChildType="+grandChildType+"&nocrawl="+noCrawl + "&iraEventStatusId=" + iraEventStatusId;
		}else{
			document.location.href = "TMATBrowseVenueCategory?dateRange1="+dateRangeStr1+"&dateRange2="+dateRangeStr2+"&parentType="+parentType+"&childType="+childType+"&grandChildType="+grandChildType+"&nocrawl="+noCrawl + "&iraEventStatusId=" + iraEventStatusId;
		}
		
	}else{
		alert('Select GrandChild Category..');
	}
  };
  
  
  $('#goToTextTMATField').result(function(event, row, formatted) {
  	$('#goToTextFieldTMATContainer').hide();
  	$('#tmatRedirectingContainer').show();
  	
  	var pageDescription;
	if (row[0] == "ARTIST") {			
		pageDescription = "<div class='searchArtistTag'>ARTIST</div>" + row[1];
	} else if (row[0] == "TOUR") {
		pageDescription = "<div class='searchTourTag'>TOUR</div>" + row[1];
		if (row[3] != "null" || row[4] != "null") {
		 	pageDescription += " - <font color='#943400'>" +  formatShortDate(new Date(row[3]*1)) + "-" + formatShortDate(new Date(row[4]*1)) + "</font>";
		 }
		 pageDescription += "</div>";		 
	} else if (row[0] == "EVENT") {
		pageDescription = "<div class='searchEventTag'>EVENT</div>" + row[1] + " - <font color='#943400'>" +  formatShortDate(new Date(row[3]*1)) + "</font>";
			} else if(row[0] == "VENUE"){
		pageDescription = "<div class='searchVenueTag'>ARTIST</div>" + row[1];		
	}
  	
	dateRangeStr1 = $('#dateRange1').val();
  	dateRangeStr2 = $('#dateRange2').val();
	
  	var flag=true;
	if(flag){
		$('#tmatRedirectingPage').html(pageDescription);
		if (row[0] == "ARTIST") {
			document.location.href = "TMATBrowseEvents?artistId="+row[2]+"&dateRange1="+dateRangeStr1+"&dateRange2="+dateRangeStr2;
			return;
		} else if (row[0] == "TOUR") {
			document.location.href = "TMATBrowseVenueCategory?tourId="+row[2]+"&dateRange1="+dateRangeStr1+"&dateRange2="+dateRangeStr2;
			return;
		} else if (row[0] == "EVENT") {
			document.location.href = "TMATBrowseVenueCategory?eventId="+row[2]+"&dateRange1="+dateRangeStr1+"&dateRange2="+dateRangeStr2;
			return;
		}else if(row[0] == "VENUE"){
			document.location.href = "TMATBrowseVenueCategory?venueId="+row[2]+"&dateRange1="+dateRangeStr1+"&dateRange2="+dateRangeStr2;
		}
	}else{
		$('#goToTextFieldTMATContainer').show();
		$('#tmatRedirectingContainer').hide();
	}
  });
  
  function callBrowseTickets(eventId){
		
		if(null == eventId || eventId == 'ALL' || eventId == ''){
			alert("Atleast One Event is required.");
		}else{
			document.location.href = "BrowseTickets?eventId="+eventId;
		}
	}
	function getChild(){
		var val = $('#parentType').val()
		$.ajax({
			url:"GetChildCategory?tourCategoryId=" + val ,
			success: function(res){
				var childCategory = res.split(",");
				
				var options='<option value="">--Select--</option>';
				var length = childCategory.length;
				for(var i=0;i<length;i++){
					var opt = childCategory[i];
					var temp = opt.split(':');
					options+='<option value=' + temp[0] + '>' + temp[1] + '</option>';
				}
				$('#childType').children().remove();
				$('#childType').append(options);
			}
		});
	}
	
	function getGrandChild(){
		var val = $('#childType').val()
		$.ajax({
			url:"GetGrandChildCategory?childCategoryId=" + val ,
			success: function(res){
				var grandChildCategory = res.split(",");
				
				var options='<option value="">--Select--</option>';
				var length = grandChildCategory.length;
				for(var i=0;i<length;i++){
					var opt = grandChildCategory[i];
					var temp = opt.split(':');
					options+='<option value=' + temp[0] + '>' + temp[1] + '</option>';
				}
				$('#grandChildType').children().remove();
				$('#grandChildType').append(options);
			}
		});
	}
</script>
