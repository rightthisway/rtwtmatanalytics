<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<script type="text/javascript">
var dateRangeStr1 = null;
var dateRangeStr2 = null;


</script>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
	<a href="..">Home</a> 
	&gt; <a href="TMATBrowseEvents">Browse Inventory</a>
	<c:choose>
		<c:when test="${eventId == 'ALL'}">
			&gt; Browse All Inventory
		</c:when>
		<c:when test="${not empty eventId}">
			&gt; <a href="TMATBrowseShorts?tourId=${event.tourId}">Browse Tour Inventory</a> 
			&gt; Browse Event Inventory
		</c:when>
		<c:otherwise>
			&gt; Browse Tour Inventory
		</c:otherwise>
	</c:choose>
</div>

<c:choose>
	<c:when test="${eventId == 'ALL'}">
		<h1>Browse All Sale</h1>
	</c:when>
	<c:when test="${not empty eventId}">
		<h1>Browse Event Sale</h1>
	</c:when>
	<c:otherwise>
		<h1>Browse Tour Sale</h1>
	</c:otherwise>
</c:choose>

Date: <input type="text" id="dateRange1" name="dateRange1" class="date-pick" style="width: 240px"/> to <input type="text" id="dateRange2" name="dateRange2" class="date-pick" style="width: 240px"/> 
<%-- <div style="float: right">
	<input id="nocrawl" name ="nocrawl" type="hidden" value="${noCrawl}">
	<input id="reload" name ="reload" type="hidden" value="${reload}">
	<span class="refreshLink" style="display:none">
	
		<img src="../images/ico-reload.gif" align="absbottom"/>
		Page updated <span id="numMinsSinceLastUpdate"></span>
	</span>
	<span class="refreshedRecently" style="display:none">
		<img src="../images/ico-reload.gif" align="absbottom"/>
		<i>(updated less than <fmt:formatNumber pattern="0" value="${ticketListingCrawler.minCrawlPeriod / 60}"/>mn ago)</i>
	</span>
	<span class="refreshCrawlInfoSpan" style="display:none">
		<img src="../images/process-running.gif" align="absbottom"/>
		<span class="refreshCrawlInfo" style="color:green"></span>
	</span>
</div> --%>
<br/>
<br/>

<div id="tmatHeaderGoTo">
	<b>Category Type: </b>
	<select id="selectCat">
		<option value="-">--Select--</option>
		<option value="" <c:if test="${catType==''}">selected</c:if>>All</option>
		<option value="cat" <c:if test="${catType=='cat'}">selected</c:if>>Categorized</option>
		<option value="uncat" <c:if test="${catType=='uncat'}">selected</c:if>>Uncategorized</option>
	</select>

	<b>Inventory Type: </b>
	<select id="selectInventory">
		<option value="-">--Select--</option>
		<option value="" <c:if test="${filter==''}">selected</c:if>>All</option>
		<option value="short" <c:if test="${filter=='short'}">selected</c:if>>Short</option>
		<option value="inventory" <c:if test="${filter=='inventory'}">selected</c:if>>Long Inventory</option>
	</select>
	<b>View : </b>
	<select id="selectView">
		<option value="html">HTML</option>
		<option value="csv">CSV</option>
	</select>
	<br/><br/>
	<span id="goToTextFieldTMATContainer">
		Go to <input type="text" id="goToTextTMATField" style="width: 240px" />
		<span id="goToTextTMATFieldStatus" style="color:#888888;font-style:italic"></span>
	</span>
	<span id="tmatRedirectingContainer" style="display:none">
  		<div>
			<div style="float:left"><img src="../images/process-running.gif" align="absbottom" /> Redirecting to &nbsp;</div>
			<div id="tmatRedirectingPage" style="float:left"></div>
		</div>
	</span>

	or <!--<a href="javascript: return null;" onclick="browseAllEvents();">Browse ALL</a> -->
	Parent Type:
	<select id="browseEvents" >
		<option value="">--Select--</option>
		<option value="sports">Browse All Sports</option>
		<option value="concerts">Browse All Concerts</option>
		<option value="theaters">Browse All Theaters</option>
		<option value="all">Browse All</option>
	</select>
	<input id="changeGroupButton" type="button" class="medButton" onclick="browseAllEvents($('#browseEvents'));"  value="GET Inventory"/><br/><br/>
</div>        
<br/>
<div class="info">
	<div class="infoText">
		A seating zone is required for determining PGM%.<br/>
		If zones are not mapped for this event, the PGM% will always be 0.00.<br/>
		If zones are mapped for this event and the PGM is 0.00, that means TMAT could not find any tickets to fill.
	</div>
</div> 
<br/>
<br/>
<!-- 
Category Group:
<select id="catScheme">
	<c:forEach var="catg" items="${catGroups}">	
		<option value="${catg}" <c:if test="${catg == catGroupName}">selected</c:if>>${catg}</option>
	</c:forEach>
</select>
 -->
<!---->
<!--
Show: 
<c:choose>
	<c:when test="${filter == 'uncat'}">
		<b>UNCAT</b> -
		<a href="javascript: return null;" onclick="browseShortOnly('${fromDateRange}','${toDateRange}');">Short Only</a> -
		<a href="javascript: return null;" onclick="browseInventoryOnly('${fromDateRange}','${toDateRange}');">Inventory Only</a> - 
		<a href="javascript: return null;" onclick="browseBoth('${fromDateRange}','${toDateRange}');">Short And Long Invertory</a>
	</c:when>
	<c:when test="${filter == 'short'}">
		<a href="javascript: return null;" onclick="browseUnCatOnly('${fromDateRange}','${toDateRange}');">UNCAT</a> - 
		<b>Short Only</b> - 
		<a href="javascript: return null;" onclick="browseInventoryOnly('${fromDateRange}','${toDateRange}');">Inventory Only</a> - 
		<a href="javascript: return null;" onclick="browseBoth('${fromDateRange}','${toDateRange}');">Short And Long Invertory</a>
	</c:when>
	<c:when test="${filter == 'inventory'}">
		<a href="javascript: return null;" onclick="browseUnCatOnly('${fromDateRange}','${toDateRange}');">UNCAT</a> - 
		<a href="javascript: return null;" onclick="browseShortOnly('${fromDateRange}','${toDateRange}');">Short Only</a> - 
		<b>Inventory Only</b> - 
		<a href="javascript: return null;" onclick="browseBoth('${fromDateRange}','${toDateRange}');">Short And Long Invertory</a>
	</c:when>
	<c:when test="${empty filter}">
		<a href="javascript: return null;" onclick="browseUnCatOnly('${fromDateRange}','${toDateRange}');">UNCAT</a> - 
		<a href="javascript: return null;" onclick="browseShortOnly('${fromDateRange}','${toDateRange}');">Short Only</a>  - 
		<a href="javascript: return null;" onclick="browseInventoryOnly('${fromDateRange}','${toDateRange}');">Inventory Only</a> - 
		<b>Short And Long Invertory</b>
	</c:when>
	
</c:choose>

<br /><br />
-->
<a href="javascript:expandAll()"><img src="../images/ico-expand.gif" />Expand All</a> -
<a href="javascript:collapseAll()"><img src="../images/ico-collapse.gif" />Collapse All</a>
<br /><br />
<table id="statusTable" class="list">
	<thead>
		<tr>
			<th>Event Name</th>
			<th>Event Date</th>
			<th>Event Time</th>
			<th>Venue</th>		
			<th>Qty</th>
			<th>Section</th>
			<th>Row</th>		
			<th>Cost/T</th>
			<th>Po Date</th>
			<th>PGM</th>
			<th>Inv Date</th>
			<th>Sell/T</th>
			<th>90 DTE</th>
			<th>60 DTE</th>
			<th>30 DTE</th>
			<th>14 DTE</th>
			<th>7 DTE</th>
			<th>4 DTE</th>
			<th>1 DTE</th>			
		</tr>	
	</thead>
<c:if test="${not empty eventStatuses }">
	<c:forEach var="eventStatus" items="${eventStatuses}">
		<%--
			EVENT STATUS LEVEL
		--%>
		<tbody>
			<tr class="eventRow">
				<td style='text-align:left'>
					<a onclick="toggleStatusTBody(${eventStatus.eventId})" style="cursor: pointer">
						<img id="event-expand-img-${eventStatus.eventId}" class="event-expand-img" src="../images/ico-expand.gif" />
						${eventStatus.description}
					</a>  					
				</td>
				<td align="right">${eventStatus.date}</td>
				<td align="right">${eventStatus.time}</td>
				<td style='text-align:center'><c:if test="${eventStatus.venue != null}">
					<img title="${eventStatus.venue}" id="show-venue-image" src="../images/question.jpg"></c:if>
				</td>
				<td align="right">${eventStatus.qtySold}</td>				
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td align="right" style="background-color:${ao:getMarginBackgroundColor(eventStatus.projGrossMargin)}">
					<span style='color:black'><fmt:formatNumber value="${eventStatus.projGrossMargin}" pattern="0.00" /></span>
				</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
		</tbody>
		<c:forEach var="categoryStatus" items="${eventStatus.children}">		
			<%--
				CATEGORY STATUS LEVEL
			--%>
			<tbody class="status-child status-child-tbody-${eventStatus.eventId}" style="display:none">
				<tr class="catRow">
					<%-- event name --%>
					<td style='text-align:left'>
						<img src="../images/ico-branch.gif" />
						<a onclick="toggleStatusTDoubleBody(${eventStatus.eventId}, ${categoryStatus.categoryId})" style="cursor: pointer">
							<img id="event-expand-img-${eventStatus.eventId}-${categoryStatus.categoryId}" class="category-expand-img" src="../images/ico-expand.gif" />
							${categoryStatus.category}							
						</a>  					
					</td>
					<%-- Empty Date --%>
					<td> &nbsp; </td>
					<td> &nbsp; </td>
					<td> &nbsp; </td>
					<%-- Qty Sold --%>
					<td align="right">${categoryStatus.qtySold}</td>					
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<%-- PGM --%>
					<td align="right" style="background-color:${ao:getMarginBackgroundColor(categoryStatus.projGrossMargin)}"> 
						<span style='color:black'><fmt:formatNumber value="${categoryStatus.projGrossMargin}" pattern="0.00" /></span>
					</td> 
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>						
				</tr>
			</tbody>				
			<%--
				TICKET GROUP STATUS LEVEL
			--%>
			<tbody id="status-tbody-${eventStatus.eventId}-${categoryStatus.categoryId}" class="status-grandchild status-child-tbody-${eventStatus.eventId}" style="display:none" >
				<c:forEach var="ticketGroupStatus" items="${categoryStatus.children}">	
					<tr style='text-align:left'>
						<%-- event name --%>
						<td>
							<img src="../images/ico-branch.gif" />
							<img src="../images/ico-branch.gif" />

							${ticketGroupStatus.description} Quantity : ${ticketGroupStatus.qtySold}
							<input id="getCatStatsButton" type="button" class="smallButton" onclick="getStats(${ticketGroupStatus.eventId}, '${ticketGroupStatus.category}', ${ticketGroupStatus.categoryId},${ticketGroupStatus.qtySold})"  value="Tix" tooltip="Get tickets for category ${categoryStatus.category}" />		
						</td>
						<%-- Empty Date --%>
						<td> &nbsp; </td>
						<td> &nbsp; </td>
						<td> &nbsp; </td>
						<%-- Qty --%>
						<td align="right">${ticketGroupStatus.qtySold}</td>
								
						<%-- Section --%>
						<td>
							<span tooltip="${ticketGroupStatus.section}">${ticketGroupStatus.normalizedSection}</span>
							<input id="exStatsButton" type="button" class="smallButton" onclick="getExactStats(${ticketGroupStatus.eventId}, ${ticketGroupStatus.transactionId})"  value="Tix" tooltip="Get tickets for section ${ticketGroupStatus.section}" />
						</td>

						<%-- Row --%>
						<td tooltip="${ticketGroupStatus.row}">${ticketGroupStatus.normalizedRow}</td>
						
						<%--Cost Per Ticket --%>
						<td>
							${ticketGroupStatus.buyingPrice }
						</td>	
						<%--Po Date --%>
						<td>
							<fmt:formatDate value="${ticketGroupStatus.poDate }" pattern="MM/dd/yyyy"/>
						</td>
						<%-- PGM --%>
						<td align="right" style="background-color:${ao:getMarginBackgroundColor(ticketGroupStatus.projGrossMargin)}">
							<span style='color:black'><fmt:formatNumber value="${ticketGroupStatus.projGrossMargin}" pattern="0.00" /></span>
						</td>
						
						
						<%--Invoice Date --%>
						<td>
						 	<fmt:formatDate value="${ticketGroupStatus.invoiceDate }" pattern="MM/dd/yyyy"/>	
						</td>
						<%--Selling price Per Ticket --%>
						<td>
							${ticketGroupStatus.sellingPrice }
						</td>					
						<td>
							<c:choose>
								<c:when test="${ticketGroupStatus.dataPointDiff > 90}">
									<center><b>-</b></center>
								</c:when>
								<c:otherwise>
									${ticketGroupStatus.priceAtDate90 }
								</c:otherwise>
							</c:choose>							
						</td>
						<td>
						 	<c:choose>
								<c:when test="${ticketGroupStatus.dataPointDiff > 60}">
									<center><b>-</b></center>
								</c:when>
								<c:otherwise>
									${ticketGroupStatus.priceAtDate60 }
								</c:otherwise>
							</c:choose>								
						</td>
						<td>
							<c:choose>
								<c:when test="${ticketGroupStatus.dataPointDiff > 30}">
									<center><b>-</b></center>
								</c:when>
								<c:otherwise>
									${ticketGroupStatus.priceAtDate30 }
								</c:otherwise>
							</c:choose>								
						</td>
						<td>
							<c:choose>
								<c:when test="${ticketGroupStatus.dataPointDiff > 14}">
									<center><b>-</b></center>
								</c:when>
								<c:otherwise>
									${ticketGroupStatus.priceAtDate14 }
								</c:otherwise>
							</c:choose>								
						</td>
						<td>
							<c:choose>
								<c:when test="${ticketGroupStatus.dataPointDiff > 7}">
									<center><b>-</b></center>
								</c:when>
								<c:otherwise>
									${ticketGroupStatus.priceAtDate7 }
								</c:otherwise>
							</c:choose>							
						</td>
						<td>
							<c:choose>
								<c:when test="${ticketGroupStatus.dataPointDiff > 4}">
									<center><b>-</b></center>
								</c:when>
								<c:otherwise>
									${ticketGroupStatus.priceAtDate4 }
								</c:otherwise>
							</c:choose>								
						</td>
						<td>
							<c:choose>
								<c:when test="${ticketGroupStatus.dataPointDiff > 1}">
									<center><b>-</b></center>
								</c:when>
								<c:otherwise>
									${ticketGroupStatus.priceAtDate1 }
								</c:otherwise>
							</c:choose>								
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</c:forEach>
	</c:forEach>
</c:if>
</table>
<br /><br />

<div style="border-top: 1px dashed black; border-bottom: 1px dashed black;padding-top: 5px; padding-bottom: 5px;">
    Show tickets for the following sites: <a href="javascript:checkAllSites()" style="font-size:11px">check all</a> - <a href="javascript:uncheckAllSites()" style="font-size:11px">uncheck all</a> 
    <br />
	<div style="float:left">
		<c:forEach var="sid" items="${constants.siteIds}">
			<div style="float: left; width: 280px;height: 24px;">
				<c:set var="safeSid" value="[${sid}]" />
				
				<input type="hidden" id="${sid}-checkbox-input" name="${sid}_checkbox" value=""/> 
				<input type="checkbox" id="${sid}-checkbox" class="site-checkbox"
					<c:if test="${not fn:contains(constants.disabledSiteIdsString, safeSid)}">checked</c:if>
					<c:if test="${fn:contains(constants.disabledSiteIdsString, safeSid)}">disabled</c:if>
				>
				<label for="${sid}_checkbox">
			    	<img src="../images/ico-${sid}.gif" border="0" align="absmiddle"/>						
					<c:choose>
					  <c:when test="${fn:contains(constants.disabledSiteIdsString, safeSid)}">
					    <span style="color: #888888; text-decoration: line-through" tooltip="${sid} is disabled">
						  ${sid}
					    </span>
					  </c:when>
					  <c:otherwise>
					    ${sid}
					  </c:otherwise>
					</c:choose>
				</label>
			</div>
		</c:forEach>
	</div>
	<div style="clear:both"></div>
</div>

<br /><br />

<div id="statsDiv"></div>

<script type="text/javascript">
	
/*var lastUpdated=new Date(parseInt($('#reload').val()));
	
	function showRefreshedRecently() {
		refreshRecentlySpanFirstTime();
	
	$('.refreshLink').hide();
	$('.refreshedRecently').show();
	var now = new Date();
	var numMins = parseInt((now.getTime() - lastUpdated.getTime()) / 60000);
	if (numMins >= ${1000 * ticketListingCrawler.minCrawlPeriod}) {
		refreshRecentlySpanFirstTime();
	} else {
	  setTimeout(refreshRecentlySpanFirstTime, ${1000 * ticketListingCrawler.minCrawlPeriod} - numMins);
	}
	
	}

	function refreshRecentlySpanFirstTime() {
		$('.refreshedRecently').hide();
		$('.refreshLink').show();
		refreshRecentlySpan();
		setInterval("refreshRecentlySpan()", 60000);
	}

	function refreshRecentlySpan() {
		var now = new Date();
		var numMins = parseInt((now.getTime() - lastUpdated.getTime()) / 60000);
		if (numMins == 0) {
		  $('#numMinsSinceLastUpdate').text("just now");
		} else {
		  $('#numMinsSinceLastUpdate').text(numMins + " mn ago");
		}
	}

	 var totalCrawls;
	function checkCrawlsFinished() {
		//alert("eventID = "+eventID);
		var eventIdstr = "${eventIds}";
		eventIdstr = eventIdstr.substring(0, eventIdstr.length - 1);
		//alert(eventIdstr);
		var eventIds = eventIdstr.split(",");
		
		
		CrawlerDwr.getCrawlsExecutedForEventsCount(eventIdstr, function(response) {
		  var count = parseInt(response);
		  $('.refreshCrawlInfo').html(parseInt(100 * (count / (totalCrawls))) + "% updated");
		  if(count < (totalCrawls)) {
			setTimeout("checkCrawlsFinished()", 10000);
		  } else {
			
			// force reload (avoid caching by adding a dummy parameter)
			$('.refreshCrawlInfo').html("100% updated! Reloading page...");
			if('${tourId}' != ''){
				location.href = 'TMATBrowseSales?tourId=${tourId}&nocrawl=true&dateRange1=${dateRangeStr1}&dateRange2=${dateRangeStr2}';
				//tourId=20883&dateRange1=[object HTMLInputElement]&dateRange2=[object HTMLInputElement]
			} else  if('${eventId}' != ''){
				location.href = 'TMATBrowseSales?eventId=${eventId}&nocrawl=true&dateRange1=${dateRangeStr1}&dateRange2=${dateRangeStr2}';
			}
		  }
		});
		
	}
	function updateCrawls(isNoCrawl) {
		// Code that uses jQuery's $ can follow here.
		//alert("hi all .. !!!");
		//var newCrawl = udpateCrawls();
		//alert("updateCrawl called");
		//alert("isNoCrawl = "+isNoCrawl);
		if( '${tourId}' != '' ||  '${eventId}' != ''){
			if(isNoCrawl=='true'){
				var time= parseInt($('#reload').val());
				  lastUpdated = new Date(time);
				  showRefreshedRecently();
				  return;
			}
			var eventIdstr = "${eventIds}";
			eventIdstr = eventIdstr.substring(0, eventIdstr.length - 1);
			//alert(eventIdstr);
			/* var eventIds = eventIdstr.split(",");
			
			for (i in eventIds)
			{ 
				//alert(eventIds[i]);
				//eventId = eventId.val().replace(',','');
				CrawlerDwr.forceRecrawlForEvents(eventIdstr, function(response) {
					//alert(response);
					var tokens = response.split('|');
					if (tokens[0] != 'OK') {
						lastUpdated = new Date();
						showRefreshedRecently();
						
					} else {
						totalCrawls = parseInt(tokens[1]);
						var runningCrawls = parseInt(tokens[2]);
						if (totalCrawls == 0) {
							lastUpdated = new Date(tokens[3]);
							showRefreshedRecently();
							return;
						}
						$('.refreshLink').hide();
						$('.refreshedRecently').hide();
						$('.refreshCrawlInfoSpan').show();
						$('.refreshCrawlInfo').html(parseInt(100 * (runningCrawls / totalCrawls)) + "% updated");
						setTimeout("checkCrawlsFinished()", 10000);
					}
				});
				
			 } 
		}
		return false;
	} */

    function changeGroup(eventId) {
        var catGroup = $.trim($('#catScheme').val());
		if(eventId == undefined) {
	        document.location.href = "TMATBrowseSales?catScheme=" + catGroup;
		} else {
			document.location.href = "TMATBrowseSales?eventId=" + eventId + "&catScheme=" + catGroup;
		}
    }

    function getAll() {
        var catGroup = $.trim($('#catScheme').val());;
		document.location.href = "TMATBrowseSales?eventId=ALL&catScheme=" + catGroup;
    }

	function expandAll() {
		$('#statusTable').find('.event-expand-img').attr('src', '../images/ico-collapse.gif');
		$('#statusTable').find('.category-expand-img').attr('src', '../images/ico-collapse.gif');
		$('#statusTable').find('.status-child').show();
		$('#statusTable').find('.status-grandchild').show();
	};

	function collapseAll() {
		$('#statusTable').find('.event-expand-img').attr('src', '../images/ico-expand.gif');
		$('#statusTable').find('.category-expand-img').attr('src', '../images/ico-expand.gif');
		$('#statusTable').find('.status-child').hide();
		$('#statusTable').find('.status-grandchild').hide();
	};

	function toggleStatusTBody(eId) {
		if ($('#event-expand-img-' + eId).attr('src').indexOf('expand') >= 0) {
			$('#status-tbody-' + eId).show();
			$.each($('.status-child-tbody-' + eId), function(i, elt) {
				if ($(elt).hasClass("status-child")) {
					$(elt).show();
					$(elt).find('.category-expand-img').attr('src', '../images/ico-expand.gif');
				} else {
					$(elt).hide();
				}
				
			});
			$('#event-expand-img-' + eId).attr('src', '../images/ico-collapse.gif');
		} else {
			$('#status-tbody-' + eId).hide();
			$('.status-child-tbody-' + eId).hide();
			$('#event-expand-img-' + eId).attr('src', '../images/ico-expand.gif');
		}
	};
	    
	function toggleStatusTDoubleBody(eId, cId) {
		if ($('#event-expand-img-' + eId + '-' + cId).attr('src').indexOf('expand') >= 0) {
			$('#status-tbody-' + eId + '-' + cId).show();
			$('#event-expand-img-' + eId + '-' + cId).attr('src', '../images/ico-collapse.gif');
		} else {
			$('#status-tbody-' + eId + '-' + cId).hide();
			$('#event-expand-img-' + eId + '-' + cId).attr('src', '../images/ico-expand.gif');
		}
	};
	
	function getStats(eId, category, categoryId, lotSize) {

 		if(category != 'Not Set') {

			var url;		

			url = "TMATPreviewStats?eventId=" + eId
					+ "&categoryId=" + categoryId
					+ "&lotSize=" + lotSize
					+ "&catScheme=${catGroupName}";
					
			 $.each($('.site-checkbox'), function(i, elt) {
    			var siteId = $(elt).attr('id').split("-")[0];
    			url += "&" + siteId + "_checkbox=" + $(elt).attr('checked');  	
    		});
			
			
			$('#statsDiv').empty();
			$('#statsDiv').append(
				$.IMG({src: '../images/process-running.gif', align: 'absbottom'}),
				"Fetching preview. Please wait..."		
			);		
			
	
			$.get(url, function(data){
				$('#statsDiv').html(data);
	 		});

		} else {
	 		alert("Zone Not Mapped for Event!");
			return;
		} 
	};

	function getExactStats(eId, tId) {
		var url;		

		url = "TMATPreviewStats?transactionId=" + tId + "&eventId=" + eId + "&catScheme=${catGroupName}";
				
		 $.each($('.site-checkbox'), function(i, elt) {
			var siteId = $(elt).attr('id').split("-")[0];
			url += "&" + siteId + "_checkbox=" + $(elt).attr('checked');  	
		});
		
		$('#statsDiv').empty();
		$('#statsDiv').append(
			$.IMG({src: '../images/process-running.gif', align: 'absbottom'}),
			"Fetching preview. Please wait..."		
		);		
	
		$.get(url, function(data){
			$('#statsDiv').html(data);
 		});
	};
	
	
  //
  // Autocomplete
  //
  
  $('#goToTextTMATField').val('Search Shorts Sold');
  $('#goToTextTMATField').css({color: '#aaaaaa', 'font-style': 'italic'});
  
  $('#goToTextTMATField').focus(function() {
	$('#goToTextTMATField').val("");
	$('#goToTextTMATField').css({color: '#000000', 'font-style': 'normal'});
	$('#goToTextTMATFieldStatus').empty();
  });

  $('#goToTextTMATField').blur(function() {
	$('#goToTextTMATField').val('Search Shorts Sold');
	$('#goToTextTMATField').css({color: '#aaaaaa', 'font-style': 'italic'});
	$('#goToTextTMATFieldStatus').empty();
  });
  
  $(function() {
		//alert("tourId"+${tourId});
		//alert("eventId"+${eventId});
		//alert('${tourId}');
		//alert('${eventId}');
		/* if('${tourId}' != '' ||  '${eventId}' != '' ){
			//alert("hi");
			updateCrawls(${noCrawl});
		} */
		var date1 = new Date(); 
		$('#dateRange1').datepicker({changeMonth:true,changeYear:true,minDate:new Date(),maxDate:'+1y',showMonthAfterYear: true,onSelect: function(dateText,inst){dateRangeStr1 = dateText;}});
		var prettyDate =(date1.getMonth()+1) + '/' + date1.getDate() + '/' +date1.getFullYear();
		$('#dateRange1').val(prettyDate);
		$('#dateRange2').datepicker({changeMonth:true,changeYear:true,minDate:new Date(),maxDate:'+1y',showMonthAfterYear: true,onSelect: function(dateText,inst){dateRangeStr2 = dateText;}});
		prettyDate =(date1.getMonth()+2) + '/' + date1.getDate() + '/' +date1.getFullYear();
		$('#dateRange2').val(prettyDate);
  });

  
  
  $('#goToTextTMATField').autocomplete("AutoCompleteSearch", {
  		width: 550,
		max: 1000,
		minChars: 2,		
		formatItem: function(row, i, max) {
			if (row[0] == "ARTIST") {			
				return "<div class='searchArtistTag'>ARTIST</div>" + row[1];
			} else if (row[0] == "TOUR") {
				var tourRow = "<div  class='searchTourTag'>TOUR</div>" + row[1];
				if (!isNaN(row[3]*1) && !isNaN(row[4] * 1)) {
				 	tourRow += " - <font color='#943400'>" +  formatShortDate(new Date(row[3]*1)) + "-" + formatShortDate(new Date(row[4]*1)) + "</font>";
				 }
				 return tourRow;
			} else if (row[0] == "EVENT") {
				return "<div  class='searchEventTag'>EVENT</div>" + row[1] + " - <font color='#943400'>" +  formatDate(new Date(row[3]*1)) + "</font>";
			}
		},
		formatResult: function(row) {
			return row[1];
		},
		startSearch: function() {
			$('#goToTextTMATFieldStatus').empty();
			$('#goToTextTMATFieldStatus').append($.IMG({src: '../images/process-running.gif', align: 'absbottom'}));
		},
		stopSearch: function(rows) {
			$('#goToTextTMATFieldStatus').empty();
			if (rows.length == 0) {
				$('#goToTextTMATFieldStatus').text("No results");
			} else {
				$('#goToTextTMATFieldStatus').text("Found " + rows.length + " results");
			}
		}
  });
  
  function checkAllSites() {
  	$('.site-checkbox').attr('checked', true);
  };

  function uncheckAllSites() {
  	$('.site-checkbox').attr('checked', false);
  };
  
  function browseAllEvents(obj) {
	var eventType = $(obj).val();
	var dateRangeStr1 = document.getElementById("dateRange1").value;
	var dateRangeStr2 = document.getElementById("dateRange2").value;
	var flag=true;
	if(eventType==''){
		flag=false;
	}
	var cat = $('#selectCat').val();
	if(cat=='-'){
		flag=false;
		alert('Select Category Type..');
		$(obj).val('');
	}
	
	var inventoryType = $('#selectInventory').val();
	if(inventoryType=='-'){
		flag=false;
		alert('Select Inventory Type..');
		$(obj).val('');
	}
	var view = $('#selectView').val();
	if(flag){
		if(view=='html'){
			document.location.href = "TMATBrowseSales?eventId=ALL&dateRange1="+dateRangeStr1+"&dateRange2="+dateRangeStr2+"&filter="+inventoryType+"&eventType="+eventType+"&catType="+cat;
		}else{
			document.location.href = "TMATGetBrowseShortCSV?eventId=ALL&dateRange1="+dateRangeStr1+"&dateRange2="+dateRangeStr2+"&filter="+inventoryType+"&eventType="+eventType+"&catType="+cat;
		}
	}else{
		alert('Select Parent Type..');
	}
  };
  
  function browseShortOnly(dateRange1String,dateRange2String) {
	  var dateRange1= null;
	  var dateRange2= null;
	  if(dateRangeStr1 == null && dateRangeStr2 == null)
	  {
		  dateRange1=dateRange1String;
		  dateRange2=dateRange2String;
	  }else{
		  dateRange1=dateRangeStr1;
		  dateRange2=dateRangeStr2;
	  }
	document.location.href = "TMATBrowseSales?eventId=${eventId}&tourId=${tourId}&filter=short&dateRange1="+dateRange1+"&dateRange2="+dateRange2;
  };
  
  function browseInventoryOnly(dateRange1String,dateRange2String){
	  var dateRange1= null;
	  var dateRange2= null;
	  if(dateRangeStr1 == null && dateRangeStr2 == null)
	  {
		  dateRange1=dateRange1String;
		  dateRange2=dateRange2String;
	  }else{
		  dateRange1=dateRangeStr1;
		  dateRange2=dateRangeStr2;
	  }
  	document.location.href = "TMATBrowseSales?eventId=${eventId}&tourId=${tourId}&filter=inventory&dateRange1="+dateRange1+"&dateRange2="+dateRange2;
  };
  
  function browseBoth(dateRange1String,dateRange2String){
	  var dateRange1= null;
	  var dateRange2= null;
	  if(dateRangeStr1 == null && dateRangeStr2 == null)
	  {
		  dateRange1=dateRange1String;
		  dateRange2=dateRange2String;
	  }else{
		  dateRange1=dateRangeStr1;
		  dateRange2=dateRangeStr2;
	  }
  	document.location.href = "TMATBrowseSales?eventId=${eventId}&tourId=${tourId}&dateRange1="+dateRange1+"&dateRange2="+dateRange2;
  };
  function browseUnCatOnly(dateRange1String,dateRange2String){
	  var dateRange1= null;
	  var dateRange2= null;
	  if(dateRangeStr1 == null && dateRangeStr2 == null)
	  {
		  dateRange1=dateRange1String;
		  dateRange2=dateRange2String;
	  }else{
		  dateRange1=dateRangeStr1;
		  dateRange2=dateRangeStr2;
	  }
  	document.location.href = "TMATBrowseSales?eventId=${eventId}&tourId=${tourId}&filter=uncat&dateRange1="+dateRange1+"&dateRange2="+dateRange2;
  };
  function showPreviewTickets(eventId, quantity, categoryId, transactionId, type, ticketId) {
	$('#statsDiv').empty();
	$('#statsDiv').append(
		$.IMG({src: '../images/process-running.gif', align: 'absbottom'}),
		"Fetching tickets. Please wait..."		
	);		

    var url = "TMATPreviewTickets?eventId=" + eventId
    		+ "&quantity=" + quantity
    		+ "&categoryId=" + categoryId
    		+ "&transactionId=" + transactionId
    		+ "&type=" + type
    		+ "&ticketId=" + ticketId;
	$.get(url, function(data){
		$('#statsDiv').html(data);
	});
  }
  
  $('#goToTextTMATField').result(function(event, row, formatted) {
  	$('#goToTextFieldTMATContainer').hide();
  	$('#tmatRedirectingContainer').show();
  	
  	var pageDescription;
	if (row[0] == "ARTIST") {			
		pageDescription = "<div class='searchArtistTag'>ARTIST</div>" + row[1];
	} else if (row[0] == "TOUR") {
		pageDescription = "<div class='searchTourTag'>TOUR</div>" + row[1];
		if (row[3] != "null" || row[4] != "null") {
		 	pageDescription += " - <font color='#943400'>" +  formatShortDate(new Date(row[3]*1)) + "-" + formatShortDate(new Date(row[4]*1)) + "</font>";
		 }
		 pageDescription += "</div>";		 
	} else if (row[0] == "EVENT") {
		pageDescription = "<div class='searchEventTag'>EVENT</div>" + row[1] + " - <font color='#943400'>" +  formatShortDate(new Date(row[3]*1)) + "</font>";
	}
  	
	var dateRangeStr1 = document.getElementById('dateRange1');
  	var dateRangeStr2 = document.getElementById('dateRange2');
	
  	var flag=true;
	var cat = $('#selectCat').val();
	if(cat=='-'){
		flag=false;
		alert('Select Category Type..');
	}
	var inventoryType = $('#selectInventory').val();
	if(inventoryType=='-'){
		flag=false;
		alert('Select Inventory Type..');
	}
	var view = $('#selectView').val();
	if(flag){
		$('#tmatRedirectingPage').html(pageDescription);
		if (row[0] == "ARTIST") {
			document.location.href = "TMATBrowseEvents?artistId="+row[2]+"&dateRange1="+dateRangeStr1+"&dateRange2="+dateRangeStr2;
			return;
		} else if (row[0] == "TOUR") {
			if(view=='html'){
				document.location.href = "TMATBrowseSales?tourId="+row[2]+"&dateRange1="+dateRangeStr1+"&dateRange2="+dateRangeStr2+"&filter="+inventoryType+"&catType="+cat;
			}else{
				$('#goToTextFieldTMATContainer').show();
				$('#tmatRedirectingContainer').hide();
				document.location.href = "TMATGetBrowseShortCSV?tourId="+row[2]+"&dateRange1="+dateRangeStr1+"&dateRange2="+dateRangeStr2+"&filter="+inventoryType+"&catType="+cat;
			}
	//document.location.href = "TMATBrowseShorts?tourId="+row[2]+"&dateRange1="+dateRangeStr1+"&dateRange2="+dateRangeStr2;
			return;
		} else if (row[0] == "EVENT") {
			if(view=='html'){
				document.location.href = "TMATBrowseSales?eventId="+row[2]+"&dateRange1="+dateRangeStr1+"&dateRange2="+dateRangeStr2+"&filter="+inventoryType+"&catType="+cat;
			}else{
				$('#goToTextFieldTMATContainer').show();
				$('#tmatRedirectingContainer').hide();
				document.location.href = "TMATGetBrowseShortCSV?eventId="+row[2]+"&dateRange1="+dateRangeStr1+"&dateRange2="+dateRangeStr2+"&filter="+inventoryType+"&catType="+cat;
			}
	//document.location.href = "TMATBrowseShorts?eventId="+row[2]+"&dateRange1="+dateRangeStr1+"&dateRange2="+dateRangeStr2;
			return;
		}
	}else{
		$('#goToTextFieldTMATContainer').show();
		$('#tmatRedirectingContainer').hide();
	}
  });
</script>
