<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
  <a href="..">Home</a> 
  &gt; <a href="TMATBrowseShorts">Browse Shorts</a>
    <c:if test="${not empty tour}">
    &gt; <a href="TMATShortOverview?tourId=${tour.id}">Browse Tour Shorts</a>
  </c:if>
  &gt; Change Tour Prices
</div>

<h1>Change Tour Prices</h1>

<h2>Current Settings</h2>
<table>
  <tr>
    <td>User Name: </td>
    <td>${username}</td>
  </tr>
  <tr>
    <td>Current Tour: </td>
    <td>${tour.name}</td>
  </tr>
</table>
<h2>Tour Settings</h2>
<c:forEach var="sid" items="${constants.siteIds}">
<h3>Current Tour Adjustments for ${sid}:</h3>
<c:set var="varname" value="tAdjs_${sid}" />
<c:choose>
	<c:when test="${not empty varname}">	
		<display:table class="list" name="${varname}" id="tAdj"  requestURI="TMATChangeTourPrice">
			<display:column title="Tour"  >${tour.name}</display:column >
		    <display:column title="Site" sortable="false" property="siteId" />
		    <display:column title="Short % Fee" sortable="false" property="shortPercent" format="{0,number,###,###.##}" />
		    <display:column title="Short Flat Fee" sortable="false" property="shortFlatFee" format="{0,number,###,###.##}"/>
		</display:table>
	</c:when>
	<c:otherwise>
		No Tour Adjustment Found
	</c:otherwise>
</c:choose>

<h3>Change Tour Adjustments:</h3>
<table>
  <tr>
    <td>Set Flat Fee: </td>
    <td>$<input id="tourFlatFee-${sid}" type="text"/></td>
  </tr>
  <tr>
    <td>Set Percent Fee: </td>
    <td><input id="tourPercentFee-${sid}" type="text"/>%</td>
  </tr>
  </table>
<br/>
<input id="addTourNewPriceButton-${sid}" type="button" class="medButton" onclick="addTourFees('${sid}')"  value="Add Tour Fees"/>
</c:forEach>
<br/><br/>
  <c:if test="${not empty tour}">
	<a href="TMATBrowseEvents?tourId=${tour.id}">Return to Browse Events</a> 
  </c:if>

<script type="text/javascript">
<!--//--><![CDATA[//><!--
  
    function addTourFees(siteId) {
    
    var tId = "${tour.id}";
    var flat = $.trim($('#tourFlatFee-' + siteId).val());
    var per = $.trim($('#tourPercentFee-' + siteId).val());
     
     if((flat.length == 0) && (per.length == 0)) {
     	alert("Please enter either a Flat Fee or a Percentage Fee or Both!");
     	return;
     }
        
	if (flat.length == 0) {
	  flat = "0.00";
	}
	    
	if (per.length == 0) {
	  per = "0.00";
	}

    ShortTransactionDwr.changeTourShortPrice(tId, siteId, flat, per, function(response) {                
             alert(response);
             document.location.href=("TMATChangeTourPrice?tourId=" + tId);
     });
  }
//--><!]]>
</script>
