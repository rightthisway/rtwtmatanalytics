<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
  <a href="..">Home</a> 
  &gt; <a href="TMATBrowseShorts">Browse Shorts</a>
  <c:if test="${not empty eventId}">
    &gt; <a href="TMATShortOverview?eventId=${eventId}">Browse Tour Shorts</a>
  	&gt; <a href="TMATBrowseShorts?eventId=${eventId}">Browse Event Shorts</a> 
  </c:if>
  &gt; Exclude Ticket
</div>

<c:if test="${not empty ticket}">
	<br /><br />
	<input id="excludeFromAll" type="button" class="largeButton" onclick="excludeFromAll()"  value="Exclude Ticket From All Shorts"/>
</c:if>

<c:if test="${not empty short}">
	<br /><br />
	<input id="excludeFromAll" type="button" class="largeButton" onclick="excludeFromThis()"  value="Exclude Ticket From This Short"/>
</c:if>

<c:if test="${not empty excludedTix}">
	<br /><br />
	<display:table class="list" name="${excludedTix}" id="excludedTicket" pagesize="20" requestURI="TMATExcludeTickets">
		<display:column title="Event Name" sortable="false" >${event.name}</display:column>
		<display:column title="Ticket" sortable="false" >
			Price:${idToTicketMap[excludedTicket.ticketId].currentPrice} 
			Section:${idToTicketMap[excludedTicket.ticketId].normalizedSection} 
			Row:${idToTicketMap[excludedTicket.ticketId].normalizedRow} 
			Lot Size:${idToTicketMap[excludedTicket.ticketId].lotSize} 
		</display:column>
		<display:column title="Excluded From" sortable="false" >	
			<c:if test="${empty excludedTicket.shortId}">
				All Shorts
			</c:if>
			<c:if test="${not empty excludedTicket.shortId}">
				Short: ${excludedTicket.shortId}
			</c:if>
		</display:column>
		<display:column sortable="false" ><input id="deleteMap" type="button" class="medButton" onclick="deleteMap(${excludedTicket.id})"  value="Delete"/></display:column>
	</display:table>
</c:if>

<script type="text/javascript">

  function excludeFromAll() {
    var tId = "${ticket.id}";
    var eId = "${event.id}";

	ShortTransactionDwr.addExclusion(tId, eId, null, function(response) {
        if (response != 'OK') {
          alert(response);
          return;
        }
        
        alert(response);
        //return to browse
        document.location.href = "TMATBrowseShorts?eventId=" + eId;
      });
  }
    
  function excludeFromThis() {
    var tId = "${ticket.id}";
    var eId = "${event.id}";
    var sId = "${short.id}";

	ShortTransactionDwr.addExclusion(tId, eId, sId, function(response) {
        if (response != 'OK') {
          alert(response);
          return;
        }
        
        alert(response);
        //return to browse
        document.location.href = "TMATBrowseShorts?eventId=" + eId;
      });
  }  
  
  function deleteMap(mapId) {
    var tId = "${ticket.id}";
    var eId = "${event.id}";
    var sId = "${short.id}";

	ShortTransactionDwr.deleteExclusion(mapId, function(response) {
        if (response != 'OK') {
          alert(response);
          return;
        }
        
        alert(response);
        //return to browse
        document.location.href = "TMATExcludeTicket?ticketId=" + tId + "&eventId=" + eId + "&shortId=" + sId;
      });
  }
    
</script>