<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<tiles:useAttribute id="selectedSubMenu" name="selectedSubMenu" scope="request" />

<div id="editorSubMenu">
  <c:choose>
    <c:when test="${selectedSubMenu == 'analytic'}">
      <b>Analytics</b>
    </c:when>
    <c:otherwise>
      <a href="BrowseTicket">Analytics</a>
    </c:otherwise>
    </c:choose>
    |
  <c:choose>
    <c:when test="${selectedSubMenu == 'Valuation Factors'}">
      <b>Valuation Factors</b>
    </c:when>
    <c:otherwise>
      <a href="EditorValuationFactors">Valuation Factors</a>
    </c:otherwise>
  </c:choose>
  
  </div>
  <tiles:insertAttribute name="subBody" />