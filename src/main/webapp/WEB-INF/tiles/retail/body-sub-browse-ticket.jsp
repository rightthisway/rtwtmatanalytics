<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<script type="text/javascript">
$(document).ready(function() {

	if(${selectedArtist != null && selectedArtist != ""}){
		$('#selectedArtist').val(${selectedArtist});
	}
	if(${selectedTour != null && selectedTour != ""}){
		$('#selectedTour').val(${selectedTour});
	} else{
		$('#selectedTour').val("defaultTour");
	} 
	
	
});


</script>


<div id="breadCrumbPath" class="breadCrumbPathEditor">
	<a href="..">Analytics</a>
	&gt; browse Ticket
	
</div>
<br/>
<div>
<div id="eventLinks">
	<b>Arstist:</b>
			<select id="selectedArtist" onchange="document.location.href='BrowseTicket?artistId=' + this.value">
				<c:forEach var="art" items="${artistList}">
					<option value="${art.id}" >${art.name}</option>
				</c:forEach>
			</select>
			
			<br/><br/>
		<%-- <c:choose>
			<c:when test="${empty tourList}">
				No Tour found for selected Artist. 
			</c:when>
			
			<c:otherwise>
				<b>Tours:&nbsp</b>
					<select id="selectedTour" onchange="document.location.href='BrowseTicket?tourId=' + this.value">
						<option value="defaultTour" selected="selected">----Select Tour to Browse----</option>
						<c:forEach var="tour" items="${tourList}">
							<option value="${tour.id}" >${tour.name}</option>
						</c:forEach>
					</select>
			<br/><br/>
			</c:otherwise>
		</c:choose> --%>
		
		<c:choose>
			<c:when test="${empty eventList}">
				<c:if test="${eventInfo != null}">${eventInfo}</c:if>
			</c:when>
			<c:otherwise>
				<b>event: &nbsp </b>
				<select onchange="document.location.href='BrowseTicket?eventId=' + this.value">
					 <option value="defaultEvent" selected="selected">----Select event to Browse----</option> 
					<c:forEach var="ev" items="${eventList}">
						<option value="${ev.id}">${ev.name}&nbsp;${ev.formattedEventDate}&nbsp;${ev.formattedVenueDescription}</option>
					</c:forEach>
				</select>
			</c:otherwise>
		</c:choose>
		
			

	</div>
	
</div>

<div style="clear: both"></div>

<div id="wholePageDiv" >
<h1>Browse Tickets</h1>
</div>