<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
  <a href="..">Analytics</a> 
  &gt;Manage Zones 
  &gt; ${tour.name}
</div>

<div id="quickLinks">
 	<a href="BrowseEvents?tourId=${tour.id}">Go to Browse Events</a>
	| <a href="TMATBrowseEvents?tourId=${tour.id}">Go to Shorts</a>
 	<%-- | <a href="EditorDetailTour?tourId=${tour.id}">Edit Tour</a> --%>
</div>
<div style="clear:both"></div>

<c:if test="${not empty param.error}">
<div class="error">
<div class="errorText">
${param.error}
</div>
</div>
</c:if>

<div id="info" class="info" style="display:none">
<div id="mainInfoText" class="infoText">
</div>
</div>
<div id="pleaseWaitDiv" style="display:none"><img src="../images/process-running.gif" align="absbottom" /> Processing. Please wait...</div>

<b>Tour:</b>
<select name="tourId" onchange="location.href='EditorEditCategories?tourId=' + $(this).val()">
  <c:forEach var="tour" items="${tours}">
    <option value="${tour.id}" <c:if test="${tour.id == param.tourId}">selected</c:if>> ${tour.name}</option>
  </c:forEach>
</select>
<br/><br/>

 Zone Group:
  ${catGroupName}
<select id="catScheme">
  <c:forEach var="catg" items="${catGroups}">
    <option value="${catg}">${catg}</option>
  </c:forEach>
</select>
   <input id="changeGroupButton" type="button" class="medButton" onclick="changeGroup()"  value="Change"/><br/><br/>
 <!--  
Send update request to zone:   
   <input id="changeGroupButton" type="button" class="medButton" onclick="sendUpdateRequest();"  value="Update"/><br/><br/> -->
   
<h2>Zones</h2>
<table width="100%">
<tr>
<td valign="top" width="600px">
<table id="categoryTable" class="list">
  <colgroup>
    <col width="60" />
    <col width="140" />
    <col width="200" />
    <col width="100" />
    <col width="100" />
    <col width="180" />
  </colgroup>
  <thead>
    <th>Symbol</th>
    <th>Description</th>
    <th>Equivalents</th>
    <th>Group</th>
    <th>Seats</th>
    
  </thead>
  <tbody id="categoriesBody">
  </tbody>
</table>
<div id="noCategoryDiv">(No zone)</div>

</td>
<td valign="top" style="text-align:right">
<!-- <img src="../images/ico-plus.gif" align="absbottom" /> <a id="addCategoryLink" href="#" onclick="showAddCategory(); return false">Add zone</a> -->

<div id="addOrEditCategory" style="float:right; text-align:left; display:none">
  <b id="addOrEditCategoryTitle">Add Zone</b><br/> 
  <input id="category_id" type="hidden"/>
  <table>
    <tr>
      <td style="text-align: right">Symbol:</td>
      <td>
       <input id="category_name" type="text"/>
      </td>
    </tr>
    <tr>
      <td style="text-align: right">Description:</td>
      <td>
        <input id="category_description" type="text"/>
      </td>
    </tr>
    <tr>
      <td style="text-align: right">Equivalent or Better Zones:</td>
      <td>
        <input id="category_eq_map" type="text"/>
      </td>
    </tr>
    <tr>
      <td style="text-align: right">Group:</td>
      <td>
        <input id="category_group_name" type="text"/>
      </td>
    </tr>
    <tr>
      <td style="text-align: right">Seat Quantity:</td>
      <td>
        <input id="category_seat_quantity" type="text"/>
      </td>
    </tr>
  </table>
  <!-- <input id="addOrEditCategoryButton" type="button" class="medButton" value="Add"/>
  <input type="button" class="medButton" value="Cancel" onclick="$('#addCategoryLink').show();$('#addOrEditCategory').hide()"/> -->
   <br/>
   <br/>
   <b id="addCategoryAliasTitle">Add Zone Alias</b><br/> 
  <table>
    <tr>
      <td style="text-align: right">New Alias:</td>
      <td>
        <input id="category_alias" type="text"/>
      </td>
            <td>
        <input id="category_alias_end" type="text"/>
      </td>
    </tr>
  </table>
  <input id="addCategoryAliasButton" type="button" class="medButton" value="Add Alias"/>
  <input type="button" class="medButton" value="Cancel" onclick="$('#addCategoryLink').show();$('#addOrEditCategory').hide()"/>
   <br/>
   <br/>
   <div id="removeCategoryAliasDiv" style="display:none">
     <b id="removeCategoryAliasTitle">Remove Zone Alias</b><br/> 
    <table id="categoryAliasTable" class="list">
	  <thead>
	    <th>Zone Symbol</th>
	    <th>Start Alias</th>
	    <th>End Alias</th>
	    <th></th>
	  </thead>
	  <tbody id="aliasesBody">
	  </tbody>
	</table>
   </div>
</div>

</td>
</tr>
</table>
<form:form action="EditorEditCategories" method="POST">
<h2>Events</h2>
<br/>
 <!-- Set category group for Zone on tour level:
<select id="catSchemeForZone">
	<option value="-1">PLEASE SELECT</option>
  <c:forEach var="catg" items="${catGroups}">
	<option value="${catg}"  <c:if test="${catg == zoneCatGroup}">selected</c:if>>${catg}</option>
  </c:forEach>
</select>
   <input id="changeGroupButton" type="button" class="medButton" onclick="setGroupForZone()"  value="Change"/><br/><br/>
<br/> 
Choose the event you want to categorize.<br/>
<input type="submit" value="Update" class="medButton" />  -->
<display:table class="list" name="${events}" id="event" requestURI="BrowseEvents">

    <display:column value="View" href="EditorEditEventCategories" paramId="eventId" paramProperty="id"  />
    <display:column title="Date" sortable="true">
	    <fmt:formatDate pattern="MM/dd/yyyy" value="${event.date}" />
		<c:choose>
		  <c:when test="${not empty event.time}">
		    <fmt:formatDate pattern="HH:mm" value="${event.time}" />
		  </c:when>
		  <c:otherwise>
		    TBD
		  </c:otherwise>
		</c:choose>
    </display:column>
    <display:column title="Venue" sortable="true" sortProperty="venue.building">
      <img src="../images/ico-${fn:toLowerCase(event.eventType)}.gif" align="absbottom" title="${event.eventType}"/> ${event.venue.building}
    </display:column>
    <display:column title="City" sortable="true" property="venue.city" />
    <display:column title="State" sortable="true" property="venue.state" />
    <display:column title="Country" sortable="true" property="venue.country"  />
   
	</display:table>
	
	<input type="hidden" name="tourId" value="${tour.id}" />
	<input type="hidden" name="zoneEventCategory" value="true" />
	
</form:form>

<h2>Upload Zones for Tour</h2>
<div class="info">
	<div class="infoText">
		You can easily create mapping by importing a CSV file. The format is as follows:<br/>
		venue, sections, rows, seats, categorySymbol, categoryGroup, categorySeatQuantity(optional - can be empty), equivalentCats
		<br/><br/>
		Examples:<br/>
		Rogers Centre, 12, 1, *, A, DEFAULT,, A<br/>
		Rogers Centre, 12-14, *, *, A, DEFAULT, 20, A<br/>
		Rogers Centre, 12-14, 1-5, *, A, DEFAULT, 15, A<br/>
		Rogers Centre, LOGE, *, B, *, DEFAULT,, A B,<br/>
		Rogers Centre, S1-S19, *, *, B, DEFAULT, 80, A B,<br/>
		<br/>
		<b>Note 1:</b> that if you CSV contains an undefined zone, it will be created automatically for you.<br/>
		<b>Note 2:</b> if you need to use the character - (hyphen) for a section, row, or seat please use %MINUS%, for instance:<br/>
		Rogers Centre, CRT%MINUS%56, *, 1, DEFAULT, 20, A<br/>
		Rogers Centre, CRT%MINUS%56-CRT%MINUS%87, 1%MINUS%5-1%MINUS%9, 1, DEFAULT, 50, A<br/>
		<b>Note 3:</b> The categorization will be run against the normalized section/row (the gray part is ignored).<br/>
		<b>Note 4:</b> You can also define rule such as "contain the string XX or YY", then you can do as follows:<br/>
		Rogers Centre, >XX>YY, *, *, A, DEFAULT,, A<br/>
		<b>Note 5:</b> To make the categorization works on multiple events which share the same venue, one can do as follows:<br/>		
		Rogers Centre, >XX>YY, *, *, A, DEFAULT,, A<br/>
		In that case all the events occuring between 02/21/2009 and 03/14/2009 will be categorized with the rule. The field time will be ignored.
	</div>
</div>
<form enctype="multipart/form-data" action="EditorUploadCategoryMappingCsvFile" method="POST">
    <input type="hidden" name="MAX_FILE_SIZE" value="1000000" />
    <input type="hidden" name="tourId" value="${tour.id}" />
	<img src="../images/ico-upload.gif" align="absmiddle" /> <b>Import csv file:</b> <input name="file" type="file"/> <input type="submit" name="submit" class="medButton "value="Upload"/>
</form>
(This will remove all the zone definitions and rules)

<h2>Export Data</h2>
<a href="EditorDownloadCategoryMappingCsvFile?tourId=${tour.id}"><img src="../images/ico-download.gif" align="absbottom" /> Download CSV file</a>

<h2>Operations</h2>
<a href="#" onclick="removeCategories(); return false"><img src="../images/ico-delete.gif" align="absbottom" /> Remove All</a> (run it if you want to remove everything: zones, mappings and unassign zones from tickets for this tour)<br/>
<br/>
<a href="#" onclick="removeCategoryMappings(); return false"><img src="../images/ico-delete.gif" align="absbottom" /> Remove All Zone Mappings</a><br/>
<script type="text/javascript">
  refreshCategories();

  function changeGroup() {
      var catGroup = $.trim($('#catScheme').val());;
      document.location.href = "EditorEditCategories?tourId=" + ${tourId} + "&catScheme=" + catGroup;
  }


  function setGroupForZone(){
	  var catGroup = $.trim($('#catSchemeForZone').val());;
	  if(catGroup != '-1'){
      	document.location.href = "EditorEditCategories?tourId=" + ${tourId} + "&setGroupForZone=true&catScheme=" + catGroup;
      }else{
      	alert('Please select a zone');
      }  
  }
  
  
  function removeCategories() {
    var answer = confirm('Are you sure you want to remove all the zones?');
    if (answer) {
      CategoryDwr.removeCategories(${tour.id}, "${catGroupName}", function(response) {
    	  if (response == 'OK') {
    		  $('#mainInfoText').html("Zones deleted!");
    		  $('#info').show();
			  refreshCategories();
    	  }
      });
    }
  }

  function removeCategoryMappings() {
    $('#pleaseWaitDiv').show();
    CategoryMappingDwr.removeAllCategoryMapping(${tour.id}, "${catGroupName}", function(response) {
        $('#pleaseWaitDiv').hide();
    	if (response == 'OK') {
    		$('#mainInfoText').html("Zone Mappings removed!");
    		$('#info').show();
    	}
    });
  }

  function removeCategory(id, symbol) {
    var answer = confirm('Are you sure you want to remove the zone ' + symbol + '?');
    if (answer) {
      CategoryDwr.removeCategory(id);
      refreshCategories();
    }
  }

  function refreshCategories() {
    $('#categoriesBody').children().remove();
    CategoryDwr.getCategories(${tour.id}, "${catGroupName}", function(categories) {
		if (categories.length == 0) {
		  $('#categoryTable').hide();
		  $('#noCategoryDiv').show();
		  return;
		}
		
  	    $('#categoryTable').show();
		$('#noCategoryDiv').hide();
    	$.each(categories, function(intIndex, category) {
    	  var editLink = $.A({href:'#'}, 'Edit');
    	  $(editLink).click(function(event) {
    	    showEditCategory(this.parentNode.parentNode);
    	    event.preventDefault();
    	  });

    	  var removeLink = $.A({href:'#'}, 'Remove');
    	  $(removeLink).click(function(event) {
    	    removeCategory(this.parentNode.parentNode.id.substring(4), this.parentNode.parentNode.name);
    	    event.preventDefault();
    	  });

          var tr = $.TR({id:'cat_' + category.id, name:category.symbol, 'class': ((intIndex % 2 == 0)?'even':'odd')},
                     $.TD({}, category.symbol),
                     $.TD({}, category.description),
                     $.TD({}, category.equalCats),
                     $.TD({}, category.groupName),
                     $.TD({}, category.seatQuantity)
	                
                   );
          $('#categoriesBody').append(tr);               
    	});
    });
  }
  
  function showAliases(catId) {
    $('#aliasesBody').children().remove();
    CategoryDwr.getAliases(catId, function(aliases) {
		if (aliases == null || aliases.length == 0) {
		  $('#categoryAliasTable').hide();
		  $('#removeCategoryAliasDiv').hide();
		  return;
		}
		$('#removeCategoryAliasDiv').show();
  	    $('#categoryAliasTable').show();
    	$.each(aliases, function(intIndex, alias) {
    	  var deleteLink = $.A({href:'#'}, 'Delete');
     	  $(deleteLink).click(function(event) {
    	    removeAlias(alias.id, alias.startSynonym, alias.catId);
    	    event.preventDefault();
    	  });

          var tr = $.TR({id:'alias_' + alias.id, name:alias.startSynonym, 'class': ((intIndex % 2 == 0)?"even":"odd")},
                     $.TD({}, alias.category.symbol),
                     $.TD({}, alias.startSynonym),
                     $.TD({}, alias.endSynonym),
                     $.TD({}, deleteLink)
                   );
          $('#aliasesBody').append(tr);               
    	});
    });
  }
  
  function removeAlias(id, syn, catId) {
    var answer = confirm('Are you sure you want to remove the alias ' + syn + '?');
    if (answer) {
      CategoryDwr.removeAlias(id);
      showAliases(catId);
    }
  }
  
  function addCategory() {
    var categoryName = $.trim($('#category_name').val());
    var categoryDesc = $.trim($('#category_description').val());
    var categoryEq = $.trim($('#category_eq_map').val());
    var categoryGroup = $.trim($('#category_group_name').val());
    var categoryQty = $.trim($('#category_seat_quantity').val());
    if (categoryEq.length == 0) {
    	categoryEq = "*";
    }

    if (categoryName.length == 0 || categoryDesc.length == 0) {
        ("Please provide a name and description");
      return;
    }

    if (categoryGroup.length == 0) {
        ("Please provide a group name ie: DEFAULT");
      return;
    }

	CategoryDwr.addCategory(${tour.id}, categoryName, categoryDesc, categoryEq, categoryGroup, categoryQty, function (response) {
      if (response != 'OK') {
        alert(response);
        return;
      }
      refreshCategories();
      $('#addCategoryLink').show();
      $('#addOrEditCategory').hide();
	});
  }

  function editCategory() {
    var categoryId = $('#category_id').val();
    var categoryName = $.trim($('#category_name').val());
    var categoryDesc = $.trim($('#category_description').val());
    var categoryEq = $.trim($('#category_eq_map').val());
    var categoryQty = $.trim($('#category_seat_quantity').val());
    var categoryGroup = $.trim($('#category_group_name').val());
    
    if (categoryEq.length == 0) {
    	categoryEq = "*";
    }
    
    
    if (categoryName.length == 0 || categoryDesc.length == 0) {
      alert("Please provide a name and description");
      return;
    }
    
	CategoryDwr.editCategory(categoryId, ${tour.id}, categoryName, categoryDesc, categoryEq, categoryGroup, categoryQty, function (response) {
      if (response != 'OK') {
        $('#info').html(response);
        return;
      }
      refreshCategories();
      $('#addCategoryLink').show();
      $('#addOrEditCategory').hide();
	});
  }

  function addAlias() {
    var categoryId = $('#category_id').val();
    var newAlias = $.trim($('#category_alias').val());
    var newAliasEnd = $.trim($('#category_alias_end').val());
    
    if (newAlias.length == 0) {
      alert("Please provide an alias");
      return;
    }
    
	CategoryDwr.addAlias(categoryId, newAlias, newAliasEnd, function (response) {
      if (response != 'OK') {
      	alert(response);
        $('#info').html(response);
        return;
      }
      $('#addCategoryLink').show();
      $('#addOrEditCategory').hide();
	});
  }

  function showEditCategory(tr) {
    $('#addCategoryLink').hide();
    var children = $(tr).children('td');
    $('#category_name').val($(children[0]).text());
    $('#category_description').val($(children[1]).text());
    $('#category_eq_map').val($(children[2]).text());
    $('#category_group_name').val($(children[3]).text());
    $('#category_seat_quantity').val($(children[4]).text());

    $('#addOrEditCategoryTitle').html('Edit Zone');
    $('#addOrEditCategoryButton').val("Update");
    $('#addOrEditCategory').show();
    $('#addOrEditCategoryButton').click(editCategory);
    $('#addCategoryAliasButton').click(addAlias);
    $('#category_id').val(tr.id.substring(4));
    showAliases(tr.id.substring(4));
  }

  function showAddCategory() {
    $('#addCategoryLink').hide();
    $('#addOrEditCategoryTitle').html("Add Zone");
    $('#addOrEditCategoryButton').val("Add");
    $('#addOrEditCategory').show();
    $('#addOrEditCategoryButton').click(addCategory);
  }
  
  function sendUpdateRequest() {
  	CategoryDwr.categoryUpdateRequestToZone(${tour.id}, function (response) {
      if (response != 'OK') {
        $('#info').html(response);
        alert("Done sending request to zone");
        return;
      }
	});
  }

</script>