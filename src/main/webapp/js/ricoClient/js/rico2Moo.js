/**
  *  Copyright (c) 2009 Matt Brown
  *
  *  Licensed under the Apache License, Version 2.0 (the "License"); you may not use this
  *  file except in compliance with the License. You may obtain a copy of the License at
  *
  *         http://www.apache.org/licenses/LICENSE-2.0
  *
  *  Unless required by applicable law or agreed to in writing, software distributed under the
  *  License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
  *  either express or implied. See the License for the specific language governing permissions
  *  and limitations under the License.
  **/

if (typeof MooTools=='undefined') throw('This version of Rico requires the MooTools library');


Rico.Lib='MooTools';
Rico.LibVersion=MooTools.version;
Rico.extend=$extend;
Rico.tryFunctions = $try;
Rico.trim=function(s) { return s.trim(); };

Rico.select=function(selector, element) {
  return $(element || document).getElements(selector);
};
  
Rico.eventBind=function(element, eventName, handler) {
  $(element).addEvent(eventName, handler);
};

Rico.eventUnbind=function(element, eventName, handler) {
  $(element).removeEvent(eventName, handler);
};

Rico.eventHandle=function(object, method) {
  return object[method].bindWithEvent(object);
};

Rico.eventElement=function(ev) {
  return ev.target;
};

Rico.eventClient=function(ev) {
  return ev.client;
};

Rico.eventKey=function(ev) {
  return ev.code;
};

Rico.eventStop=function(ev) {
  ev.stop();
};

Rico.eventLeftClick=function(ev) {
  return !ev.rightClick;
};
  
Rico.addClass=function(element, className) {
  return $(element).addClass(className);
};

Rico.removeClass=function(element, className) {
  return $(element).removeClass(className);
};

Rico.hasClass=function(element, className) {
  return $(element).hasClass(className);
};
  
Rico.getStyle=function(element, property) {
  return $(element).getStyle(property);
};

Rico.setStyle=function(element, properties) {
  return $(element).setStyles(properties);
};

/**
 * @returns available height, excluding scrollbar & margin
 */
Rico.windowHeight=function() {
  return Window.getSize().y;
};

/**
 * @returns available width, excluding scrollbar & margin
 */
Rico.windowWidth=function() {
  return Window.getSize().x;
};

Rico._fixOffsets=function(o) {
  return {top: o.y, left: o.x};
}

Rico.positionedOffset=function(element) {
  var p, valueT = 0, valueL = 0;
  do {
    valueT += element.offsetTop  || 0;
    valueL += element.offsetLeft || 0;
    element = element.offsetParent;
    if (element) {
      p = $(element).getStyle('position');
      if (p == 'relative' || p == 'absolute') break;
    }
  } while (element);
  return {left: valueL, top: valueT};
};

Rico.cumulativeOffset=function(element) {
  return this._fixOffsets($(element).getPosition());
};

Rico.docScrollLeft=function() {
  return Window.getScroll().x;
};

Rico.docScrollTop=function() {
  return Window.getScroll().y;
};

Rico.getDirectChildrenByTag=function(element, tagName) {
  return $(element).getChildren(tagName);
};

Rico.ajaxRequest=function(url,options) {
  this.mooSend(url,options);
}

Rico.ajaxRequest.prototype = {
  mooSend : function(url,options) {
    this.onSuccess=options.onSuccess;
    this.onComplete=options.onComplete;
    var mooOptions = {
      onComplete : Rico.bind(this,'mooComplete'),
      onSuccess : Rico.bind(this,'mooSuccess'),
      onFailure : options.onFailure,
      method : options.method,
      data : options.parameters,
      url : url
    }
    this.mooRequest = new Request(mooOptions);
    this.mooRequest.send();
  },
  
  mooSuccess : function() {
    if (this.onSuccess) this.onSuccess(this.mooRequest.xhr);
  },
  
  mooComplete : function() {
    if (this.onComplete) this.onComplete(this.mooRequest.xhr);
  }
}

Rico.ajaxSubmit=function(form,url,options) {
  options.parameters=$(form).toQueryString();
  if (!options.method) options.method='post';
  url=url || form.action;
  new Rico.ajaxRequest(url,options);
}


// Animation

Rico.fadeIn=function(element,duration,onEnd) {
  var a = new Fx.Tween(element, {duration:duration, onComplete:onEnd});
  a.start('opacity', 1);
};

Rico.fadeOut=function(element,duration,onEnd) {
  var a = new Fx.Tween(element, {duration:duration, onComplete:onEnd});
  a.start('opacity', 0);
};

Rico.animate=function(element,options,properties) {
  options.onComplete=options.onEnd;
  var effect=new Fx.Morph(element,options);
  effect.start(properties);
  return effect;
};
