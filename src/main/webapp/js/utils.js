function formatFloat(n) {
	
	var sign = (n < 0)?"-":""
	n = parseInt(Math.abs(n * 100), 10);

	var part1 = parseInt(n / 100, 10);
	var part2 = n % 100;
	
	return sign + part1 + "." + ((part2 < 10)?"0" + part2:part2);
};

function __formatTwoFigures(n) {
	return (n < 10)?("0" + n):n;
};

function formatDate(d) {
	if (d.getHours() == 0 && d.getMinutes() == 0){ 
		return __formatTwoFigures(d.getMonth() + 1) + "/" + __formatTwoFigures(d.getDate()) + "/" + d.getFullYear() + " " + "TBD";
	}
	else
	{
		return __formatTwoFigures(d.getMonth() + 1) + "/" + __formatTwoFigures(d.getDate()) + "/" + d.getFullYear() + " " + __formatTwoFigures(d.getHours()) + ":" + __formatTwoFigures(d.getMinutes());
	}
};

function formatShortDate(d) {
	return __formatTwoFigures(d.getMonth() + 1) + "/" + __formatTwoFigures(d.getDate()) + "/" + d.getFullYear();
};

function formatTime(d, showSeconds) {
	if (d == null || d == "") {
		return "";
	}
	
	if (typeof(d) == 'number') {
		d = new Date(d);
	}
	
	if (showSeconds) {
		return __formatTwoFigures(d.getHours()) + ":" + __formatTwoFigures(d.getMinutes()) + ":" +  __formatTwoFigures(d.getSeconds());
	} else {
		return __formatTwoFigures(d.getHours()) + ":" + __formatTwoFigures(d.getMinutes());
	}
};
