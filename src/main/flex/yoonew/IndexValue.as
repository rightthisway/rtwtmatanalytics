package yoonew
{

	public class IndexValue {
	
		public var averagePrice:Number;
		public var medianPrice:Number;
		public var minPrice:Number;
		public var maxPrice:Number;
		public var deviation:Number;
		public var ticketCount:uint;
		public var minQuantity:uint;
		public var maxQuantity:uint;
		public var inTicketCount:uint;
		public var outTicketCount:uint;
		public var liquidityRatio:Number;

		public var timestamp:Date;
		
		public function IndexValue(timestamp:Date, averagePrice:Number, medianPrice:Number,
				minPrice:Number, maxPrice:Number, deviation:Number,
				ticketCount:uint, minQuantity:uint, maxQuantity:uint,
				inTicketCount:uint, outTicketCount:uint, liquidityRatio: Number) {
				
			this.timestamp = timestamp;
			this.averagePrice = averagePrice;
			this.medianPrice = medianPrice;
			this.minPrice = minPrice;
			this.maxPrice = maxPrice;
			this.deviation = deviation;
			this.ticketCount = ticketCount;
			this.minQuantity = minQuantity;
			this.maxQuantity = maxQuantity;
			this.inTicketCount = inTicketCount;
			this.outTicketCount = outTicketCount;
			this.liquidityRatio = liquidityRatio;
		}
		
		public function get price():Number {
			return this.averagePrice;
		}
	}		
}
