${submitter} has submitted the following error feedback:

- Submission date: ${submissionDate}
- Url: ${url}
- Referrer url: ${referrerUrl}
- Cookies: ${cookies}
- User Agent: ${userAgent}
- Client IP Address: ${clientIpAddress}
- Preferences:  #foreach( $preference in $preferences ) ${preference.name} = '${preference.value}',  #end

- Feedback Summary: ${errorFeedbackSummary}

- Error feedback Message:
${errorFeedbackMessage}

- Error stack trace:
${errorStackTrace}