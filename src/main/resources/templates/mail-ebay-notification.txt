You received this message because some of the tickets for eBay for an event you are
managing have been updated.

You can view the updated list at:
${staticUrl}/a1/EbayBrowseEvents?hideNoPending=on&manager=${username}
or
${staticUrl}/a1/EbayInventory?tourId=ALL&hideValidGroup=on&username=${username}

${message}
