package com.admitone.tmat.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
//import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.admitone.tmat.pojo.CrawlServerResponse;
//import com.admitone.tmat.pojo.WSZonesWrapper;
import com.admitone.tmat.utils.SpringUtil;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;
import com.admitone.tmat.common.SharedProperty;
import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.crawler.util.CrawlStatus;
import com.admitone.tmat.crawler.util.CrawlerWSUtil;
import com.admitone.tmat.dao.DAORegistry;
//import com.admitone.tmat.dao.DAORegistry;
//import com.admitone.tmat.data.MXPZonesTicket;
import com.thoughtworks.xstream.XStream;

public class CrawlerController extends MultiActionController {
//	private JMSMessageSender sender;
	private CrawlerWSUtil crawlerUtil; 
	private SharedProperty sharedProperty;
	
	public void forceCrawlerByEvent(HttpServletRequest request , HttpServletResponse response) throws Exception{
		ServletOutputStream out = response.getOutputStream();
		String eventId = request.getParameter("eventId");
		String url = sharedProperty.getBrowseUrl()+"WSForceEvents";		
//		HttpClient hc = new DefaultHttpClient();
		SimpleHttpClient hc = HttpClientStore.createHttpClient();
		HttpPost hp = new HttpPost(url);
		NameValuePair nameValuePair = new BasicNameValuePair("eventIds", eventId);
		NameValuePair nameValuePair1 = new BasicNameValuePair("postBackUrl", sharedProperty.getAnalyticsUrl()+"ForceCrawledEventResult");
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		parameters.add(nameValuePair);
		parameters.add(nameValuePair1);
		UrlEncodedFormEntity entity = new UrlEncodedFormEntity(parameters);
		hp.setEntity(entity);
		HttpResponse res = hc.execute(hp);
		HttpEntity result = res.getEntity();
		String content = EntityUtils.toString(result);
//		sender.sendMessageToBrowse("crawl-force-event-analytics", eventId);
		XStream xstream = new XStream();
		xstream.alias("CrawlResponseWrapper", CrawlServerResponse.class);
		xstream.processAnnotations(CrawlServerResponse.class);
		CrawlServerResponse csr = new CrawlServerResponse();
		xstream.fromXML(content,csr);
		
		Date leastDate =null;
		Collection<TicketListingCrawl> crawls = DAORegistry.getTicketListingCrawlDAO().getAllActiveTicketListingCrawlByEvent(Integer.parseInt(eventId.trim()));
		for (TicketListingCrawl crawl : crawls) {
			if (crawl.getEndCrawl() == null) {
				continue;
			}
			if (leastDate == null) {
				leastDate = crawl.getEndCrawl();
			} else if (crawl.getEndCrawl().before(leastDate)) {
				leastDate = crawl.getEndCrawl();
			}
		}
		Long timeInMillis = -1L;
		if (leastDate != null) {
			timeInMillis = leastDate.getTime();
		}
		String msg = "OK|" + csr.getNumberofcrawls() +"|0"+ "|" + timeInMillis;
		out.write(msg.toString().getBytes());
	}
	
	public ModelAndView forceCrawlersByIds(HttpServletRequest request , HttpServletResponse response) throws IOException{
//		ServletOutputStream out = response.getOutputStream();
		String crawlIds = request.getParameter("crawlIds");
		String eventId = request.getParameter("eventId");
		String url = sharedProperty.getBrowseUrl()+"WSForceCrawls";		
		HttpClient hc = new DefaultHttpClient();
		HttpPost hp = new HttpPost(url);
		NameValuePair nameValuePair = new BasicNameValuePair("crawlIds", crawlIds);
		NameValuePair eventIdNameValuePair = new BasicNameValuePair("eventId", eventId);
		NameValuePair postBackUrlNameValuePair = new BasicNameValuePair("postBackUrl", sharedProperty.getAnalyticsUrl()+"ForceCrawledEventResult");
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		parameters.add(nameValuePair);
		parameters.add(postBackUrlNameValuePair);
		parameters.add(eventIdNameValuePair);
		UrlEncodedFormEntity entity = new UrlEncodedFormEntity(parameters);
		hp.setEntity(entity);
		hc.execute(hp);
		/*
		HttpResponse res = hc.execute(hp);
		HttpEntity result = res.getEntity();
		
		String content = EntityUtils.toString(result);
//		sender.sendMessageToBrowse("crawl-force-event-analytics", eventId);
		XStream xstream = new XStream();
		xstream.alias("CrawlResponseWrapper", CrawlServerResponse.class);
		xstream.processAnnotations(CrawlServerResponse.class);
		CrawlServerResponse csr = new CrawlServerResponse();
		xstream.fromXML(content,csr);
		String msg = "OK|" + csr.getNumberofcrawls() +"|0";
		out.write(msg.toString().getBytes());
	 */		
		return new ModelAndView("http://tmatanalytics/a1/BrowseTicketsAnalytic?eventId="+ eventId +"&view=analytic");
	}
	public void forceCrawledEventResult(HttpServletRequest request , HttpServletResponse response) throws IOException{
		String res = request.getParameter("msg");
		String eventId = request.getParameter("eventId");
		String[] result=res.split("--");
		if(result.length>0){
			if(result[0].equals("OK")){
				crawlerUtil.addExecutedEvents(eventId, Integer.parseInt(result[1]), Integer.parseInt(result[2]));
			}
		}
	}
	public void getCrawlerStatus(HttpServletRequest request , HttpServletResponse response) throws IOException{
		String eventId = request.getParameter("eventId");
		Map<String,CrawlStatus> eventStatusMap = crawlerUtil.getCrawlStatusMap();
		CrawlStatus status = eventStatusMap.get(eventId);
		ServletOutputStream out = response.getOutputStream();
		Date now = new Date();
		String res="";
		if(status!=null){
			if(now.getTime() - status.getLastTimeCrawl().getTime()> 5*60*1000){
	//		if(res==null){
				res="OK|2|0";
			}else{
				int executedCrawl = status.getExecutedCrawl();
				int totalCrawl = status.getTotalCrawl();
				if(totalCrawl==0){
					res="OK|2|0"; 
				}else{
					res="OK|" + totalCrawl + "|" + executedCrawl;
				}
			}
		}else{
			res="OK|2|0";
		}
		out.write(res.toString().getBytes());
	}
	
	public void getCrawlsExecutedForEventsCount(HttpServletRequest request , HttpServletResponse response) throws IOException{
		String eventIds = request.getParameter("eventIds");
		ServletOutputStream out = response.getOutputStream();
		String res=":";
		Map<String,CrawlStatus> eventStatusMap = crawlerUtil.getCrawlStatusMap();
		Integer count=0;
		for(String eventId:eventIds.split(",")){
			if(eventId==null || eventId.isEmpty()){
				continue;
			}
			CrawlStatus status = eventStatusMap.get(eventId);
			Date now = new Date();
			if(status!=null){
				if(now.getTime() - status.getLastTimeCrawl().getTime()<= 120*60*1000){
					int executedCrawl = status.getExecutedCrawl();
//					int totalCrawl = status.getTotalCrawl();
					count=count+executedCrawl;
				}	
			}
		}
		res= count + ":" + eventIds;
		out.write(res.getBytes());
	}
	
	public void getCrawlerStatusForEvents(HttpServletRequest request , HttpServletResponse response) throws IOException{
		String eventIds = request.getParameter("eventIds");
		Map<String,CrawlStatus> eventStatusMap = crawlerUtil.getCrawlStatusMap();
		ServletOutputStream out = response.getOutputStream();
		Integer count=0;
		for(String eventId:eventIds.split(",")){
			if(eventId==null || eventId.isEmpty()){
				continue;
			}
			CrawlStatus status = eventStatusMap.get(eventId);
			Date now = new Date();
			if(status!=null){
				if(now.getTime() - status.getLastTimeCrawl().getTime()<= 5*60*1000){
					int executedCrawl = status.getExecutedCrawl();
//					int totalCrawl = status.getTotalCrawl();
					count=count+executedCrawl;
				}	
			}
		}
		out.write(count.toString().getBytes());
	}
	
	public Date getLeastRecentlyCrawledDateForTour(String eventIds) {
		Date finalCrawledDateForTour = null;
		String[] eventIdList = eventIds.split(",");
		finalCrawledDateForTour  =  SpringUtil.getTicketListingCrawler().getLeastCrawledDateForEvent(Integer.parseInt(eventIdList[0]));
		
		for(int i = 0; i < eventIdList.length; i++){
			Date currentDate  =  SpringUtil.getTicketListingCrawler().getLeastCrawledDateForEvent(Integer.parseInt(eventIdList[i]));
			if(finalCrawledDateForTour == null){
				finalCrawledDateForTour = currentDate;
			}
			if(currentDate != null){
				if(!(finalCrawledDateForTour.compareTo(currentDate)>0)){
	        		System.out.println(finalCrawledDateForTour.toString()+" is before "+currentDate.toString());
	        		finalCrawledDateForTour = currentDate;
				}
			}
		}
		return finalCrawledDateForTour;
	}	

	/*public JMSMessageSender getSender() {
		return sender;
	}

	public void setSender(JMSMessageSender sender) {
		this.sender = sender;
	}*/


	public CrawlerWSUtil getCrawlerUtil() {
		return crawlerUtil;
	}


	public void setCrawlerUtil(CrawlerWSUtil crawlerUtil) {
		this.crawlerUtil = crawlerUtil;
	}


	public SharedProperty getSharedProperty() {
		return sharedProperty;
	}


	public void setSharedProperty(SharedProperty sharedProperty) {
		this.sharedProperty = sharedProperty;
	}

}
