package com.admitone.tmat.web;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.ChildTourCategory;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.GrandChildTourCategory;

public class AjaxController extends MultiActionController{

	
	public void getChildCategory(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String tourCategoryId = request.getParameter("tourCategoryId");
		Collection<ChildTourCategory> cats = DAORegistry.getChildTourCategoryDAO().getChildTourCategoryByTourCategory(Integer.parseInt(tourCategoryId));
		OutputStream out =  response.getOutputStream();
		String res="";
		for(ChildTourCategory tourCategory:cats){
			res+=tourCategory.getId() + ":" + tourCategory.getName() + ",";
		}
		if(!res.isEmpty()){
			res= res.substring(0,res.length()-1);
			out.write(res.getBytes());
		}
	}
	
	public void getGrandChildCategory(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String childCategoryId = request.getParameter("childCategoryId");
		Collection<GrandChildTourCategory> cats = DAORegistry.getGrandChildTourCategoryDAO().getGrandChildTourCategoryByChildTourCategory(Integer.parseInt(childCategoryId));
		OutputStream out =  response.getOutputStream();
		String res="";
		for(GrandChildTourCategory tourCategory:cats){
			res+=tourCategory.getId() + ":" + tourCategory.getName() + ",";
		}
		if(!res.isEmpty()){
			res= res.substring(0,res.length()-1);
			out.write(res.getBytes());
		}
	}
	
	public void getEventsByTourId(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String artistId = request.getParameter("artistId");
		Collection<Event> events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(Integer.parseInt(artistId));
		OutputStream out =  response.getOutputStream();
		String res="";
		for(Event event:events){
			res+=event.getId() + ":" + event.getName() + ",";
		}
		if(!res.isEmpty()){
			res= res.substring(0,res.length()-1);
			out.write(res.getBytes());
		}
	}
}
