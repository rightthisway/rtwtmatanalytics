package com.admitone.tmat.web.filters;

import java.util.Date;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * Web filter that stores in the attribute "requestDate" the
 * timestamp at which the request went through the filter.
 * Define this filter in web.xml to be run first.
 * In the JSP file, one can simply get the date ${requestDate}
 * and compute the difference with the present time to
 * know how long it took to process the request 
 */
public class RequestTimeFilter implements Filter
{
    private FilterConfig filterConfig;

    public void doFilter(ServletRequest req, 
        ServletResponse res, FilterChain fc)
        throws java.io.IOException, javax.servlet.ServletException
    {
      req.setAttribute("requestDate", new Date());
      fc.doFilter(req,res); // invoke next item in the chain -- 
                            // either another filter or the
                            // originally requested resource. 
      // no post filtering on output
    }

    public FilterConfig getFilterConfig()
    {
      // Execute tasks
      return filterConfig;
    }

    public void setFilterConfig(FilterConfig cfg)
    {
      // Execute tasks 
      filterConfig = cfg;
    }

	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		
	}
}