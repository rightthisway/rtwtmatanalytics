package com.admitone.tmat.web.pojo;

import java.util.List;

public class EventCapacityStatusAndRisk {
	private Integer eventId;
	private String eventName;
	private String eventDate;
	private String eventTime;
	private String venueName;
	private String zone;
	private Integer zoneCapacity;
	private Integer zoneInHand;
	private Integer zoneShort;
	private Integer totalAvailable;
	private Integer inHandPercentage;
	private Integer shortsPercentage;
	private Integer totalRisk;
	private Integer totalRiskPercentage;
	private Integer totalNotInPlay;
	private Integer totalNotInPlayPercentage;
	private List<EventCapacityStatusAndRisk> children; 
	
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public String getEventDate() {
		return eventDate;
	}
	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}
	public String getEventTime() {
		return eventTime;
	}
	public void setEventTime(String eventTime) {
		this.eventTime = eventTime;
	}
	public String getVenueName() {
		return venueName;
	}
	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}
	public Integer getTotalAvailable() {
		return totalAvailable;
	}
	public void setTotalAvailable(Integer totalAvailable) {
		this.totalAvailable = totalAvailable;
	}
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	public Integer getZoneCapacity() {
		return zoneCapacity;
	}
	public void setZoneCapacity(Integer zoneCapacity) {
		this.zoneCapacity = zoneCapacity;
	}
	public Integer getZoneInHand() {
		return zoneInHand;
	}
	public void setZoneInHand(Integer zoneInHand) {
		this.zoneInHand = zoneInHand;
	}
	public Integer getZoneShort() {
		return zoneShort;
	}
	public void setZoneShort(Integer zoneShort) {
		this.zoneShort = zoneShort;
	}
	public Integer getInHandPercentage() {
		return inHandPercentage;
	}
	public void setInHandPercentage(Integer inHandPercentage) {
		this.inHandPercentage = inHandPercentage;
	}
	public Integer getShortsPercentage() {
		return shortsPercentage;
	}
	public void setShortsPercentage(Integer shortsPercentage) {
		this.shortsPercentage = shortsPercentage;
	}
	public Integer getTotalNotInPlay() {
		return totalNotInPlay;
	}
	public void setTotalNotInPlay(Integer totalNotInPlay) {
		this.totalNotInPlay = totalNotInPlay;
	}
	public List<EventCapacityStatusAndRisk> getChildren() {
		return children;
	}
	public void setChildren(List<EventCapacityStatusAndRisk> children) {
		this.children = children;
	}
	public Integer getTotalRisk() {
		return totalRisk;
	}
	public void setTotalRisk(Integer totalRisk) {
		this.totalRisk = totalRisk;
	}
	public Integer getTotalRiskPercentage() {
		return totalRiskPercentage;
	}
	public void setTotalRiskPercentage(Integer totalRiskPercentage) {
		this.totalRiskPercentage = totalRiskPercentage;
	}
	public Integer getTotalNotInPlayPercentage() {
		return totalNotInPlayPercentage;
	}
	public void setTotalNotInPlayPercentage(Integer totalNotInPlayPercentage) {
		this.totalNotInPlayPercentage = totalNotInPlayPercentage;
	}
	
}
