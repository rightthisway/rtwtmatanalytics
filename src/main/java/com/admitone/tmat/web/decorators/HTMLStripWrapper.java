package com.admitone.tmat.web.decorators;

import javax.servlet.jsp.PageContext;

import org.displaytag.decorator.DisplaytagColumnDecorator;
import org.displaytag.exception.DecoratorException;
import org.displaytag.properties.MediaTypeEnum;

public class HTMLStripWrapper implements DisplaytagColumnDecorator {

	public Object decorate(Object value, PageContext pageContext, MediaTypeEnum media)
			throws DecoratorException {
		// if it is HTML do nothing
		
		if (media.equals(MediaTypeEnum.HTML)) {
			return value;
		}
		
		if (value != null) {
			return value.toString().replaceAll("<[^>]+>", "").trim();
		}
		return null;
	}
}
