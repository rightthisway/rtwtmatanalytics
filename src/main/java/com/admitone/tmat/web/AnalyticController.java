package com.admitone.tmat.web;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.servlet.view.RedirectView;

import com.admitone.tmat.analytics.ValuationFactorComputation;
import com.admitone.tmat.analytics.ValuationFactorParser;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Artist;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.ValuationFactor;
import com.admitone.tmat.data.Venue;
import com.admitone.tmat.enums.EventStatus;
import com.admitone.tmat.utils.TextUtil;
import com.admitone.tmat.utils.ValuationFactorUtil;

public class AnalyticController extends MultiActionController {
	
	
	
	private ValuationFactorParser valuationFactorParser;
	//private CreationEventListManager creationEventListManager;
	
	
	
	//From DATA CONTROLLER
	
	public ModelAndView loadEditorValuationFactors(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ModelAndView mav = new ModelAndView("page-editor-valuation-factors");
		Collection<Artist> artists = DAORegistry.getArtistDAO().getAllActiveArtists();
		mav.addObject("artists", artists);
		
		String action = request.getParameter("action");		
		Integer artistId;
		String artistIdStr = request.getParameter("artistId");
		if (artistIdStr == null) {
			artistId = artists.iterator().next().getId();
		} else {
			artistId = Integer.valueOf(artistIdStr);
		}

		Integer eventId = null;
		String eventIdStr = request.getParameter("eventId");
		Collection<Event> events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(artistId);
		mav.addObject("events", events);
		
		if (eventIdStr == null) {
			if(!events.isEmpty()){
				eventId = events.iterator().next().getId();
			}else{
				eventId=0000;
			}
		} else {
			eventId = Integer.valueOf(eventIdStr);
		}
		
		if (action != null && action.equals("deleteAll")) {
			DAORegistry.getValuationFactorDAO().deleteAll(DAORegistry.getValuationFactorDAO().getAllValuationFactorsFromArtist(artistId));
			ValuationFactorUtil.update(artistId);
			return new ModelAndView(new RedirectView("EditorValuationFactors?artistId="
					+ artistId + "&eventId=" + eventId + "&info=Factors deleted", true));
		}
		
		mav.addObject("artistId", artistId);
		mav.addObject("eventId", eventId);
		mav.addObject("event", DAORegistry.getEventDAO().get(eventId));
		mav.addObject("valuationFactors", DAORegistry.getValuationFactorDAO().getAllValuationFactorsFromEvent(eventId));
		return mav;
	}
	
	
	
	public ModelAndView loadEditorUploadValuationFactors(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		Integer artistId = Integer.valueOf(request.getParameter("artistId"));
		Integer eventId = Integer.valueOf(request.getParameter("eventId"));
		if (request instanceof MultipartHttpServletRequest) {
			MultipartHttpServletRequest multi = (MultipartHttpServletRequest) request;
			try {
				parseValuationFactorFile(artistId, multi.getFile("file").getInputStream());
			} catch(Exception e) {
				return new ModelAndView(new RedirectView("EditorValuationFactors?artistId="
						+ artistId + "&eventId=" + eventId + "&error=" + e.getMessage(), true));
			}
			String info = "Valuation Factors uploaded sucessfully";
			return new ModelAndView(new RedirectView("EditorValuationFactors?artistId="
					+ artistId + "&eventId=" + eventId + "&info=" + info, true));
		}
		return new ModelAndView(new RedirectView("EditorValuationFactors?artistId="
				+ artistId + "&eventId=" + eventId, true));
	}
	
	
	
	
	public ModelAndView loadEditorUploadSuperValuationFactorsFinish(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		Integer artistId = Integer.valueOf(request.getParameter("artistId"));
		Integer eventId = Integer.valueOf(request.getParameter("eventId"));
		String valuationFactorsStr = request.getParameter("valuation_factors");
		
		/*Tour tour = DAORegistry.getTourDAO().get(tourId);*/
		Artist artist = DAORegistry.getArtistDAO().get(artistId);
		try {
			parseValuationFactorFile(artistId, new ByteArrayInputStream(valuationFactorsStr.getBytes()));
		} catch (Exception e) {
			ModelAndView mav = new ModelAndView("page-editor-valuation-factors-preview");
			mav.addObject("valuationFactorText", valuationFactorsStr);
			mav.addObject("artist", artist);
			mav.addObject("eventId", eventId);
			mav.addObject("error", e.getMessage());
			e.printStackTrace();
			return mav;
		}
		return new ModelAndView(new RedirectView("EditorValuationFactors?artistId="
				+ artistId + "&eventId=" + eventId + "&info=Valuation Factors uploaded", true));
	}
			
	public ModelAndView loadEditorUploadSuperValuationFactors(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		Integer artistId = Integer.valueOf(request.getParameter("artistId"));
		Integer eventId = Integer.valueOf(request.getParameter("eventId"));

		/*Tour tour = DAORegistry.getTourDAO().get(tourId);*/
		Artist artist = DAORegistry.getArtistDAO().get(artistId);
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		try {
			if (request instanceof MultipartHttpServletRequest) {
				
				MultipartHttpServletRequest multi = (MultipartHttpServletRequest) request;
				ValuationFactorComputation valuationFactorComputation = valuationFactorParser.parse(multi.getFile("file").getInputStream());
				Collection<ValuationFactorComputation.ValuationFactor> valuationFactors = valuationFactorComputation.computeValuationFactors();
				String valuationFactorText = "";
				for (ValuationFactorComputation.ValuationFactor factor: valuationFactors) {
					String dateStr = "";
					if (factor.getStartDate().equals(factor.getEndDate())) {
						dateStr = dateFormat.format(factor.getStartDate());
					} else {
						dateStr = dateFormat.format(factor.getStartDate()) + "-" + dateFormat.format(factor.getEndDate());
					}
					
					valuationFactorText += dateStr + ", " + factor.getVenue() + ", " + factor.getSection() + ", " + factor.getRow() + ", " + factor.getFactor() + "\n"; 
				}
				
				ModelAndView mav = new ModelAndView("page-editor-valuation-factors-preview");
				mav.addObject("valuationFactorText", valuationFactorText);
				mav.addObject("eventId", eventId);
				mav.addObject("artist", artist);
				return mav;
			}
		} catch (Exception e) {
			return new ModelAndView(new RedirectView("EditorValuationFactors?tourId="
					+ artistId + "&eventId=" + eventId + "&error=" + e.getMessage(), true));					
		}

		return new ModelAndView(new RedirectView("EditorValuationFactors?tourId="
				+ artistId + "&eventId=" + eventId, true));
	}

	

	
	private void parseValuationFactorFile(Integer artistId, InputStream inputStream) throws Exception {
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				inputStream));
		String line;
		int lineNum = 0;

		Collection<ValuationFactor> factors = new ArrayList<ValuationFactor>();
		while ((line = reader.readLine()) != null) {
			lineNum++;
			
			if (line.trim().length() == 0) {
				continue;
			}
			
			String[] tokens = line.split(",");
			
			if (tokens.length != 5) {
				String error = "Line:" + lineNum + ": " + line + ": Line is invalid ";
				throw new Exception(error);
			}
			
			String dateStr = TextUtil.trimQuotes(tokens[0]);
			String venueStr = TextUtil.trimQuotes(tokens[1]);
			String sections = TextUtil.trimQuotes(tokens[2]);
			String rows = TextUtil.trimQuotes(tokens[3]);
			String value = TextUtil.trimQuotes(tokens[4]);

			Date startDate = null;
			Date endDate = null;
			// parse dates
			if (dateStr.indexOf("-") > 0) {
				String[] dateTokens = dateStr.split("-");
				try {
					startDate = dateFormat.parse(dateTokens[0].trim());
					endDate = dateFormat.parse(dateTokens[1].trim());
				} catch (ParseException e) {
					e.printStackTrace();
				}
			} else {
				try {
					startDate = dateFormat.parse(dateStr);
					endDate = startDate;
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			
			Venue venue = DAORegistry.getVenueDAO().getVenue(venueStr);
			if (venue == null) {
				throw new Exception("No venue for '" + venueStr +"' found!");
			}
			
		 	Collection<Event> events = DAORegistry.getEventDAO().getAllEventsByArtistVenueAndDates(artistId, venue.getId(), startDate, endDate);
		 	for (Event event: events) {
				String startSection;
				String endSection;
				
				if (sections.indexOf("-") >= 0) {
					String[] sectionTokens = sections.split("-");
					startSection = sectionTokens[0];
					endSection = sectionTokens[1];
				} else {
					startSection = sections;
					endSection = sections;
				}

				String startRow;
				String endRow;
				
				if (rows.indexOf("-") >= 0) {
					String[] rowTokens = rows.split("-");
					startRow = rowTokens[0];
					endRow = rowTokens[1];
				} else {
					startRow = rows;
					endRow = rows;
				}

				ValuationFactor factor = new ValuationFactor(event.getId(), startSection, endSection,
											startRow, endRow, Integer.valueOf(value));
				factors.add(factor);
		 	}
		}
		
		// delete the old factors
		DAORegistry.getValuationFactorDAO().deleteAll(DAORegistry.getValuationFactorDAO().getAllValuationFactorsFromArtist(artistId));
		
		DAORegistry.getValuationFactorDAO().saveOrUpdateAll(factors);
		ValuationFactorUtil.update(artistId);
		
	}

	
	
	public ModelAndView loadDetailTourPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		int artistId = Integer.parseInt(request.getParameter("artistId"));
		String action = request.getParameter("action");
		if(action != null && !action.isEmpty()){
			if(action.equalsIgnoreCase("removeArtist")){
				/*Tour tour = DAORegistry.getTourDAO().get(tourId);
				DAORegistry.getTourDAO().deleteById(tourId);*/
				Artist artist = DAORegistry.getArtistDAO().get(artistId);
				DAORegistry.getArtistDAO().deleteById(artistId);
				String info ="Artist having ID:"+artistId+" has been Deleted.";
				return new ModelAndView(new RedirectView("EditorEditArtist?id="+artist.getId()+"&info="+info));
				
			}
		}
		
		ModelAndView mav = new ModelAndView("page-editor-detail-artist");
		mav.addObject("artist", DAORegistry.getArtistDAO().get(artistId));
		mav.addObject("events", DAORegistry.getEventDAO().getAllActiveEventsByArtistId(artistId));
		
		if (action != null && action.equals("remove")) {
			int eventId = Integer.parseInt(request.getParameter("id"));
			Event event = DAORegistry.getEventDAO().get(eventId);

			DAORegistry.getEventDAO().deleteById(eventId);
			
			String info = "The event " +event.getName()+" : "+ ((event.getLocalDate()== null)?"TBD":(event.getLocalDate()))
					+ " has been deleted!";
			return new ModelAndView(new RedirectView("EditorDetailArtist?artistId="
					+ artistId + "&info=" + info, true));
		}else if(action != null && action.equals("removeMultiple")){
			String tobeRemoved = request.getParameter("id");
			
			if(tobeRemoved != null && !tobeRemoved.isEmpty()){
				String[] removeList=tobeRemoved.split(",");
				for(String eventId:removeList){
					DAORegistry.getEventDAO().deleteById(Integer.parseInt(eventId));
				}
				String info = removeList.length+ " Events Has been Removed.";
				
				return new ModelAndView(new RedirectView("EditorDetailArtist?artistId="
						+ artistId + "&info=" + info, true));
			}
		}
			
		String info=request.getParameter("info");
		if(info!=null){
			mav.addObject("info", info);
		}
		
		return mav;
	}
	
	
	
	
	public ModelAndView loadManageToursPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		ModelAndView mav = new ModelAndView("page-editor-manage-tours");
		/*
		String filter = request.getParameter("filter");
		String action = request.getParameter("action");

		if (action != null && action.equals("remove")) {
			int tourId = Integer.parseInt(request.getParameter("id"));
			Tour tour = DAORegistry.getTourDAO().get(tourId);

			// Collection<Event> events =
			// DAORegistry.getEventDAO().getAllEventsByTour(tourId);
			// if (events != null && events.size() > 0) {
			// String info = "The tour " + tour.getName() +
			// " is used by some events(s)";
			// return new ModelAndView(new
			// RedirectView("EditorManageTours?info=" + info, true));
			// }
			
			DAORegistry.getTourDAO().deleteById(tourId);

			String info = "The tour " + tour.getName() + " has been deleted!";
			return new ModelAndView(new RedirectView("EditorManageTours?info="
					+ info, true));
		}

		Collection<Tour> tours;
		
		if (filter != null && !filter.trim().isEmpty()) {
			filter = filter.trim();
			tours = DAORegistry.getTourDAO().filterByName(filter);
		} else {
			tours = DAORegistry.getTourDAO().getAllActiveTours();
		}
		int tourCount=0;		
		int eventCount=0;
		for(Tour tour:tours){
			if(tour.getEventCount()>0){
				tourCount++;
			}
			for(Event event:tour.getEvents()){
				if(event.getCrawlCount()>0)
				eventCount++;	
			}
			
		}
		mav.addObject("tours", tours);
		mav.addObject("toursCount", tourCount);
		mav.addObject("eventsCount", eventCount);
*/
		return mav;
	}

	
	
	/*public ModelAndView loadEditorLinkAdmitone(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String firstLetter = request.getParameter("firstChar");
		String getFromDB = request.getParameter("getFromDB");
		String eventId = request.getParameter("eventId");
		if(firstLetter ==null || firstLetter.isEmpty()){
			firstLetter="a%";
		}
		boolean isGetFromDB=false;
		if(getFromDB!=null && getFromDB.equals("on")){
			isGetFromDB=true;
		}
		Event toBeChangedevent=null;
		if(eventId!=null && !eventId.isEmpty()){
			try{
				toBeChangedevent = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
			}catch (Exception e) {
				System.out.println(e.fillInStackTrace());
			}
		}
		Collection<Event> events =null;
		if(toBeChangedevent==null){
			events = DAORegistry.getEventDAO().getUnLinkedEmptyEvents(firstLetter);
		}else{
			events = new ArrayList<Event>();
			events.add(toBeChangedevent);
			isGetFromDB=true;
		}

		String eventId = request.getParameter("eventId");
		if(eventId != null && !eventId.isEmpty() ){
			events = new ArrayList<Event>();
			for(String eventIdStr: eventId.split(",")) {
				events.add(DAORegistry.getEventDAO().get(Integer.parseInt(eventIdStr)));
			}
		}
		
		Map<Integer, Collection<AdmitoneEvent>> eventListMap = null;
		
		if(events != null) {
			eventListMap = new HashMap<Integer,Collection<AdmitoneEvent>>();
			Collection<CreationEventHit> tnEvents = null;
			Map<String, Collection<AdmitoneEvent>> eventMaps= new HashMap<String, Collection<AdmitoneEvent>>();
			DateFormat df = new SimpleDateFormat("MM-dd-yyyy");
			
			if(isGetFromDB){
				Collection<AdmitoneEvent> admitoneEvents = DAORegistry.getAdmitoneEventDAO().getAll();
				Collection<AdmitoneEventLocal> admitoneEventsLocal = DAORegistry.getAdmitoneEventLocalDAO().getAll();
				for(AdmitoneEventLocal admitoneEventLocal: admitoneEventsLocal){
					AdmitoneEvent admitoneEvent = new AdmitoneEvent(admitoneEventLocal);
					admitoneEvents.add(admitoneEvent);
				}
				for(AdmitoneEvent admitoneEvent: admitoneEvents){
					Collection<AdmitoneEvent> list = eventMaps.get(df.format(admitoneEvent.getEventDate()));
					if(list==null){
						list = new ArrayList<AdmitoneEvent>();
					}
					list.add(admitoneEvent);
					eventMaps.put(df.format(admitoneEvent.getEventDate()), list);
				}
			}else{
				tnEvents = creationEventListManager.getTicketNetworkEventHits();
				for(CreationEventHit event: tnEvents){
					AdmitoneEvent admitoneEvent= new AdmitoneEvent();
					admitoneEvent.setEventDate(event.getDate());
					admitoneEvent.setEventId(event.getEventId());
					admitoneEvent.setEventName(event.getName());
					admitoneEvent.setEventTime(event.getTime());
					admitoneEvent.setVenueName(event.getBuilding());
					Collection<AdmitoneEvent> list = eventMaps.get(df.format(event.getDate()));
					if(list==null){
						list = new ArrayList<AdmitoneEvent>();
					}
					list.add(admitoneEvent);
					eventMaps.put(df.format(event.getDate()), list);
				}
			}
			
			for(Event event : events) {
				Collection<AdmitoneEvent> similarEvents =new HashSet<AdmitoneEvent>(); 
//				Collection<AdmitoneEvent> dbEvents = DAORegistry.getAdmitoneEventDAO().getAllEventsNearDate(event.getDate());
//				if(dbEvents!=null){
//					similarEvents.addAll(dbEvents);
//				}
				if(event.getLocalDate()==null){
					continue;
				}
				Collection<AdmitoneEvent> list = eventMaps.get(df.format(event.getLocalDate()));
				if(list==null){
					continue;
				}
				for (AdmitoneEvent similarEvent: list) {
					if(similarEvent.getEventName().equalsIgnoreCase(event.getName()) && similarEvent.getEventTime().equals(event.getLocalTime()) && event.getVenue().getBuilding().equalsIgnoreCase(similarEvent.getVenueName())){
						similarEvents.clear();
						similarEvents.add(similarEvent);
						break;
					}else{
						int score=QueryUtil.getMatchingScore(event.getName(), similarEvent.getEventName());
						if (score > 0) {
							similarEvents.add(similarEvent);
						}
					}
				}
				
				eventListMap.put(event.getId(), similarEvents);
			}
		}
		
		ModelAndView mav = new ModelAndView("page-editor-link-admitone");
		mav.addObject("events", events);
		mav.addObject("eventListMap", eventListMap);
		mav.addObject("firstChar", firstLetter);
		mav.addObject("getFromDB", isGetFromDB);
		return mav;
	}
*/
	
	

	//end data controller action.
	
	
	
	//Start Category COntroller Action.
	
	/*public ModelAndView loadEditorEditCategoriesPage(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Integer tourId = null;
		String tourIdStr = request.getParameter("tourId");
		String catScheme = request.getParameter("catScheme");
		String setGroupForZoneStr = request.getParameter("setGroupForZone");
		
		Event uniqueActiveEvent = null;
		Tour tour = null;
		if (tourIdStr != null) {
			tourId = Integer.valueOf(tourIdStr);
			tour = DAORegistry.getTourDAO().get(tourId);
			for(Event event : tour.getActiveEvents()){
				uniqueActiveEvent = event;
				break;
			}
		}
		
		if(tourIdStr != null && catScheme != null && catScheme.length() > 0 && setGroupForZoneStr != null && setGroupForZoneStr.toLowerCase().equals("true")){
			//Delete all the ZonesEventCategoryMapping for the tour
			Collection<ZonesEventCategoryMapping> zecmList = new ArrayList<ZonesEventCategoryMapping>();
			for(Event event : tour.getActiveEvents()){
				ZonesEventCategoryMapping zecm = DAORegistry.getZoneEventCategoryMappingDAO().getZonesEventCategoryMappingByEvent(event.getId());
				if(zecm == null){
					zecm = new ZonesEventCategoryMapping();
				}
				zecm.setEventId(event.getId());
				zecm.setGroupName(catScheme);
				zecmList.add(zecm);
				VenueCategory venueCategory = DAORegistry.getVenueCategoryDAO().getVenueCategoryByVenueAndCategoryGroup(event.getVenueId(), catScheme);
				Collection<CategoryMapping> categoryMappingList = null;
				if(venueCategory != null){
					categoryMappingList = DAORegistry.getCategoryMappingDAO().getAllCategoryMappingsByVenueCategoryId(venueCategory.getId());
				}
				for(CategoryMapping cm : categoryMappingList){
					cm.setUseForZoneMap(Boolean.TRUE);
				}
				DAORegistry.getCategoryMappingDAO().saveOrUpdateAll(categoryMappingList);
			}
			DAORegistry.getZoneEventCategoryMappingDAO().saveOrUpdateAll(zecmList);
		}
		String zoneCatGroup = "";
		if(uniqueActiveEvent != null){
			ZonesEventCategoryMapping zoneEventCategoryMapping = DAORegistry.getZoneEventCategoryMappingDAO().getZonesEventCategoryMappingByEvent(uniqueActiveEvent.getId());
			if(zoneEventCategoryMapping != null){
				zoneCatGroup = zoneEventCategoryMapping.getGroupName();
			}
		}else{
			zoneCatGroup = null;
		}
		Collection<Event> eventList = DAORegistry.getEventDAO().getAllEventsByTour(tourId, EventStatus.ACTIVE);
		Collection<ZonesEventCategoryMapping> zecmList = new ArrayList<ZonesEventCategoryMapping>();
		String zoneEventCategory = request.getParameter("zoneEventCategory");
		System.out.println("*******zoneEventCategory*****"+zoneEventCategory);
		if(zoneEventCategory != null)
		{
			Map<Integer, VenueCategory> venueCategoryMappingMap = new HashMap<Integer, VenueCategory>();
			for(Event event:eventList)
			{
				String eventCategory = request.getParameter("category_"+ event.getId());
//				System.out.println("*******category_"+ event.getId()+"****"+eventCategory);
				if(eventCategory !=null && !eventCategory.equals("-1"))
				{
					ZonesEventCategoryMapping zecm = DAORegistry.getZoneEventCategoryMappingDAO().getZonesEventCategoryMappingByEvent(event.getId());
					if(zecm == null){
						zecm = new ZonesEventCategoryMapping();
					}
					zecm.setEventId(event.getId());
					zecm.setGroupName(eventCategory);
					zecmList.add(zecm);
					event.setZoneCategoryGroupName(eventCategory);
					
					VenueCategory venueCategory = venueCategoryMappingMap.get(event.getVenueId());
					if(venueCategory == null){
						venueCategory = DAORegistry.getVenueCategoryDAO().getVenueCategoryByVenueAndCategoryGroup(event.getVenueId(), catScheme);
					}

					Collection<CategoryMapping> categoryMappingList = DAORegistry.getCategoryMappingDAO().getAllCategoryMappingsByVenueCategoryId(venueCategory.getId());
					for(CategoryMapping cm : categoryMappingList){
						cm.setUseForZoneMap(Boolean.TRUE);
					}
					DAORegistry.getCategoryMappingDAO().saveOrUpdateAll(categoryMappingList);
				}
			}
			DAORegistry.getZoneEventCategoryMappingDAO().saveOrUpdateAll(zecmList);
		}
		
		
		Collection<String> catGroups = DAORegistry.getCategoryDAO().get(tourId);

		if(catScheme == null || catScheme.isEmpty()){
			if (catGroups.iterator().hasNext()) {
				catScheme = catGroups.iterator().next();
			} else {
				catScheme = "";
			}
		}
			
		for(Event event:eventList )
		{
			ZonesEventCategoryMapping zoneEventCategoryMapping = DAORegistry.getZoneEventCategoryMappingDAO().getZonesEventCategoryMappingByEvent(event.getId());
			if(zoneEventCategoryMapping != null)
			{
				event.setZoneCategoryGroupName(zoneEventCategoryMapping.getGroupName());
			}
		}
		ModelAndView mav = new ModelAndView("page-editor-edit-categories");
		mav.addObject("catGroupName", catScheme);
		mav.addObject("zoneCatGroup",zoneCatGroup);
		mav.addObject("tour", DAORegistry.getTourDAO().get(tourId));
		mav.addObject("tourId", tourIdStr);
		mav.addObject("tours", DAORegistry.getTourDAO().getAll());
		mav.addObject("events", eventList);
		mav.addObject("catGroups", catGroups);
		return mav;	
	}
	
	
	
	public ModelAndView loadEditorEditEventCategoriesPage(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Integer eventId = null;
		String eventIdStr = request.getParameter("eventId");
		if (eventIdStr != null) {
			eventId = Integer.valueOf(eventIdStr);
		}

		Event event = DAORegistry.getEventDAO().get(eventId);
//		Collection<String> catGroups = Categorizer.getCategoryGroupsByEvent(eventId);
		Collection<String> catGroups = DAORegistry.getCategoryDAO().getAllCategoryGroupsByTour(event.getTourId());
		
		String catScheme = request.getParameter("catScheme");
		if(catScheme == null || catScheme.isEmpty()){
			if (catGroups.iterator().hasNext()) {
				catScheme = catGroups.iterator().next();
			} else {
				catScheme = "";
			}
		}
		
		ModelAndView mav = new ModelAndView("page-editor-edit-event-categories");
		mav.addObject("event", event);
		mav.addObject("eventId", eventIdStr);
		mav.addObject("catGroupName", catScheme);
		mav.addObject("categories", DAORegistry.getCategoryDAO()
				.getAllCategories(event.getTourId(), catScheme));
		mav.addObject("catGroups", catGroups);
		// mav.addObject("categoryMappings",
		// DAORegistry.getCategoryMappingDAO().getAllCategoryMappings(eventId));
		return mav;
	}
	*/
	

	//End Category COntroller Action.

	public ValuationFactorParser getValuationFactorParser() {
		return valuationFactorParser;
	}

	public void setValuationFactorParser(ValuationFactorParser valuationFactorParser) {
		this.valuationFactorParser = valuationFactorParser;
	}

	/*public CreationEventListManager getCreationEventListManager() {
		return creationEventListManager;
	}

	public void setCreationEventListManager(
			CreationEventListManager creationEventListManager) {
		this.creationEventListManager = creationEventListManager;
	}*/
}
