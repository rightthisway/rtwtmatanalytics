package com.admitone.tmat.web;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.acegisecurity.ui.webapp.AuthenticationProcessingFilter;
import org.apache.commons.io.IOUtils;
import org.apache.velocity.tools.generic.DateTool;
import org.apache.velocity.tools.generic.NumberTool;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.servlet.view.RedirectView;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.AdmitoneEvent;
import com.admitone.tmat.data.AdmitoneEventLocal;
import com.admitone.tmat.data.AdmitoneTicketMark;
import com.admitone.tmat.data.Artist;
import com.admitone.tmat.data.Category;
import com.admitone.tmat.data.CategoryMapping;
import com.admitone.tmat.data.DefaultPurchasePrice;
import com.admitone.tmat.data.DuplicateTicketMap;
import com.admitone.tmat.data.EbayInventoryGroup;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.ManagePurchasePrice;
import com.admitone.tmat.data.ManageTourPrice;
import com.admitone.tmat.data.MarketMakerEventTracking;
import com.admitone.tmat.data.Preference;
import com.admitone.tmat.data.Quote;
import com.admitone.tmat.data.QuoteCustomer;
import com.admitone.tmat.data.Stat;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.data.TicketQuote;
import com.admitone.tmat.data.User;
import com.admitone.tmat.data.Venue;
import com.admitone.tmat.data.VenueCategory;
import com.admitone.tmat.enums.AdmitoneTicketMarkType;
import com.admitone.tmat.enums.BookmarkType;
import com.admitone.tmat.enums.EventStatus;
import com.admitone.tmat.enums.RemoveDuplicatePolicy;
import com.admitone.tmat.enums.TicketStatus;
import com.admitone.tmat.pojo.ActionIndicator;
import com.admitone.tmat.utils.ActionIndicatorUtil;
import com.admitone.tmat.utils.Categorizer;
import com.admitone.tmat.utils.PreferenceManager;
import com.admitone.tmat.utils.StatHelper;
import com.admitone.tmat.utils.TicketQuotesUtil;
import com.admitone.tmat.utils.TicketSorter;
import com.admitone.tmat.utils.TicketUtil;
import com.admitone.tmat.utils.mail.MailAttachment;
import com.admitone.tmat.utils.mail.MailManager;
import com.admitone.tmat.web.pojo.QuotesEvent;
import com.admitone.tmat.web.pojo.WebTicketRow;


/**
 * Class that handle the views for pages that can be accessed with the USER role.
 */
public class RetailController extends  MultiActionController {
	private PreferenceManager preferenceManager;
//	private TicketListingCrawler ticketListingCrawler;
	private MailManager mailManager;
	/*private static Map<Integer, Map<String, ManageTourPrice>> manageTourPriceMap= new HashMap<Integer, Map<String,ManageTourPrice>>();
	private static Map<Integer, Long> manageTourPriceTimeMap= new HashMap<Integer, Long>();*/
	private static Map<Integer, Map<String, ManagePurchasePrice>> managePurchasePriceMap= new HashMap<Integer, Map<String,ManagePurchasePrice>>();
	private static Map<Integer, Long> managePurchasePriceTimeMap= new HashMap<Integer, Long>();
	private static int UPDATE_FREQUENCY =15*60*1000;
	
	public ModelAndView loadMarketMakerEventTrackingUpdatePage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {		
		String creator = (String)request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);
		String[] eventIds = request.getParameter("eventId").split(",");
		String marketMakerName = request.getParameter("marketMaker");
		
		for (String eventIdStr: eventIds) {
			if (eventIdStr.trim().isEmpty()) {
				continue;
			}
			Integer eventId = Integer.valueOf(eventIdStr.trim());
			MarketMakerEventTracking oldMarketMakerEventTracking = DAORegistry.getMarketMakerEventTrackingDAO().getCurrentMarketMakerEventTracking(eventId);
			
			if (oldMarketMakerEventTracking != null) {
				if (oldMarketMakerEventTracking.getMarketMakerUsername().equals(marketMakerName)) {
					continue;
				}
				oldMarketMakerEventTracking.setEnabled(false);
				oldMarketMakerEventTracking.setEndDate(new Date());
				DAORegistry.getMarketMakerEventTrackingDAO().update(oldMarketMakerEventTracking);
			}
			
			if (!marketMakerName.equals("DESK")) {
				MarketMakerEventTracking marketMakerEventTracking = new MarketMakerEventTracking(creator,
						marketMakerName, eventId);
				DAORegistry.getMarketMakerEventTrackingDAO().save(marketMakerEventTracking);
			}
		}
		
		if (!request.getHeader("Referer").isEmpty()) {
			return new ModelAndView(new RedirectView(request.getHeader("Referer")));
		}
		
		return null;
	}
		
	/**
	 * Browse tour page
	 */
	/*
	public ModelAndView loadBrowseToursPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		String username = (String)request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);
		String artistId = request.getParameter("artistId");
		String filter = request.getParameter("filter");
		
		Collection<Tour> tours;

		String view = request.getParameter("view");
		if (view != null && !view.isEmpty()) {
			if (!view.equals("quick")) {
				view = null;
			}
			
			preferenceManager.updatePreference(username, "browseToursView", view);
		} else {
			view = preferenceManager.getPreferenceValue(username, "browseToursView");
		}

		ModelAndView mav;
		
		if (view == null || view.length() == 0) {
			mav = new ModelAndView("page-browse-tours");						
			mav.addObject("view", "normal");
		} else {
			mav = new ModelAndView("page-browse-tours-quick");
			mav.addObject("view", "quick");
		}
		
		if (artistId == null || artistId.isEmpty()) {
			if (filter != null && !filter.trim().isEmpty()) {
				tours = DAORegistry.getTourDAO().filterByName(filter);				
			} else {
				tours = DAORegistry.getTourDAO().getAll();
			}
			
		} else {
			mav.addObject("artistBookmark", DAORegistry.getBookmarkDAO().getBookmark(username, BookmarkType.ARTIST, Integer.valueOf(artistId)));
			if (filter != null && !filter.trim().isEmpty()) {
				tours = DAORegistry.getTourDAO().filterToursByArtistByName(Integer.parseInt(artistId), filter);														
			} else {
				tours = DAORegistry.getTourDAO().getAllToursByArtist(Integer.parseInt(artistId));										
			}
			//mav.addObject("artist", DAORegistry.getArtistDAO().get(Integer.parseInt(artistId)));
			mav.addObject("priceAdjustments", DAORegistry.getArtistPriceAdjustmentDAO().getPriceAdjustments(Integer.parseInt(artistId)));
		}
		
		mav.addObject("artists", DAORegistry.getArtistDAO().getAllActiveArtists());
		mav.addObject("tours", tours);
		mav.addObject("siteById", DAORegistry.getSiteDAO().getSiteByIdMap());
		mav.addObject("tourBookmarks", DAORegistry.getBookmarkDAO().getAllTourBookmarks(username));
		return mav;
	}
	*/
	/**
	 * Browse artist page.
	 * does not seems to be used anymore.
	 */
	@Deprecated
	public ModelAndView loadBrowseArtistsPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String username = (String)request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);
		Collection<Artist> artists;
		
		String filter = request.getParameter("filter");
		
		if (filter != null && !filter.trim().isEmpty()) {
			artists = DAORegistry.getArtistDAO().filterByName(filter, EventStatus.ACTIVE);
			
		} else {
			artists = DAORegistry.getArtistDAO().getAllActiveArtists();
		}
		ModelAndView mav = new ModelAndView("page-browse-artists");
		mav.addObject("artists", artists);
		mav.addObject("artistBookmarks", DAORegistry.getBookmarkDAO().getAllArtistBookmarks(username));
		mav.addObject("user", DAORegistry.getUserDAO().getUserByUsername(username));
		return mav;
	}
	
	private void updateLastBrowsedEvent(String username, Integer eventId) {
		Preference lastBrowsedEvents = DAORegistry.getPreferenceDAO().getPreference(username, "lastBrowsedEventIds");
		if (lastBrowsedEvents == null) {
			lastBrowsedEvents = new Preference(username, "lastBrowsedEventIds", "" + eventId);
			DAORegistry.getPreferenceDAO().save(lastBrowsedEvents);
		} else {
			int j = 1;
			String[] eventIds = lastBrowsedEvents.getValue().split(",");
			String s = "" + eventId;
			for(int i = 0 ; i < eventIds.length && j < 10; i++) {
				if (Integer.parseInt(eventIds[i]) != eventId) {
					s += "," + eventIds[i];				
					j++;
				}
			}
			lastBrowsedEvents.setValue(s);
			DAORegistry.getPreferenceDAO().update(lastBrowsedEvents);
		}
	}

	/**
	 * Browse event page.
	 */
	public ModelAndView loadBrowseEventsPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String username = (String)request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);
		
		String catString = "";
		Integer cntOfCats = 0;
		String catList = "";
		
		String artistId = request.getParameter("artistId");
		String venueId = request.getParameter("venueId");
		
		ModelAndView mav = new ModelAndView("page-browse-events");
		//Tour tour = null;
		Artist artist = null;
		Venue venue = null;
		Collection<Event> events;
		Map<Integer, Venue> eventToVenue = null;
		if (artistId != null && !artistId.isEmpty()) {
			mav.addObject("artistBookmark", DAORegistry.getBookmarkDAO().getBookmark(username, BookmarkType.ARTIST, Integer.valueOf(artistId)));
			artist = DAORegistry.getArtistDAO().get(Integer.parseInt(artistId));
			events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(Integer.parseInt(artistId));						
			//artist = tour.getArtist();
			eventToVenue = new HashMap<Integer, Venue>();
			for(Event event : events){
				eventToVenue.put(event.getId(), event.getVenue());
			}
			mav.addObject("priceAdjustments", DAORegistry.getArtistPriceAdjustmentDAO().getPriceAdjustments(artist.getId()));		
//			Date start = new Date();
			
//			Set<String> catSet = new TreeSet<String>();
			// removed temporarily by chirag
			/*for(Category category : tour.getCategories()){
				catSet.add(category.getGroupName().trim());
				cntOfCats = cntOfCats + 1;
				catList = catList + category.getSymbol()+ ": " + category.getDescription() + "<br/>";
			}
			for(String s : catSet){
				catString = catString + s;
			}*/
			
		}else if(venueId != null && !venueId.isEmpty()){
			mav.addObject("venueBookmark",DAORegistry.getBookmarkDAO().getBookmark(username, BookmarkType.VENUE, Integer.valueOf(venueId)));
			events = DAORegistry.getEventDAO().getAllEventsByVenueAndStatus(Integer.parseInt(venueId), EventStatus.ACTIVE);
			venue = DAORegistry.getVenueDAO().get(Integer.parseInt(venueId));
			eventToVenue = new HashMap<Integer, Venue>();
			for(Event event : events){
				eventToVenue.put(event.getId(), event.getVenue());
			}
		}else{
			events = DAORegistry.getEventDAO().getAllEventsByStatus(EventStatus.ACTIVE);
		}
		
		//mav.addObject("artist", artist);
		mav.addObject("venue", venue);
		mav.addObject("marketMakers", DAORegistry.getUserDAO().getAll());
		mav.addObject("artist", artist);
		mav.addObject("eventToVenue", eventToVenue);
		mav.addObject("siteById", DAORegistry.getSiteDAO().getSiteByIdMap());
		mav.addObject("events", events);
		mav.addObject("browseEventsPage", "BrowseEvents");
		mav.addObject("eventBookmarks", DAORegistry.getBookmarkDAO().getAllEventBookmarks(username));
//		mav.addObject("cats",catString);  // Removed by chirag to see use of this field
//		mav.addObject("cntOfCats",cntOfCats);
//		mav.addObject("catList",catList);
		return mav;
	}

	/**
	 * Browse event page.
	 */
	public ModelAndView loadBrowseEventsAnalyticPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String username = (String)request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);
		String artistId = request.getParameter("artistId");
		String venueId = request.getParameter("venueId");
		
		ModelAndView mav = new ModelAndView("page-browse-events");
		Artist artist= null;
		Venue venue=null;
		Collection<Event> events;
		Map<Integer, Venue> eventToVenue = null;
		if (artistId != null && !artistId.isEmpty()) {
			mav.addObject("tourBookmark", DAORegistry.getBookmarkDAO().getBookmark(username, BookmarkType.TOUR, Integer.valueOf(artistId)));
			artist = DAORegistry.getArtistDAO().get(Integer.parseInt(artistId));
			events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(Integer.parseInt(artistId));
			eventToVenue = new HashMap<Integer, Venue>();
			for(Event event : events){
				eventToVenue.put(event.getId(), event.getVenue());
			}
			mav.addObject("priceAdjustments", DAORegistry.getArtistPriceAdjustmentDAO().getPriceAdjustments(artist.getId()));		
			Date start = new Date();
			Map<Integer, Map<String, Integer>> eventToActions = ActionIndicatorUtil.getActionsByTour(events);
			Date end = new Date();
			
			mav.addObject("eventToActions", eventToActions);			
		}else if(venueId != null && !venueId.isEmpty()){
			mav.addObject("venueBookmark",DAORegistry.getBookmarkDAO().getBookmark(username, BookmarkType.VENUE, Integer.valueOf(venueId)));
			events = DAORegistry.getEventDAO().getAllEventsByVenueAndStatus(Integer.parseInt(venueId), EventStatus.ACTIVE);
			venue = DAORegistry.getVenueDAO().get(Integer.parseInt(venueId));
			Map<Integer, Map<String, Integer>> eventToActions = ActionIndicatorUtil.getActionsByTour(events);
			eventToVenue = new HashMap<Integer, Venue>();
			for(Event event : events){
				eventToVenue.put(event.getId(), event.getVenue());
			}
			mav.addObject("eventToActions", eventToActions);
		}else {	
			events = DAORegistry.getEventDAO().getAllEventsByStatus(EventStatus.ACTIVE);
		}
		
		mav.addObject("venue", venue);
		//mav.addObject("tour", tour);
		mav.addObject("artist", artist);
		mav.addObject("eventToVenue", eventToVenue);
		mav.addObject("siteById", DAORegistry.getSiteDAO().getSiteByIdMap());
		mav.addObject("events", events);
		mav.addObject("browseEventsPage", "BrowseEventsAnalytic");
		mav.addObject("eventBookmarks", DAORegistry.getBookmarkDAO().getAllEventBookmarks(username));
		return mav;
	}	
	
	public ModelAndView loadBrowseTickets(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		ModelAndView mav = new ModelAndView("page-sub-browse-ticket");
		String artistIdStr = request.getParameter("artistId");
		/*String tourIdStr = request.getParameter("tourId");*/
		String eventIdStr = request.getParameter("eventId");
		//Collection<Tour> tour;
		Collection<Event> event=null;
		Event browseEvent=null;
		
		
		if(artistIdStr != null && !artistIdStr.isEmpty()){
			int artistId = Integer.parseInt(artistIdStr);
			//tour = DAORegistry.getTourDAO().getAllToursByArtist(artistId);
			//if(!tour.isEmpty()){
				//Tour firstTour = tour.iterator().next();
				event = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(artistId);
				if(event.isEmpty()){
					mav.addObject("eventInfo","No Event found for selected Tour.");
				}
				if(event.size() == 1){
					browseEvent = event.iterator().next();
					return new ModelAndView(new RedirectView("BrowseTicketsAnalytic?eventId="+browseEvent.getId()+
					"&view=analytic"));
				}
			//}
			//mav.addObject("tourList",tour);
			mav.addObject("eventList",event);
			mav.addObject("selectedArtist",artistIdStr);
		
		}/*else if(tourIdStr != null && !tourIdStr.isEmpty()){
			int tourId = Integer.parseInt(tourIdStr);
			tour = DAORegistry.getTourDAO().getAllActiveTours();
			event = DAORegistry.getEventDAO().getAllEventsByTour(tourId,EventStatus.ACTIVE);
			if(event.isEmpty()){
				mav.addObject("eventInfo","No Event found for selected Tour.");
			}
			
			Tour browseTour = DAORegistry.getTourDAO().get(tourId);
			
			if(event.size() == 1){
				browseEvent = event.iterator().next();
				return new ModelAndView(new RedirectView("BrowseTicketsAnalytic?eventId="+browseEvent.getId()+
				"&view=analytic"));
			}
			
			mav.addObject("eventList",event);
			mav.addObject("tourList",tour);
			mav.addObject("selectedTour",tourIdStr);
			mav.addObject("selectedArtist",browseTour.getArtistId());
			
		}*/else if(eventIdStr != null && !eventIdStr.isEmpty()){
			//browseEvent = DAORegistry.getEventDAO().get(Integer.parseInt(eventIdStr)); 
			return new ModelAndView(new RedirectView("BrowseTicketsAnalytic?eventId="+eventIdStr+
					"&view=analytic"));
		}
		/*else{
			mav.addObject("tourList",DAORegistry.getTourDAO().getAllActiveTours());
			
		}*/
		
		
		mav.addObject("artistList",DAORegistry.getArtistDAO().getAllActiveArtists());
		return mav;
	}
	

	/**
	 * Browse ticket page.
	 */
	public ModelAndView loadBrowseTicketsPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		Boolean isAnalyticView = request.getRequestURL().toString().contains("BrowseTicketsAnalytic");
		String eventIdStr = request.getParameter("eventId");
//		String artistIdStr = request.getParameter("artistId");
//		String tourIdStr = request.getParameter("tourId");
		String dateRange1 = request.getParameter("fromDate");
		String dateRange2 = request.getParameter("toDate");
		
		int eventId = Integer.parseInt(eventIdStr);
		
 		String view = request.getParameter("view");
		Boolean reset = "1".equals(request.getParameter("reset"));
		String username = (String)request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);	
		
		if (view == null) {
			if(reset) {
				view = "analytic";
			} 
			/*else if (isAnalyticView) {
				view = preferenceManager.getPreferenceValue(username, "browseTicketView", "analytic");
			} */else {				
				preferenceManager.updatePreference(username,  "browseTicketView", "analytic");
				view = preferenceManager.getPreferenceValue(username, "browseTicketView","analytic");				
			}
		}
		
		if ((view.equals("compact") || view.equals("expanded")) && isAnalyticView) {
			return new ModelAndView(new RedirectView("BrowseTickets?eventId=" + eventId + "&view=" + view));
		}

		if (view.equals("analytic") && !isAnalyticView) {
			return new ModelAndView(new RedirectView("BrowseTicketsAnalytic?eventId=" + eventId + "&view=" + view));
		}
	

		Map<String, Long> timeStats = new HashMap<String, Long>();
		
		
		
		Boolean noCrawl="true".equals(request.getParameter("nocrawl"));
		String reloadTemp=request.getParameter("reload");
		Long reload=0l;
		if(reloadTemp!=null && !"".equals(reloadTemp)){
			reload= Long.parseLong(reloadTemp);	
		}
		
			
		//int zoneEventId = Integer.parseInt(request.getParameter("zoneEventId"));
		
		Event event = DAORegistry.getEventDAO().get(eventId);
		/*List<Integer> artistIds = new ArrayList<Integer>();
		artistIds.add(event.getArtistId());*/
		Integer artistId = event.getArtistId();
		List<ManagePurchasePrice> manageTourPrices = new ArrayList<ManagePurchasePrice>();
		
		manageTourPrices = DAORegistry.getManagePurchasePriceDAO().getAllByManagePurchasePriceByArtistId(artistId);
		

		preferenceManager.updatePreference(username, "browseTicketView", view);
		
		Long handlePreferencesStartTime = System.currentTimeMillis();			
		
		timeStats.put("handlePreferences", System.currentTimeMillis() - handlePreferencesStartTime);		
		
		
		Map<String, Object> map = handlePreferences(request);
		/*updateLastBrowsedEvent(username, eventId);*/

		/*
		 * Ticket filter
		 */

		
		
		
		boolean multisort = (Boolean)map.get("multisort");
		//boolean zoneMultisort = (Boolean)map.get("zoneMultisort");
		
		//Tickets Filter
		String categorySymbol = (String)map.get("categorySymbol");	
		String categorySymbols = (String)map.get("categorySymbols");
	
		List<Integer> categoryIds = (List<Integer>)map.get("categoryIds");
		Map<String, Boolean> catFilters = (Map<String, Boolean>)map.get("catFilters");
		String catScheme = (String)map.get("catScheme");
		String zoneCatScheme = (String)map.get("zoneCatScheme");
		
		//Liquidity Filter
		String liquidityCategorySymbol = (String)map.get("liquidityCategorySymbol");
		String liquidityCategorySymbols = (String)map.get("liquidityCategorySymbols");		
		List<Integer> liquidityCategoryIds = (List<Integer>)map.get("liquidityCategoryIds");
		Map<String, Boolean> liquidityCatFilters = (Map<String, Boolean>)map.get("liquidityCatFilters");
		String liquidityCatScheme = (String)map.get("liquidityCatScheme");
		//History Filter
		String historyCategorySymbol = (String)map.get("historyCategorySymbol");
		String historyCategorySymbols = (String)map.get("historyCategorySymbols");		
		List<Integer> historyCategoryIds = (List<Integer>)map.get("historyCategoryIds");
		Map<String, Boolean> historyCatFilters = (Map<String, Boolean>)map.get("historyCatFilters");
		String historyCatScheme = (String)map.get("historyCatScheme");
		//Stats Filter
		String statsCategorySymbol = (String)map.get("statsCategorySymbol");
		String statsCategorySymbols = (String)map.get("statsCategorySymbols");		
		List<Integer> statsCategoryIds = (List<Integer>)map.get("statsCategoryIds");
		Map<String, Boolean> statsCatFilters = (Map<String, Boolean>)map.get("statsCatFilters");
		String statsCatScheme = (String)map.get("statsCatScheme");
		List<String> catSchemes = Categorizer.getCategoryGroupsByVenueId(event.getVenueId());
		String defaultScheme = "";
		if(catSchemes != null && !catSchemes.isEmpty()){
			defaultScheme = catSchemes.get(0);
		}
		if((catScheme == null || catScheme.isEmpty()) && event.getVenueCategory()!=null){
//			catScheme = Categorizer.getCategoryGroupsByEvent(eventId).get(0);  // TODO CAT
			catScheme = event.getVenueCategory().getCategoryGroup();
		}
		
		/*else{
			catScheme = defaultScheme;
		}*/
		if((liquidityCatScheme == null || liquidityCatScheme.isEmpty()) && event.getVenueCategory()!=null){
//			liquidityCatScheme = Categorizer.getCategoryGroupsByEvent(eventId).get(0); TODO CAT
			liquidityCatScheme = event.getVenueCategory().getCategoryGroup();
		}else{
			liquidityCatScheme = defaultScheme;
		}
		if((historyCatScheme == null || historyCatScheme.isEmpty()) && event.getVenueCategory()!=null){
//			historyCatScheme = Categorizer.getCategoryGroupsByEvent(eventId).get(0); TODO CAT
			historyCatScheme = event.getVenueCategory().getCategoryGroup();
		}else{
			historyCatScheme = defaultScheme;
		}
		if((statsCatScheme == null || statsCatScheme.isEmpty())&& event.getVenueCategory()!=null){
//			statsCatScheme = Categorizer.getCategoryGroupsByEvent(eventId).get(0); TODO CAT
			statsCatScheme = event.getVenueCategory().getCategoryGroup();
		}else{
			statsCatScheme = defaultScheme;
		}
		
		
		
		String section = (String)map.get("section");
		String row = (String)map.get("row");
		Double startPrice = (Double)map.get("startPrice");
		Double endPrice = (Double)map.get("endPrice");
		Double zoneStartPrice = (Double)map.get("zoneStartPrice");
		Double zoneEndPrice = (Double)map.get("zoneEndPrice");
		
		Map<String, Boolean> siteFilters = (Map<String, Boolean>)map.get("siteFilters");
		ArrayList<Integer> quantities = (ArrayList<Integer>)map.get("quantities");
		ArrayList<Integer> liquidityQunatities = (ArrayList<Integer>)map.get("liquidityQuantities");
		ArrayList<Integer> historyQuantities = (ArrayList<Integer>)map.get("historyQuantities");
		ArrayList<Integer> statsQunatities = (ArrayList<Integer>)map.get("statsQuantities");		
		RemoveDuplicatePolicy removeDuplicatePolicy = (RemoveDuplicatePolicy)map.get("removeDuplicatePolicy");
		int[] multisortFields = (int[])map.get("multisortFields");
		Map<String, Boolean> qtyFilters = (Map<String, Boolean>)map.get("qtyFilters");
		Map<String, Boolean> zoneQtyFilters = (Map<String, Boolean>)map.get("zoneQtyFilters");
		Map<String, Boolean> liquidityQtyFilters = (Map<String, Boolean>)map.get("liquidityQtyFilters");
		Map<String, Boolean> historyQtyFilters = (Map<String, Boolean>)map.get("historyQtyFilters");
		Map<String, Boolean> statsQtyFilters = (Map<String, Boolean>)map.get("statsQtyFilters");		
		String show = (String)map.get("show");
		boolean showDuplicates = (Boolean) map.get("showDuplicates");
		Integer numTicketRows = (Integer) map.get("numTicketRows");
		String priceType = (String) map.get("priceType");
								
		Long startTimeMarkDuplicate = System.currentTimeMillis();
		handleMarkDuplicates(request, eventId);
		timeStats.put("handleMarkDuplicates", System.currentTimeMillis() - startTimeMarkDuplicate);		

		
		// Chart logic
		String tabName= request.getParameter("selectedTab");
		Collection<Category> catsChart = null;
		if((catScheme==null || catScheme.isEmpty()) && event.getVenueCategory()!=null){
			catsChart = DAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(event.getVenueCategory().getId());
		}else{
			catsChart = DAORegistry.getCategoryDAO().getAllCategoriesByVenueIdAndCategoryGroup(event.getVenueId(), catScheme);
		}
		List<Integer> categoryIdChart = new ArrayList<Integer>();
		for(Category category:catsChart){
			categoryIdChart.add(category.getId());
		}
		
		ArrayList<Integer> quantityChart = new ArrayList<Integer>();
		for(int i=1; i<=11; i++){
			quantityChart.add(i);
		}
		
		
		List<Ticket> allTicketsChart = new ArrayList<Ticket>();	
		List<Ticket> origChartTickets = new ArrayList<Ticket>();
		List<Ticket> filteredChartTickets = new ArrayList<Ticket>();
		List<Ticket> filteredHChartTickets = new ArrayList<Ticket>();
		//TicketUtil.getFilteredTicketsForChart(eventId, categoryIdChart, quantityChart,allTicketsChart,catScheme);
		/*****Changed by CS*******/
		
		Collection<Ticket> tickets = new ArrayList<Ticket>();
		
		Long loadCategoriesStartTime = System.currentTimeMillis();
//		Event event = DAORegistry.getEventDAO().get(eventId);
//		Collection<Category> categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenueIdAndCategoryGroup(event.getVenueId(),catScheme);
		Collection<Category> categories = null;
		if((catScheme==null || catScheme.isEmpty()) && event.getVenueCategory()!=null){
			categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(event.getVenueCategory().getId());
		}else{
			categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenueIdAndCategoryGroup(event.getVenueId(), catScheme);
		}
		VenueCategory venueCategory = DAORegistry.getVenueCategoryDAO().getVenueCategoryByVenueAndCategoryGroup(event.getVenueId(), catScheme);
		Collection<CategoryMapping> categoryMappingList = DAORegistry.getCategoryMappingDAO().getAllCategoryMappingsByVenueCategoryId(venueCategory.getId());
		Map<Integer,Category> catMap = new HashMap<Integer, Category>();
		Map<Integer,List<CategoryMapping>> catMappingMap = new HashMap<Integer, List<CategoryMapping>>();
		for(Category cat:categories){
			catMap.put(cat.getId(), cat);
		}
		
		for(CategoryMapping mapping:categoryMappingList){
			List<CategoryMapping> list =catMappingMap.get(mapping.getCategoryId());
			if(list==null){
				list = new ArrayList<CategoryMapping>();
			}
			list.add(mapping);
			catMappingMap.put(mapping.getCategoryId(), list);
		}
		timeStats.put("loadCategories", System.currentTimeMillis() - loadCategoriesStartTime);
		
		Long loadTicketsStartTime = System.currentTimeMillis();
		if ("completed".equals(show)) {
			// show completed  ticket					
			tickets = DAORegistry.getTicketDAO().getAllCompletedTicketsByEvent(eventId);
		} else {
			// show active ticket
			tickets = DAORegistry.getTicketDAO().getAllActiveTicketsByEvent(eventId);
		}

		timeStats.put("loadTickets", System.currentTimeMillis() - loadTicketsStartTime);

		Long preAssignCategoriesStartTime = System.currentTimeMillis();		
		TicketUtil.preAssignCategoriesToTickets(tickets, categories); 
		timeStats.put("preAssignCategories", System.currentTimeMillis() - preAssignCategoriesStartTime);
		
		/************************/
		
		Collection<Ticket> hTickets = DAORegistry.getHistoricalTicketDAO().getAllTicketsByEvent(eventId);
		Collection<Ticket> tempHTickets = DAORegistry.getTicketDAO().getAllCompletedTicketsByEvent(eventId);
		if(tempHTickets != null) {
			hTickets.addAll(tempHTickets);
		}

		TicketUtil.preAssignCategoriesToTickets(hTickets, categories);
		
		List<DefaultPurchasePrice> defaultTourPrices = DAORegistry.getDefaultPurchasePriceDAO().getAllDefaultTourPrice();
		Map<String, DefaultPurchasePrice> defaultPurchasePriceMap = new HashMap<String, DefaultPurchasePrice>();
		if(defaultTourPrices!=null){
			for(DefaultPurchasePrice defaultTourPrice:defaultTourPrices){
				String key = defaultTourPrice.getExchange() + "-" + (defaultTourPrice.getTicketType()==null?"REGULAR":defaultTourPrice.getTicketType());
				defaultPurchasePriceMap.put(key, defaultTourPrice);
			}
		}
		
		Map<String, ManagePurchasePrice> purchasePriceMap = managePurchasePriceMap.get(event.getArtistId());
		Long time = managePurchasePriceTimeMap.get(event.getArtistId());
		if(purchasePriceMap==null){
			purchasePriceMap= new HashMap<String, ManagePurchasePrice>();
		}
		Long now =new Date().getTime();
		if(time==null || now-time>UPDATE_FREQUENCY){
			List<ManagePurchasePrice> list = DAORegistry.getManagePurchasePriceDAO().getAllByManagePurchasePriceByArtistId(event.getArtistId());
			for(ManagePurchasePrice purchasePrice:list){
				String mapKey = purchasePrice.getExchange()+ "-" + (purchasePrice.getTicketType());
				purchasePriceMap.put(mapKey,purchasePrice);
			}
			managePurchasePriceMap.put(event.getArtistId(),purchasePriceMap);
			managePurchasePriceTimeMap.put(event.getArtistId(),now);
		}
		
		if(!catsChart.isEmpty()){
			TicketUtil.getFilteredTickets(eventId, categoryIdChart, quantityChart, siteFilters, section, row, //show, 
					origChartTickets, filteredChartTickets, filteredHChartTickets,tickets,hTickets, categories, catScheme, true, startPrice, endPrice, priceType, removeDuplicatePolicy, timeStats,null,venueCategory,defaultPurchasePriceMap,purchasePriceMap,null);
		}
		if(filteredChartTickets != null && !filteredChartTickets.isEmpty()){
			allTicketsChart.addAll(filteredChartTickets);
		}
		if(filteredHChartTickets != null && !filteredHChartTickets.isEmpty()){
			allTicketsChart.addAll(filteredHChartTickets);
		}
		Map<String,ArrayList<Ticket>> categoryWiseTicket=new HashMap<String, ArrayList<Ticket>>();
		ArrayList<Ticket> listTicket=null;
		String catSchemeName = null;
		if(!catsChart.isEmpty()){
			catSchemeName = TicketUtil.getCatScheme(categoryIdChart);
		}else{
			catSchemeName="";
		}
		String key=null;
		Date fromDate=event.getCreationDate();		
		Date toDate=new Date();
		Calendar cal=new GregorianCalendar();
		cal.setTime(fromDate);
		Calendar toCal=new GregorianCalendar();
		toCal.setTime(toDate);
		
		Map<Date,Map<String,Double>> mapChartData=new HashMap<Date, Map<String,Double>>();
		Map<Date,Map<String,Integer>> mapQtyChartData=new HashMap<Date, Map<String,Integer>>();
		Calendar recentUpdateDate = new GregorianCalendar();
		recentUpdateDate.setTime(event.getCreationDate());
		Calendar ticketLastUpdate = new GregorianCalendar();
		ticketLastUpdate.setTime(event.getCreationDate());
		for(Category category:catsChart){
			for(int i=1; i<=11; i++){
				listTicket=new ArrayList<Ticket>();
				
				key=category.getSymbol().replaceAll("\\s++","_")+"_"+i;				
				for(Ticket origTicket:allTicketsChart){
					ticketLastUpdate.setTime(origTicket.getLastUpdate());
					if(compareDates(recentUpdateDate, ticketLastUpdate)<0){
						recentUpdateDate.setTime(origTicket.getLastUpdate());
					}
					Category cat = origTicket.getCategory();
					if(i==11){
						if(cat!= null && cat.getId().equals(category.getId()) && (origTicket.getQuantity().compareTo(i)>=0)){
							listTicket.add(origTicket);		
							
						}
					}else{
						if(cat!= null && cat.getId().equals(category.getId()) && origTicket.getQuantity().equals(i)){
							listTicket.add(origTicket);		
							
						}
					}
					
				}
				categoryWiseTicket.put(key, listTicket);				
			}			
		}
		
		ArrayList<Ticket> listTicketChart=new ArrayList<Ticket>();
		Collection<Ticket> dayTickets = new ArrayList<Ticket>();
		Calendar dayCalendarChart = new GregorianCalendar();
		
		for(Category category:catsChart){
			for(int i=1; i<=11; i++){
				listTicketChart=categoryWiseTicket.get(category.getSymbol().replaceAll("\\s++", "_")+"_"+i);
				cal.setTime(fromDate);
				for(Date d = fromDate; compareDates(cal,toCal) < 0; ){
					dayCalendarChart.setTime(d);
					dayTickets.clear();
					for (Ticket ticket: listTicketChart) {
						Calendar insertionCalendar = new GregorianCalendar();
						insertionCalendar.setTime(ticket.getInsertionDate());
						Calendar updateCalendar = new GregorianCalendar();
						updateCalendar.setTime(ticket.getLastUpdate());
						if (!ticket.getTicketStatus().equals(TicketStatus.ACTIVE) ||  (compareDates(insertionCalendar, dayCalendarChart) <= 0 && compareDates(dayCalendarChart, updateCalendar) <= 0)
								|| (compareDates(dayCalendarChart, recentUpdateDate)>0)) {
							
								 dayTickets.add(ticket);					
							}

					}
					Stat stat = StatHelper.getStat(dayTickets, dayCalendarChart.getTime(),false,null);
					if(stat != null){
						Map<String, Double> innerMap =  mapChartData.get(d);
						  Map<String, Integer> innerQtyMap= mapQtyChartData.get(d);
						  String keyName = category.getSymbol().replaceAll("\\s++", "_")+"_"+i;
						  if(innerMap == null){
							     innerMap = new HashMap<String, Double>();							     
							     innerMap.put(keyName,stat.getMinPrice());
							     mapChartData.put(d, innerMap);
						  }else{
							  	innerMap.put(keyName,stat.getMinPrice());
							  	mapChartData.put(d, innerMap);
						  }
						  if(innerQtyMap == null){
								  innerQtyMap = new HashMap<String, Integer>();
								  innerQtyMap.put(keyName, stat.getEndTicketCount());
								  mapQtyChartData.put(d, innerQtyMap);
						  }else{							  
								 	innerQtyMap.put(keyName,stat.getEndTicketCount());
								 	mapQtyChartData.put(d, innerQtyMap);
							  
							  
						  }				     
					}else{
						Map<String, Double> innerMap =  mapChartData.get(d);
						  Map<String, Integer> innerQtyMap= mapQtyChartData.get(d);
						  String keyName = category.getSymbol().replaceAll("\\s++", "_")+"_"+i;
						  if(innerMap == null){
							     innerMap = new HashMap<String, Double>();							     
							     innerMap.put(keyName,0D);
							     mapChartData.put(d, innerMap);
						  }else{
							  	innerMap.put(keyName,0D);
							  	mapChartData.put(d, innerMap);
						  }
						  if(innerQtyMap == null){
								  innerQtyMap = new HashMap<String, Integer>();
								  innerQtyMap.put(keyName, 0);
								  mapQtyChartData.put(d, innerQtyMap);
						  }else{							  
								 	innerQtyMap.put(keyName,0);
								 	mapQtyChartData.put(d, innerQtyMap);
							  
							  
						  }	
					}
					
					cal.setTime(d);
					cal.add(Calendar.DATE, 1);
					d=cal.getTime();
					
				}
			}
				
		}
		 
		Map<String,String> categoryPriceMap=new HashMap<String, String>();
		Map<String,Double> innerCategoryMap=new HashMap<String, Double>();
		Map<String,String> categoryQtyMap=new HashMap<String, String>();
		Map<String,Integer> innerCategoryQtyMap=new HashMap<String, Integer>();
		String strPrice=null;
		String strQty=null;
		cal.setTime(fromDate);
		
		List<Date> dateList = new ArrayList<Date>();
		for(Date date=fromDate; compareDates(cal, toCal)< 0;){
			dateList.add(date);
			cal.setTime(date);
			cal.add(Calendar.DATE, 1);
			date=cal.getTime();
			
		}
		Date latestDate = Collections.max(dateList);
		String priceQuantyStr ="";
		String maxQuantyStr ="";
		int catSize = catsChart.size();
		int k=1;
		Map<String, Integer> finalPriceDtlMap = new HashMap<String, Integer>();
		Map<String, Integer> finalMaxQuantityDtlMap = new HashMap<String, Integer>();
		
		for(Category category:catsChart){
			List<Double> minPriceList = new ArrayList<Double>();
			List<Integer> MaxQuantityList = new ArrayList<Integer>();
			Map<Integer, Integer> quantityMap = new HashMap<Integer, Integer>();
			Map<Double, Integer> priceMap = new HashMap<Double, Integer>();
			Double cheapestPrice  =null;
			Integer highestQuantity=null;
			innerCategoryMap=mapChartData.get(latestDate);
			innerCategoryQtyMap=mapQtyChartData.get(latestDate);
			String categorySymbolNew = category.getSymbol().replaceAll("\\s++", "_");
			
			for(int i=2; i<=11; i++){
				String strKey=categorySymbolNew+"_"+i;
				Double minPrice=innerCategoryMap.get(strKey);
				Integer maxQuantity = innerCategoryQtyMap.get(strKey);
				
				if(minPrice != 0.0){
					minPriceList.add(minPrice);
					priceMap.put(minPrice,i);
				}
				MaxQuantityList.add(maxQuantity);
				quantityMap.put(maxQuantity, i);
			}
			if(null == minPriceList || minPriceList.isEmpty()){
				minPriceList.add(0.0);
				priceMap.put(0.0,2);
			}
			cheapestPrice  = Collections.min(minPriceList);
			highestQuantity  = Collections.max(MaxQuantityList);
			strPrice = null;
			strQty = null;
			int priceQuant = priceMap.get(cheapestPrice);
			int highQuant = quantityMap.get(highestQuantity);
			String strPriceKey=categorySymbolNew+"_"+priceQuant;
			String strQuantityKey=categorySymbolNew+"_"+highQuant;
			finalPriceDtlMap.put(categorySymbolNew, priceQuant);
			finalMaxQuantityDtlMap.put(categorySymbolNew, highQuant);
			if(catSize == k){
				priceQuantyStr += priceQuant;
				maxQuantyStr += highQuant;
			}else{
				priceQuantyStr += priceQuant+",";
				maxQuantyStr += highQuant+",";
			}
			cal.setTime(fromDate);
			for(Date date=fromDate; compareDates(cal, toCal)< 0;){
				
				innerCategoryMap=mapChartData.get(date);
				innerCategoryQtyMap=mapQtyChartData.get(date);
				if(strPrice==null){
					strPrice=new String();
					strPrice=String.valueOf(innerCategoryMap.get(strPriceKey));
				}else{					
					strPrice=strPrice+","+String.valueOf(innerCategoryMap.get(strPriceKey));
				}
				if(strQty==null){
					strQty=new String();
					strQty=String.valueOf(innerCategoryQtyMap.get(strQuantityKey));
				}else{
					
					strQty=strQty+","+String.valueOf(innerCategoryQtyMap.get(strQuantityKey));
				}
				cal.setTime(date);
				cal.add(Calendar.DATE, 1);
				date=cal.getTime();
			}
			categoryPriceMap.put(strPriceKey, strPrice);
			categoryQtyMap.put(strQuantityKey, strQty);
			k++;
		}
		
		
		
		/*for(Category category:catsChart){
			for(int i=1; i<=11; i++){
				String strKey=category.getSymbol().replaceAll("\\s++", "_")+"_"+i;
				strPrice=null;
				strQty=null;
				cal.setTime(fromDate);
				for(Date d=fromDate; compareDates(cal, toCal)< 0;){
					innerCategoryMap=mapChartData.get(d);
					innerCategoryQtyMap=mapQtyChartData.get(d);
					if(strPrice==null){
						strPrice=new String();
						strPrice=String.valueOf(innerCategoryMap.get(strKey));
					}else{					
						strPrice=strPrice+","+String.valueOf(innerCategoryMap.get(strKey));
					}
					if(strQty==null){
						strQty=new String();
						strQty=String.valueOf(innerCategoryQtyMap.get(strKey));
					}else{
						
						strQty=strQty+","+String.valueOf(innerCategoryQtyMap.get(strKey));
					}
					cal.setTime(d);
					cal.add(Calendar.DATE, 1);
					d=cal.getTime();
				}
				categoryPriceMap.put(strKey, strPrice);
				categoryQtyMap.put(strKey, strQty);
			}
		}*/
		String chartStartDate=new String();
		chartStartDate="20"+String.valueOf(fromDate.getYear()-100);
		chartStartDate=chartStartDate+","+String.valueOf(fromDate.getMonth());
		chartStartDate=chartStartDate+","+String.valueOf(fromDate.getDate());
		
		//End of chart logic
		
		
		List<Ticket> filteredTickets = new ArrayList<Ticket>();
		List<Ticket> filteredTicktesForHistoryTab = new ArrayList<Ticket>();
		List<Ticket> filteredTicketsForStatsTab = new ArrayList<Ticket>();
		List<Ticket> filteredHistoricalTickets;
		List<Ticket> filteredHistoricalTicktesForHistoryTab;
		List<Ticket> filteredHistoricalTicketsForStatsTab;
		if (isAnalyticView) {
			filteredHistoricalTickets = new ArrayList<Ticket>();
		} else {
			filteredHistoricalTickets = null;
		}
		if(isAnalyticView){
			filteredHistoricalTicktesForHistoryTab = new ArrayList<Ticket>();
		}else{
			filteredHistoricalTicktesForHistoryTab = null;
		}
		if(isAnalyticView){
			filteredHistoricalTicketsForStatsTab = new ArrayList<Ticket>();
		}else{
			filteredHistoricalTicketsForStatsTab = null;
		}
		// lotSize, quantity, category, section, row, currentPrice, buyItNowPrice, title, seller, itemId, endTime, insertionDate, lastUpdate, status
		ModelAndView mav = new ModelAndView("page-browse-tickets");
		AdmitoneEventLocal admitoneEventLocal = null;
		if(event.getAdmitoneId()!=null){
			AdmitoneEvent admitoneEvent = DAORegistry.getAdmitoneEventDAO().getEventByEventId(event.getAdmitoneId());
			if(admitoneEvent!=null){
				mav.addObject("admitoneEventName", admitoneEvent.getEventName());
				mav.addObject("admitoneEventDate", admitoneEvent.getFormatedDate());
				
			}else{
				//String[] time = event.getLocalTime().toString().split(":");
				//Date date = new  Date(1900,01,01,Integer.valueOf(time[0]),Integer.valueOf(time[1]),time[2]);
				
				admitoneEventLocal =DAORegistry.getAdmitoneEventLocalDAO().getEventByVenueCityStateDateTime(event.getVenue().getBuilding(),event.getVenue().getCity(),event.getVenue().getState(), event.getLocalDate(), event.getLocalTime()); 
				if(admitoneEventLocal != null){
					mav.addObject("admitoneEventName", admitoneEventLocal.getEventName());
					mav.addObject("admitoneEventDate", admitoneEventLocal.getFormatedDate());
				} else {
					mav.addObject("admitoneEventName", "");
					mav.addObject("admitoneEventDate", null);
				
				}
			}
		}else{
			admitoneEventLocal =DAORegistry.getAdmitoneEventLocalDAO().getEventByVenueCityStateDateTime(event.getVenue().getBuilding(),event.getVenue().getCity(),event.getVenue().getState(), event.getLocalDate(), event.getLocalTime()); 
			if(admitoneEventLocal != null){
				mav.addObject("admitoneEventName", admitoneEventLocal.getEventName());
				mav.addObject("admitoneEventDate", admitoneEventLocal.getFormatedDate());
			} else {
				mav.addObject("admitoneEventName", "");
				mav.addObject("admitoneEventDate", null);
		
			}
		}
		Long filteredTicketsStartTime = System.currentTimeMillis();
		List<Ticket> origTickets = new ArrayList<Ticket>();
		List<Ticket> origTicktesForHistoryTab = new ArrayList<Ticket>();
		List<Ticket> origTicketsForStatsTab = new ArrayList<Ticket>();

		TicketUtil.getFilteredTickets(eventId, categoryIds, quantities, siteFilters, section, row,// show, 
				origTickets, filteredTickets, filteredHistoricalTickets,tickets,hTickets,categories, catScheme, true, startPrice, endPrice, priceType, removeDuplicatePolicy, timeStats,admitoneEventLocal,venueCategory,defaultPurchasePriceMap,purchasePriceMap,null);
		if(historyCategoryIds!=null){
			TicketUtil.getFilteredTickets(eventId, historyCategoryIds, historyQuantities, siteFilters, section, row,// show, 
				origTicktesForHistoryTab, filteredTicktesForHistoryTab, filteredHistoricalTicktesForHistoryTab,tickets,hTickets,categories, historyCatScheme, true, startPrice, endPrice, priceType, removeDuplicatePolicy, timeStats,admitoneEventLocal,venueCategory,defaultPurchasePriceMap,purchasePriceMap,null);
		}
		if(statsCategoryIds!=null){
			TicketUtil.getFilteredTickets(eventId, statsCategoryIds, statsQunatities, siteFilters, section, row,// show, 
				origTicketsForStatsTab, filteredTicketsForStatsTab, filteredHistoricalTicketsForStatsTab, tickets,hTickets,categories,statsCatScheme, true, startPrice, endPrice, priceType, removeDuplicatePolicy, timeStats,admitoneEventLocal,venueCategory,defaultPurchasePriceMap,purchasePriceMap,null);
		}
		timeStats.put("filteredTickets", System.currentTimeMillis() - filteredTicketsStartTime);		
		
		Integer availableSeats = getAvailableTicketSeats(venueCategory, categoryIds);
			
		// filters
		mav.addObject("tabName",tabName);
		mav.addObject("chartStartDate",chartStartDate);
		mav.addObject("chartData",categoryPriceMap);
		mav.addObject("chartQtyData", categoryQtyMap);
		mav.addObject("finalPriceDtlMap", finalPriceDtlMap);
		mav.addObject("finalMaxQuantityDtlMap", finalMaxQuantityDtlMap);
		mav.addObject("tabName",tabName);
		mav.addObject("siteFilters", siteFilters);
		mav.addObject("qtyFilters", qtyFilters);
		mav.addObject("zoneQtyFilters", zoneQtyFilters);
		mav.addObject("liquidityQtyFilters", liquidityQtyFilters);
		mav.addObject("historyQtyFilters", historyQtyFilters);
		mav.addObject("statsQtyFilters", statsQtyFilters);		
		mav.addObject("manageTourPrices", manageTourPrices);
		mav.addObject("noCrawl",noCrawl);
		mav.addObject("reload",reload);
		mav.addObject("fromDate",dateRange1);
		mav.addObject("toDate",dateRange2);
		
		String browseTicketFilter = preferenceManager.getPreferenceValue(username, "browseTicketFilter", "0");
		mav.addObject("browseTicketFilter", browseTicketFilter);
		
		if (!removeDuplicatePolicy.equals(RemoveDuplicatePolicy.NONE)) {
			mav.addObject("duplicateTicketMap", DAORegistry.getDuplicateTicketMapDAO().getDuplicateTicketMap(eventId));
		}

		int removedDuplicateCount = 0;
//		Double tempPrice = 0.00;
		List<WebTicketRow> newFilteredWebTicketRows = TicketUtil.getWebTicketRows(origTickets, filteredTickets, showDuplicates);
		/*for(WebTicketRow wTicketRow:newFilteredWebTicketRows){
			tempPrice = wTicketRow.getBuyItNowPrice();
			
			Map<String, ManageTourPrice> tourPriceMap = manageTourPriceMap.get(event.getTourId());
			Long time = manageTourPriceTimeMap.get(event.getTourId());
			if(tourPriceMap==null){
				tourPriceMap= new HashMap<String, ManageTourPrice>();
			}
			String mapKey = wTicketRow.getSiteId()+ "-" + (wTicketRow.getTicketDeliveryType()==null?"REGULAR":wTicketRow.getTicketDeliveryType());
			Long now =new Date().getTime();
			if(tourPriceMap.get(mapKey)==null || now-time>UPDATE_FREQUENCY){
				List<ManageTourPrice> list = DAORegistry.getManageTourPriceDAO().getAllByManageTourPriceByTourId(event.getTourId());
				for(ManageTourPrice tourPrice:list){
					tourPriceMap.put(tourPrice.getExchange()+"-" + tourPrice.getTicketType(),tourPrice);
				}
				manageTourPriceMap.put(event.getTourId(),tourPriceMap);
				manageTourPriceTimeMap.put(event.getTourId(),now);
			}
			ManageTourPrice manageTourPrice=tourPriceMap.get(mapKey);
			String priceByQuantity="Qty : Price&#13;";
			DecimalFormat df2 = new DecimalFormat( "#,###,###,##0.00" );
			if(manageTourPrice!=null){
				double serviceFee=Double.parseDouble(manageTourPrice.getServiceFee());
				double  shippingFee=Double.parseDouble(manageTourPrice.getShipping());
				int currencyType=manageTourPrice.getCurrencyType();
				tempPrice=(currencyType==1?((wTicketRow.getBuyItNowPrice()*(1+serviceFee/100)) + (shippingFee/wTicketRow.getQuantity())):(wTicketRow.getBuyItNowPrice() + serviceFee + (shippingFee/wTicketRow.getQuantity())));
				for(int i=1;i<=wTicketRow.getQuantity();i++){
					if(EbayUtil.isPartition(i,wTicketRow.getQuantity())){
						priceByQuantity+= i + " : " + df2.format((currencyType==1?((wTicketRow.getBuyItNowPrice()*(1+serviceFee/100)) + (shippingFee/i)):(wTicketRow.getBuyItNowPrice() + serviceFee + (shippingFee/i)))) + "&#13;" ;
					}
				}
			}
			wTicketRow.setPriceByQuantity(priceByQuantity);
			wTicketRow.setPurchasePrice(tempPrice);
			}
		*/
		List<WebTicketRow> ticketList= new ArrayList<WebTicketRow>(); 
		if(priceType != null && priceType.equalsIgnoreCase("purchase")){
			boolean priceRange= true;
			for(WebTicketRow wTicketRow:newFilteredWebTicketRows){
				priceRange = TicketUtil.isInPriceRange(wTicketRow, startPrice, endPrice, priceType);
				if(!priceRange){
					ticketList.add(wTicketRow);
					priceRange = true;
				}
			}
			newFilteredWebTicketRows.removeAll(ticketList);
		}
		
		if (!showDuplicates) {
			removedDuplicateCount = origTickets.size() - filteredTickets.size();
		} else {
			removedDuplicateCount = TicketUtil.getRemovedDuplicateCount(newFilteredWebTicketRows);
		}		
		
		if (isAnalyticView) {
//			Long removeDuplicateHistoricalTicketsStartTime = System.currentTimeMillis();
			
			// action indication
			Long actionsForTicketsStartTime = System.currentTimeMillis();
			Map<Integer, ActionIndicator> actions = ActionIndicatorUtil.getActionsForTickets(newFilteredWebTicketRows, false);
			mav.addObject("actions", actions);
			timeStats.put("actionsForTickets", System.currentTimeMillis() - actionsForTicketsStartTime);
			
			// event stats		
			Long eventStatsStartTime = System.currentTimeMillis();			
			Map<String, Map<String, Integer>> siteStatByCategory = StatHelper.getTicketCountByCategoryAndSite(filteredTicketsForStatsTab, statsCatScheme);
			Map<String, Integer> totalStatBySite = new HashMap<String, Integer>();
			if (siteStatByCategory != null) {
				for (String catSymbol: siteStatByCategory.keySet()) {
					for (String siteId: siteStatByCategory.get(catSymbol).keySet()) {
						int totalStatCount = ((totalStatBySite.get(siteId) == null)?0:totalStatBySite.get(siteId)) + siteStatByCategory.get(catSymbol).get(siteId);
						totalStatBySite.put(siteId, totalStatCount);
					}
				}
			}
			
			Map<String, Map<String, Integer>> quantityStatBySiteId = StatHelper.getTicketEntryCountBySiteAndQuantity(filteredTicketsForStatsTab);
			Map<String, Integer> totalStatByQuantity = new HashMap<String, Integer>();
			if (quantityStatBySiteId != null) {
				for (String siteId: quantityStatBySiteId.keySet()) {
					for (String qty: quantityStatBySiteId.get(siteId).keySet()) {
						int totalStatCount = ((totalStatByQuantity.get(qty) == null)?0:totalStatByQuantity.get(qty)) + quantityStatBySiteId.get(siteId).get(qty);
						totalStatByQuantity.put(qty, totalStatCount);
					}
				}
			}
			
			Map<String, Map<String, Integer>> quantityStatByCategory = StatHelper.getTicketEntryCountByCategoryAndQuantity(filteredTicketsForStatsTab, statsCatScheme);
			
			timeStats.put("eventStats", System.currentTimeMillis() - eventStatsStartTime);
			
			Long historyStatsStartTime = System.currentTimeMillis();						
			//Date startDate = new Date(new Date().getTime() - 30L * 24L * 3600L * 1000L);
			
			//to calculate number of day between startdate and enddate of chart and history tab ticket
			Calendar today = Calendar.getInstance();
			Calendar from = Calendar.getInstance();
			from.setTime(fromDate);
			
			
			try {
				DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
				if(dateRange1 != null && dateRange1.trim().length() >0){
					from.setTime(df.parse(dateRange1));
					from.add(Calendar.DATE, -1);
					fromDate = from.getTime();
				}
				if(dateRange2 != null && dateRange2.trim().length() >0){
					today.setTime(df.parse(dateRange2));
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			int day =0;
			while(from.before(today)){
				from.add(Calendar.DAY_OF_MONTH, 1);
				day++;
			}
			
			Collection<Ticket> combinedTickets = new ArrayList<Ticket>(filteredTicktesForHistoryTab);
			combinedTickets.addAll(filteredHistoricalTicktesForHistoryTab);
			mav.addObject("stats", StatHelper.getEventCategorySiteQuantityLotStats(fromDate, day, combinedTickets, historyCatScheme,recentUpdateDate));
			timeStats.put("historyStats", System.currentTimeMillis() - historyStatsStartTime);
			
			mav.addObject("siteStatByCategory", siteStatByCategory);
			mav.addObject("quantityStatBySiteId", quantityStatBySiteId);
			mav.addObject("quantityStatByCategory", quantityStatByCategory);
			
			mav.addObject("totalStatBySite", totalStatBySite);
			mav.addObject("totalStatByQuantity", totalStatByQuantity);			

			//
			// LIQUIDITY RATIO, IN, OUT COMPUTATION
			//
			Long liquidityStatsStartTime = System.currentTimeMillis();
			Date yesterday = new Date(new Date().getTime() - (24L * 60L * 60L * 1000L));
			
			Collection<Ticket> liquidityTickets = DAORegistry.getTicketDAO().getAllTicketsByEventUpdatedAfter(eventId, yesterday);
			if (liquidityTickets == null) {
				liquidityTickets = new ArrayList<Ticket>();
			}
	
			Map<String, Integer> inTicketCountBySiteId = new HashMap<String, Integer>();		
			Map<String, Integer> outTicketCountBySiteId = new HashMap<String, Integer>();
			Map<String, Integer> startTicketCountBySiteId = new HashMap<String, Integer>();
			Map<String, Integer> endTicketCountBySiteId = new HashMap<String, Integer>();
			Map<String, Double> liquidityRatioBySiteId = new HashMap<String, Double>();
			Map<String, Double> marketPercentBySiteId = new HashMap<String, Double>();
	
			Map<String, Integer> inTicketCountByCategory = new HashMap<String, Integer>();		
			Map<String, Integer> outTicketCountByCategory = new HashMap<String, Integer>();
			Map<String, Integer> startTicketCountByCategory = new HashMap<String, Integer>();
			Map<String, Integer> endTicketCountByCategory = new HashMap<String, Integer>();
			Map<String, Double> liquidityRatioByCategory = new HashMap<String, Double>();
			Map<String, Double> marketPercentByCategory = new HashMap<String, Double>();
	
			for(String siteId: Constants.getInstance().getSiteIds()) {
				inTicketCountBySiteId.put(siteId, 0);
				outTicketCountBySiteId.put(siteId, 0);
				liquidityRatioBySiteId.put(siteId, 0.0);
				startTicketCountBySiteId.put(siteId, 0);
				endTicketCountBySiteId.put(siteId, 0);
				marketPercentBySiteId.put(siteId, 0.0);
			}
	
			Collection<String> categorySymbolList = new ArrayList<String>();
//			TODOCHECK
//			Collection<Category> categoryList = DAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(venueCategory.getId());
			if(liquidityCategoryIds!=null){
				if (liquidityCategoryIds.contains(Category.ALL_ID)) {
					for (Category category : categories) {
						categorySymbolList.add(category.getSymbol());
					}
					categorySymbolList.add("UNCAT");
				}else if (liquidityCategoryIds.contains(Category.CATEGORIZED_ID)) {
					for (Category category : categories) {
	
						categorySymbolList.add(category.getSymbol());
	
					}
				}else if (liquidityCategoryIds.contains(Category.UNCATEGORIZED_ID)) {
					categorySymbolList.add("UNCAT");
				}else {
					if (liquidityCategoryIds != null
							&& !liquidityCategoryIds.isEmpty()) {
						for (Category category : categories) {
							if (liquidityCategoryIds.contains(category.getId())) {
								categorySymbolList.add(category.getSymbol());
							}
						}
					}
				}
			}
			
			
	
			for(String catSymbol: categorySymbolList) {
				inTicketCountByCategory.put(catSymbol, 0);
				outTicketCountByCategory.put(catSymbol, 0);
				liquidityRatioByCategory.put(catSymbol, 0.0);
				startTicketCountByCategory.put(catSymbol, 0);
				endTicketCountByCategory.put(catSymbol, 0);
				marketPercentByCategory.put(catSymbol, 0.0);			
			}
			

			int inTicketCount = 0;
			int outTicketCount = 0;
			int startTicketCount = 0;
			int endTicketCount = 0;
			double liquidityRatio = 0.0;
			double marketPercent = 0.0;

			int inTicketCountWithDuplicateRemovalPolicy = 0;
			int outTicketCountWithDuplicateRemovalPolicy = 0;
			int startTicketCountWithDuplicateRemovalPolicy = 0;
			int endTicketCountWithDuplicateRemovalPolicy = 0;
			double liquidityRatioWithDuplicateRemovalPolicy = 0.0;
			double marketPercentWithDuplicateRemovalPolicy = 0.0;
			
			Map<String, Integer> offeredTicketsByCat = new HashMap<String, Integer>();
			Map<String, Integer> availableTicketsByCat = new HashMap<String, Integer>();
			
			Set<String> scannedTicketKeys = new HashSet<String>();
			VenueCategory venueCategoryLiquidity = null;
			if((liquidityCatScheme == null || liquidityCatScheme.isEmpty()) && event.getVenueCategory()!=null){
				venueCategoryLiquidity = event.getVenueCategory();
			}else{
				venueCategoryLiquidity = DAORegistry.getVenueCategoryDAO().getVenueCategoryByVenueAndCategoryGroup(event.getVenueId(), liquidityCatScheme);
			}
			
			for(Ticket ticket: liquidityTickets) {
				if ((liquidityQunatities == null || liquidityQunatities.isEmpty() || liquidityQunatities.contains(ticket.getRemainingQuantity()) 
						|| (liquidityQunatities.contains(TicketUtil.MAX_QTY) && (ticket.getRemainingQuantity().compareTo(TicketUtil.MAX_QTY) == 1) ))
						&& (
								//FIXME
								liquidityCategorySymbol == null || liquidityCategorySymbol.isEmpty()
								|| (liquidityCategorySymbol.equals("CATS") && ticket.getCategoryId(venueCategoryLiquidity,liquidityCatScheme,catMap,catMappingMap) != null)
								|| (liquidityCategorySymbol.equals("UNCATS") && ticket.getCategoryId(venueCategoryLiquidity,liquidityCatScheme,catMap,catMappingMap) == null)
						)
						&& siteFilters.get(ticket.getSiteId())
						&& (section == null || section.isEmpty() || (ticket.getSection() != null && ticket.getSection().toLowerCase().contains(section)))
						&& (row == null || row.isEmpty() || (ticket.getRow() != null && ticket.getRow().toLowerCase().contains(row)))) {
				
					String ticketKey = TicketUtil.getTicketDuplicateKey(ticket, removeDuplicatePolicy);
					boolean isTicketDuplicate = scannedTicketKeys.contains(ticketKey);
					
					String catSymbol = "UNCAT";
					if(venueCategoryLiquidity!=null){
//						catSymbol = (ticket.getCategory(venueCategoryLiquidity, liquidityCatScheme) == null)?"UNCAT":ticket.getCategory(event.getVenueCategory(), liquidityCatScheme).getSymbol();						
						catSymbol = (ticket.getCategory() == null)?"UNCAT":ticket.getCategory().getSymbol();
					}
					
					if(!categorySymbolList.contains("UNCAT") && catSymbol.equals("UNCAT")){
						continue;
					}
					if(!categorySymbolList.contains(catSymbol)){
						continue;
					}
	
					if (ticket.getTicketStatus().equals(TicketStatus.ACTIVE)) {
						endTicketCount += ticket.getRemainingQuantity();
						endTicketCountBySiteId.put(ticket.getSiteId(), endTicketCountBySiteId.get(ticket.getSiteId()) + ticket.getRemainingQuantity());
	
						if (!isTicketDuplicate) {
							endTicketCountWithDuplicateRemovalPolicy += ticket.getRemainingQuantity();
							endTicketCountByCategory.put(catSymbol, endTicketCountByCategory.get(catSymbol) + ticket.getRemainingQuantity());
						}
					}
	
					if (ticket.getInsertionDate().before(yesterday)
							&& (ticket.getTicketStatus().equals(TicketStatus.ACTIVE)
									|| ticket.getLastUpdate().after(yesterday))) {
						startTicketCount += ticket.getRemainingQuantity();
						startTicketCountBySiteId.put(ticket.getSiteId(), startTicketCountBySiteId.get(ticket.getSiteId()) + ticket.getRemainingQuantity());						
	
						if (!isTicketDuplicate) {
							startTicketCountWithDuplicateRemovalPolicy += ticket.getRemainingQuantity();
							startTicketCountByCategory.put(catSymbol, startTicketCountByCategory.get(catSymbol) + ticket.getRemainingQuantity());						
						}
					}
	
					if (ticket.getInsertionDate().after(yesterday)) {
						inTicketCount += ticket.getRemainingQuantity();
						inTicketCountBySiteId.put(ticket.getSiteId(), inTicketCountBySiteId.get(ticket.getSiteId()) + ticket.getRemainingQuantity());
	
						if (!isTicketDuplicate) {
							inTicketCountWithDuplicateRemovalPolicy += ticket.getRemainingQuantity();
							inTicketCountByCategory.put(catSymbol, inTicketCountByCategory.get(catSymbol) + ticket.getRemainingQuantity());
						}
	
					}
					if (!ticket.getTicketStatus().equals(TicketStatus.ACTIVE) && ticket.getLastUpdate().after(yesterday)) {
						outTicketCount += ticket.getRemainingQuantity();
						outTicketCountBySiteId.put(ticket.getSiteId(), outTicketCountBySiteId.get(ticket.getSiteId()) + ticket.getRemainingQuantity());
						
						if (!isTicketDuplicate) {
							outTicketCountWithDuplicateRemovalPolicy += ticket.getRemainingQuantity();
							outTicketCountByCategory.put(catSymbol, outTicketCountByCategory.get(catSymbol) + ticket.getRemainingQuantity());
						}					
					}
					
					scannedTicketKeys.add(ticketKey);
					
					if(offeredTicketsByCat.get(catSymbol) == null) {
						offeredTicketsByCat.put(catSymbol, 0);
					}
					offeredTicketsByCat.put(catSymbol, offeredTicketsByCat.get(catSymbol) + ticket.getRemainingQuantity());
				}
			}
			for(Category cat : categories){
				if(categorySymbolList.contains(cat.getSymbol())){					
					availableTicketsByCat.put(cat.getSymbol(), cat.getSeatQuantity());
				}
			}
			if (startTicketCount > 0) {
				liquidityRatio = (double)(outTicketCount - inTicketCount) / startTicketCount;
				marketPercent = (double)outTicketCount * 100.0 / startTicketCount;
			}

			if (startTicketCountWithDuplicateRemovalPolicy > 0) {
				liquidityRatioWithDuplicateRemovalPolicy = (double)(outTicketCountWithDuplicateRemovalPolicy - inTicketCountWithDuplicateRemovalPolicy) / startTicketCountWithDuplicateRemovalPolicy;
				marketPercentWithDuplicateRemovalPolicy = (double)outTicketCountWithDuplicateRemovalPolicy * 100.0 / startTicketCountWithDuplicateRemovalPolicy;
			}

			for(String siteId: Constants.getInstance().getSiteIds()) {
				if (startTicketCountBySiteId.get(siteId) > 0) {
					liquidityRatioBySiteId.put(siteId, (double)(outTicketCountBySiteId.get(siteId) - inTicketCountBySiteId.get(siteId)) * 100.0 / startTicketCountBySiteId.get(siteId));
					marketPercentBySiteId.put(siteId, (double)(outTicketCountBySiteId.get(siteId) * 100.0 / startTicketCountBySiteId.get(siteId)));				
				}
			}

			for(String catSymbol: categorySymbolList) {
				if (startTicketCountByCategory.get(catSymbol) > 0) {
					liquidityRatioByCategory.put(catSymbol, (double)(outTicketCountByCategory.get(catSymbol) - inTicketCountByCategory.get(catSymbol)) * 100.0 / startTicketCountByCategory.get(catSymbol));
					marketPercentByCategory.put(catSymbol, (double)(outTicketCountByCategory.get(catSymbol) * 100.0 / startTicketCountByCategory.get(catSymbol)));
				}
			}
			timeStats.put("liquidityStats", System.currentTimeMillis() - liquidityStatsStartTime);
			
			// liquidity ratio		
			mav.addObject("categorySymbolList", categorySymbolList);

			mav.addObject("inTicketCount", inTicketCount);
			mav.addObject("inTicketCountWithDuplicateRemovalPolicy", inTicketCountWithDuplicateRemovalPolicy);
			mav.addObject("outTicketCount", outTicketCount);
			mav.addObject("outTicketCountWithDuplicateRemovalPolicy", outTicketCountWithDuplicateRemovalPolicy);
			mav.addObject("liquidityRatio", liquidityRatio);
			mav.addObject("liquidityRatioWithDuplicateRemovalPolicy", liquidityRatioWithDuplicateRemovalPolicy);
			mav.addObject("inTicketCountBySiteId", inTicketCountBySiteId);
			mav.addObject("outTicketCountBySiteId", outTicketCountBySiteId);
			mav.addObject("liquidityRatioBySiteId", liquidityRatioBySiteId);
			mav.addObject("startTicketCountBySiteId", startTicketCountBySiteId);
			mav.addObject("endTicketCountBySiteId", endTicketCountBySiteId);
			mav.addObject("marketPercentBySiteId", marketPercentBySiteId);
			mav.addObject("inTicketCountByCategory", inTicketCountByCategory);
			mav.addObject("outTicketCountByCategory", outTicketCountByCategory);
			mav.addObject("liquidityRatioByCategory", liquidityRatioByCategory);
			mav.addObject("startTicketCountByCategory", startTicketCountByCategory);
			mav.addObject("endTicketCountByCategory", endTicketCountByCategory);
			mav.addObject("marketPercentByCategory", marketPercentByCategory);
			mav.addObject("startTicketCount", startTicketCount);
			mav.addObject("startTicketCountWithDuplicateRemovalPolicy", startTicketCountWithDuplicateRemovalPolicy);
			mav.addObject("endTicketCount", endTicketCount);
			mav.addObject("endTicketCountWithDuplicateRemovalPolicy", endTicketCountWithDuplicateRemovalPolicy);
			mav.addObject("marketPercent", marketPercent);
			mav.addObject("marketPercentWithDuplicateRemovalPolicy", marketPercentWithDuplicateRemovalPolicy);
			
			mav.addObject("availableTicketsByCat", availableTicketsByCat);
			mav.addObject("offeredTicketsByCat", offeredTicketsByCat);			
		}					
		
		int qtyOffered = 0;

		int adminOneTicketsCount = 0;
		for(WebTicketRow ticket : newFilteredWebTicketRows){
			Category cat = ticket.getCategory(venueCategory, catScheme,catMap,catMappingMap);
			if(cat != null){
				ticket.setCategorySymbol(cat.getSymbol());
//				ticket.setCatId(cat.getId());
			}
			
			if (ticket.getSeller() != null && ticket.getSeller().equals("ADMT1")) {
				adminOneTicketsCount += ticket.getRemainingQuantity();
			}
			qtyOffered += ticket.getRemainingQuantity();
		}


		mav.addObject("numTicketRows", numTicketRows);
		mav.addObject("catGroupName", catScheme);		
		mav.addObject("liquidityCatScheme",liquidityCatScheme);
		mav.addObject("historyCatScheme",historyCatScheme);
		mav.addObject("statsCatScheme",statsCatScheme);
		
		
		mav.addObject("zoneCatGroupName", zoneCatScheme);
		
		mav.addObject("catGroups", Categorizer.getCategoryGroupsByVenueId(event.getVenueId()));
		
		
		mav.addObject("qtyAvailable", availableSeats);
		mav.addObject("qtyOffered", new Integer(qtyOffered));
		mav.addObject("sectionFilter", section);
		mav.addObject("rowFilter", row);
		mav.addObject("startPriceFilter", startPrice);
		mav.addObject("endPriceFilter", endPrice);
		mav.addObject("priceType", priceType);
		mav.addObject("zoneStartPrice", zoneStartPrice);
		mav.addObject("zoneEndPrice", zoneEndPrice);
				
		//Do an alphanumeric sort based on the section.
		//Collections.sort(newFilteredWebTicketRows, new WebTicketRowANSorting());
		int cnt = 1;
		for(WebTicketRow wrt : newFilteredWebTicketRows){
			wrt.setNumerizedSection(cnt);
			cnt++;
		}
//		System.out.println("The count is: " + cnt);
//		for(WebTicketRow wrt : newFilteredWebTicketRows){
//			System.out.println("the wrt section id is: " + wrt.getNormalizedSection() + " .The numerized section is: "+ wrt.getNumerizedSection());
//		}
		
		// only need to sort if we have multisort
		if (multisort) {
			Long sortTicketsStartTime = System.currentTimeMillis();
			TicketSorter.sortTickets(newFilteredWebTicketRows, multisortFields, catScheme);
			timeStats.put("sortTickets", System.currentTimeMillis() - sortTicketsStartTime);			
		}
		
		
		mav.addObject("webTickets", newFilteredWebTicketRows);
		ArrayList<Integer> zoneQuantityList = (ArrayList<Integer>)map.get("zoneQuantities");
		 
		
		//
		/*List <EbayInventoryGroup> ticketgroups= TicketUtil.calculateGroup(eventId,request.getSession(true),zoneQuantityList,zoneStartPrice,zoneEndPrice,filteredTickets);
		
		ticketgroups=handleZoneTab(request, map,ticketgroups, username, reset, event);		
		mav.addObject("zoneCatScheme", map.get("zoneCatScheme"));
		mav.addObject("zoneCatFilters", map.get("zoneCatFilters"));
		mav.addObject("zoneCategoryIds", map.get("zoneCategoryIds"));
		mav.addObject("zoneCategorySymbol", map.get("zoneCategorySymbol"));
		mav.addObject("zoneCategorySymbols", map.get("zoneCategorySymbols"));
		mav.addObject("zoneCatGroups",(List<String>)request.getSession().getAttribute("tempZoneCatGroups"));
		
		
		if(ticketgroups!=null && !ticketgroups.isEmpty()){
			mav.addObject("zoneCatGroupName", ticketgroups.get(0).getCategory());			
		}
				
		mav.addObject("ticketgroups", ticketgroups);*/
		mav.addObject("tab",request.getParameter("tab"));
		Artist artist = event.getArtist();
		mav.addObject("artist", artist);
		//mav.addObject("artist", tour.getArtist());
		mav.addObject("venue", event.getVenue());
		mav.addObject("ticketCount", new Integer(qtyOffered));
		mav.addObject("removeDuplicatePolicy", removeDuplicatePolicy.toString());
		mav.addObject("removedDuplicateCount", removedDuplicateCount);
		mav.addObject("showDuplicates", showDuplicates);
		mav.addObject("multisort", multisort);
		mav.addObject("multisortField0", multisortFields[0]);
		mav.addObject("multisortField1", multisortFields[1]);
		mav.addObject("multisortField2", multisortFields[2]);
		mav.addObject("multisortField3", multisortFields[3]);		
		mav.addObject("event", event);
		mav.addObject("categorySymbol", categorySymbol);
		mav.addObject("categorySymbols", categorySymbols);
		mav.addObject("liquidityCategorySymbol", liquidityCategorySymbol);
		mav.addObject("liquidityCategorySymbols", liquidityCategorySymbols);
		mav.addObject("historyCategorySymbol", historyCategorySymbol);
		mav.addObject("historyCategorySymbols", historyCategorySymbols);
		mav.addObject("statsCategorySymbol", statsCategorySymbol);
		mav.addObject("statsCategorySymbols", statsCategorySymbols);
		
		mav.addObject("quantities", quantities);
		mav.addObject("zoneQuantities", zoneQuantityList);
		
		mav.addObject("show", show);
		mav.addObject("view", view);
		mav.addObject("categories", DAORegistry.getCategoryDAO().getAllCategoriesByVenueIdAndCategoryGroup(event.getVenueId(), catScheme));
		mav.addObject("statsCategories", DAORegistry.getCategoryDAO().getAllCategoriesByVenueIdAndCategoryGroup(event.getVenueId(), statsCatScheme));
		mav.addObject("catFilters", catFilters);
		mav.addObject("liquidityCatFilters", liquidityCatFilters);
		mav.addObject("historyCatFilters", historyCatFilters);
		mav.addObject("statsCatFilters", statsCatFilters);		
		mav.addObject("adminOneTicketsCount", adminOneTicketsCount);
		
		mav.addObject("priceAdjustments", DAORegistry.getEventPriceAdjustmentDAO().getPriceAdjustments(event.getId()));		
		mav.addObject("siteById", DAORegistry.getSiteDAO().getSiteByIdMap());
		mav.addObject("customers", DAORegistry.getQuoteCustomerDAO().getAll());

		// bookmarks
		mav.addObject("ticketBookmarks", DAORegistry.getBookmarkDAO().getAllTicketBookmarks(username));
		mav.addObject("eventBookmark", DAORegistry.getBookmarkDAO().getBookmark(username, BookmarkType.EVENT, eventId));
		
		// crawls
		mav.addObject("crawls", DAORegistry.getTicketListingCrawlDAO().getAllActiveTicketListingCrawlByEvent(eventId));
//		mav.addObject("ticketListingCrawler", ticketListingCrawler);
//		mav.addObject("lastFullUpdate", ticketListingCrawler.getLeastCrawledDateForEvent(eventId));

		if (isAnalyticView) {
			mav.addObject("ticketsPage", "BrowseTicketsAnalytic");
		} else {
			mav.addObject("ticketsPage", "BrowseTickets");			
		}

		int eventIndex = 0;
		List<Event> artistEvents = new ArrayList<Event>(artist.getEvents());
		for (int i = 0 ; i < artistEvents.size() ; i++) {
			if (artistEvents.get(i).getId().equals(event.getId())) {
				eventIndex = i;
				break;
			}
		}
		
		Event previousEvent = null;
		Event nextEvent = null;

		if (eventIndex > 0) {
			previousEvent = artistEvents.get(eventIndex - 1);
		}
		
		if (eventIndex < artistEvents.size() - 1) {
			nextEvent = artistEvents.get(eventIndex + 1);
		}
		mav.addObject("previousEvent", previousEvent);
		mav.addObject("nextEvent", nextEvent);	

		mav.addObject("timeStats", timeStats);
		mav.addObject("marketMaker", DAORegistry.getMarketMakerEventTrackingDAO().getCurrentMarketMakerForEvent(eventId));
		mav.addObject("marketMakers", DAORegistry.getUserDAO().getAll());
		
		TicketUtil.flushAdjustmentMaps();
		
		Event browseEvent = DAORegistry.getEventDAO().get(eventId);
		
		mav.addObject("artistList",DAORegistry.getArtistDAO().getAllActiveArtists());
		mav.addObject("selectedArtist",browseEvent.getArtistId());
//		mav.addObject("artistsList",DAORegistry.getArtistDAO().getAllToursByArtist(browseEvent.getArtistId()));
//		mav.addObject("selectedArtist",browseEvent.getArtistId());
		return mav;
	}	
	
	public void sendVenueMapAsEmail(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		String venueId = request.getParameter("venueId");
		String catScheme = request.getParameter("catScheme");
		String mailTo = request.getParameter("mailTo");
		
		String username = (String)request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);
		User user = DAORegistry.getUserDAO().getUserByUsername(username);
		String cc = user.getEmail();
		String venuName = DAORegistry.getVenueDAO().get(Integer.parseInt(venueId)).getBuilding();
		
		String filename =  "C:/TMATIMAGESFINAL/" + venueId + "_" + catScheme + ".gif";
	 	
	 	File file = new File(filename);
        
        if(!file.exists()) {
        	
        	PrintWriter out = response.getWriter();
    		out.println("No Map Found For "+venuName);
        }
        
        Map<String, Object> map = new HashMap<String, Object>();
        MailAttachment[] mailAttachments = new MailAttachment[1];       
        
		map.put("venuName", venuName);
		mailAttachments[0] = new MailAttachment(IOUtils.toByteArray(new FileInputStream(file)), "image/gif", venuName+".gif");
		
		String subject = venuName + " Seating Chart";
		String resource =  "mail-venue-map.txt";		
        String mimeType = "text/html";
		String from = DAORegistry.getPropertyDAO().get("smtp.from").getValue();
		String smtpHost = DAORegistry.getPropertyDAO().get("smtp.host").getValue();
		Integer smtpPort = Integer.valueOf(DAORegistry.getPropertyDAO().get("smtp.port").getValue());
		String smtpUsername = DAORegistry.getPropertyDAO().get("smtp.username").getValue();
		String smtpPassword = DAORegistry.getPropertyDAO().get("smtp.password").getValue();
		String smtpSecureConnection = DAORegistry.getPropertyDAO().get("smtp.secure.connection").getValue();
		Boolean smtpAuth = Boolean.valueOf(DAORegistry.getPropertyDAO().get("smtp.auth").getValue());
		String transportProtocol = "";

		mailManager.sendMailNow(smtpHost, smtpPort, transportProtocol, smtpSecureConnection, smtpAuth,
				smtpUsername, smtpPassword, null, from, mailTo, cc, null, subject,resource, map, mimeType, mailAttachments);
	
		PrintWriter out = response.getWriter();
		out.println("Email sent Successfully.");
	}
	
	private void handleMarkDuplicates(HttpServletRequest request, Integer eventId) {
		String dupAction = request.getParameter("dupAction");
		
		if (dupAction == null) {
			return;
		}

		String dups = request.getParameter("dups");
		if (dups == null) {
			return;
		}

		if(dupAction.equals("admit") || dupAction.equals("unadmit")){
			handleMarkAdmitOne(dupAction, dups, eventId);
			return;
		} 
		
		Map<Integer, DuplicateTicketMap> ticketMap = new HashMap<Integer, DuplicateTicketMap>();
		for (DuplicateTicketMap duplicateTicketMap: DAORegistry.getDuplicateTicketMapDAO().getAll()) {
			ticketMap.put(duplicateTicketMap.getTicketId(), duplicateTicketMap);
		}

				
		if (dupAction.equals("create")) {
			Long groupId = System.currentTimeMillis();
	
			for(String ticketIdStr: dups.split(",")) {
				Integer ticketId = Integer.valueOf(ticketIdStr);
				
				DuplicateTicketMap duplicateTicketMap = ticketMap.get(ticketId);
				if (duplicateTicketMap != null) {
					DAORegistry.getDuplicateTicketMapDAO().deleteByGroupId(duplicateTicketMap.getGroupId());
				}
				
				duplicateTicketMap = new DuplicateTicketMap(ticketId, eventId, groupId);
				DAORegistry.getDuplicateTicketMapDAO().saveOrUpdate(duplicateTicketMap);
			}
		} else {
			for(String ticketIdStr: dups.split(",")) {
				Integer ticketId = Integer.valueOf(ticketIdStr);
				DAORegistry.getDuplicateTicketMapDAO().deleteById(ticketId);
			}
		}
		
		// remove all duplicates which consist of of one ticket
		DAORegistry.getDuplicateTicketMapDAO().deleteGroupsWithOneEntry(eventId);
	}
	

	private Map<String, Object> handlePreferences(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<String, Object>();
		String username = (String)request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);
		Boolean reset = "1".equals(request.getParameter("reset"));
		int eventId = Integer.parseInt(request.getParameter("eventId"));
		Event event = DAORegistry.getEventDAO().get(eventId);
		map.put("event", event);
		
		String eventIdString = "" + eventId;
		String lastEventIdString = preferenceManager.getPreferenceValue(username, "browseTicketLastEventId", "");
		preferenceManager.updatePreference(username, "browseTicketLastEventId", "" + eventIdString);
		
		boolean resetFilters = !eventIdString.equals(lastEventIdString);
		if (resetFilters) {
			preferenceManager.resetFilterPreferenceForUser(username);
		}
		
		/*
		 * Show duplicates
		 */
		
		String showDuplicatesString = request.getParameter("showDuplicates");
		if (showDuplicatesString == null && !reset) {
			showDuplicatesString = preferenceManager.getPreferenceValue(username, "browseTicketShowDuplicates", "false");
		}

		Boolean showDuplicates = false;
		try {
			showDuplicates = Boolean.parseBoolean(showDuplicatesString);
		} catch(Exception e) {
		}

		preferenceManager.updatePreference(username, "browseTicketShowDuplicates", showDuplicates.toString());
		map.put("showDuplicates", showDuplicates);

		/*
		 * Duplicate scrub filter
		 */
		String removeDuplicatePolicyString = request.getParameter("removeDuplicatePolicy");
		if (removeDuplicatePolicyString == null && !reset) {
		removeDuplicatePolicyString = preferenceManager.getPreferenceValue(username, "browseTicketRemoveDuplicatePolicy", "SUPER_SMART");
		}
		
		RemoveDuplicatePolicy removeDuplicatePolicy = RemoveDuplicatePolicy.SUPER_SMART;
		try {
			removeDuplicatePolicy = RemoveDuplicatePolicy.valueOf(removeDuplicatePolicyString);
		} catch (Exception e) {};

		preferenceManager.updatePreference(username, "browseTicketRemoveDuplicatePolicy", removeDuplicatePolicy.toString());
		map.put("removeDuplicatePolicy", removeDuplicatePolicy);
		
		/*
		 * Num ticket Rows 
		 */
		String numTicketRows = request.getParameter("numTicketRows");
		if (reset) {
			numTicketRows = "24";
		} else if (numTicketRows == null) {                              
			numTicketRows = preferenceManager.getPreferenceValue(username, "browseTicketNumTicketRows", "24");
		}
		
		preferenceManager.updatePreference(username, "browseTicketNumTicketRows", numTicketRows);		
		map.put("numTicketRows", Integer.valueOf(numTicketRows));
		
		/*
		 * Category filter
		 */
		String categorySymbol = request.getParameter("category");
		if (categorySymbol == null && !reset) {
			categorySymbol = preferenceManager.getPreferenceValue(username, "browseTicketCategory");
		}
		/*
		 * Category filter
		 */
		
		
		/*
		 * Category Scheme
		 */
		String catScheme = request.getParameter("catScheme");
			
		if(catScheme == null){
			catScheme = preferenceManager.getPreferenceValue(username, "browseTicketCatGroup");
		}
		
		preferenceManager.updatePreference(username, "browseTicketCategory", categorySymbol);
		preferenceManager.updatePreference(username, "browseTicketCatGroup", catScheme);
		
		Categorizer.fillCategoryFilterMap(event, catScheme, categorySymbol, map,"tickets",username);
		catScheme = request.getParameter("historyCatScheme");
		categorySymbol = request.getParameter("historyCategory");
		
		Categorizer.fillCategoryFilterMap(event, catScheme, categorySymbol, map,"history",username);
		//fillCategoryFilterMap(event, catScheme, categorySymbol, map,"history",username);
		
		catScheme = request.getParameter("liquidityCatScheme");
		categorySymbol = request.getParameter("liquidityCategory");
		
		Categorizer.fillCategoryFilterMap(event, catScheme, categorySymbol, map,"liquidity",username);
		//fillCategoryFilterMap(event, catScheme, categorySymbol, map,"liquidity",username);
		
		catScheme = request.getParameter("statsCatScheme");
		categorySymbol = request.getParameter("statsCategory");
		
		Categorizer.fillCategoryFilterMap(event, catScheme, categorySymbol, map,"stats",username);
		//fillCategoryFilterMap(event, catScheme, categorySymbol, map,"stats",username);
		
						

		//boolean wholesale = false;
		  String priceType = request.getParameter("priceType");
		  if(priceType == null || priceType.equalsIgnoreCase("") ){
		   //wholesaleStr = preferenceManager.getPreferenceValue(username, "browseTicketWholesale", "0");
		   priceType=null;
		  }else{
		   preferenceManager.updatePreference(username, priceType, "1");
		  }

		  /*if (wholesaleStr.equals("1")) {
		   wholesale = true;
		  }*/
		  
		  map.put("priceType", priceType);	

		

		/*
		 * Show filter
		 */
		String show = request.getParameter("show");
		if (show == null && !reset) {
			show = preferenceManager.getPreferenceValue(username, "browseTicketShow");
		} else {
			preferenceManager.updatePreference(username, "browseTicketShow", show);
		}

		map.put("show", show);
		
		/*
		 * Section filter
		 */
		String section = request.getParameter("section");
		
		if (section == null  && !reset) {
			section = preferenceManager.getPreferenceValue(username, "browseTicketSection");
		}
		
		if (section != null) {
			section = section.toLowerCase();
			preferenceManager.updatePreference(username, "browseTicketSection", section);
		}
		map.put("section", section);
		
		/*
		 * Row filter
		 */
		String row = request.getParameter("row");
		if (row == null && !reset) {
			row = preferenceManager.getPreferenceValue(username, "browseTicketRow");
		}
		
		if (row != null) {
			row = row.toLowerCase();
			preferenceManager.updatePreference(username, "browseTicketRow", row);
		}	
		map.put("row", row);
		
		/*
		 * Start Price filter
		 */
		String startPrice = request.getParameter("startPrice");
		
		if (startPrice == null  && !reset) {
			startPrice = preferenceManager.getPreferenceValue(username, "browseTicketStartPrice");
		}
		Double sPrice = null;
		if (startPrice != null) {
			if(!startPrice.equals("")){ 
				sPrice = Double.parseDouble(startPrice);
			}
			preferenceManager.updatePreference(username, "browseTicketStartPrice", startPrice);
		}
		map.put("startPrice", sPrice);
		
		/*
		 * End Price filter
		 */
		String endPrice = request.getParameter("endPrice");
		
		if (endPrice == null  && !reset) {
			endPrice = preferenceManager.getPreferenceValue(username, "browseTicketEndPrice");
		}
		Double ePrice = null;
		if (endPrice != null) {
			if(!endPrice.equals("")){
				ePrice = Double.parseDouble(endPrice);
			}
			preferenceManager.updatePreference(username, "browseTicketEndPrice", endPrice);
		}
		map.put("endPrice", ePrice);
		
       String zoneStartPrice = request.getParameter("zoneStartPrice");
		
		if (zoneStartPrice == null  && !reset) {
			zoneStartPrice = preferenceManager.getPreferenceValue(username, "browseZoneStartPrice");
		}
		Double zoneSPrice = null;
		if (zoneStartPrice != null) {
			if(!zoneStartPrice.equals("")){ 
				zoneSPrice = Double.parseDouble(zoneStartPrice );
			}
			preferenceManager.updatePreference(username, "browseZoneStartPrice", zoneStartPrice);
		}
		map.put("zoneStartPrice", zoneSPrice);
		
		/*
		 * End Price filter
		 */
		String zoneEndPrice = request.getParameter("zoneEndPrice");
		
		if (zoneEndPrice == null  && !reset) {
			zoneEndPrice = preferenceManager.getPreferenceValue(username, "browseZoneEndPrice");
		}
		Double zoneEPrice = null;
		if (zoneEndPrice != null) {
			if(!zoneEndPrice.equals("")){
				zoneEPrice = Double.parseDouble(zoneEndPrice);
			}
			preferenceManager.updatePreference(username, "browseZoneEndPrice", zoneEndPrice);
		}
		map.put("zoneEndPrice", zoneEPrice);
		
		/*
		 * Quantity filter
		 */
		String quantityString = request.getParameter("quantity");
		fillQuantityFilterMap(quantityString, map, username,reset,"tickets");
		
		quantityString = request.getParameter("historyQuantity");
		fillQuantityFilterMap(quantityString, map, username,reset,"history");
		
		quantityString = request.getParameter("liquidityQuantity");
		fillQuantityFilterMap(quantityString, map, username,reset,"liquidity");
		
		quantityString = request.getParameter("statsQuantity");
		fillQuantityFilterMap(quantityString, map, username,reset,"stats");
		
		Map<String, Boolean> zoneQtyFilters = new HashMap<String, Boolean>();
		zoneQtyFilters.put("ALL", new Boolean(true));
		for(int i = 1 ; i <= 11 ; i++){
			zoneQtyFilters.put(i + "", new Boolean(false));
		}
		ArrayList<Integer> zoneQuantities = new ArrayList<Integer>();
		String zoneQuantityString = request.getParameter("zoneQuantity");
		if (zoneQuantityString == null && !reset) {
			zoneQuantityString = preferenceManager.getPreferenceValue(username, "browseZoneQuantity");
		} else {
			preferenceManager.updatePreference(username, "browseZoneQuantity", zoneQuantityString);
		}
		
		boolean lookForAllZone = true;
		
		if (zoneQuantityString != null && zoneQuantityString.length() > 0) {
			StringTokenizer tokenizer = new StringTokenizer(zoneQuantityString);
			lookForAllZone = false;
			while(tokenizer.hasMoreTokens()){
				String qty = tokenizer.nextToken(",").trim();
				if(qty.equals("ALL")){
					zoneQtyFilters.put(qty, new Boolean(true));
					lookForAllZone = true;
				} else if(!qty.isEmpty()) {
					zoneQtyFilters.put(qty, new Boolean(true));
					zoneQuantities.add(Integer.parseInt(qty));
				}
			}
		}
		if(lookForAllZone){
			zoneQuantities = new ArrayList<Integer>();
		} else {
			zoneQtyFilters.put("ALL", new Boolean(false));
		}
		
		map.put("zoneQtyFilters", zoneQtyFilters);
		map.put("zoneQuantities", zoneQuantities);
		
		/*
		 * SiteId filter
		 */
		Boolean uncheckSiteFilters = "1".equals(request.getParameter("uncheckSiteFilters"));
		Map<String, Boolean> siteFilters = new HashMap<String, Boolean>();
		
		String[] preferenceSiteIds;
		if (preferenceManager.getPreferenceValue(username, "browseTicketSites") == null) {
			preferenceSiteIds = Constants.getInstance().getSiteIds();
		} else {
			preferenceSiteIds = preferenceManager.getPreferenceValue(username, "browseTicketSites").split(",");
		}
		
		String preferenceSiteString="";
		int checkSiteIdCount = 0;
		for (String siteId: Constants.getInstance().getSiteIds()) {
			String checkedString = request.getParameter(siteId + "_checkbox");
			Boolean checked;
			if (checkedString == null && !reset && !uncheckSiteFilters) {
				// check if the site is in the preference
				checkedString = "false";
				for(String preferenceSiteId: preferenceSiteIds) {
					if (preferenceSiteId.equals(siteId)) {
						checkedString = "true";
						break;
					}
				}
			}
			if (checkedString == null) {
				checked = !uncheckSiteFilters;
			} else {
				checked = Boolean.parseBoolean(checkedString);
			}			
			
			if (checked) {
				preferenceSiteString += "," + siteId;
				checkSiteIdCount++;
			}
			siteFilters.put(siteId, checked);
			
		}

		if (!preferenceSiteString.isEmpty()) {
			preferenceSiteString = preferenceSiteString.substring(1);
		}
		
		preferenceManager.updatePreference(username, "browseTicketSites", preferenceSiteString);

		String[] siteIds = new String[checkSiteIdCount];
		int j = 0;
		for (String site: siteFilters.keySet()) {
			if (siteFilters.get(site)) {
				siteIds[j++] = site;
			}
		}
				
		map.put("siteFilters", siteFilters);
		map.put("siteIds", siteIds);
		
		//preferenceManager.updatePreference(username, "browseTicketQuantity", quantityString);
		//map.put("quantities", quantities);

		String multisortString = request.getParameter("multisort");
		if (multisortString == null) {
			multisortString = preferenceManager.getPreferenceValue(username, "browseTicketMultisort", "0");
		} else {
			preferenceManager.updatePreference(username, "browseTicketMultisort", multisortString);
		}
		boolean multisort = "1".equals(multisortString);
		map.put("multisort", multisort);
		
		// the preference browseTicketMultisortFields should contains
		// field1,field2,field3,field4
		Integer[] multisortpreferenceFields;
		if (preferenceManager.getPreferenceValue(username, "browseTicketMultisortFields") == null) {
			multisortpreferenceFields = new Integer[] {0, 0, 0, 0};
		} else {
			String[] multisortpreferenceFieldsToken = preferenceManager.getPreferenceValue(username, "browseTicketMultisortFields", "").split(",");
			if (multisortpreferenceFieldsToken.length != 4) {
				multisortpreferenceFields = new Integer[] {0, 0, 0, 0};				
			} else {
				multisortpreferenceFields = new Integer[4];
				for(int i = 0 ; i < 4 ; i++) {
					try {
						multisortpreferenceFields[i] = Integer.parseInt(multisortpreferenceFieldsToken[i]);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			
		}
		
		String multisortFieldpreferenceString = "";
		int[] multisortFields = new int[4];
		for (int i = 0 ; i < 4 ; i++) {
			String multisortFieldString = request.getParameter("multisort_field" + i);
			if (multisortFieldString == null) {
				// check if the preference
				multisortFields[i] = multisortpreferenceFields[i];				
			} else {				
				try {
					multisortFields[i] = Integer.parseInt(multisortFieldString);
				} catch (Exception e) {};
			}
			multisortFieldpreferenceString += "," + multisortFields[i]; 
		}
		
		map.put("multisortFields", multisortFields);
		
		multisortFieldpreferenceString = multisortFieldpreferenceString.substring(1);
		preferenceManager.updatePreference(username, "browseTicketMultisortFields", multisortFieldpreferenceString);
		return map;
	}

	private List<EbayInventoryGroup> handleZoneTab(HttpServletRequest request,
			Map<String, Object> map, List<EbayInventoryGroup> ticketgroups,String username, Boolean reset,
			Event event) {
		HttpSession session = request.getSession();
		//Collection<Category> cats = (Collection<Category>) session.getAttribute("zoneCategories");
		 String zoneCatScheme =  session.getAttribute("tempZoneCatScheme")!=null? (String) request.getSession().getAttribute("tempZoneCatScheme"):"";
		List<Integer> zoneCategoryIds = new ArrayList<Integer>();
		/*start*/
		List<EbayInventoryGroup> newGroup = new ArrayList<EbayInventoryGroup>();
		String zoneCategorySymbol = request.getParameter("zoneCategory");
		if (zoneCategorySymbol == null && !reset) {
			zoneCategorySymbol = preferenceManager.getPreferenceValue(username, "browseTicketCategory");
		}
		String zoneCategorySymbols = "";
		
		Collection<Category> zoneCategories =(Collection<Category>) session.getAttribute("zoneCategories");
		List<String> zoneCatSymbolList = new ArrayList<String>();
		Map<String, Boolean> zoneCatFilters = new HashMap<String, Boolean>();
		zoneCatFilters.put("ALL", new Boolean(false));
		zoneCatFilters.put("CAT", new Boolean(false));
		zoneCatFilters.put("UNCAT", new Boolean(false));
		if(zoneCategories != null){
			
			for(Category cat: zoneCategories){
				if(zoneCatSymbolList.contains(cat.getSymbol())){
					continue;
				}
				zoneCatFilters.put(cat.getSymbol(), new Boolean(false));				
				zoneCategorySymbols = zoneCategorySymbols + cat.getSymbol() + ",";
				zoneCatSymbolList.add(cat.getSymbol());
				
			}
			
			if(zoneCatSymbolList!=null && !zoneCatSymbolList.isEmpty()){
				Collections.sort(zoneCatSymbolList);
				zoneCategorySymbols = zoneCatSymbolList.toString();
				zoneCategorySymbols=zoneCategorySymbols.substring(1, zoneCategorySymbols.length()-1);
			}
			
		}
		if (zoneCategorySymbol == null) {
			zoneCategorySymbol = ""; 
		}
		if (zoneCategorySymbol.equals("")) {
			zoneCategoryIds.add(Category.ALL_ID);
			zoneCatFilters.put("ALL", new Boolean(true));
			newGroup=ticketgroups;
		} else if(zoneCategorySymbol.length() >= 6) {
			
		} else if(zoneCategorySymbol.length() >= 4) {
			if (zoneCategorySymbol.substring(0,3).equals("ALL")) {
				zoneCategoryIds.add(Category.ALL_ID);
				zoneCatFilters.put("ALL", new Boolean(true));
				newGroup=ticketgroups;
			} 
		} 
				
		
		if(!zoneCategorySymbol.equals("")) {
			StringTokenizer tokenizer = new StringTokenizer(zoneCategorySymbol);
			while(tokenizer.hasMoreTokens()){
				String cat = tokenizer.nextToken(",").trim();
				if(!cat.equals("ALL") 					
						&& !cat.equals("")){
					zoneCatFilters.put(cat, new Boolean(true));
					Category category = DAORegistry.getCategoryDAO().getCategoryByVenueCategoryIdAndCategorySymbol(event.getVenueCategory().getId(), cat);
					if(category != null){
						zoneCategoryIds.add(category.getId());
					}
				}
			}
		}
			
		if(zoneCategoryIds.isEmpty()){
			zoneCategoryIds.add(Category.ALL_ID);
		}
		if(ticketgroups!=null){
		for(EbayInventoryGroup ticketgroup:ticketgroups){
			if(zoneCategoryIds.contains(ticketgroup.getEbayInventory1().getTicket().getCategory().getId())){
				newGroup.add(ticketgroup);
			}
		}
		}
		if(newGroup!=null){		
		Collections.sort(newGroup,new java.util.Comparator<EbayInventoryGroup>() {

			public int compare(EbayInventoryGroup o1, EbayInventoryGroup o2) {
				if(o1.getEbayInventory1().getTicket().getCategory().getSymbol()==null ||o2.getEbayInventory1().getTicket().getCategory().getSymbol()==null ){
					return 0;
				}
			
				int symbolDiff =o1.getEbayInventory1().getTicket().getCategory().getSymbol().compareTo(o2.getEbayInventory1().getTicket().getCategory().getSymbol()) ;
				if(symbolDiff==0){
					return o1.getQuantity().compareTo(o2.getQuantity());
				}else{
					return symbolDiff;
				}
			}
		});
		}
		map.put("zoneCatScheme", zoneCatScheme);
		map.put("zoneCatFilters", zoneCatFilters);
		map.put("zoneCategoryIds", zoneCategoryIds);
		map.put("zoneCategorySymbol", zoneCategorySymbol);
		map.put("zoneCategorySymbols", zoneCategorySymbols);
		return newGroup;
		
		
		
	}	
	
	public void fillQuantityFilterMap(String quantityString,Map<String,Object> map,String username,Boolean reset,String tabName){
		ArrayList<Integer> quantities = new ArrayList<Integer>();
		Map<String, Boolean> qtyFilters = new HashMap<String, Boolean>();
		qtyFilters.put("ALL", new Boolean(true));
		for(int i = 1 ; i <= 11 ; i++){
			qtyFilters.put(i + "", new Boolean(false));
		}
		
			if (quantityString == null && !reset) {
				quantityString = preferenceManager.getPreferenceValue(username, "browseTicketQuantity");
			} else {
				if(tabName.equals("tickets")){
					preferenceManager.updatePreference(username, "browseTicketQuantity", quantityString);
				}
			}
		
		boolean lookForAll = true;
		
		if (quantityString != null && quantityString.length() > 0) {
			StringTokenizer tokenizer = new StringTokenizer(quantityString);
			lookForAll = false;
			while(tokenizer.hasMoreTokens()){
				String qty = tokenizer.nextToken(",").trim();
				if(qty.equals("ALL")){
					qtyFilters.put(qty, new Boolean(true));
					lookForAll = true;
				} else if(!qty.isEmpty()) {
					qtyFilters.put(qty, new Boolean(true));
					quantities.add(Integer.parseInt(qty));
				}
			}
		}
		if(lookForAll){
			quantities = new ArrayList<Integer>();
		} else {
			qtyFilters.put("ALL", new Boolean(false));
		}
		if(tabName.equals("tickets")){
			map.put("qtyFilters", qtyFilters);
			map.put("quantities", quantities);
		}
		if(tabName.equals("liquidity")){
			map.put("liquidityQtyFilters", qtyFilters);
			map.put("liquidityQuantities", quantities);
		}
		if(tabName.equals("history")){
			map.put("historyQtyFilters", qtyFilters);
			map.put("historyQuantities", quantities);
		}
		if(tabName.equals("stats")){
			map.put("statsQtyFilters", qtyFilters);
			map.put("statsQuantities", quantities);
		}
		if(tabName.equals("crawls")){
			map.put("crawlsQtyFilters", qtyFilters);
			map.put("crawlsQuantities", quantities);
		}
		if(tabName.equals("filters")){
			map.put("filtersQtyFilters", qtyFilters);
			map.put("filtersQuantities", quantities);
		}
	}
	
		
	/**
	 * Index History Page. Used by the flash chart.
	 */
	public ModelAndView loadIndexHistory(HttpServletRequest request,
			   HttpServletResponse response) throws ServletException, IOException {
		try {
		String username = (String)request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);
		String catScheme = request.getParameter("catScheme");
		int eventId = Integer.parseInt(request.getParameter("eventId"));
		Event event = DAORegistry.getEventDAO().get(eventId);
		if((catScheme == null || catScheme.isEmpty()) && event.getVenueCategory()!=null ){
//			catScheme = Categorizer.getCategoryGroupsByEvent(eventId).get(0);
			catScheme = event.getVenueCategory().getCategoryGroup();
		}
		
		
		String categorySymbol = preferenceManager.getPreferenceValue(username, "browseTicketCategory");
		List<Integer> categoryIds = new ArrayList<Integer>();
		if (categorySymbol == null) {
			categorySymbol = ""; 
		}
		if (categorySymbol.equals("")) {
			categoryIds.add(Category.ALL_ID);
		} else if(categorySymbol.length() >= 3) {
			if (categorySymbol.substring(0,3).equals("ALL")) {
				categoryIds.add(Category.ALL_ID);
			} else if (categorySymbol.substring(0,3).equals("CAT")) {
				categoryIds.add(Category.CATEGORIZED_ID);	
			}
		} else if(categorySymbol.length() >= 5) {
			if (categorySymbol.substring(0,5).equals("UNCAT")) {
				categoryIds.add(Category.UNCATEGORIZED_ID);		
			} 
		} else {
			String[] tokens = categorySymbol.split(",");
			for(String cat: tokens) {
				cat = cat.trim();
				if(!cat.equals("ALL") && !cat.equals("CAT") && !cat.equals("UNCAT") && !cat.equals("")){
					Category category = DAORegistry.getCategoryDAO().getCategoryByVenueCategoryIdAndCategorySymbol(event.getVenueCategory().getId(),cat);
					if(category != null){
						categoryIds.add(category.getId());
					}
				}
			}
		}
		
		ArrayList<Integer> quantities = new ArrayList<Integer>();
		// quantity filter
		String quantityString = preferenceManager.getPreferenceValue(username, "browseTicketQuantity");
		try {
			if (!quantityString.contains("ALL")) {
				for (String q: quantityString.split(",")) {
					quantities.add(Integer.valueOf(q));
				}
			}
		} catch (Exception e) {
			logger.warn("Error parsing quantities: " + quantityString + " got to: " + quantities);
		}
		
		String section = preferenceManager.getPreferenceValue(username, "browseTicketSection");
		String row = preferenceManager.getPreferenceValue(username, "browseTicketRow");
		
		// site ids
		Map<String, Boolean> siteFilters = new HashMap<String, Boolean>();
		String[] preferenceSiteIds;
		if (preferenceManager.getPreferenceValue(username, "browseTicketSites") == null) {
			preferenceSiteIds = Constants.getInstance().getSiteIds();
		} else {
			preferenceSiteIds = preferenceManager.getPreferenceValue(username, "browseTicketSites").split(",");
		}
		
		int checkSiteIdCount = 0;
		for (String siteId: Constants.getInstance().getSiteIds()) {
			String checkedString = request.getParameter(siteId + "_checkbox");
			// check if the site is in the preference
			checkedString = "false";
			for(String preferenceSiteId: preferenceSiteIds) {
				if (preferenceSiteId.equals(siteId)) {
					checkedString = "true";
					break;
				}
			}
			boolean checked = Boolean.parseBoolean(checkedString);
			
			if (checked) {
				checkSiteIdCount++;
				siteFilters.put(siteId, checked);
			}
			
		}

		String[] siteIds = new String[checkSiteIdCount];		
		siteFilters.keySet().toArray(siteIds);
		
		// duplicate filter
		String removeDuplicatePolicyString = request.getParameter("removeDuplicatePolicy");
		if (removeDuplicatePolicyString == null) {
			removeDuplicatePolicyString = preferenceManager.getPreferenceValue(username, "browseTicketRemoveDuplicatePolicy", "MANUAL");
		}

		if (removeDuplicatePolicyString == null) {
			removeDuplicatePolicyString = "MANUAL";
		}
		
		RemoveDuplicatePolicy removeDuplicatePolicy = RemoveDuplicatePolicy.valueOf(removeDuplicatePolicyString);

		Date startDate = new Date(new Date().getTime() - 30L * 24L * 3600L * 1000L); 
		
		Double startPrice = null;
		if(preferenceManager.getPreferenceValue(username, "startPrice") != null && !preferenceManager.getPreferenceValue(username, "startPrice").isEmpty()){
			startPrice = Double.parseDouble(preferenceManager.getPreferenceValue(username, "startPrice"));
		}
		Double endPrice = null;
		if(preferenceManager.getPreferenceValue(username, "endPrice") != null && !preferenceManager.getPreferenceValue(username, "endPrice").isEmpty()){
			endPrice = Double.parseDouble(preferenceManager.getPreferenceValue(username, "endPrice"));
		}
		String show = preferenceManager.getPreferenceValue(username, "show");
		String priceType = null;
		if(preferenceManager.getPreferenceValue(username, "wholesale") != null && !preferenceManager.getPreferenceValue(username, "wholesale").isEmpty()){
			boolean wholesale = Boolean.parseBoolean(preferenceManager.getPreferenceValue(username, "wholesale"));
			if(wholesale){
				priceType="wholesale";
			}
		}
		List<Ticket> origTickets = new ArrayList<Ticket>();
		List<Ticket> filteredTickets = new ArrayList<Ticket>();
		List<EbayInventoryGroup> filteredZones = new ArrayList<EbayInventoryGroup>();
		List<Ticket> hTickets = new ArrayList<Ticket>();
		Collection<Ticket> tickets = new ArrayList<Ticket>();
		
		Long loadCategoriesStartTime = System.currentTimeMillis();
//		Event event = DAORegistry.getEventDAO().get(eventId);
		Collection<Category> categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(event.getVenueCategory().getId());
		Map<String, Long>  timeStats =new HashMap<String, Long>();
		timeStats.put("loadCategories", System.currentTimeMillis() - loadCategoriesStartTime);
		
		Long loadTicketsStartTime = System.currentTimeMillis();
		if ("completed".equals(show)) {
			// show completed  ticket					
			tickets = DAORegistry.getTicketDAO().getAllCompletedTicketsByEvent(eventId);
		} else {
			// show active ticket
			tickets = DAORegistry.getTicketDAO().getAllActiveTicketsByEvent(eventId);
		}

		timeStats.put("loadTickets", System.currentTimeMillis() - loadTicketsStartTime);

		Long preAssignCategoriesStartTime = System.currentTimeMillis();		
		TicketUtil.preAssignCategoriesToTickets(tickets, categories); 
		timeStats.put("preAssignCategories", System.currentTimeMillis() - preAssignCategoriesStartTime);
		
		List<DefaultPurchasePrice> defaultPurchasePrices = DAORegistry.getDefaultPurchasePriceDAO().getAllDefaultTourPrice();
		Map<String, DefaultPurchasePrice> defaultTourPriceMap = new HashMap<String, DefaultPurchasePrice>();
		if(defaultPurchasePrices!=null){
			for(DefaultPurchasePrice defaultTourPrice:defaultPurchasePrices){
				String key = defaultTourPrice.getExchange() + "-" + (defaultTourPrice.getTicketType()==null?"REGULAR":defaultTourPrice.getTicketType());
				defaultTourPriceMap.put(key, defaultTourPrice);
			}
		}
		
		Map<String, ManagePurchasePrice> tourPriceMap = managePurchasePriceMap.get(event.getArtistId());
		Long time = managePurchasePriceTimeMap.get(event.getArtistId());
		if(tourPriceMap==null){
			tourPriceMap= new HashMap<String, ManagePurchasePrice>();
		}
		Long now =new Date().getTime();
		if(time==null || now-time>UPDATE_FREQUENCY){
			List<ManagePurchasePrice> list = DAORegistry.getManagePurchasePriceDAO().getAllByManagePurchasePriceByArtistId(event.getArtistId());
			for(ManagePurchasePrice tourPrice:list){
				String mapKey = tourPrice.getExchange()+ "-" + (tourPrice.getTicketType());
				tourPriceMap.put(mapKey,tourPrice);
			}
			managePurchasePriceMap.put(event.getArtistId(),tourPriceMap);
			managePurchasePriceTimeMap.put(event.getArtistId(),now);
		}
		
		TicketUtil.getFilteredTickets(new Integer(eventId), categoryIds, quantities, siteFilters, section, row, //show, 
				origTickets, filteredTickets, hTickets,tickets,hTickets,categories, catScheme, true, startPrice, endPrice, priceType, removeDuplicatePolicy, timeStats,null,event.getVenueCategory(),defaultTourPriceMap,tourPriceMap,null);		
		for (Stat stat: StatHelper.getEventCategorySiteQuantityLotStats(startDate, 30, filteredTickets, catScheme)) {
			String line = String.format("%1d,%2.2f,%3.2f,%4.2f,%5.2f,%6.2f,%7d,%8d,%9d,%10d,%11d,%12.2f\n",
					stat.getDate().getTime(), 
					stat.getAverage(),
					stat.getMedian(),
					stat.getMinPrice(), 
					stat.getMaxPrice(),
					stat.getDeviation(),
					stat.getCount(),
					stat.getMinQuantity(),
					stat.getMaxQuantity(),
					stat.getInTicketCount(),
					stat.getOutTicketCount(),
					stat.getLiquidityRatio());
			response.getOutputStream().write(line.getBytes());			
		}
	} catch (Exception e) {
		System.out.println("Caught exception in Charting: " + e);
		e.printStackTrace();
	}
		return null;
	}

	private void handleMarkAdmitOne(String action, String ticketIds, Integer eventId) {

		try {
				for(String ticketIdStr: ticketIds.split(",")) {
					Ticket thisTicket = DAORegistry.getTicketDAO().get(Integer.parseInt(ticketIdStr));
					if (action.equals("admit")) {
						AdmitoneTicketMark mark = DAORegistry.getAdmitoneTicketMarkDAO().get(thisTicket.getId());
						if (mark == null) {
							mark = new AdmitoneTicketMark(eventId, thisTicket.getId(), AdmitoneTicketMarkType.MARK);
							DAORegistry.getAdmitoneTicketMarkDAO().save(mark);
						} else if (mark.getType().equals(AdmitoneTicketMarkType.UNMARK)) {
							mark.setType(AdmitoneTicketMarkType.MARK);
							DAORegistry.getAdmitoneTicketMarkDAO().update(mark);
						}
					} else {
						AdmitoneTicketMark mark = DAORegistry.getAdmitoneTicketMarkDAO().get(thisTicket.getId());
						if (mark == null) {
							mark = new AdmitoneTicketMark(eventId, thisTicket.getId(), AdmitoneTicketMarkType.UNMARK);
							DAORegistry.getAdmitoneTicketMarkDAO().save(mark);
						} else if (mark.getType().equals(AdmitoneTicketMarkType.MARK)) {
							mark.setType(AdmitoneTicketMarkType.UNMARK);
							DAORegistry.getAdmitoneTicketMarkDAO().update(mark);
						}						
					}
				}
		} catch (Exception e) {
			System.out.println("Caught Exception waiting for scheduler in ViewController.markAdmitOne");
		}

	}
	
	private Integer getAvailableTicketSeats(VenueCategory venueCategory, List<Integer> categoryIds) {
		
		Long startTime = System.currentTimeMillis();
		
		Collection<Category> categories = null;
//		Event event = DAORegistry.getEventDAO().get(eventId);
		if ((categoryIds.get(0).equals(Category.ALL_ID) || categoryIds.get(0).equals(Category.CATEGORIZED_ID)) && venueCategory!=null) {
			 categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(venueCategory.getId());
		} else if (categoryIds.get(0).equals(Category.UNCATEGORIZED_ID)) {
			return 0;							
		} else {
			categories = new ArrayList<Category>();
			for(Integer catId : categoryIds){
				categories.add(DAORegistry.getCategoryDAO().get(catId));		
			}
		}
		int available = 0;
		for(Category cat : categories){
			if(cat==null){
				continue;
			}
			Integer catSeatQty = cat.getSeatQuantity();
			if(catSeatQty != null) {
				available += catSeatQty;
			}
		}
		
		System.out.println("getAvailableTicketSeats took " + (System.currentTimeMillis() - startTime));
		
		return new Integer(available);
	}
	
	public ModelAndView loadDownloadTicketsCsvPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		String username = (String)request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);

		Map<String, Long> timeStats = new HashMap<String, Long>();
		
		Boolean reset = "1".equals(request.getParameter("reset"));
		int eventId = Integer.parseInt(request.getParameter("eventId"));	
		
		Event event = DAORegistry.getEventDAO().get(eventId);
		
		String view = "compact";
		
		Long handlePreferencesStartTime = System.currentTimeMillis();			
		Map<String, Object> map = handlePreferences(request);
		timeStats.put("handlePreferences", System.currentTimeMillis() - handlePreferencesStartTime);		
		
		/*updateLastBrowsedEvent(username, eventId);*/

		/*
		 * View filter
		 */

		boolean multisort = (Boolean)map.get("multisort");
		
		String categorySymbol = (String)map.get("categorySymbol");	
		String categorySymbols = (String)map.get("categorySymbols");		
		List<Integer> categoryIds = (List<Integer>)map.get("categoryIds");
		Map<String, Boolean> catFilters = (Map<String, Boolean>)map.get("catFilters");
		String catScheme = (String)map.get("catScheme");
		if(catScheme == null || catScheme.isEmpty()){
//			catScheme = Categorizer.getCategoryGroupsByEvent(eventId).get(0); TODO CAT
			catScheme = event.getVenueCategory().getCategoryGroup();
		}
		String section = (String)map.get("section");
		String row = (String)map.get("row");
		Double startPrice = (Double)map.get("startPrice");
		Double endPrice = (Double)map.get("endPrice");
		Map<String, Boolean> siteFilters = (Map<String, Boolean>)map.get("siteFilters");
		ArrayList<Integer> quantities = (ArrayList<Integer>)map.get("quantities");
		RemoveDuplicatePolicy removeDuplicatePolicy = (RemoveDuplicatePolicy)map.get("removeDuplicatePolicy");
		int[] multisortFields = (int[])map.get("multisortFields");
		Map<String, Boolean> qtyFilters = (Map<String, Boolean>)map.get("qtyFilters");		
		String show = (String)map.get("show");
		boolean showDuplicates = (Boolean) map.get("showDuplicates");
		Integer numTicketRows = (Integer) map.get("numTicketRows");
		String priceType = (String) map.get("priceType");
								
		Long startTimeMarkDuplicate = System.currentTimeMillis();
		handleMarkDuplicates(request, eventId);
		timeStats.put("handleMarkDuplicates", System.currentTimeMillis() - startTimeMarkDuplicate);		

		List<Ticket> filteredTickets = new ArrayList<Ticket>();
		
		Long filteredTicketsStartTime = System.currentTimeMillis();
		List<Ticket> origTickets = new ArrayList<Ticket>();
		
		Collection<Ticket> tickets = new ArrayList<Ticket>();
		
		Long loadCategoriesStartTime = System.currentTimeMillis();
//		Event event = DAORegistry.getEventDAO().get(eventId);
		Collection<Category> categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(event.getVenueCategory().getId());
		timeStats.put("loadCategories", System.currentTimeMillis() - loadCategoriesStartTime);
		
		Long loadTicketsStartTime = System.currentTimeMillis();
		if ("completed".equals(show)) {
			// show completed  ticket					
			tickets = DAORegistry.getTicketDAO().getAllCompletedTicketsByEvent(eventId);
		} else {
			// show active ticket
			tickets = DAORegistry.getTicketDAO().getAllActiveTicketsByEvent(eventId);
		}

		timeStats.put("loadTickets", System.currentTimeMillis() - loadTicketsStartTime);

		Long preAssignCategoriesStartTime = System.currentTimeMillis();		
		TicketUtil.preAssignCategoriesToTickets(tickets, categories); 
		timeStats.put("preAssignCategories", System.currentTimeMillis() - preAssignCategoriesStartTime);
		
//		Collection<Ticket> hTickets = DAORegistry.getHistoricalTicketDAO().getAllTicketsByEvent(eventId);
		Collection<Ticket> hTickets = new ArrayList<Ticket>();
		Collection<Ticket> tempHTickets = DAORegistry.getTicketDAO().getAllCompletedTicketsByEvent(eventId);
		if(tempHTickets != null) {
			hTickets.addAll(tempHTickets);
		}

		TicketUtil.preAssignCategoriesToTickets(hTickets, categories);
		
		List<DefaultPurchasePrice> defaultTourPrices = DAORegistry.getDefaultPurchasePriceDAO().getAllDefaultTourPrice();
		Map<String, DefaultPurchasePrice> defaultTourPriceMap = new HashMap<String, DefaultPurchasePrice>();
		if(defaultTourPrices!=null){
			for(DefaultPurchasePrice defaultTourPrice:defaultTourPrices){
				String key = defaultTourPrice.getExchange() + "-" + (defaultTourPrice.getTicketType()==null?"REGULAR":defaultTourPrice.getTicketType());
				defaultTourPriceMap.put(key, defaultTourPrice);
			}
		}
		
		Map<String, ManagePurchasePrice> tourPriceMap = managePurchasePriceMap.get(event.getArtistId());
		Long time = managePurchasePriceTimeMap.get(event.getArtistId());
		if(tourPriceMap==null){
			tourPriceMap= new HashMap<String, ManagePurchasePrice>();
		}
		Long now =new Date().getTime();
		if(time==null || now-time>UPDATE_FREQUENCY){
			List<ManagePurchasePrice> list = DAORegistry.getManagePurchasePriceDAO().getAllByManagePurchasePriceByArtistId(event.getArtistId());
			for(ManagePurchasePrice tourPrice:list){
				String mapKey = tourPrice.getExchange()+ "-" + (tourPrice.getTicketType());
				tourPriceMap.put(mapKey,tourPrice);
			}
			managePurchasePriceMap.put(event.getArtistId(),tourPriceMap);
			managePurchasePriceTimeMap.put(event.getArtistId(),now);
		}
		
		TicketUtil.getFilteredTickets(eventId, categoryIds, quantities, siteFilters, section, row, //show, 
				origTickets, filteredTickets, null, tickets,hTickets,categories,catScheme, true, startPrice, endPrice, priceType, removeDuplicatePolicy, timeStats,null,event.getVenueCategory(),defaultTourPriceMap,tourPriceMap,null);
		
		List<WebTicketRow> newFilteredWebTicketRows = TicketUtil.getWebTicketRows(origTickets, filteredTickets, showDuplicates);
		
		StringBuffer stringBuffer = new StringBuffer();
		for (WebTicketRow ticketRow: newFilteredWebTicketRows) {
			String category = ticketRow.getCategorySymbol();
			if (category == null) {
				category = "";
			}
			stringBuffer.append(ticketRow.getQuantity() + "," + category + "," + ticketRow.getSection()
					   + "," + ticketRow.getRow() + "," + ticketRow.getAdjustedCurrentPrice() + "," + ticketRow.getCurrentPrice()
					   + "," + dateFormat.format(ticketRow.getInsertionDate()) + "," + dateFormat.format(ticketRow.getLastUpdate()) + "\n");
		}

		// output the result
//		Date today = new Date();
// 		DateFormat format = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
		
		OutputStream os = response.getOutputStream();
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		String dateStr="TBD";
		if(event.getDate()!=null){
			dateStr=dateFormat.format(event.getDate());
		}
		response.setHeader("Content-disposition",
				"attachment; filename=tickets_" + URLEncoder.encode(username) + "_" + URLEncoder.encode(event.getName()) 
				+ dateStr + ".csv");
		os.write(stringBuffer.toString().getBytes());
		return null;
	}
	

	public ModelAndView loadQuotesPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String username = (String)request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);
		String creator = (String)request.getParameter("creator");
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		ModelAndView mav = null;
		String action = request.getParameter("action");		
		mav = new ModelAndView("page-quotes");
		
		String from = DAORegistry.getPropertyDAO().get("quotes.smtp.from").getValue();
		
		Collection<String> creators = DAORegistry.getQuoteDAO().getQuoteUsernames();
		if (creators == null || creators.isEmpty()) {
			return mav;
		}

		if (creator == null) {
			if (creators.contains(username)) {
				creator = username;
			} else {
				creator = creators.iterator().next();
			}
		}
		
		Collection<QuoteCustomer> customers = DAORegistry.getQuoteCustomerDAO().getCustomersByUsername(creator);

		if (customers == null || customers.isEmpty()) {
			return mav;
		}
		
		
		QuoteCustomer customer = null;
		if (request.getParameter("customerId") == null) {
			Quote quote = DAORegistry.getQuoteDAO().getMostRecentQuote(creator);
			customer = quote.getCustomer();
		} else {
			customer = DAORegistry.getQuoteCustomerDAO().get(Integer.valueOf(request.getParameter("customerId")));
			if (DAORegistry.getQuoteDAO().getQuotesByUserAndCustomer(creator, customer.getId()).isEmpty()) {
				Quote quote = DAORegistry.getQuoteDAO().getMostRecentQuote(creator);
				customer = quote.getCustomer();
			}
		}

		Collection<java.sql.Date> dates = DAORegistry.getQuoteDAO().getQuotesDatesByCustomer(customer.getId());
		java.sql.Date date = null;
		if (request.getParameter("date") == null && (action == null || !action.startsWith("delete"))) {
			date = dates.iterator().next();
		} else {
			try {
				date = new java.sql.Date(dateFormat.parse(request.getParameter("date")).getTime());
				if (!dates.contains(date)) {
					date = dates.iterator().next();
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return mav;
			} 
		}
		
		Collection<Quote> quotes = DAORegistry.getQuoteDAO().getQuotesByCustomerAndDate(customer.getId(), date);
		String quoteIdStr = request.getParameter("quoteId");
		Quote quote;
		if (quoteIdStr == null) {
			quote = quotes.iterator().next();
		} else {
			quote = DAORegistry.getQuoteDAO().get(Integer.valueOf(quoteIdStr));
			if (!quote.getCustomer().getName().equals(customer.getName())) {
				quote = quotes.iterator().next();
			}
		}
		List<Integer> ids = new ArrayList<Integer>();
		for(Quote q:quotes){
			ids.add(q.getId());
			
		}
		//Collection<TicketQuote> ticketQuotes = new ArrayList<TicketQuote>();
		Collection<TicketQuote> ticketQuotes = null;
		Collection<TicketQuote>  zoneQuotes= new ArrayList<TicketQuote>();
		if(quoteIdStr != null){
			ticketQuotes = DAORegistry.getTicketQuoteDAO().getTicketQuotesByQuoteId(quote.getId());
		}else{
			ticketQuotes = DAORegistry.getTicketQuoteDAO().getTicketQuotesByQuoteIds(ids);
		}
		if(ticketQuotes!=null && !ticketQuotes.isEmpty()){
		for(TicketQuote tq: ticketQuotes){
			if(tq.getZone()!=null && !tq.getZone().equalsIgnoreCase("")){
				zoneQuotes.add(tq);
			}
		}		
		}
		Map<Integer, QuotesEvent> quotesEventMap = new HashMap<Integer, QuotesEvent>();
		String defaultSubject = "Your Ticket Quote ";
		String lastEventName="";
		for (QuotesEvent quotesEvent: TicketQuotesUtil.getQuotesEvent(ticketQuotes)) {
			for(TicketQuote tQuote:quotesEvent.getQuotes()){
				if(tQuote.getFileName() != null){
					quotesEvent.setSavedFile(tQuote.getFileName().replaceAll("\\\\+", "\\\\\\\\"));
					break;
				}
				
			}
			quotesEventMap.put(quotesEvent.getEvent().getId(), quotesEvent);
			lastEventName = quotesEvent.getEvent().getName().trim();
			/*if(TicketQuotesUtil.getQuotesEvent(ticketQuotes).size() == 1){
			defaultSubject +=" - "+ quotesEvent.getEvent().getName() +" ("+quotesEvent.getEvent().getFormatedDate()+")";
			}*/
		}
		if(quotesEventMap.size()>1){
			defaultSubject += " - Multiple Events";
		}else{
			defaultSubject += " - " + lastEventName;
		}
		if (action != null) {
			try {
				if (action.equalsIgnoreCase("deleteQuotes")) {
					int customerId = Integer.valueOf(request.getParameter("customerId"));
					DAORegistry.getQuoteDAO().delete(quote);
					return new ModelAndView(new RedirectView("Quotes?info=" + customer.getName() + "'s quotes have been deleted."));
				} else if (action.equalsIgnoreCase("deleteCustomer")) {
					int customerId = Integer.valueOf(request.getParameter("customerId"));
					DAORegistry.getQuoteCustomerDAO().deleteById(customerId);
					return new ModelAndView(new RedirectView("Quotes?info=" + customer.getName() + " has been deleted."));
				} else if (action.equalsIgnoreCase("updateEmail")) {
//					int customerId = Integer.valueOf(request.getParameter("customerId"));
//					java.sql.Date date = new java.sql.Date(dateFormat.parse(request.getParameter("date")).getTime());
//					String email = request.getParameter("email");
//					TicketQuoteCustomer customer = DAORegistry.getTicketQuoteCustomerDAO().get(customerId);
//					customer.setEmail(email);
//					DAORegistry.getTicketQuoteCustomerDAO().update(customer);
				} else if (action.toLowerCase().startsWith("send") || action.toLowerCase().startsWith("preview")) {
					User user = DAORegistry.getUserDAO().getUserByUsername(username);
					String to = request.getParameter("to");
					String bcc = request.getParameter("bcc");
					//customer.setEmail(to);
					DAORegistry.getQuoteCustomerDAO().update(customer);
					String subject = request.getParameter("subject");
					String header = request.getParameter("header");
					String footer = request.getParameter("footer");
					String sendingZoneData = request.getParameter("sendingZoneData");
					String sendingTicketData = request.getParameter("sendingTicketData");
					if (footer == null) {
						footer = "";
					}
					
					for (TicketQuote ticketQuote: ticketQuotes) {
						String markPrice = request.getParameter("markedPrice_" + ticketQuote.getId());
						if(markPrice!=null && !markPrice.equals("")){
							markPrice = markPrice.replaceAll(",", "");
							ticketQuote.setMarkedPrice(Double.valueOf(markPrice));
						}
						
						Integer remQty = ticketQuote.getTicket() != null ? ticketQuote.getTicket().getRemainingQuantity() : 0;
						String zoneMarkPrice = request.getParameter("zoneMarkedPrice_" + ticketQuote.getId());
						if(zoneMarkPrice!=null && !zoneMarkPrice.equals("")){
							zoneMarkPrice = zoneMarkPrice.replaceAll(",", "");
							ticketQuote.setZoneMarkedPrice(Double.valueOf(zoneMarkPrice));
						}
						
											
						if(request.getParameter("qty_" + ticketQuote.getId()) != null && (remQty >= Integer.valueOf(request.getParameter("qty_" + ticketQuote.getId())))){
						 ticketQuote.setQty(Integer.valueOf(request.getParameter("qty_" + ticketQuote.getId())));
						}
					}
					DAORegistry.getTicketQuoteDAO().updateAll(ticketQuotes);
					
			        MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest)request;
			        multipartHttpServletRequest.getFileNames();
			        ArrayList<Integer> eventIdList = new ArrayList<Integer>();
			        ArrayList<MailAttachment> mailAttachmentsList = new ArrayList<MailAttachment>();	
			        Collection<TicketQuote> tQuotes = new ArrayList<TicketQuote>();
			        for (Object entryObj: multipartHttpServletRequest.getFileMap().entrySet()) {
			        	Map.Entry<String, MultipartFile> entry = (Map.Entry<String, MultipartFile>)entryObj; 
			        	Integer eventId = Integer.parseInt(entry.getKey().replaceAll("map_", ""));
			        	MultipartFile file = entry.getValue();
			        	if (!file.isEmpty()) {
			        		eventIdList.add(eventId);
				        	quotesEventMap.get(eventId).setBoundToImage(true);
				        	for(TicketQuote tQuote:quotesEventMap.get(eventId).getQuotes()){
				        		tQuote.setFileName("S:\\TMATIMAGESFINAL\\"+file.getOriginalFilename());
				        		tQuotes.add(tQuote);
				        	}
				        	String imageType = file.getOriginalFilename().replaceAll(".*\\.", "");
				        	quotesEventMap.get(eventId).setImageType(imageType);
				        	mailAttachmentsList.add(new MailAttachment(IOUtils.toByteArray(file.getInputStream()), "image/" + imageType, DAORegistry.getEventDAO().get(eventId).getName()+ "." + imageType));
			        	}
			        }
			        for(QuotesEvent q:quotesEventMap.values()){
			        	String fileName = request.getParameter("map_"+q.getEvent().getId());
			        	fileName = fileName.replaceAll("\\\\\\\\+", "\\\\");
			        	if(fileName != null && !"".equals(fileName)){
			        	File file = new File(fileName);
			        	if (file.exists()) { 
			        		boolean flag = true;
				        	q.setBoundToImage(true);
				        	String imageType = file.getName().replaceAll(".*\\.", "");
				        	q.setImageType(imageType);
				        	if(eventIdList.contains(q.getEvent().getId())){
				        			flag = false;				        			
				        		}
				        	
				        	if(flag){
				        	mailAttachmentsList.add(new MailAttachment(IOUtils.toByteArray(new FileInputStream(file)), "image/" + imageType, q.getEvent().getName() + "." + imageType));
				        	q.setSavedFile(fileName);
				        	for(TicketQuote tQuote:q.getQuotes()){
				        		tQuote.setFileName(fileName);
				        		tQuotes.add(tQuote);
				        	}
				        	}
			        	}
			        	}
			        }
			        DAORegistry.getTicketQuoteDAO().saveOrUpdateAll(tQuotes);
			        MailAttachment[] mailAttachments = new MailAttachment[mailAttachmentsList.size() + 1];
			        int i = 1;
			        for (MailAttachment mailAttachment: mailAttachmentsList) {
			        	mailAttachments[i++] = mailAttachment; 
			        }

					Map<String, Object> map = new HashMap<String, Object>();
					map.put("customer", customer);
					map.put("user", DAORegistry.getUserDAO().getUserByUsername(username));
					map.put("header", header.replaceAll("\n", "<br/>\n").replaceAll("%CUSTOMER%", customer.getName()));
					map.put("footer", footer.replaceAll("\n", "<br/>\n"));
					map.put("quotesEvents", quotesEventMap.values());
				    map.put("date", new DateTool());
				    map.put("number", new NumberTool());
				    map.put("mapQuoteReference", quote.getReferral());
				    map.put("sendingZoneData", sendingZoneData);
				    map.put("sendingTicketData", sendingTicketData);
			        String imageFilename = request.getSession().getServletContext().getRealPath("") + "/images/admitone_logo.gif";
					mailAttachments[0] = new MailAttachment(IOUtils.toByteArray(new FileInputStream(imageFilename)), "image/gif", "admitone_logo.gif");
					String smtpHost = DAORegistry.getPropertyDAO().get("quotes.smtp.host").getValue();
					Integer smtpPort = Integer.valueOf(DAORegistry.getPropertyDAO().get("quotes.smtp.port").getValue());
					String smtpUsername = DAORegistry.getPropertyDAO().get("quotes.smtp.username").getValue();
					String smtpPassword = DAORegistry.getPropertyDAO().get("quotes.smtp.password").getValue();
					String smtpSecureConnection = DAORegistry.getPropertyDAO().get("quotes.smtp.secure.connection").getValue();
					Boolean smtpAuth = Boolean.valueOf(DAORegistry.getPropertyDAO().get("quotes.smtp.auth").getValue());
					String transportProtocol = "";
					String info= "Email not sent.";
					
					if(action.toLowerCase().startsWith("send") && to != null && !"".equals(to)){						
					mailManager.sendMailNow(smtpHost, smtpPort, transportProtocol, smtpSecureConnection, smtpAuth, smtpUsername, smtpPassword, "", from, to, null, bcc, subject, "mail-quotes.html", map, "text/html", mailAttachments);
					quote.setSentDate(new Date());
					quote.setSentTo(to);
					info= "Email sent.";
					}
					DAORegistry.getQuoteDAO().saveOrUpdate(quote);
					return new ModelAndView(new RedirectView("Quotes?creator=" + creator + "&customerId=" + customer.getId() + "&date=" + dateFormat.format(date) + "&quoteId=" + quote.getId() + "&info="+info));
				}
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
						
		String customerWithCapFirstLetter = customer.getName().substring(0, 1).toUpperCase() + customer.getName().substring(1); 
		mav.addObject("quotesHeader", DAORegistry.getPropertyDAO().get("quotes.header").getValue().replaceAll("%CUSTOMER%", customerWithCapFirstLetter));
		//mav.addObject("quotesHeader","Thank you for choosing Admit One. The following quotes are for your review. Please let me know if you have any questions.");
		mav.addObject("quotesFooter", DAORegistry.getPropertyDAO().get("quotes.footer").getValue().replaceAll("%CUSTOMER%", customerWithCapFirstLetter));
		mav.addObject("from", from);
		mav.addObject("user", DAORegistry.getUserDAO().getUserByUsername(username));
		mav.addObject("customers", customers);
		mav.addObject("creator", creator);
		mav.addObject("creators", creators);
		mav.addObject("customer", customer);
		mav.addObject("quote", quote);
		mav.addObject("quoteDates", dates);
		mav.addObject("quotes", quotes);
		mav.addObject("zoneQuotes", zoneQuotes);		
		mav.addObject("quotesEvents", quotesEventMap.values());
		mav.addObject("subject", defaultSubject);
		return mav;
	}
	
	public ModelAndView loadViewQuotesPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String username = (String)request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);
		String creator = (String)request.getParameter("creator");
		
		ModelAndView mav = new ModelAndView("page-view-quotes");
		listquotes(request, username, creator, mav,true);
		return mav;
	}
	
	public ModelAndView loadListQuotesPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String username = (String)request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);
		String creator = (String)request.getParameter("creator");
				
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		ModelAndView mav = new ModelAndView("page-list-quotes");
		listquotes(request, username, creator, mav);
		
		return mav;
	}
	public void listquotes(HttpServletRequest request, String username,
			String creator, ModelAndView mav) {
		listquotes(request, username,
				creator,  mav,false);
		
	}
	public void listquotes(HttpServletRequest request, String username,
			String creator, ModelAndView mav,boolean both) {
		String quoteType = (String)request.getParameter("type");
		mav.addObject("type", quoteType);
		if(request.getParameter("action") != null && "delete".equals(request.getParameter("action"))){
			String ticketQuoteId = request.getParameter("ticketQuoteId");
			
			if(ticketQuoteId != null && !"".equals(ticketQuoteId)){
				TicketQuote ticketQuote = DAORegistry.getTicketQuoteDAO().get(Integer.valueOf(ticketQuoteId));
				List<Integer> ids = new ArrayList<Integer>();
				if(ticketQuote != null){
				Integer quoteId = ticketQuote.getQuote().getId();	
				Collection<TicketQuote> ticketQuotes = DAORegistry.getTicketQuoteDAO().getTicketQuotesByQuoteId(ticketQuote.getQuote().getId());
				for(TicketQuote tq:ticketQuotes){
					if(tq.getTicket().getEvent().getId().equals(ticketQuote.getTicket().getEvent().getId())){
						ids.add(tq.getId())	;
					}
				}
				DAORegistry.getTicketQuoteDAO().deleteByIds(ids);
				DAORegistry.getQuoteDAO().deleteById(quoteId);
				}
			}
		}
		Collection<String> creators = DAORegistry.getQuoteDAO().getQuoteUsernames();
		QuoteCustomer customer = null;
		if (creator == null) {
			if (creators.contains(username)) {
				creator = username;
			} else {
				creator = creators.iterator().next();
			}
		}
		Collection<QuoteCustomer> customers = null;
		
		if(creator.equals("ALL")){
			customers = DAORegistry.getQuoteCustomerDAO().getAll();
		}else{
			customers = DAORegistry.getQuoteCustomerDAO().getCustomersByUsername(creator);
		}
		mav.addObject("customers", customers);
		List<Quote> quotes = new ArrayList<Quote>();
		//List<Quote> zoneQuotes = new ArrayList<Quote>();
		String customerParam = request.getParameter("customerId");
		if(customerParam == null)
			customerParam = "ALL";
		ArrayList<String> emails = new ArrayList<String>();		
		if(customerParam != null && !"ALL".equals(customerParam)){
			customer = DAORegistry.getQuoteCustomerDAO().get(Integer.valueOf(customerParam));
			
				quotes = (List<Quote>) DAORegistry.getQuoteDAO().getQuotesByCustomer(Integer.valueOf(customerParam));
				emails.add(customer.getEmail());
			
		}else if(customerParam.equals("ALL")){			
				quotes = (List<Quote>) DAORegistry.getQuoteDAO().getQuotes(false);
			
		}
		if(quotes!=null) {
		for(Quote q:quotes){
			if(q.getSentTo()!= null){
			for(String emailString:q.getSentTo().split(",")){
				if(!emails.contains(emailString)){
					emails.add(emailString);
				}
			}
			}
		}
	 }
		
		quotes.removeAll(quotes);
				
		String email = request.getParameter("email");
		String status = request.getParameter("status");
		if(email == null){
			email = "ALL";
		}
		if(status == null){
			status = "1";
		}
		String fromDate = request.getParameter("fromDay");
		String fromMonth = request.getParameter("fromMonth");
		String fromYear = request.getParameter("fromYear");
		String toDate = request.getParameter("toDay");
		String toMonth = request.getParameter("toMonth");
		String toYear = request.getParameter("toYear");
		Date d = new Date();
		if(fromDate == null){
			fromDate = Integer.valueOf(d.getDate()).toString();
		}
		if(fromMonth == null){
			fromMonth = Integer.valueOf(d.getMonth()+1).toString();
		}
		if(fromYear == null){
			fromYear = Integer.valueOf(d.getYear()+1900).toString();
		}
		if(toDate == null){
			toDate = Integer.valueOf(d.getDate()).toString();
		}
		if(toMonth == null){
			toMonth = Integer.valueOf(d.getMonth()+1).toString();
		}
		if(toYear == null){
			toYear = Integer.valueOf(d.getYear()+1900).toString();
		}
		if((request.getParameter("fetch") != null && "fetch".equals(request.getParameter("fetch"))) || (request.getParameter("action") != null && "delete".equals(request.getParameter("action")))){
			try{
				String custId = request.getParameter("customerId");
				if (request.getParameter("customerId") == null) {
					quotes = (List<Quote>) DAORegistry.getQuoteDAO().getAll();
					customer = quotes.get(0).getCustomer();
				}
				if(custId != null && !"ALL".equals(custId)){
					customer = DAORegistry.getQuoteCustomerDAO().get(Integer.valueOf(custId));
					custId = customer.getId().toString();
				}
				if(custId != null && "ALL".equals(custId)){
					custId = "ALL";
				}	
			
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
				
			java.sql.Date fromQuoteDate = null;
			if(fromDate != null &&fromMonth != null && fromYear != null){
				fromQuoteDate = new java.sql.Date(dateFormat.parse(fromMonth+"/"+fromDate+"/"+fromYear).getTime());	
			}			
			
			java.sql.Date toQuoteDate = null;
			if(toDate  != null && toMonth != null && toYear != null){
				DateFormat toDateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss aa");
				toQuoteDate = new java.sql.Date(toDateFormat.parse(toMonth+"/"+toDate+"/"+toYear+" 23:59:59 PM").getTime());
			}
			
			quotes = DAORegistry.getQuoteDAO().getFilteredQuotes(creator,custId,fromQuoteDate,toQuoteDate,email,status);
						
			mav.addObject("append", true);
			mav.addObject("status",status);
			mav.addObject("email",email);
			
			}catch (Exception e) {
				// TODO: handle exception
			}
		}
		
		List<Integer> ids = new ArrayList<Integer>();		
		if(quotes != null && !quotes.isEmpty()){
		for(Quote qt:quotes){			
			ids.add(qt.getId());
		}	
		}
		
		List<TicketQuote> ticketQuotes = new ArrayList<TicketQuote>();
		if(quotes != null && quotes.size() != 0){
			ticketQuotes = DAORegistry.getTicketQuoteDAO().getTicketQuotesByQuoteIds(ids);	
		}
		
		List<TicketQuote> finalTicketQuotes = new ArrayList<TicketQuote>();
		if(ticketQuotes.size() > 0){
			finalTicketQuotes.addAll(ticketQuotes);
		}
				
		if(ticketQuotes!=null && !ticketQuotes.isEmpty()){
		for(TicketQuote ticketQuote:ticketQuotes){
			boolean flag = false;
			for(TicketQuote finalTicketQuote:finalTicketQuotes){
				if(finalTicketQuote.getTicket() 
						!= null && ticketQuote.getTicket()!= null && finalTicketQuote.getTicket().getEvent().getId().equals(ticketQuote.getTicket().getEvent().getId()) && finalTicketQuote.getQuote().getId().equals(ticketQuote.getQuote().getId())){
					flag = true ;
					break;
				}
			}
			if(flag == false){				
				finalTicketQuotes.add(ticketQuote);
			}
		}	
		}		
		mav.addObject("ticketQuotes", finalTicketQuotes);		
		mav.addObject("emails",emails);
		mav.addObject("creators", creators);
		mav.addObject("creator", creator);
		mav.addObject("customer", customer);		
		mav.addObject("quotes", quotes);
		mav.addObject("fromDate",fromDate);
		mav.addObject("fromMonth",fromMonth);
		mav.addObject("fromYear",fromYear);
		mav.addObject("toDate",toDate);
		mav.addObject("toMonth",toMonth);
		mav.addObject("toYear",toYear);
		mav.addObject("users", DAORegistry.getUserDAO().getAll());
		mav.addObject("ticketIds", (String)request.getParameter("ticketIds"));
		mav.addObject("groupIds", (String)request.getParameter("groupIds"));		
		mav.addObject("customerName", (String)request.getParameter("customer"));
		mav.addObject("quoteDate", (String)request.getParameter("quoteDate"));
		mav.addObject("markupPercentAddition", (String)request.getParameter("markup"));		
		
		
		 
	}
	
	public void deleteTicketQuote(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException {
		String ticketOuoteId = request.getParameter("ticketOuoteId");
		String message = "TicketQuote not deleted";
		if(ticketOuoteId != null){
			DAORegistry.getTicketQuoteDAO().deleteById(Integer.valueOf(ticketOuoteId));
			message ="TicketQuote deleted successfully";
		}
		IOUtils.write(message.getBytes(), response.getOutputStream());
	}
	
	public void updateTicketQuote(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException {
		String ticketQuoteId = request.getParameter("ticketQuoteId");
		String ticketQty = request.getParameter("ticketQty");
		String zone = request.getParameter("zone");
		String message = "TicketQuote is not updated";
		if(ticketQuoteId != null){
			TicketQuote quote= DAORegistry.getTicketQuoteDAO().get(Integer.valueOf(ticketQuoteId));
			if(quote!=null){
				try{
					int qty = Integer.valueOf(ticketQty);
					if(zone.equals("0")){
						quote.setQty(qty);
					}else{
						quote.setZoneQty(qty);
					}
					DAORegistry.getTicketQuoteDAO().update(quote);
					message ="TicketQuote updated successfully";
				}catch (Exception ex) {
					message ="Quantity must be whole no.";
				}
			}else{
				message ="TicketQuote is not found";
			}
		}
		IOUtils.write(message.getBytes(), response.getOutputStream());
	}
	/*public void setTicketListingCrawler(TicketListingCrawler ticketListingCrawler) {
		this.ticketListingCrawler = ticketListingCrawler;
	}*/
	
	public PreferenceManager getPreferenceManager() {
		return preferenceManager;
	}

	public void setPreferenceManager(PreferenceManager preferenceManager) {
		this.preferenceManager = preferenceManager;
	}	

	public MailManager getMailManager() {
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		this.mailManager = mailManager;
	}
	private static int compareDates(Calendar cal1, Calendar cal2) {
		if (cal1.get(Calendar.YEAR) < cal2.get(Calendar.YEAR)) {
			return -1;
		} else if (cal1.get(Calendar.YEAR) > cal2.get(Calendar.YEAR)) {
			return 1;
		}

		if (cal1.get(Calendar.MONTH) < cal2.get(Calendar.MONTH)) {
			return -1;
		} else if (cal1.get(Calendar.MONTH) > cal2.get(Calendar.MONTH)) {
			return 1;
		}

		if (cal1.get(Calendar.DATE) < cal2.get(Calendar.DATE)) {
			return -1;
		} else if (cal1.get(Calendar.DATE) > cal2.get(Calendar.DATE)) {
			return 1;
		}
		
		return 0;
	}
	
	public static void main(String[] args) {
		
		Date todate = new Date();
		Date fromDate = new Date(2012, 11, 01);
		
		Calendar cal = new GregorianCalendar();
		cal.setTime(fromDate);
		
		Calendar toCal = new GregorianCalendar();
		toCal.setTime(todate);
		
		System.out.println("D---->"+compareDates(cal, toCal));
		
		for(Date date=fromDate; compareDates(cal, toCal)< 0;){
			
			System.out.println("D---->"+date);
			
		}
	}

}
