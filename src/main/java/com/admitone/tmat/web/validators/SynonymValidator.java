package com.admitone.tmat.web.validators;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.admitone.tmat.data.Synonym;
import com.admitone.tmat.data.Venue;

public class SynonymValidator implements Validator{

	public boolean supports(Class clazz) {
		return clazz.equals(Synonym.class);
	}

	public void validate(Object command, Errors errors) {
		Synonym synonym = (Synonym)command;
		
		if (synonym == null) {
			errors.reject("error.nullpointer", "Null data received");
			return;
		} 
	}
	
}
