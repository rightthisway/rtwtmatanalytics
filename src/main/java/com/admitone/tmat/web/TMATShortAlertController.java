package com.admitone.tmat.web;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.acegisecurity.ui.webapp.AuthenticationProcessingFilter;
import org.apache.velocity.tools.generic.NumberTool;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.servlet.view.RedirectView;

import com.admitone.tmat.common.MailIdProperty;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Property;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.data.User;
import com.admitone.tmat.data.UserAlert;
import com.admitone.tmat.enums.AlertFor;
import com.admitone.tmat.enums.UserAlertStatus;
import com.admitone.tmat.enums.UserAlertType;
import com.admitone.tmat.utils.PreferenceManager;
import com.admitone.tmat.utils.mail.MailManager;
import com.admitone.tmat.web.Constants;

public class TMATShortAlertController extends MultiActionController {
	
	private PreferenceManager preferenceManager;
	private MailManager mailManager; 
	private MailIdProperty mailIdProperty;

	public MailIdProperty getMailIdProperty() {
		return mailIdProperty;
	}

	public void setMailIdProperty(MailIdProperty mailIdProperty) {
		this.mailIdProperty = mailIdProperty;
	}
	/**
	 * Add alerts page.
	 */
	public ModelAndView loadManageUserShortAlertsPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		// check if this is the page for my alert
		// or the page for "manage user alerts"
		String manageAlertsUrl;
		boolean modeAllUser = request.getRequestURI().contains("EditorManageAlerts");
		String username = (String)request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);
		
		ModelAndView mav;
		if (modeAllUser) {
			manageAlertsUrl = "EditorManageAlerts";
			mav = new ModelAndView("page-editor-manage-alerts");			
			mav.addObject("users", DAORegistry.getUserDAO().getAll());

			// the key is status + "-" + AlertFor (e.g. ACTIVE-TMAT)
			Map<String, Integer> statByKey = new HashMap<String, Integer>();

			int alertCount = 0;
			
			Collection<UserAlert> allUserAlerts = DAORegistry.getUserAlertDAO().getAll();
			for(UserAlert ua: allUserAlerts) {
				String key = ua.getUserAlertStatus() + "-" + ua.getAlertFor();
				Integer count = statByKey.get(key);
				count = (count == null)?1:(count+1);
				statByKey.put(key, count);

				String totalRowKey = ua.getUserAlertStatus() + "-TOTAL";
				Integer totalRowCount = statByKey.get(totalRowKey);
				totalRowCount = (totalRowCount == null)?1:(totalRowCount+1);
				statByKey.put(totalRowKey, totalRowCount);

				String totalColKey = "TOTAL-" + ua.getAlertFor();
				Integer totalColCount = statByKey.get(totalColKey);
				totalColCount = (totalColCount == null)?1:(totalColCount+1);
				statByKey.put(totalColKey, totalColCount);
				
				alertCount++;
			}
			
			statByKey.put("TOTAL-TOTAL", alertCount);
			mav.addObject("statByKey", statByKey);		
		} else {
			manageAlertsUrl = "MyAlerts";			
			mav = new ModelAndView("page-manage-my-short-alerts");			
		}


		String action = request.getParameter("action");
				
		
		if (action == null) {
			action = "";
		} else if (action.equalsIgnoreCase("delete")) {
			Collection<UserAlert> selectedAlerts = new ArrayList<UserAlert>();
			for(String selectedAlertId: request.getParameter("alertIds").split(",")) {
				int alertId = Integer.parseInt(selectedAlertId);
				UserAlert selectedAlert = DAORegistry.getUserAlertDAO().get(alertId);
				if(selectedAlert == null) {
					return new ModelAndView(new RedirectView(manageAlertsUrl + "?error=" + URLEncoder.encode("Invalid alert Id " + alertId)));				
				}
				if (!modeAllUser && !selectedAlert.getUsername().equals(username) && !selectedAlert.getMarketMakerUsername().equals(username)) {
					return new ModelAndView(new RedirectView(manageAlertsUrl + "?error=" + URLEncoder.encode("You cannot view another user's alert")));
				}
				selectedAlerts.add(selectedAlert);
			}
			for(UserAlert selectedAlert: selectedAlerts) {
				selectedAlert.setUserAlertStatus(UserAlertStatus.DELETED);
				DAORegistry.getUserAlertDAO().update(selectedAlert);				
			}
			return new ModelAndView(new RedirectView(manageAlertsUrl + "?info=" + URLEncoder.encode("The " + selectedAlerts.size()+ " selected alert(s) have been deleted")));
		} else if (action.equalsIgnoreCase("check")) {
			String alertIdString = request.getParameter("alertId");
			UserAlert userAlert = null;
			Integer userAlertId = null;
			if(alertIdString != null && !alertIdString.isEmpty()) {
				userAlertId = Integer.parseInt(alertIdString);
				userAlert = DAORegistry.getUserAlertDAO().get(userAlertId);			
			}			
			if(userAlert == null) {
				return new ModelAndView(new RedirectView(manageAlertsUrl + "?error=" + URLEncoder.encode("Invalid alert id")));				
			}

			if (!modeAllUser && !userAlert.getUsername().equals(username) && !userAlert.getMarketMakerUsername().equals(username)) {
				return new ModelAndView(new RedirectView(manageAlertsUrl + "?error=" + URLEncoder.encode("You cannot edit another user's alert")));
			}
			
			Collection<Ticket> alertTickets = DAORegistry.getTicketDAO().getAllTicketsMatchingUserAlert(userAlert);
			mav.addObject("userAlert", userAlert);
			mav.addObject("alertTickets", alertTickets);
		} else if (action.equalsIgnoreCase("disable")) {
			Collection<UserAlert> selectedAlerts = new ArrayList<UserAlert>();
			for(String selectedAlertId: request.getParameter("alertIds").split(",")) {
				int alertId = Integer.parseInt(selectedAlertId);
				UserAlert selectedAlert = DAORegistry.getUserAlertDAO().get(alertId);
				if(selectedAlert == null) {
					return new ModelAndView(new RedirectView(manageAlertsUrl + "?error=" + URLEncoder.encode("Invalid alert Id " + alertId)));				
				}
				if (!modeAllUser && !selectedAlert.getUsername().equals(username) && !selectedAlert.getMarketMakerUsername().equals(username)) {
					return new ModelAndView(new RedirectView(manageAlertsUrl + "?error=" + URLEncoder.encode("You cannot view another user's alert")));
				}
				selectedAlerts.add(selectedAlert);
			}
			for(UserAlert selectedAlert: selectedAlerts) {
				selectedAlert.setUserAlertStatus(UserAlertStatus.DISABLED);
				DAORegistry.getUserAlertDAO().update(selectedAlert);				
			}
			return new ModelAndView(new RedirectView(manageAlertsUrl + "?info=" + URLEncoder.encode("The " + selectedAlerts.size()+ " selected alert(s) have been disabled")));
		} else if (action.equalsIgnoreCase("enable")) {
			Collection<UserAlert> selectedAlerts = new ArrayList<UserAlert>();
			for(String selectedAlertId: request.getParameter("alertIds").split(",")) {
				int alertId = Integer.parseInt(selectedAlertId);
				UserAlert selectedAlert = DAORegistry.getUserAlertDAO().get(alertId);
				if(selectedAlert == null) {
					return new ModelAndView(new RedirectView(manageAlertsUrl + "?error=" + URLEncoder.encode("Invalid alert Id " + alertId)));				
				}
				if (!modeAllUser && !selectedAlert.getUsername().equals(username) && !selectedAlert.getMarketMakerUsername().equals(username)) {
					return new ModelAndView(new RedirectView(manageAlertsUrl + "?error=" + URLEncoder.encode("You cannot view another user's alert")));
				}
				selectedAlerts.add(selectedAlert);
			}
			for(UserAlert selectedAlert: selectedAlerts) {
				selectedAlert.setUserAlertStatus(UserAlertStatus.ACTIVE);
				DAORegistry.getUserAlertDAO().update(selectedAlert);				
			}
			return new ModelAndView(new RedirectView(manageAlertsUrl + "?info=" + URLEncoder.encode("The " + selectedAlerts.size()+ " selected alert(s) have been enabled")));
		} 
		//Shorting an alert - circles alert on shorting should become wholesale alert
		else if (action.equalsIgnoreCase("short")) {
			Collection<UserAlert> selectedAlerts = new ArrayList<UserAlert>();
			for(String selectedAlertId: request.getParameter("alertIds").split(",")) {
				int alertId = Integer.parseInt(selectedAlertId);
				UserAlert selectedAlert = DAORegistry.getUserAlertDAO().get(alertId);
				if(selectedAlert == null) {
					return new ModelAndView(new RedirectView(manageAlertsUrl + "?error=" + URLEncoder.encode("Invalid alert Id " + alertId)));				
				}
				if (!modeAllUser && !selectedAlert.getUsername().equals(username) && !selectedAlert.getMarketMakerUsername().equals(username)) {
					return new ModelAndView(new RedirectView(manageAlertsUrl + "?error=" + URLEncoder.encode("You cannot view another user's alert")));
				}
				selectedAlerts.add(selectedAlert);
			}
			for(UserAlert selectedAlert: selectedAlerts) {
				// send short mail
				String resource;
				if (selectedAlert.getAlertFor() != null && (selectedAlert.getAlertFor().equals(AlertFor.CIRCLES) || selectedAlert.getAlertFor().equals(AlertFor.CIRCLES_PORTAL))) {
					resource =  "mail-ticket-alert-short-circles.txt";
				} else {
					resource =  "mail-ticket-alert-short-tmat.txt";						
				}
				String mimeType = "text/plain";
				
				DateFormat dateFormat =	new SimpleDateFormat("MM/dd/yyyy HH:mm");

				Map<String, Object> map = new HashMap<String, Object>();
				map.put("userAlert", selectedAlert);
				map.put("now", dateFormat.format(new Date()));
				map.put("number", new NumberTool());
				map.put("serverName", Constants.getInstance().getHostName());
				
				Date expirationDate = new Date(new Date().getTime() + 24L * 60L * 60L * 1000L);
				map.put("expirationDate", dateFormat.format(expirationDate));
				
				int venueId = selectedAlert.getEvent().getVenueId();
				String catScheme = selectedAlert.getCategoryGroupName();
				String imageMap = "E:/TMATIMAGESFINAL/" + venueId + "_" + catScheme + ".gif";
				File imageFile = new File(imageMap);
				
			
				if (imageFile.exists()){
					map.put("mapUrl", "http://tmat/a1/VenueImage?venueId=" + selectedAlert.getEvent().getVenueId() + "&catScheme=" + selectedAlert.getCategoryGroupName());
				} 
			
				Property alertMailCcPropertyDAO = DAORegistry.getPropertyDAO().get("alert.mail.cc");
				String mailCc = null;
				if (alertMailCcPropertyDAO != null) {
					mailCc = alertMailCcPropertyDAO.getValue();
				}
				
				Property alertMailBccPropertyDAO = DAORegistry.getPropertyDAO().get("alert.mail.bcc");
				String mailBcc = null;
				if (alertMailBccPropertyDAO != null) {
					mailBcc = alertMailBccPropertyDAO.getValue();
				}
				
				//User alertUser = DAORegistry.getUserDAO().get(selectedAlert.getUsername());
				User shortUser = DAORegistry.getUserDAO().getUserByUsername(username);

				String fromName = shortUser.getFirstName().trim() + " " + shortUser.getLastName().trim() + "(" + shortUser.getUsername().trim() + ") ";
				String email = null; 
				String subject =  "CATEGORY SALE ALERT for " + selectedAlert.getEvent().getName() + " - " + selectedAlert.getEvent().getFormattedEventDate();
				
				if ("1".equals(DAORegistry.getPropertyDAO().get("alert.mail.marketmaker.enabled").getValue())) {						
					String mmEmail;
					User marketMaker = DAORegistry.getMarketMakerEventTrackingDAO().getCurrentMarketMakerForEvent(selectedAlert.getEventId());
					if (marketMaker == null) {
						// default handler of the alert
						mmEmail = mailIdProperty.getBccList();//"araut@ticketgallery.com";
					} else {
						mmEmail = marketMaker.getEmail();
					}
					
					email = mmEmail;
				}

				if (email == null) {
					email = mailCc;
				} else {
					email += "," + mailCc;
				}
				
				if (email == null && mailCc == null) {
					email = mailBcc;
				} else {
					email += "," + mailCc + "," + mailBcc;
				} 
				//email += "," + shortUser.getEmail();

				mailManager.sendMail(fromName, email, null, null, subject, resource, map, mimeType, null);
				logger.info("Successfully sending mail for alert:" + selectedAlert.getId() + "!");
				selectedAlert.setLastEmailDate(new Date());
				selectedAlert.setAlertFor(AlertFor.TMAT);
				DAORegistry.getUserAlertDAO().update(selectedAlert);
			}			
			return new ModelAndView(new RedirectView(manageAlertsUrl + "?info=" + URLEncoder.encode(selectedAlerts.size()+ " short email(s) were sent for the selected alert(s) ")));
		}
		
		String filterArtistIdString = request.getParameter("filterArtistId");
		/*if (filterTourIdString == null) {
			filterTourIdString = preferenceManager.getPreferenceValue(username, "alertTourId", "");
		}*/

		Integer filterArtistId = null;
		try {
			filterArtistId = Integer.parseInt(filterArtistIdString);
		} catch (Exception e) {
			filterArtistIdString = "";
		}
		
//		preferenceManager.updatePreference(username, "alertTourId", filterTourIdString);


		String filterEventIdString = request.getParameter("filterEventId");
		if (filterEventIdString == null) {
			filterEventIdString = preferenceManager.getPreferenceValue(username, "alertEventId", "");
		}

		Integer filterEventId = null;
		if (filterArtistId != null) {
			try {
				filterEventId = Integer.parseInt(filterEventIdString);
			} catch (Exception e) {
				filterEventIdString = "";
			}
		} else {
			filterEventIdString = "";			
		}
		String alertUsername = null;
		if (modeAllUser) {
			String filterUsername = request.getParameter("filterUsername");
			if (filterUsername == null) {
				filterUsername = preferenceManager.getPreferenceValue(username, "alertUsername", "");
			}

			preferenceManager.updatePreference(username, "alertUsername", filterUsername);
			
			if (!filterUsername.isEmpty()) {
				alertUsername = filterUsername;
			}
			
			mav.addObject("filterUsername", filterUsername);

		} else {
			alertUsername = username;			
		}

		preferenceManager.updatePreference(username, "alertEventId", filterEventIdString);
				
		//
		// VIEW: CREATED, MARKETMAKER OR BOTH
		// 
		String view = request.getParameter("view");
		if (view == null) {
			view = preferenceManager.getPreferenceValue(username, "alertView", null);
		} else {
			preferenceManager.updatePreference(username, "alertView", view);			
		}

		
		//
		// ALERTFOR: TMAT, CIRCLES OR BOTH
		// 
		String alertForString = request.getParameter("alertFor");
		if (alertForString == null) {
			alertForString = preferenceManager.getPreferenceValue(username, "alertFor", null);
		} 
		
		AlertFor filterAlertFor = null;
		if (alertForString != null) {
			if (alertForString.equalsIgnoreCase(AlertFor.TMAT.toString())) {
				filterAlertFor = AlertFor.TMAT;
				alertForString = "tmat";
			} else if (alertForString.equalsIgnoreCase(AlertFor.CIRCLES_PORTAL.toString())) {
				filterAlertFor = AlertFor.CIRCLES_PORTAL;				
				alertForString = "circles";
//			} else if (alertForString.equalsIgnoreCase(AlertFor.CIRCLES_PORTAL.toString())) {
//				filterAlertFor = AlertFor.CIRCLES_PORTAL;				
//				alertForString = "circles_portal";
			} else {
				alertForString = null;
			}
		} 
		preferenceManager.updatePreference(username, "alertFor", alertForString);
		
		// INCLUDE DISABLED ALERTS
		String includeDisabledAlertsString = request.getParameter("filterIncludeDisabledAlerts");
		if (includeDisabledAlertsString == null) {
			includeDisabledAlertsString = preferenceManager.getPreferenceValue(username, "alertIncludeDisabledAlerts", "0");
		} 
		
		Boolean includeDisabledAlerts = false;
		if (includeDisabledAlertsString != null && includeDisabledAlertsString.equals("1")) {
			includeDisabledAlerts = true;
		} 
		preferenceManager.updatePreference(username, "alertIncludeDisabledAlerts", includeDisabledAlerts?"1":"0");
		
		UserAlertStatus filterUserAlertStatus = null;
		if (!includeDisabledAlerts) {
			filterUserAlertStatus = UserAlertStatus.ACTIVE;
		}

		Map<Integer, UserAlert> userAlertById = new HashMap<Integer, UserAlert>();
		if (view == null || view.isEmpty() || view.equals("created")) {
			Collection<UserAlert> creatorUserAlerts;
			if (filterEventId == null) {
				creatorUserAlerts = DAORegistry.getUserAlertDAO().getAlertsByUsernameAndTourId(alertUsername, filterArtistId, filterAlertFor, filterUserAlertStatus);
			} else {
				creatorUserAlerts = DAORegistry.getUserAlertDAO().getAlertsByUsernameAndEventId(alertUsername, filterEventId, filterAlertFor, filterUserAlertStatus);
			}
			if (creatorUserAlerts != null) {
				for(UserAlert userAlert: creatorUserAlerts) {
					if(userAlert.getAlertTransType().equalsIgnoreCase("BROWSE")){
						continue;
					}
					userAlertById.put(userAlert.getId(), userAlert);
				}
				
			}
		}

		if (view == null || view.isEmpty() || view.equals("marketmaker")) {
			Collection<UserAlert> marketMakerUserAlerts;
			if (filterEventId == null) {
				marketMakerUserAlerts = DAORegistry.getUserAlertDAO().getAlertsByMarketMakerAndTourId(alertUsername, filterArtistId, filterAlertFor, filterUserAlertStatus);
			} else {
				marketMakerUserAlerts = DAORegistry.getUserAlertDAO().getAlertsByMarketMakerAndEventId(alertUsername, filterEventId, filterAlertFor, filterUserAlertStatus);				
			}
			if (marketMakerUserAlerts != null) {
				for(UserAlert userAlert: marketMakerUserAlerts) {
					if(userAlert.getAlertTransType().equalsIgnoreCase("BROWSE")){
						continue;
					}
					userAlertById.put(userAlert.getId(), userAlert);
				}
				
			}
		}
		
		
		/*Map<Integer, Integer> frequencyByAlertId = new HashMap<Integer, Integer>();
		for(UserAlert forAlert: userAlertById.values()) {
			if (forAlert.getEvent()!=null){
			frequencyByAlertId.put(forAlert.getId(), crawlerSchedulerManager.getFrequency(forAlert.getEvent().getDate()));
			}
		}*/
		
		Collection<Event> events = null;
		if (filterArtistId != null) {
			events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(filterArtistId);
		}

		mav.addObject("includeDisabledAlerts", includeDisabledAlerts);
		mav.addObject("view", view);
		mav.addObject("alertFor", filterAlertFor);
		 
		mav.addObject("events", events);
		//mav.addObject("frequencyByAlertId", frequencyByAlertId);
		mav.addObject("alerts", userAlertById.values());
		mav.addObject("filterTourId", filterArtistId);
		mav.addObject("filterEventId", filterEventId);
		mav.addObject("artist", DAORegistry.getArtistDAO().getAllActiveArtists());
		mav.addObject("username", username);	
		mav.addObject("mode", modeAllUser?"editAll":"editMine");
		return mav;
	}
	
	
	public void createAlertForShort(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try{
			String username = (String)request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);				
			String eventId = request.getParameter("eventId");
			String category = request.getParameter("category");
			//String section = request.getParameter("section");
			//String row = request.getParameter("row");
			String qty = request.getParameter("qty");
			String minPrice = request.getParameter("minPrice");
			String maxPrice = request.getParameter("maxPrice");
			//row = row.replace('-', ':');
			
			if(null != category && category.trim().length() >0 && !"undefined".equalsIgnoreCase(category)){
				if(category.contains(" ")){
					String[] cateory = category.split(" ");
					
					for (String catObj : cateory) {
						if(catObj.contains("Category")){
							continue;
						}
						category = catObj;
					}
				}
				if(null != category && category.equals("")){
					category = null;
				}
			}else{
				category = null;
			}
			
			Collection<Site> siteList =DAORegistry.getSiteDAO().getAll();
			String siteIds="";
			for (Site site : siteList) {
				siteIds += site.getId()+",";
			}
			UserAlert userAlert = new UserAlert(); 
			userAlert.setEventId(Integer.parseInt(eventId.trim()));
			userAlert.setUsername(username);
			userAlert.setPriceRange(minPrice+":"+maxPrice);
			userAlert.setSectionRange(":");
			userAlert.setRowRange(":");
			userAlert.setCategory(category);
			userAlert.setCreationDate(new Date());
			userAlert.setQuantities(qty);
			userAlert.setUserAlertStatus(UserAlertStatus.ACTIVE);
			userAlert.setUserAlertType(UserAlertType.IN);
			userAlert.setAlertFor(AlertFor.TMAT);
			userAlert.setSiteIdsString(siteIds);
			userAlert.setAlertTransType("IRA");
			DAORegistry.getUserAlertDAO().save(userAlert);
			PrintWriter out = response.getWriter();
			JSONArray alertDetails = new JSONArray();
			JSONObject jObj = new JSONObject();
            jObj.put("minPrice", minPrice);
            jObj.put("maxPrice", maxPrice);
            alertDetails.put(jObj);
	        out.println(alertDetails.toString());
		}catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	
	public void editShortAlert(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try{
			String eventId = request.getParameter("shortAlertId");
			String minPrice = request.getParameter("minPrice");
			String maxPrice = request.getParameter("maxPrice");
			String quantity = request.getParameter("qty");
			
			UserAlert userAlert = DAORegistry.getUserAlertDAO().get(Integer.parseInt(eventId));
			userAlert.setPriceRange(minPrice+":"+maxPrice);	
			userAlert.setQuantities(quantity.trim());
			userAlert.setMailInterval(null);
			userAlert.setLastEmailDate(null);
			userAlert.setLastCheck(null);
			DAORegistry.getUserAlertDAO().update(userAlert);
			PrintWriter out = response.getWriter();
			JSONArray alertDetails = new JSONArray();
			JSONObject jObj = new JSONObject();
            jObj.put("minPrice", minPrice);
            jObj.put("maxPrice", maxPrice);
            alertDetails.put(jObj);
	        out.println(alertDetails.toString());
		}catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	public PreferenceManager getPreferenceManager() {
		return preferenceManager;
	}

	public void setPreferenceManager(PreferenceManager preferenceManager) {
		this.preferenceManager = preferenceManager;
	}	
	

	public void setMailManager(MailManager mailManager) {
		this.mailManager = mailManager;
	}

	 
	
}
