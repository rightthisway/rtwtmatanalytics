package com.admitone.tmat.web.logger;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserActionLogger {
	private static final Logger logger = LoggerFactory.getLogger(UserActionLogger.class);

	private UserActionLogger(){ }
	
	public static void logUserAction(String username, String action, String page){
		logger.info(username + " " + action + " on " +  page);
	}
}
