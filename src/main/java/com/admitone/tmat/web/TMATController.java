package com.admitone.tmat.web;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.acegisecurity.ui.webapp.AuthenticationProcessingFilter;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.admitone.tmat.common.SharedProperty;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.AdmitoneInventory;
import com.admitone.tmat.data.AdmitoneInventoryLocal;
import com.admitone.tmat.data.Artist;
import com.admitone.tmat.data.Category;
import com.admitone.tmat.data.ChildTourCategory;
import com.admitone.tmat.data.DefaultPurchasePrice;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.ExcludeTicketMap;
import com.admitone.tmat.data.GrandChildTourCategory;
import com.admitone.tmat.data.LongTransaction;
import com.admitone.tmat.data.ManagePurchasePrice;
import com.admitone.tmat.data.ShortTransaction;
import com.admitone.tmat.data.Stat;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.data.TourCategory;
import com.admitone.tmat.enums.EventStatus;
import com.admitone.tmat.enums.RemoveDuplicatePolicy;
import com.admitone.tmat.pojo.AOZoneTickets;
import com.admitone.tmat.pojo.CastStatus;
import com.admitone.tmat.pojo.CrawlServerResponse;
import com.admitone.tmat.pojo.IRAEventStatus;
import com.admitone.tmat.pojo.ShortStatus;
import com.admitone.tmat.utils.AOTicketUtil;
import com.admitone.tmat.utils.Categorizer;
import com.admitone.tmat.utils.ShortBroadcastUtil;
import com.admitone.tmat.utils.ShortStatsUtilComparator;
import com.admitone.tmat.utils.ShortStatusUtil;
import com.admitone.tmat.utils.StatHelper;
import com.admitone.tmat.utils.TicketUtil;
import com.admitone.tmat.web.pojo.EventCapacityStatusAndRisk;
import com.admitone.tmat.zones.ZoneData;
import com.admitone.tmat.zones.ZonesManager;
import com.thoughtworks.xstream.XStream;

public class TMATController extends  MultiActionController {
	private final Logger logger = LoggerFactory.getLogger(TMATController.class);
	private ZonesManager zonesManager;
	DateFormat formatZoneDate = new SimpleDateFormat("M/d/yyyy hh:mm:ss a");
	private IRAEventStatus iraEventStatus;
//	private JMSMessageSender sender;
	private SharedProperty sharedProperty;
	
	public TMATController() {
	}
	
	public ModelAndView loadIndexPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		ModelAndView mav = new ModelAndView("page-default");
		return mav;
	}
	public void getBrowseShortCSV(HttpServletRequest request,
			HttpServletResponse response) throws IOException{

		
		String minPGMStr = request.getParameter("minPGM");
		Double minPGM = null;
		if(minPGMStr!=null && !minPGMStr.isEmpty()){
			minPGM = Double.valueOf(minPGMStr)/100;
		}
		
		String maxPGMStr = request.getParameter("maxPGM");
		Double maxPGM = null;
		if(maxPGMStr!=null && !maxPGMStr.isEmpty()){
			maxPGM = Double.valueOf(maxPGMStr)/100;
		}
		
		String noCrawlStr =request.getParameter("nocrawl");
		Boolean noCrawl="true".equals(noCrawlStr);
		String eventType = request.getParameter("eventType");
		if(eventType!=null && eventType.equals("all")){
			eventType =null;
		}
		String dateRange1String = request.getParameter("dateRange1");
		String dateRange2String = request.getParameter("dateRange2");
//		String eventStr="";
		System.out.println("Date Range 1: " + dateRange1String);
		System.out.println("Date Range 2: " + dateRange2String);
		
//		String eventIdString = request.getParameter("eventId");
//		String catScheme = request.getParameter("catScheme");
		String catType = request.getParameter("catType");
//		String artistIdString = request.getParameter("artistId");
		
		String eventIdString = request.getParameter("eventId");
		Integer eventId =null;
		if(eventIdString!=null && !eventIdString.isEmpty() && !eventIdString.equalsIgnoreCase("all")){
			eventId = new Integer(eventIdString);
//			eventStr=eventIdString;
		}
		String catScheme = request.getParameter("catScheme");
		
		String artistIdString = request.getParameter("artistId");
		Integer artistId =null;
		if(artistIdString!=null && !artistIdString.isEmpty() && !artistIdString.equalsIgnoreCase("all")){
			artistId = new Integer(artistIdString);
		}
		String venueIdString = request.getParameter("venueId");
		Integer venueId =null;
		if(venueIdString!=null && !venueIdString.isEmpty() && !venueIdString.equalsIgnoreCase("all")){
			venueId = new Integer(venueIdString);
		}
		// filter can be null(show all), short(show only short) or inventory (show only inventory)
		String filter = request.getParameter("filter");		
		
		Collection<ShortStatus> eventStatuses = null;
//		Event event = null;

		if(catScheme == null || catScheme.isEmpty()){
//			catScheme = Categorizer.getAllCategoryGroups().get(0);  TODO CAT
			catScheme = "";
		}
		
//		ModelAndView mav = new ModelAndView("page-tmat-browse-shorts");
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		Date dateRange1 = null;
		Date dateRange2 = null;
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("America/New_York"));
		if(dateRange1String !=null){
			try{
				dateRange1 = df.parse(dateRange1String);
				calendar.setTime(dateRange1);
				calendar.add(Calendar.MILLISECOND, -1);
				dateRange1= calendar.getTime();
			}catch(Exception e){
				dateRange1 = null;
			}
		}
		
		if(dateRange2String !=null){
			try{
				dateRange2 = df.parse(dateRange2String);
				calendar.setTime(dateRange2);
				calendar.add(Calendar.DATE, 1);
				dateRange2= calendar.getTime();
			}catch(Exception e){
				dateRange2 = null;
			}
		}
		System.out.println("Date Range 1: " + dateRange1);
		List<Event>events = null;
		if(dateRange1==null || dateRange2 == null){
			//skip..
		}else if(noCrawl){
			String iraEventStatusIdStr = request.getParameter("iraEventStatusId");
			Long iraEventStatusId = Long.parseLong(iraEventStatusIdStr);
			events = iraEventStatus.getEventFromMapById(iraEventStatusId);
			if(events!=null){
				eventStatuses = ShortStatusUtil.getAllEventStatusesByEvents(events, filter, catType, eventType,null,null,null,minPGM,maxPGM);
			}
			noCrawl=false; 
			
		}else{
			noCrawl=true;
			events = ShortStatusUtil.getFilteredEvents(venueId,eventId, artistId, filter,catType, dateRange1, dateRange2, eventType,null,null);
			String eventIds="";
			for(Event event:events){
				eventIds=eventIds + event.getId() + ",";
			}
			eventIds=eventIds.substring(0, eventIds.length()-1);
			Long time = new Date().getTime();
			if(!eventIds.isEmpty()){
				eventIds=eventIds.substring(0, eventIds.length()-1);
				String url =  sharedProperty.getBrowseUrl() + "WSForceEvents";
				HttpClient hc = new DefaultHttpClient();
				HttpPost hp = new HttpPost(url);
				NameValuePair eventIdParam = new BasicNameValuePair("eventIds", eventIds);
				NameValuePair destinationParam = new BasicNameValuePair("postBackUrl", sharedProperty.getAnalyticsUrl()+"ForceCrawledEventResult");
				List<NameValuePair> parameters = new ArrayList<NameValuePair>();
				parameters.add(eventIdParam);
				parameters.add(destinationParam);
				UrlEncodedFormEntity entity = new UrlEncodedFormEntity(parameters);
				hp.setEntity(entity);
				HttpResponse res = hc.execute(hp);
				HttpEntity resEntity =res.getEntity();
				String content = EntityUtils.toString(resEntity);
				XStream xstream = new XStream();
				xstream.alias("CrawlResponseWrapper", CrawlServerResponse.class);
				xstream.processAnnotations(CrawlServerResponse.class);
				CrawlServerResponse csr = new CrawlServerResponse();
				xstream.fromXML(content,csr);
				if(csr.getStatus().equalsIgnoreCase("success")){
					iraEventStatus.getEventMap().put(time, events);
					iraEventStatus.getEventStringMap().put(time, eventIds);
				}
			}
			
		}
		BufferedWriter writer = new BufferedWriter(response.getWriter());
		String fileName="";
		
		if(artistIdString!=null && !artistIdString.equals("")){
			Artist artist= DAORegistry.getArtistDAO().get(Integer.parseInt(artistIdString));
			fileName+= artist.getName().toUpperCase().replaceAll("\\s++", "-").replaceAll(":", "-");
		}else{
			fileName+="AllArtist";
		}
		DateFormat df1= new SimpleDateFormat("MM-dd-yyyy");
		if(eventIdString!=null && !eventIdString.equals("") && !eventIdString.equalsIgnoreCase("All")){
			Event event = events.get(0); 
			fileName+= "-" + event.getName().toUpperCase().replaceAll("\\s++", "-").replaceAll(":", "-") + df1.format(event.getDate());
		}else{
			fileName+= "-AllEvent";
		}
		
		if(dateRange1!=null){
			calendar.setTime(dateRange1);
			calendar.add(Calendar.MILLISECOND, 1);
			dateRange1=calendar.getTime();
			fileName+="_"+df1.format(dateRange1);
		}
		if(dateRange2!=null){
			calendar.setTime(dateRange2);
			calendar.add(Calendar.DATE, -1);
			dateRange2= calendar.getTime();
			fileName+="_" + df1.format(dateRange2);
		}
		
		fileName=fileName +  ((filter==null||filter.isEmpty())?"":("-"+filter.toUpperCase())) + ((catType==null||catType.isEmpty())?"":("-"+catType.toUpperCase())) + "-IRA-INVENTORY" +".csv";
	    response.setHeader("Content-Disposition","attachment; filename=\"" + fileName + "\"");
	    response.setContentType ( "application/vnd.ms-excel");
	    writer.write("EventName,EventDate,EventTime,Venue,Category,Type,Qty,Cost/Revenue,Exposure,PGM,Total Market Price,Market Price,Market Mover,Section,Row,Invoice,Customer\n");
	    DecimalFormat decimalFormat = new DecimalFormat("##0.00");
		for(ShortStatus status:eventStatuses){
			String text="";
			text += status.getDescription() + ",";
			text += (status.getDate()==null?"":status.getDate()) + ",";
			text += (status.getTime()==null?"":status.getTime()) + ",";
			text += (status.getVenue()==null?"":status.getVenue().replaceAll(",", "-")) + ",";
			text +=","; //Category
			text +=","; //Ticket
//			text += status.getCategory();
			text += status.getQtySold() + ",";
			text += "$"+ decimalFormat.format(status.getPriceSold()) + ",";
			text += "$"+ decimalFormat.format(status.getExposure()) + ",";
			text += decimalFormat.format(status.getProjGrossMargin()) + ",";
			text += "$"+ decimalFormat.format(status.getTotalMinPrice()) + ",";
			text += "$"+ decimalFormat.format(status.getMinPrice()) + ",";
			text += "$"+ decimalFormat.format(status.getMarketMover()) + ",";
			text += ","; //Section
			text += ","; //Row
			text += ","; //Invoice
			text += "\n"; //Customer
			writer.write(text);
			text="";
			if(status.getChildren()!=null){
				for(ShortStatus child:status.getChildren()){
					text += ",,,,"; //EventName,EventDate,EventTime,Venue
					text += child.getCategory() + ",,"; //Tix
					text += child.getQtySold() + ",";
					text += "$"+ decimalFormat.format(status.getPriceSold()) + ",";
					text += "$"+ decimalFormat.format(status.getExposure()) + ",";
					text += decimalFormat.format(status.getProjGrossMargin()) + ",";
					text += "$"+ decimalFormat.format(status.getTotalMinPrice()) + ",";
					text += "$"+ decimalFormat.format(status.getMinPrice()) + ",";
					text += "$"+ decimalFormat.format(status.getMarketMover()) + ",";
					text += ","; //Section
					text += ","; //Row
					text += ","; //Invoice
					text += "\n"; //Customer
					writer.write(text);
					text="";
					for(ShortStatus tix:child.getChildren()){
						text += ",,,,,"; //EventName,EventDate,EventTime,Venue,Category
						text += tix.getDescription() + " Quantity :"+ tix.getQtySold() +",";
						text += tix.getQtySold() + ",";
						text += "$"+ decimalFormat.format(status.getPriceSold()) + ",";
						text += "$"+ decimalFormat.format(status.getExposure()) + ",";
						text += decimalFormat.format(status.getProjGrossMargin()) + ",";
						text += "$"+ decimalFormat.format(status.getTotalMinPrice()) + ",";
						text += "$"+ decimalFormat.format(status.getMinPrice()) + ",";
						text += "$"+ decimalFormat.format(status.getMarketMover()) + ",";
						text += tix.getSection() + ",";
						text += tix.getRow() + ",";
						text += (tix.getInvoice()==null?"":tix.getInvoice()) + ",";
						text += tix.getCustomer() + "\n";
						writer.write(text);
						text="";
					}
				}
			}
		}
//	    myDbData = (Whatever) modelMap.get("modelKey");
//	    {writer.write(myDbData csv row); writer.newLine(); }
	    writer.flush(); 
	    writer.close();

	}

	public ModelAndView loadBrowseUncategorizedShortsPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
//		String noCrawlStr =request.getParameter("nocrawl");
//		Boolean noCrawl="true".equals(noCrawlStr);
//		String minPGMStr = request.getParameter("minPGM");
//		Double minPGM = Double.valueOf(minPGMStr);
		
		String dateRange1String = (request.getParameter("dateRange1")==null?"":request.getParameter("dateRange1"));
		String dateRange2String = (request.getParameter("dateRange2")==null?"":request.getParameter("dateRange2"));
		String submitStr = request.getParameter("submit");
		String eventStr="";
		boolean submit=false;
		if(submitStr!=null && !submitStr.isEmpty()){
			submit =true;
		}
		System.out.println("Date Range 1: " + dateRange1String);
		System.out.println("Date Range 2: " + dateRange2String);
//		String eventType=null;
		String eventType = request.getParameter("eventType");
		String eventTypeStore = eventType;
		if(eventType!=null && eventType.equals("all")){
			eventType =null;
		}
		String eventIdString = request.getParameter("eventId");
		Integer eventId =null;
		if(eventIdString!=null && !eventIdString.isEmpty() && !eventIdString.equalsIgnoreCase("all")){
			eventId = new Integer(eventIdString);
//			eventStr=eventIdString;
		}
//		String catScheme = request.getParameter("catScheme");
		
		String artistIdString = request.getParameter("artistId");
		Integer artistId =null;
		if(artistIdString!=null && !artistIdString.isEmpty() && !artistIdString.equalsIgnoreCase("all")){
			artistId = new Integer(artistIdString);
		}
		
		String venueIdString = request.getParameter("venueId");
		Integer venueId =null;
		if(venueIdString!=null && !venueIdString.isEmpty() && !venueIdString.equalsIgnoreCase("all")){
			venueId = new Integer(venueIdString);
		}
		// filter can be null(show all), short(show only short) or inventory (show only inventory)
		String filter = "short";		
		String catType = "UNCAT";
		
		Collection<ShortStatus> eventStatuses = null;
//		Event event = null;

		/*if(catScheme == null || catScheme.isEmpty()){
//			catScheme = Categorizer.getAllCategoryGroups().get(0); TODO CAT
			catScheme = "END_STAGE";
		}*/
		
		ModelAndView mav = new ModelAndView("page-tmat-browse-uncategorized-shorts");
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		Date dateRange1 = null;
		Date dateRange2 = null;
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("America/New_York"));
		if(dateRange1String !=null && !dateRange1String.isEmpty()){
			try{
				dateRange1 = df.parse(dateRange1String);
				calendar.setTime(dateRange1);
				calendar.add(Calendar.MILLISECOND, -1);
				dateRange1= calendar.getTime();
			}catch(Exception e){
				dateRange1 = null;
			}
		}
		
		if(dateRange2String !=null && !dateRange2String .isEmpty()){
			try{
				dateRange2 = df.parse(dateRange2String);
				calendar.setTime(dateRange2);
				calendar.add(Calendar.DATE, 1);
				dateRange2= calendar.getTime();
			}catch(Exception e){
				dateRange2 = null;
			}
		}
		System.out.println("Date Range 1: " + dateRange1);
		List<Event> events =null;
		
		if(submit){
//			List<String> catScheme = DAORegistry.getVenueCategoryDAO().getCategoryGroupByVenueId(venueId);
			events = ShortStatusUtil.getFilteredEvents(venueId,eventId, artistId, filter, catType, dateRange1, dateRange2, eventType,null,null);
		}
		if(events!=null && !events.isEmpty()){
			eventStatuses = ShortStatusUtil.getAllEventStatusesByEvents(events, filter, catType, eventType,null,null,null,null,null);
		}
		mav.addObject("reload", new Date().getTime());
//		mav.addObject("catGroupName", catScheme);
		mav.addObject("eventType", eventTypeStore);
		mav.addObject("eventStatuses", eventStatuses);
//		mav.addObject("view", view);
		mav.addObject("eventId", eventIdString);
		mav.addObject("artistId", artistIdString);
		mav.addObject("totalEvetnStr", eventStr);
		mav.addObject("filter", filter);
		mav.addObject("catType", catType);
		mav.addObject("fromDateRange", dateRange1String);
		mav.addObject("toDateRange", dateRange2String);
		return mav;
	}
	public ModelAndView loadBrowseShortsPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		String userName = (String)request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);
		String minPGMStr = request.getParameter("minPGM");
		Double minPGM = null;
		if(minPGMStr!=null && !minPGMStr.isEmpty()){
			minPGM = Double.valueOf(minPGMStr)/100;
		}
		
		String maxPGMStr = request.getParameter("maxPGM");
		Double maxPGM = null;
		if(maxPGMStr!=null && !maxPGMStr.isEmpty()){
			maxPGM = Double.valueOf(maxPGMStr)/100;
		}
		
		String noCrawlStr =request.getParameter("nocrawl");
		Boolean noCrawl="true".equals(noCrawlStr);
		String sortBy = request.getParameter("sortBy");
		
		String dateRange1String = (request.getParameter("dateRange1")==null?"":request.getParameter("dateRange1"));
		String dateRange2String = (request.getParameter("dateRange2")==null?"":request.getParameter("dateRange2"));
		String view = request.getParameter("view");
		String eventStr="";
		System.out.println("Date Range 1: " + dateRange1String);
		System.out.println("Date Range 2: " + dateRange2String);
//		String eventType=null;
		String eventType = request.getParameter("eventType");
		String childEventType = request.getParameter("childEventType");
		String grandChildEventType = request.getParameter("grandChildEventType");
		
		String eventTypeStore = eventType;
		String childEventTypeStore = childEventType; 
		String grandChildEventTypeStore = grandChildEventType;
		if(eventType!=null && eventType.equals("all")){
			eventType =null;
		}
		
		if((childEventType == null || "".equals(childEventType)) ||  
				(childEventType!=null && (childEventType.equals("0") || childEventType.equalsIgnoreCase("undefined")))){
			childEventType =null;
		}
		
		if((grandChildEventType == null || "".equals(grandChildEventType) ) ||  
				(grandChildEventType!=null && (grandChildEventType.equals("0") || grandChildEventType.equalsIgnoreCase("undefined")))){
			grandChildEventType =null;
		}
		String eventIdString = request.getParameter("eventId");
		Integer eventId =null;
		if(eventIdString!=null && !eventIdString.isEmpty() && !eventIdString.equalsIgnoreCase("all")){
			eventId = new Integer(eventIdString);
//			eventStr=eventIdString;
		}
//		String catScheme = request.getParameter("catScheme");
		
		String artistIdString = request.getParameter("artistId");
		Integer artistId =null;
		if(artistIdString!=null && !artistIdString.isEmpty() && !artistIdString.equalsIgnoreCase("all")){
			artistId = new Integer(artistIdString);
		}
		
		String venueIdString = request.getParameter("venueId");
		Integer venueId =null;
		if(venueIdString!=null && !venueIdString.isEmpty() && !venueIdString.equalsIgnoreCase("all")){
			venueId = new Integer(venueIdString);
		}
		// filter can be null(show all), short(show only short) or inventory (show only inventory)
		String filter = request.getParameter("filter");		
		String catType = request.getParameter("catType");
		
		Collection<ShortStatus> eventStatuses = null;
//		Event event = null;
		
		ModelAndView mav = new ModelAndView("page-tmat-browse-shorts");
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		Date dateRange1 = null;
		Date dateRange2 = null;
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("America/New_York"));
		if(dateRange1String !=null && !dateRange1String.isEmpty()){
			try{
				dateRange1 = df.parse(dateRange1String);
				calendar.setTime(dateRange1);
				calendar.add(Calendar.MILLISECOND, -1);
				dateRange1= calendar.getTime();
			}catch(Exception e){
				dateRange1 = null;
			}
		}
		
		if(dateRange2String !=null && !dateRange2String .isEmpty()){
			try{
				dateRange2 = df.parse(dateRange2String);
				calendar.setTime(dateRange2);
				calendar.add(Calendar.DATE, 1);
				dateRange2= calendar.getTime();
			}catch(Exception e){
				dateRange2 = null;
			}
		}
		System.out.println("Date Range 1: " + dateRange1);
		if(eventId ==null && artistId==null && venueId ==null && (dateRange1==null || dateRange2 == null)){
			//skip..
		}else { //if(noCrawl){
			/***  Chirag:Remove this code to get data from database  ***/
		/*	String iraEventStatusIdStr = request.getParameter("iraEventStatusId");
			if(iraEventStatusIdStr!=null && !iraEventStatusIdStr.isEmpty()){
				Long iraEventStatusId = Long.parseLong(iraEventStatusIdStr);
				List<Event>events = iraEventStatus.getEventFromMapById(iraEventStatusId);
		*/
			List<Event> events = ShortStatusUtil.getFilteredEvents(venueId,eventId, artistId, filter, catType, dateRange1, dateRange2, eventType,childEventType,grandChildEventType);
			if(events!=null){
				eventStatuses = ShortStatusUtil.getAllEventStatusesByEvents(events, filter, catType, eventType,childEventType,grandChildEventType,userName,minPGM,maxPGM);
			}
//			}
			noCrawl=false; 
			/***  Chirag:Remove this code to get data from database  ***/
		}/*else{   
			noCrawl=true;
			/* if(artistId!= null){
//				mav.addObject("catGroups", Categorizer.getCategoryGroupsByTour(artistId));
			}else if(eventId != null){
//				mav.addObject("catGroups", Categorizer.getCategoryGroupsByEvent(eventId));
			}else{
//				mav.addObject("catGroups", Categorizer.getAllCategoryGroups());  TODO CAT
			}* /
			mav.addObject("catGroups", null);  // need to fix it  
//			String eventStr="";
			List<Event> events = ShortStatusUtil.getFilteredEvents(venueId,eventId, artistId, filter, catType, dateRange1, dateRange2, eventType,childEventType,grandChildEventType);
//			int count =0;
			for(Event event:events){
				eventStr += event.getId()+",";
			}
			if(!eventStr.isEmpty()){
				eventStr=eventStr.substring(0, eventStr.length()-1);
				String url =  sharedProperty.getBrowseUrl() + "WSForceEvents";
				HttpClient hc = new DefaultHttpClient();
				HttpPost hp = new HttpPost(url);
				NameValuePair eventIdParam = new BasicNameValuePair("eventIds", eventStr);
				NameValuePair destinationParam = new BasicNameValuePair("postBackUrl", sharedProperty.getAnalyticsUrl()+"ForceCrawledEventResult");
				List<NameValuePair> parameters = new ArrayList<NameValuePair>();
				parameters.add(eventIdParam);
				parameters.add(destinationParam);
				UrlEncodedFormEntity entity = new UrlEncodedFormEntity(parameters);
				hp.setEntity(entity);
				HttpResponse res = hc.execute(hp);
				HttpEntity resEntity =res.getEntity();
				String content = EntityUtils.toString(resEntity);
				XStream xstream = new XStream();
				xstream.alias("CrawlResponseWrapper", CrawlServerResponse.class);
				xstream.processAnnotations(CrawlServerResponse.class);
				CrawlServerResponse csr = new CrawlServerResponse();
				xstream.fromXML(content,csr);
				if(csr.getStatus().equalsIgnoreCase("success")){
					mav.addObject("totalCrawl", csr.getNumberofcrawls());
					String eventIds ="";
					if(csr.getMsg()!=null && !csr.getMsg().isEmpty()){
						String temp[] = csr.getMsg().split(":");
						if(temp.length==2){
							eventIds = temp[1];
						}
					}
					Long time = new Date().getTime();
					iraEventStatus.getEventMap().put(time, events);
					iraEventStatus.getEventStringMap().put(time, eventIds);
					mav.addObject("iraEventStatusId", time);
				}
			}
			
		}*/
		
		//Manually Sorting as we are using html table 
		//pdhebar
		//legends for pgm
		String sortingType = request.getParameter("sortingType");
		if((eventStatuses != null && !eventStatuses.isEmpty()) && (sortBy != null && !sortBy.isEmpty()))
			Collections.sort((List<ShortStatus>)eventStatuses, new ShortStatsUtilComparator(sortBy,sortingType));
		
		getchildAndGrandChildEventDetails(mav, eventTypeStore, childEventType);
		 
		
		 if(null != childEventTypeStore && childEventTypeStore.trim().length() >0 && !childEventTypeStore.equalsIgnoreCase("undefined")){
			mav.addObject("childEventType", Integer.valueOf(childEventTypeStore));
		}else if(null == childEventTypeStore || "".equals(childEventTypeStore) || childEventTypeStore.equalsIgnoreCase("undefined")){
			mav.addObject("childEventType", "");
		}
		 
		 if(null != grandChildEventTypeStore && grandChildEventTypeStore.trim().length() >0 && !grandChildEventTypeStore.equalsIgnoreCase("undefined")){
			mav.addObject("grandChildEventType", Integer.valueOf(grandChildEventTypeStore));
		 }else if(null == grandChildEventTypeStore || "".equals(grandChildEventTypeStore) || grandChildEventTypeStore.equalsIgnoreCase("undefined")){
			mav.addObject("grandChildEventType", "");
		 }
		
		mav.addObject("reload", new Date().getTime());
		mav.addObject("catGroupName", "");
		mav.addObject("eventType", eventTypeStore);
		
		mav.addObject("eventStatuses", eventStatuses);
		mav.addObject("sortBy", sortBy);
		mav.addObject("sortingType", sortingType);
		mav.addObject("venueId",venueId);
		mav.addObject("view", view);
		mav.addObject("noCrawl", noCrawl);
		mav.addObject("eventId", eventIdString);
		mav.addObject("artistId", artistIdString);
		mav.addObject("totalEvetnStr", eventStr);
		mav.addObject("filter", filter);
		mav.addObject("catType", catType);
		mav.addObject("fromDateRange", dateRange1String);
		mav.addObject("toDateRange", dateRange2String);
		mav.addObject("minPGM", minPGMStr);
		mav.addObject("maxPGM", maxPGMStr);
		return mav;
	}

	public ModelAndView loadBrowseSalesPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		Boolean noCrawl="true".equals(request.getParameter("nocrawl"));
		
		String dateRange1String = request.getParameter("dateRange1");
		String dateRange2String = request.getParameter("dateRange2");
		
		System.out.println("Date Range 1: " + dateRange1String);
		System.out.println("Date Range 2: " + dateRange2String);
//		String eventType=null;
		String eventType = request.getParameter("eventType");
		if(eventType!=null && eventType.equals("all")){
			eventType =null;
		}
		String eventIdString = request.getParameter("eventId");
		String catScheme = request.getParameter("catScheme");
		
		String artistIdString = request.getParameter("artistId");
		
		// filter can be null(show all), short(show only short) or inventory (show only inventory)
		String filter = request.getParameter("filter");		
		String catType = request.getParameter("catType");
		
		Collection<ShortStatus> eventStatuses = null;
		Event event = null;

		if(catScheme == null || catScheme.isEmpty()){
//			catScheme = Categorizer.getAllCategoryGroups().get(0); TODO CAT 
			catScheme = "END_STAGE";
		}
		
		ModelAndView mav = new ModelAndView("page-tmat-browse-sales");
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		Date dateRange1 = null;
		Date dateRange2 = null;
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("America/New_York"));
		if(dateRange1String !=null){
			try{
				dateRange1 = df.parse(dateRange1String);
				calendar.setTime(dateRange1);
				calendar.add(Calendar.MILLISECOND, -1);
				dateRange1= calendar.getTime();
			}catch(Exception e){
				dateRange1 = null;
			}
		}
		
		if(dateRange2String !=null){
			try{
				dateRange2 = df.parse(dateRange2String);
				calendar.setTime(dateRange2);
				calendar.add(Calendar.DATE, 1);
				dateRange2= calendar.getTime();
			}catch(Exception e){
				dateRange2 = null;
			}
		}
		System.out.println("Date Range 1: " + dateRange1);
		if(artistIdString != null && !artistIdString.isEmpty()) {
			int artistId = Integer.parseInt(artistIdString);
			if(dateRange1 == null && dateRange2 == null){
				eventStatuses = ShortStatusUtil.getAllSaleEventStatuses(artistId, catScheme, filter,catType,eventType);
			}else if(dateRange1 != null && dateRange2 == null){
				Calendar cal = Calendar.getInstance();
				cal.setTime(new Date());
				cal.add(Calendar.DATE, 90);
				dateRange2 = cal.getTime();
				eventStatuses = ShortStatusUtil.getAllSaleEventStatuses(artistId, catScheme, filter,catType,dateRange1, dateRange2,eventType);
			}else if(dateRange1 == null && dateRange2 != null){
				dateRange1 = new Date();
				eventStatuses = ShortStatusUtil.getAllSaleEventStatuses(artistId, catScheme, filter,catType,dateRange1, dateRange2,eventType);
			}else{
				eventStatuses = ShortStatusUtil.getAllSaleEventStatuses(artistId, catScheme, filter,catType,dateRange1, dateRange2,eventType);
			}
			/*Collection<Event> activeEventsOfTour = DAORegistry.getEventDAO().getAllEventsByTour(artistId,EventStatus.ACTIVE);*/
			Collection<Event> activeEventsOfArtist = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(artistId);
			String eventIdsTour = "";
			if(!activeEventsOfArtist.isEmpty())
			{
				for(Event activeEvent : activeEventsOfArtist){
					eventIdsTour+=activeEvent.getId()+",";
				}
			
			}
//			mav.addObject("catGroups", Categorizer.getCategoryGroupsByTour(artistId)); TODO CAT
			mav.addObject("catGroups", ""); 
			//mav.addObject("noCrawl", false);
			//mav.addObject("eventIds", eventIdsTour);
		} else if(eventIdString == null || eventIdString.isEmpty() || eventIdString.equals("ALL")) { 
			if(eventIdString != null && !eventIdString.isEmpty()){
				if(dateRange1 == null && dateRange2 == null){
					eventStatuses = ShortStatusUtil.getAllEventStatuses(catScheme, filter,catType,eventType,null,null);
				}else if(dateRange1 != null && dateRange2 == null){
					Calendar cal = Calendar.getInstance();
					cal.setTime(new Date());
					cal.add(Calendar.DATE, 90);
					dateRange2 = cal.getTime();
					eventStatuses = ShortStatusUtil.getAllSaleEventStatuses(catScheme, filter,catType, dateRange1, dateRange2,eventType);
				}else if(dateRange1 == null && dateRange2 != null){
					dateRange1 = new Date();
					eventStatuses = ShortStatusUtil.getAllSaleEventStatuses(catScheme, filter,catType, dateRange1, dateRange2,eventType);
				}else{
					eventStatuses = ShortStatusUtil.getAllSaleEventStatuses(catScheme, filter,catType, dateRange1, dateRange2,eventType);
				}
				
			}
//			mav.addObject("catGroups", Categorizer.getAllCategoryGroups()); TODO CAT
			mav.addObject("catGroups", "");
		} else {
			Integer eventId = new Integer(eventIdString);
			event = DAORegistry.getEventDAO().get(eventId);

			eventStatuses = new ArrayList<ShortStatus>();
			eventStatuses.add(ShortStatusUtil.getStatusByEvent(event, filter,catType,eventType,null,null,null,null,null));
//			mav.addObject("catGroups", Categorizer.getCategoryGroupsByEvent(eventId)); TODO CAT
			mav.addObject("catGroups", "");
			//mav.addObject("noCrawl", false);
			//mav.addObject("eventId",eventId);
			//mav.addObject("eventIds",eventId+",");
		}
		
		mav.addObject("catGroupName", catScheme);
		mav.addObject("eventStatuses", eventStatuses);
		//mav.addObject("eventIds", eventIdsString);
		mav.addObject("noCrawl", noCrawl);
		mav.addObject("eventId", eventIdString);
		mav.addObject("artistId", artistIdString);
		mav.addObject("event", event);
		mav.addObject("filter", filter);
		mav.addObject("catType", catType);
		mav.addObject("fromDateRange", dateRange1String);
		mav.addObject("toDateRange", dateRange2String);
		return mav;
	}
	
	public ModelAndView loadBrowseBroadcastPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
				
		String eventIdString = request.getParameter("eventId");
		String artistIdString = request.getParameter("artistId");
		//String artistIdString = request.getParameter("artistId");
		String catScheme = request.getParameter("catScheme");
		Collection<CastStatus> eventStatuses = null;

		ModelAndView mav = new ModelAndView("page-tmat-browse-broadcast");
		if(eventIdString != null) {
			if(eventIdString.equals("ALL")) { 
				if(catScheme == null || catScheme.isEmpty()){
//					catScheme = Categorizer.getAllCategoryGroups().get(0); TODO CAT
					catScheme = "";
				}
				eventStatuses = ShortBroadcastUtil.getAllStatuses(catScheme);
			} else {
				Integer eventId = Integer.parseInt(eventIdString);
				Event event = DAORegistry.getEventDAO().get(eventId);
				if(catScheme == null || catScheme.isEmpty()){
//					catScheme = Categorizer.getCategoryGroupsByEvent(eventId).get(0); TODO CAT
					catScheme = "";
				}
				CastStatus status = ShortBroadcastUtil.getStatusByEvent(eventId, catScheme);
				eventStatuses = new ArrayList<CastStatus>();
				if(status != null) {
					eventStatuses.add(status);
				}
				mav.addObject("catGroups", Categorizer.getCategoriesByEvent(event.getVenueCategory()));
			}
		} else if(artistIdString != null){
			Integer artistId = Integer.parseInt(artistIdString);
			if(catScheme == null || catScheme.isEmpty()){
//				catScheme = Categorizer.getCategoryGroupsByTour(artistId).get(0); TODO CAT
				catScheme = "END_STAGE";
			}
			eventStatuses = new ArrayList<CastStatus>();
			for(Event event : DAORegistry.getEventDAO().getAllActiveEventsByArtistId(artistId)){
				CastStatus status = ShortBroadcastUtil.getStatusByEvent(event.getId(), catScheme);
				if(status != null) {
					eventStatuses.add(status);
				}
			}
		} /*else if(artistIdString != null){
			if(catScheme == null || catScheme.isEmpty()){
//				catScheme = Categorizer.getAllCategoryGroups().get(0); TODO CAT
				catScheme = "END_STAGE";
			}
			eventStatuses = new ArrayList<CastStatus>();
			Artist artist = DAORegistry.getArtistDAO().get(Integer.parseInt(artistIdString));
//			for(Artist artist : DAORegistry.getArtistDAO().getAllToursByArtist(Integer.parseInt(artistIdString))){
				for(Event event : DAORegistry.getEventDAO().getAllActiveEventsByArtistId(artist.getId())){
					CastStatus status = ShortBroadcastUtil.getStatusByEvent(event.getId(), catScheme);
					if(status != null) {
						eventStatuses.add(status);
					}
//				}
			}
		}*/

		mav.addObject("catGroupName", catScheme);
		mav.addObject("eventStatuses", eventStatuses);
		mav.addObject("eventId", eventIdString);
		mav.addObject("artistId", artistIdString);
		mav.addObject("artistId", artistIdString);
		
		return mav;
	}

	public ModelAndView loadBrowseEventsPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
	
		String artistId = request.getParameter("artistId");
//		String artistId = request.getParameter("artistId");

		Collection<Event> events = null;
		if (artistId == null && artistId == null) {
			events = new ArrayList<Event>();
		} else if(artistId != null ) {
			events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(Integer.parseInt(artistId));			
		} /*else if(artistId != null ) {
			Collection<Artist> tours = DAORegistry.getArtistDAO().getAllToursByArtist(Integer.parseInt(artistId));
			events = new ArrayList<Event>();
			for(Artist artist : tours){
				events.addAll(DAORegistry.getEventDAO().getAllEventsByTour(artist.getId(), EventStatus.ACTIVE));
			}
		}*/

		ModelAndView mav = new ModelAndView("page-tmat-browse-events");
		mav.addObject("events", events);
		mav.addObject("artistId", artistId);

		return mav;
	}
	
	public ModelAndView loadPreviewTicketsPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		Integer admitoneId = Integer.valueOf(request.getParameter("eventId"));
		Event event = DAORegistry.getEventDAO().get(admitoneId);
		Integer categoryId = Integer.valueOf(request.getParameter("categoryId"));
		Integer quantity = Integer.valueOf(request.getParameter("quantity"));
		Integer transactionType = request.getParameter("type").equals("short")?ExcludeTicketMap.TYPE_SHORT:ExcludeTicketMap.TYPE_LONG;
		Integer transactionId = null;
		if (request.getParameter("transactionId") != null) {
			transactionId = Integer.valueOf(request.getParameter("transactionId"));
		}

		Category category = DAORegistry.getCategoryDAO().get(categoryId);
		String groupName = category.getGroupName(); 
		List<Integer> categoryIds = Categorizer.getCategoriesByEvent(event.getVenueCategory());
		categoryIds.add(0);
		Collections.sort(categoryIds);
		
		List<Integer> equalCats;
		if(category.getEqualCats() == null) {
			//use the old method
			equalCats = new ArrayList<Integer>();
			equalCats.add(categoryId);
		} else if(category.getEqualCats().equals("*")) {					
			//use the all better cats
			//contains this cat
			equalCats = categoryIds.subList(0, categoryIds.indexOf(categoryId) + 1);
		} else {
			//use only mapped cats
			equalCats = new ArrayList<Integer>();

			System.out.println("THE CAT OBJECT IS " + category.getEqualCats());
			String[] symbols = category.getEqualCats().split("\\s");
			for(String symbol: symbols) {
				Category dbCategory = DAORegistry.getCategoryDAO().getCategoryByVenueCategoryIdAndCategorySymbol(event.getVenueCategory().getId(), symbol);
				if(dbCategory == null) {
					System.out.println("Found Bad Equal CAT! VenueCategory: " + event.getVenueCategory().getId() + " Symbol: " + symbol);
					continue;
				}
				equalCats.add(dbCategory.getId());
			}
		}
		
//		Collection<Integer> userExcludedTicketIds = ShortStatusUtil.getExcludedTickets(eventId, categoryId, transactionId);
//		
		Collection<Ticket> tickets = DAORegistry.getTicketDAO().getAllActiveRegularTicketsByEvent(event.getId());
//		Collection<Ticket> resultTickets = TicketUtil.getEventCategoriesLotsSimpleTicket(tickets, categoryIds, quantity, userExcludedTicketIds, groupName);
		ModelAndView mav = new ModelAndView("page-tmat-preview-tickets");
//		mav.addObject("tickets", resultTickets);
		
		tickets = TicketUtil.removeAdmitOneTickets(tickets);
//		Constants.reversedRankedSiteIds
		List<Integer> quantities = TicketUtil.getMatchingQuantities(quantity); 
		Map<String, Boolean> siteFilters = new HashMap<String, Boolean>();
		for (String siteId: Constants.getInstance().getSiteIds()) {
			siteFilters.put(siteId, true);
		}
		List<Ticket> filteredTickets = new ArrayList<Ticket>();
		Collection<Ticket> ticketList = new ArrayList<Ticket>();
		Map<String, Long> timeStats = new HashMap<String, Long>();
		Long loadCategoriesStartTime = System.currentTimeMillis();
//		Event event = DAORegistry.getEventDAO().get(eventId);
		Collection<Category> categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(event.getVenueCategory().getId());
		timeStats.put("loadCategories", System.currentTimeMillis() - loadCategoriesStartTime);
		ticketList = DAORegistry.getTicketDAO().getAllActiveTicketsByEvent(event.getId());
		Long loadTicketsStartTime = System.currentTimeMillis();
//		if ("completed".equals(show)) {
//			// show completed  ticket					
//			tickets = DAORegistry.getTicketDAO().getAllCompletedTicketsByEvent(eventId);
//		} else {
//			// show active ticket
//			tickets = DAORegistry.getTicketDAO().getAllActiveTicketsByEvent(eventId);
//		}

		timeStats.put("loadTickets", System.currentTimeMillis() - loadTicketsStartTime);

		Long preAssignCategoriesStartTime = System.currentTimeMillis();		
		TicketUtil.preAssignCategoriesToTickets(tickets, categories); 
		timeStats.put("preAssignCategories", System.currentTimeMillis() - preAssignCategoriesStartTime);
		Collection<Ticket> hTickets = DAORegistry.getHistoricalTicketDAO().getAllTicketsByEvent(event.getId());
		Collection<Ticket> tempHTickets = DAORegistry.getTicketDAO().getAllCompletedTicketsByEvent(event.getId());
		if(tempHTickets != null) {
			hTickets.addAll(tempHTickets);
		}

		TicketUtil.preAssignCategoriesToTickets(hTickets, categories);
		
		List<DefaultPurchasePrice> defaultTourPrices = DAORegistry.getDefaultPurchasePriceDAO().getAllDefaultTourPrice();
		Map<String, DefaultPurchasePrice> defaultTourPriceMap = new HashMap<String, DefaultPurchasePrice>();
		if(defaultTourPrices!=null){
			for(DefaultPurchasePrice defaultTourPrice:defaultTourPrices){
				String key = defaultTourPrice.getExchange() + "-" + (defaultTourPrice.getTicketType()==null?"REGULAR":defaultTourPrice.getTicketType());
				defaultTourPriceMap.put(key, defaultTourPrice);
			}
		}
		
		Map<String, ManagePurchasePrice> tourPriceMap  = new HashMap<String, ManagePurchasePrice>();
		List<ManagePurchasePrice> list = DAORegistry.getManagePurchasePriceDAO().getAllByManagePurchasePriceByArtistId(event.getArtistId());
		for(ManagePurchasePrice tourPrice:list){
			String mapKey = tourPrice.getExchange()+ "-" + (tourPrice.getTicketType());
			tourPriceMap.put(mapKey,tourPrice);
		}

		TicketUtil.getFilteredTickets(event.getId(), equalCats, quantities, siteFilters, null, null, //"active", 
				tickets, filteredTickets, null,ticketList,hTickets,categories, groupName, true, null, null, null, RemoveDuplicatePolicy.SUPER_SMART, timeStats, null,event.getVenueCategory(),defaultTourPriceMap,tourPriceMap,null);
		
		Collections.sort(filteredTickets, new Comparator<Ticket>() {

			@Override
			public int compare(Ticket ticket1, Ticket ticket2) {
				return ticket1.getCurrentPrice().compareTo(ticket2.getCurrentPrice());
			}
			
		});
		
		mav.addObject("tickets", filteredTickets);
		
		Collection<ExcludeTicketMap> tmpExcludeTicketMaps = DAORegistry.getExcludeTicketMapDAO().getAllMapsByEventId(event.getId());

		Collection<ExcludeTicketMap> excludeTicketMaps = new ArrayList<ExcludeTicketMap>();
		for (ExcludeTicketMap excludeTicketMap: tmpExcludeTicketMaps) {
			if ((excludeTicketMap.getScope().equals(ExcludeTicketMap.SCOPE_EVENT) && excludeTicketMap.getEventId().equals(event.getId()))
					|| (excludeTicketMap.getScope().equals(ExcludeTicketMap.SCOPE_CAT) && excludeTicketMap.getCategoryId().equals(categoryId))
					|| (excludeTicketMap.getScope().equals(ExcludeTicketMap.SCOPE_TX) && excludeTicketMap.getTransactionId().equals(transactionId) && excludeTicketMap.getTransactionType().equals(transactionType))) {
				excludeTicketMaps.add(excludeTicketMap);
			}
		}
		
		mav.addObject("excludeTicketMaps", excludeTicketMaps);
		mav.addObject("transactionId", transactionId);
		mav.addObject("catScheme", category.getGroupName());
		mav.addObject("transactionType", transactionType);
		mav.addObject("ticketId", request.getParameter("ticketId"));
		return mav;
	}

	public ModelAndView loadPreviewStatsPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		Integer eventId = null;
		Integer categoryId = null;
		Integer lotSize = null;
		Integer transactionId = null;
		String transId = request.getParameter("transactionId");
		String catScheme = request.getParameter("catScheme");
		if(transId == null || transId.isEmpty()){
			eventId = Integer.parseInt(request.getParameter("eventId"));
			categoryId = Integer.parseInt(request.getParameter("categoryId"));
			lotSize = Integer.parseInt(request.getParameter("lotSize"));
		} else {
			transactionId = Integer.parseInt(transId);
		}

		// siteId filter
		ArrayList<String> siteFilters = new ArrayList<String>();
		for (String siteId: Constants.getInstance().getSiteIds()) {
			String checkedString = request.getParameter(siteId + "_checkbox");
			Boolean checked = Boolean.parseBoolean(checkedString);			
			if(checked){
				siteFilters.add(siteId);
			}
		}
		List<Stat> stats = null;
		List<ExcludeTicketMap> excludeTicketMaps = new ArrayList<ExcludeTicketMap>();
		
		if(transactionId == null) {
			stats = this.getStats(eventId, categoryId, lotSize, siteFilters, null, null, catScheme);
		} else {
			ShortTransaction transaction = DAORegistry.getShortTransactionDAO().get(transactionId);
			if(transaction == null){
				AdmitoneInventory inv = DAORegistry.getAdmitoneInventoryDAO().get(transactionId);
				if(inv == null){
					LongTransaction longTransaction = DAORegistry.getLongTransactionDAO().get(transactionId);
					if(longTransaction == null ){
						AdmitoneInventoryLocal invLocal = DAORegistry.getAdmitoneInventoryLocalDAO().get(transactionId);
						if(null != invLocal){
							transaction = new ShortTransaction(invLocal);
						}
					}
				}else{
					transaction = new ShortTransaction(inv);
				}
			}
			String section =null;
			Integer quantity =null;
			if(null != transaction){
				Event event = DAORegistry.getEventDAO().getEventByAdmitoneId(transaction.getEventId());
				if(null != event){
					eventId = event.getId();
				}
				section =transaction.getSection();
				quantity =transaction.getQuantity();
			}
			stats = this.getStats(eventId, null, quantity, 
					siteFilters, section, null, catScheme);
		}

		for (Stat stat: stats) {
			excludeTicketMaps.addAll(DAORegistry.getExcludeTicketMapDAO().getAllMapsByTicketId(stat.getTicketId()));
		}

		ModelAndView mav = new ModelAndView("page-tmat-preview-stats");
		mav.addObject("stats", stats);
		mav.addObject("catScheme", catScheme);
		mav.addObject("excludeTicketMaps", excludeTicketMaps);
		return mav;
	}
	
	private ArrayList<Stat> getStats(Integer eventId, Integer categoryId, 
			Integer lotSize, ArrayList<String> siteFilters, String section, String row, String catScheme){
		
		ArrayList<Stat> stats = new ArrayList<Stat>();
		
		logger.info("looking for sellers: " + siteFilters);
		
		Integer[] sizes = {new Integer(1), new Integer(2), new Integer(4), new Integer(6), new Integer(8),
						   new Integer(10), new Integer(12), new Integer(14), new Integer(16), new Integer(18),
						   new Integer(20), new Integer(22), new Integer(24), new Integer(26), new Integer(28)
		};
		
		/* This is for Purchase price calculation - Begins */
		Map<String, ManagePurchasePrice> tourPriceMap = new HashMap<String, ManagePurchasePrice>();
		
		if(null !=eventId ){
			Event event = DAORegistry.getEventDAO().get(eventId);
			List<ManagePurchasePrice> manageTourPrices = DAORegistry.getManagePurchasePriceDAO().getAllByManagePurchasePriceByArtistId(event.getArtistId());
			for (ManagePurchasePrice manageTourPrice : manageTourPrices) {
				tourPriceMap.put(manageTourPrice.getExchange()+"-"+manageTourPrice.getTicketType(), manageTourPrice);
			}
		}
		/* This is for Purchase price calculation - Ends */
		try {
			for(int i = 0 ; i < sizes.length; i++) {
				if(sizes[i] >= lotSize) {
					for(String seller : siteFilters){
						String[] sellers = {seller};
						Stat stat = StatHelper.getEventCategorySiteQuantityLotStatActive(eventId, categoryId, section, row, 
								sellers, null, sizes[i], RemoveDuplicatePolicy.REMOVE_NON_WORD_CHARACTERS, catScheme,tourPriceMap);
						if(stat != null) {
							logger.info("adding a stat for get stats");
							stat.setSiteId(seller);
							stats.add(stat);
						} else {
							logger.info("No stat found for seller " + seller + " lotSize " + sizes[i]);
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ArrayList<Stat>();
		}
		return stats;
	}
	
	
	public ModelAndView loadAOZoneTickets(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
				
		String eventIdString = request.getParameter("eventId");
		String artistIdString = request.getParameter("artistId");
		System.out.println("***EVEntID:"+eventIdString);
		Collection<AOZoneTickets> aoZoneTickets = new  ArrayList<AOZoneTickets>();
		ModelAndView mav = new ModelAndView("page-AO-zone-ticket");
		
		String dateRange1String = request.getParameter("dateRange1");
		String dateRange2String = request.getParameter("dateRange2");
		
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		Date dateRange1 = null;
		Date dateRange2 = null;
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("America/New_York"));
		if(dateRange1String !=null){
			try{
				dateRange1 = df.parse(dateRange1String);
				calendar.setTime(dateRange1);
				calendar.add(Calendar.MILLISECOND, -1);
				dateRange1= calendar.getTime();
			}catch(Exception e){
				dateRange1 = null;
			}
		}
		
		if(dateRange2String !=null){
			try{
				dateRange2 = df.parse(dateRange2String);
				calendar.setTime(dateRange2);
				calendar.add(Calendar.DATE, 1);
				dateRange2= calendar.getTime();
			}catch(Exception e){
				dateRange2 = null;
			}
		}
				
		if(artistIdString != null)
		{
			Integer artistId = Integer.parseInt(artistIdString);
			Collection<Event> events =DAORegistry.getEventDAO().getAllActiveEventsByArtistId(artistId);
			for(Event event: events)
			{
				AOZoneTickets aoZoneTicketTmp = AOTicketUtil.computeAOZoneInventoryTickets(event);
				if(aoZoneTicketTmp != null)
					aoZoneTickets.add(aoZoneTicketTmp);
			}
		}else if(eventIdString !=null && "ALL".equals(eventIdString))
		{
			Collection<Event> events =DAORegistry.getEventDAO().getAllEventByAdmitoneId();
			for(Event event: events)
			{
				AOZoneTickets aoZoneTicketTmp = AOTicketUtil.computeAOZoneInventoryTickets(event);
				if(aoZoneTicketTmp != null)
					aoZoneTickets.add(aoZoneTicketTmp);
			}
		}else if(eventIdString != null)
		{
			Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventIdString));
			AOZoneTickets tmp = AOTicketUtil.computeAOZoneInventoryTickets(event);
			if(tmp != null)
				aoZoneTickets.add(tmp);
		}else if(dateRange1String != null || dateRange2String != null){			
			Collection<Event> events =DAORegistry.getEventDAO().getAllEventsByDates(dateRange1,dateRange2,EventStatus.ACTIVE);
			for(Event event: events)
			{
				AOZoneTickets aoZoneTicketTmp = AOTicketUtil.computeAOZoneInventoryTickets(event);
				if(aoZoneTicketTmp != null)
					aoZoneTickets.add(aoZoneTicketTmp);
			}
			mav.addObject("dateRange1", dateRange1);
			mav.addObject("dateRange2", dateRange2);
		}
		
		mav.addObject("aoZoneTickets", aoZoneTickets);
		mav.addObject("eventId", eventIdString);
		mav.addObject("artistId", artistIdString);
		return mav;
	}

	
	public ModelAndView loadZoneTickets(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String fromDateStr = request.getParameter("fromYear") + "/" + request.getParameter("fromMonth") + "/" + request.getParameter("fromDay");
		String toDateStr = request.getParameter("toYear") + "/" + request.getParameter("toMonth") + "/" + request.getParameter("toDay");
		String[] tourNames = request.getParameterValues("tourName");
		String[] venueSelected = request.getParameterValues("venue");
		String daySelected = request.getParameter("day");
		String quantitySelected = request.getParameter("quantity");
		String zoneSelected = request.getParameter("zone");		 
		String isShow = request.getParameter("showOnWeb");
		String isAlltour = request.getParameter("isAllTour");
		String isAllVenue = request.getParameter("isAllVenue");
		String action = request.getParameter("action");
		ModelAndView mav = new ModelAndView("page-zone-ticket");
		List<String> days = Arrays.asList("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
		List<String> quantities = new ArrayList<String>();
		List<String> zones = new ArrayList<String>();		
		List<String>  tourList = zonesManager.findEventNameList();		
		Collections.sort(tourList);
		if(tourNames==null)
			isAlltour="false";
		if(venueSelected == null)
			isAllVenue="false";
		if(daySelected==null)
			daySelected="ALL";
		if(quantitySelected==null)
			quantitySelected="ALL";
		if(zoneSelected==null)
			zoneSelected="ALL";
		if(isAlltour==null)
			isAlltour="false";
		if(isAllVenue==null)
			isAllVenue="false";
		DateFormat formatDate = new SimpleDateFormat("yyy/MM/dd");
		Date fromDate = null;
		Date toDate = null;
		try {
			fromDate = formatDate.parse(fromDateStr);	
			toDate = formatDate.parse(toDateStr);	
		} catch  (ParseException pe){
			fromDate = new Date();
			toDate = new Date();
		}
		List<String> venues = zonesManager.getVenueByTour(tourNames,isAllVenue);
		List<ZoneData> zoneTickets = new ArrayList<ZoneData>();
		zoneTickets = zonesManager.getZoneTickets(tourNames, isAlltour, venueSelected, isAllVenue, fromDate, toDate,daySelected, quantitySelected, zoneSelected, isShow);
		
		for(ZoneData zoneTicket:zoneTickets){
			if("ALL".equals(daySelected)){
			if(!quantities.contains(zoneTicket.getTicket())){
				quantities.add(zoneTicket.getTicket());
			}
			if(!zones.contains(zoneTicket.getZone())){
				zones.add(zoneTicket.getZone());
			}
			}else{
				if(daySelected.equalsIgnoreCase(zoneTicket.getDay_of_week())){
					if(!quantities.contains(zoneTicket.getTicket())){
						quantities.add(zoneTicket.getTicket());						
				}
			}
			}
			if("ALL".equals(quantitySelected)){
				if(!zones.contains(zoneTicket.getZone())){
					zones.add(zoneTicket.getZone());
				}	
			}else{
				if(Integer.parseInt(quantitySelected) == Integer.parseInt(zoneTicket.getTicket())){
				if(!zones.contains(zoneTicket.getZone())){
					zones.add(zoneTicket.getZone());
				}
				}
			}
			Collections.sort(quantities);
			Collections.sort(zones);
		}
		if(action!=null && "search".equals(action)){
			//zoneTickets = zonesManager.getZoneTickets(tourNames, isAlltour, venueSelected, isAllVenue, fromDate, toDate,daySelected, quantitySelected, zoneSelected, isShow);
		}else{
			zoneTickets.clear();
		}
		
		
		List<ZoneData> selectedTks = new ArrayList<ZoneData>();
		if(isShow != null && !isShow.isEmpty() && zoneTickets!=null )
		{
			for(ZoneData data: zoneTickets)
			{
				if(data.getWill_show().equalsIgnoreCase(isShow))
					selectedTks.add(data);
			}
		}else
		{
			selectedTks = zoneTickets;
		}
		if(selectedTks!=null){
			Collections.sort(selectedTks,new ZoneTicketComparator());
		}
		mav.addObject("zoneTickets", selectedTks);
		mav.addObject("zoneTour", tourNames);
		mav.addObject("tours", tourList);
		mav.addObject("tourSelected",tourNames);	
		mav.addObject("venueSelected",venueSelected);
		mav.addObject("venues", venues);
		mav.addObject("daySelected", daySelected);
		mav.addObject("days",days);
		mav.addObject("quantitySelected", quantitySelected);
		mav.addObject("quantities",quantities );
		mav.addObject("zoneSelected", zoneSelected);
		mav.addObject("zones", zones);
		mav.addObject("showOnWeb", isShow);
		mav.addObject("fromDate", fromDate);
		mav.addObject("toDate", toDate);
		mav.addObject("isAllTourSelected",isAlltour);
		mav.addObject("isAllVenueSelected",isAllVenue);
		return mav;
	}
	
	public ModelAndView loadBrowseVenueCategory(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException, ParseException {
		ModelAndView mav = new ModelAndView("page-browse-venue-category");
		String eventIdStr = request.getParameter("eventId");
		String venueIdStr = request.getParameter("venueId");
		String artistIdStr = request.getParameter("artistId");
		List<EventCapacityStatusAndRisk> capacityStatusAndRisks = new ArrayList<EventCapacityStatusAndRisk>();
		DateFormat df = new SimpleDateFormat("MMM dd yyyy");
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		DateFormat tf = new SimpleDateFormat("hh:mm aa");
		String dateRange1String = (request.getParameter("dateRange1")==null?"":request.getParameter("dateRange1"));
		String dateRange2String = (request.getParameter("dateRange2")==null?"":request.getParameter("dateRange2"));
		String eventStr="";
		boolean noCrawl=false;
		String noCrawlStr = request.getParameter("nocrawl");
		if(noCrawlStr!=null && !noCrawlStr.isEmpty() && noCrawlStr.equalsIgnoreCase("true")){
			noCrawl =true;
		}
		Integer eventId = null;
		if(eventIdStr!=null && !eventIdStr.isEmpty()){
			eventId = Integer.parseInt(eventIdStr);
		}
		Integer venueId = null;
		if(venueIdStr!=null && !venueIdStr.isEmpty()){
			venueId = Integer.parseInt(venueIdStr);
		}
		Integer artistId = null;
		if(artistIdStr!=null && !artistIdStr.isEmpty()){
			artistId = Integer.parseInt(artistIdStr);
		}
		String parentCategoryIdStr = request.getParameter("parentType");
		String childCategoryIdStr = request.getParameter("childType");
		String grandChildCategoryIdStr = request.getParameter("grandChildType");
		
		Date startDate = null;
		Date endDate = null;
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("America/New_York"));
		if(dateRange1String !=null && !dateRange1String.isEmpty()){
			try{
				startDate = dateFormat.parse(dateRange1String);
				calendar.setTime(startDate);
				calendar.add(Calendar.MILLISECOND, -1);
				startDate= calendar.getTime();
			}catch(Exception e){
				startDate = null;
			}
		}
		
		if(dateRange2String !=null && !dateRange2String .isEmpty()){
			try{
				endDate = dateFormat.parse(dateRange2String);
				calendar.setTime(endDate);
				calendar.add(Calendar.DATE, 1);
				endDate= calendar.getTime();
			}catch(Exception e){
				endDate = null;
			}
		}
		if(eventId ==null && artistId==null && venueId ==null && (startDate==null || endDate == null)){
			//skip..
		}else if(noCrawl){
			String iraEventStatusIdStr = request.getParameter("iraEventStatusId");
			if(iraEventStatusIdStr!=null && !iraEventStatusIdStr.isEmpty()){
				Long iraEventStatusId = Long.parseLong(iraEventStatusIdStr);
				List<Event>events = iraEventStatus.getEventFromMapById(iraEventStatusId);
				if(events!= null && !events.isEmpty()){
					for(Event event:events){
						EventCapacityStatusAndRisk capacityStatusAndRisk = new EventCapacityStatusAndRisk();
						capacityStatusAndRisk.setEventId(event.getId());
						capacityStatusAndRisk.setEventName(event.getName());
						String date = "TBD";
						String time = "TBD";
						if(event.getDate()!=null){
							date = df.format(event.getDate());
						}
						if(event.getTime()!=null){
							time = tf.format(event.getTime());
						}
						capacityStatusAndRisk.setEventDate(date);
						capacityStatusAndRisk.setEventTime(time);
						capacityStatusAndRisk.setVenueName(event.getVenue().getBuilding() + "," + event.getVenue().getCity() + "," + event.getVenue().getState());
						Collection<Category> categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(event.getVenueCategoryId());
						Map<Integer, Category> mapCategory = new HashMap<Integer, Category>();
						for(Category cat:categories){
							mapCategory.put(cat.getId(), cat);
						}
						Collection<Ticket> tickets = DAORegistry.getTicketDAO().getAllActiveTicketsByEvent(event.getId());
						Map<Integer,List<Ticket>> ticketByZoneMap= TicketUtil.getTicketMapByZones(tickets,categories);
						List<EventCapacityStatusAndRisk> children = new ArrayList<EventCapacityStatusAndRisk>();
						
						Double eventCapacity=0d;
						Double eventInHandCount=0d;
						Double eventArbitaryCount=0d;
						Double totalAvailable = 0d;
						
						if(null != event.getVenueCategory() &&  null != event.getVenueCategory().getVenueCapacity()) {
							totalAvailable= 0d+event.getVenueCategory().getVenueCapacity();	
						}
						
						
						for(Integer catId:ticketByZoneMap.keySet()){
							EventCapacityStatusAndRisk child = new EventCapacityStatusAndRisk();
							String zone="UNCAT";
							Double zoneCapacity=0d;
							Double inHandCount=0d;
							Double arbitaryCount=0d;
							Double totalAvailableZone=0d;
							if(catId!=0){
								Category cat = mapCategory.get(catId);
								zone = cat.getSymbol();
								zoneCapacity = cat.getCategoryCapacity()==null?0d:cat.getCategoryCapacity();
							}
							List<Ticket> tixList= ticketByZoneMap.get(catId);
							if(tixList!=null){
								for(Ticket ticket:tixList){
									if(ticket.isInHand()){
										inHandCount++;
									}else{
										arbitaryCount++;
									}
									totalAvailableZone++;
								}
							}
							eventCapacity+=zoneCapacity;
							eventInHandCount+=inHandCount;
							eventArbitaryCount+=arbitaryCount;
//							totalAvailable+=totalAvailableZone;
							
							child.setZone(zone);
							child.setZoneCapacity(zoneCapacity.intValue());
							child.setZoneInHand(inHandCount.intValue());
							child.setZoneShort(arbitaryCount.intValue());
							child.setTotalAvailable(totalAvailableZone.intValue());
							Double inHandPercentage = zoneCapacity==0?0d:(100*inHandCount/zoneCapacity);
							Double shortPercentage = zoneCapacity==0?0d:(100*arbitaryCount/zoneCapacity);
							child.setInHandPercentage((int)Math.round(inHandPercentage));
							child.setShortsPercentage((int)Math.round(shortPercentage));
							
							Double risk = (zoneCapacity==0?0:(arbitaryCount-inHandCount));
							Double notInPay = (zoneCapacity==0?0:(zoneCapacity-totalAvailableZone));
							Double riskPercentage = (zoneCapacity==0?0:(risk/zoneCapacity*100));
							Double notInPayPercentage = (zoneCapacity==0?0:(notInPay/zoneCapacity*100));
							
							child.setTotalRisk(risk.intValue());
							child.setTotalNotInPlay(notInPay.intValue());
							child.setTotalRiskPercentage((int)Math.round(riskPercentage));
							child.setTotalNotInPlayPercentage((int)Math.round(notInPayPercentage));
							children.add(child);
						}
						capacityStatusAndRisk.setZoneCapacity(eventCapacity.intValue());
						capacityStatusAndRisk.setZoneInHand(eventInHandCount.intValue());
						capacityStatusAndRisk.setZoneShort(eventArbitaryCount.intValue());
						capacityStatusAndRisk.setTotalAvailable(totalAvailable.intValue());
						Double inHandPercentage = (eventCapacity==0?0d:(100*eventInHandCount/eventCapacity));
						Double shortPercentage = (eventCapacity==0?0d:(100*eventArbitaryCount/eventCapacity));
						capacityStatusAndRisk.setInHandPercentage((int)Math.round(inHandPercentage));
						capacityStatusAndRisk.setShortsPercentage((int)Math.round(shortPercentage));
						
						Double totalRisk = eventArbitaryCount-eventInHandCount;
						Double totalNotInPay = (eventCapacity==0?0:((eventCapacity-totalAvailable)));
						Double totalRiskPercentage = (eventCapacity==0?0:totalRisk/eventCapacity*100);
						Double totalNotInPayPercentage = (eventCapacity==0?0:(totalNotInPay/eventCapacity*100));
						
						capacityStatusAndRisk.setTotalRisk(totalRisk.intValue());
						capacityStatusAndRisk.setTotalNotInPlay(totalNotInPay.intValue());
						capacityStatusAndRisk.setTotalRiskPercentage((int)Math.round(totalRiskPercentage));
						capacityStatusAndRisk.setTotalNotInPlayPercentage((int)Math.round(totalNotInPayPercentage));
						
						capacityStatusAndRisk.setChildren(children);
						capacityStatusAndRisks.add(capacityStatusAndRisk);
					}
				}
			}
			noCrawl=false; 
			
		}else{
			
			List<Event> events = new ArrayList<Event>();
			if(eventIdStr!=null && !eventIdStr.isEmpty()){
				Event event = DAORegistry.getEventDAO().get(eventId);
				events.add(event);
			}

			if(venueIdStr!=null && !venueIdStr.isEmpty()){
				Collection<Event> dbEvents = DAORegistry.getEventDAO().getAllEventsByVenue(Integer.parseInt(venueIdStr));
				events.addAll(dbEvents);
			}
			
			if(artistIdStr!=null && !artistIdStr.isEmpty()){
				Collection<Event> dbEvents= DAORegistry.getEventDAO().getAllActiveEventsByArtistId(Integer.parseInt(artistIdStr));
				events.addAll(dbEvents);
			}
			if(grandChildCategoryIdStr!=null){
				Integer grandChildCategoryId = Integer.parseInt(grandChildCategoryIdStr);
				Collection<Event> dbEvents = DAORegistry.getEventDAO().getAllEventsByGrandChildCategoryAndDateRange(startDate,endDate,grandChildCategoryId,EventStatus.ACTIVE);
				events.addAll(dbEvents);
			}
			noCrawl=true;
			for(Event event:events){
				eventStr += event.getId()+",";
			}
			if(!eventStr.isEmpty()){
				eventStr=eventStr.substring(0, eventStr.length()-1);
				String url =  sharedProperty.getBrowseUrl() + "WSForceEvents";
				HttpClient hc = new DefaultHttpClient();
				HttpPost hp = new HttpPost(url);
				NameValuePair eventIdParam = new BasicNameValuePair("eventIds", eventStr);
				NameValuePair destinationParam = new BasicNameValuePair("postBackUrl", sharedProperty.getAnalyticsUrl()+"ForceCrawledEventResult");
				List<NameValuePair> parameters = new ArrayList<NameValuePair>();
				parameters.add(eventIdParam);
				parameters.add(destinationParam);
				UrlEncodedFormEntity entity = new UrlEncodedFormEntity(parameters);
				hp.setEntity(entity);
				HttpResponse res = hc.execute(hp);
				HttpEntity resEntity =res.getEntity();
				String content = EntityUtils.toString(resEntity);
				XStream xstream = new XStream();
				xstream.alias("CrawlResponseWrapper", CrawlServerResponse.class);
				xstream.processAnnotations(CrawlServerResponse.class);
				CrawlServerResponse csr = new CrawlServerResponse();
				xstream.fromXML(content,csr);
				if(csr.getStatus().equalsIgnoreCase("success")){
					mav.addObject("totalCrawl", csr.getNumberofcrawls());
					String eventIds ="";
					if(csr.getMsg()!=null && !csr.getMsg().isEmpty()){
						String temp[] = csr.getMsg().split(":");
						if(temp.length==2){
							eventIds = temp[1];
						}
					}
					Long time = new Date().getTime();
					iraEventStatus.getEventMap().put(time, events);
					iraEventStatus.getEventStringMap().put(time, eventIds);
					mav.addObject("iraEventStatusId", time);
					mav.addObject("totalEvetnStr",eventIds);
//					mav.addObject("totalCrawl","");
				}
			}
		}
		mav.addObject("fromDateRange",dateRange1String);
		mav.addObject("toDateRange",dateRange2String);
		mav.addObject("parentType",parentCategoryIdStr);
		mav.addObject("childType",childCategoryIdStr);
		mav.addObject("grandChildType",grandChildCategoryIdStr);
		if(parentCategoryIdStr!=null && !parentCategoryIdStr.isEmpty()){
			mav.addObject("childTypes", DAORegistry.getChildTourCategoryDAO().getChildTourCategoryByTourCategory(Integer.parseInt(parentCategoryIdStr)));
		}
		if(childCategoryIdStr!=null && !childCategoryIdStr.isEmpty()){
			mav.addObject("grandChildTypes", DAORegistry.getGrandChildTourCategoryDAO().getGrandChildTourCategoryByChildTourCategory(Integer.parseInt(childCategoryIdStr)));
		}
		mav.addObject("venueId",venueIdStr);
		mav.addObject("eventId", eventIdStr);
		mav.addObject("artistId", artistIdStr);
		mav.addObject("categoriesStatus", capacityStatusAndRisks);
		mav.addObject("noCrawl", noCrawl);
		return mav;
	}
	
	
	public void getchildAndGrandChildEventDetails(ModelAndView mav,String parenEventType,String ChildEventType){
		Collection<ChildTourCategory> childTourCatList = new ArrayList<ChildTourCategory>();
		Collection<GrandChildTourCategory> grandchildTourCatList=new ArrayList<GrandChildTourCategory>();
		
		if(null != parenEventType && parenEventType.equalsIgnoreCase("all")){
			childTourCatList =DAORegistry.getChildTourCategoryDAO().getAll();
		}else if(null != parenEventType && parenEventType.trim().length() >0){
			TourCategory  tourCategoryObj=DAORegistry.getTourCategoryDAO().getTourCategoryByName(parenEventType);
			childTourCatList =DAORegistry.getChildTourCategoryDAO().getChildTourCategoryByTourCategory(tourCategoryObj.getId());	
		}
		
		
		if(null != ChildEventType && ChildEventType.equalsIgnoreCase("0")){
			grandchildTourCatList = DAORegistry.getGrandChildTourCategoryDAO().getAll();
		}else if(null != ChildEventType && ChildEventType.trim().length() >0){
			grandchildTourCatList = DAORegistry.getGrandChildTourCategoryDAO().getGrandChildTourCategoryByChildTourCategory(Integer.parseInt(ChildEventType.trim()));
		}
		
		for (GrandChildTourCategory grandChildTourCategory : grandchildTourCatList) {
			grandChildTourCategory.setName("Browse "+grandChildTourCategory.getName());
		}
		for (ChildTourCategory ChildTourCategory : childTourCatList) {
			ChildTourCategory.setName("Browse All "+ChildTourCategory.getName());
		}
		
		mav.addObject("childTourCatList", childTourCatList);
		mav.addObject("grandchildTourCatList", grandchildTourCatList);
		
	}
	
	public void getChidEventDetails(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		try{
			String parentType = request.getParameter("parentEventType");
			Collection<ChildTourCategory> childTourCatList = new ArrayList<ChildTourCategory>();
			if(parentType.equalsIgnoreCase("all")){
				childTourCatList =DAORegistry.getChildTourCategoryDAO().getAll();
			}else{
				TourCategory  tourCategoryObj=DAORegistry.getTourCategoryDAO().getTourCategoryByName(parentType);
				childTourCatList =DAORegistry.getChildTourCategoryDAO().getChildTourCategoryByTourCategory(tourCategoryObj.getId());	
			}
					
			PrintWriter out = response.getWriter();
			JSONArray childTourDetails = new JSONArray();
			JSONObject jObj = null;
			for (ChildTourCategory childTourCatObj : childTourCatList) {
				 jObj = new JSONObject();
				 jObj.put("childTourId", childTourCatObj.getId());
		         jObj.put("childTourName",  "Browse All "+childTourCatObj.getName());
		         childTourDetails.put(jObj);
			}
			out.println(childTourDetails.toString());
		}catch (Exception e) {
			throw e;
		}
	}
	
	public void getGrandChidEventDetails(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		try{
			Collection<GrandChildTourCategory> grandchildTourCatList=new ArrayList<GrandChildTourCategory>();
			String parentType = request.getParameter("childEventTypeId");
			if(parentType.equalsIgnoreCase("0")){
				grandchildTourCatList = DAORegistry.getGrandChildTourCategoryDAO().getAll();
			}else{
				grandchildTourCatList = DAORegistry.getGrandChildTourCategoryDAO().getGrandChildTourCategoryByChildTourCategory(Integer.parseInt(parentType.trim()));
			}
			PrintWriter out = response.getWriter();
			JSONArray grandChildTourDetails = new JSONArray();
			JSONObject jObj = null;
			for (GrandChildTourCategory GrandChildTourCategory : grandchildTourCatList) {
				 jObj = new JSONObject();
				 jObj.put("grandChildTourId", GrandChildTourCategory.getId());
		         jObj.put("grandChildTourName",  "Browse "+GrandChildTourCategory.getName());
		         grandChildTourDetails.put(jObj);
			}
			out.println(grandChildTourDetails.toString());
		}catch (Exception e) {
			throw e;
		}
	}
	public ZonesManager getZonesManager() {
		return zonesManager;
	}

	public void setZonesManager(ZonesManager zonesManager) {
		this.zonesManager = zonesManager;
	}
	
	class ZoneTicketComparator implements Comparator<ZoneData>{

		@Override
		public int compare(ZoneData o1, ZoneData o2) {
			if(o1.getEvent_date()!=null && o2.getEvent_date()!=null){
			 try {
				return formatZoneDate.parse(o1.getEvent_date()).compareTo(formatZoneDate.parse(o2.getEvent_date()));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return 0;
			}
			}
			return 0;
		}
		
	}	

	public IRAEventStatus getIraEventStatus() {
		return iraEventStatus;
	}

	public void setIraEventStatus(IRAEventStatus iraEventStatus) {
		this.iraEventStatus = iraEventStatus;
	}
	
	public SharedProperty getSharedProperty() {
		return sharedProperty;
	}
	public void setSharedProperty(SharedProperty sharedProperty) {
		this.sharedProperty = sharedProperty;
	}
	
}

