package com.admitone.tmat.web;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jxl.Sheet;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.acegisecurity.ui.webapp.AuthenticationProcessingFilter;
import org.springframework.orm.hibernate3.annotation.AnnotationSessionFactoryBean;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.HarvestEvent;
import com.admitone.tmat.data.HarvestTicket;
import com.admitone.tmat.data.CategoryMapping;
import com.admitone.tmat.data.UserAction;
import com.admitone.tmat.enums.RemoveDuplicatePolicy;
import com.admitone.tmat.enums.TourType;
import com.admitone.tmat.utils.Categorizer;
import com.admitone.tmat.utils.SectionRowStripper;
import com.admitone.tmat.utils.TextUtil;
import com.admitone.tmat.utils.TicketUtil;
import com.admitone.tmat.web.pojo.WebTicketRow;

/**
 * TODO:Delete web tickets from memory every X hours.
 *
 */
public class ReportController extends  MultiActionController {
	private AnnotationSessionFactoryBean sessionFactoryBean;
	private static final int MAX_ROWS_PER_SHEET = 65000;

	//private static final Pattern numberPattern = Pattern.compile("^\\d+$");
	private String numberRegex = "^\\d+$";

	private Map<String, List<WebTicketRow>> webTicketsByUserEventMap = new HashMap<String, List<WebTicketRow>>();
	private Map<String, List<CategoryMapping>> categoryMappingByUserMap = new HashMap<String, List<CategoryMapping>>();

	private Map<String, String> returnedMessageById = new HashMap<String, String>();
	
	/**
	 * Show report page.
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public ModelAndView loadReportPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String username = (String)request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);
		
		Collection<UserAction> userActions = null; 
		if(username != null && !username.isEmpty()){
			userActions = DAORegistry.getUserActionDAO().getActionsByUsername(username);
		} else {
			userActions = DAORegistry.getUserActionDAO().getAll();
		}
		
		ModelAndView mav = new ModelAndView("page-report");	
		mav.addObject("actions", userActions);

		return mav;
	}	
	
	/**
	 * Show event lists on the page.
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public ModelAndView loadReportDataHarvestPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		
		ModelAndView mav = new ModelAndView("page-data-harvest");	
		String fromDateStr = request.getParameter("fromYear") + "/" + request.getParameter("fromMonth") + "/" + request.getParameter("fromDay");
		String toDateStr = request.getParameter("toYear") + "/" + request.getParameter("toMonth") + "/" + request.getParameter("toDay");
	
		DateFormat formatDate = new SimpleDateFormat("yyy/MM/dd");
		Date fromDate = null;
		Date toDate = null;
		try {
			fromDate = formatDate.parse(fromDateStr);	
			toDate = formatDate.parse(toDateStr);	
		} catch  (ParseException pe){
			fromDate = new Date();
			toDate = new Date();
		}
		
		if (action != null && action.equalsIgnoreCase("Mark")) {
			Enumeration enumeration = request.getParameterNames();
			Map<Integer, Boolean> selectedEventMap = new HashMap<Integer, Boolean>();
			while (enumeration.hasMoreElements()) {
				String param = (String)enumeration.nextElement();
				if (!param.startsWith("event-")) {
					continue;
				}
				
				String eventType = request.getParameter("eventType");
				Integer eventId = Integer.valueOf(param.replaceAll("event-", ""));
				selectedEventMap.put(eventId, true);
				HarvestEvent event = DAORegistry.getHarvestEventDAO().get(eventId);
				event.setEventType(TourType.valueOf(eventType));
				DAORegistry.getHarvestEventDAO().update(event);
			}
			mav.addObject("selectedEventMap", selectedEventMap);
		}

		Collection<HarvestEvent> events = DAORegistry.getHarvestEventDAO().getEventsInDateRange(fromDate, toDate);

		mav.addObject("events", events);
		mav.addObject("fromDate", fromDate);
		mav.addObject("toDate", toDate);

		return mav;
	}

	private List<WebTicketRow> loadWebTicketsFromMap(String username, Integer eventId) {
		String key = username + eventId;
		// load it from DB
		Collection<HarvestTicket> tickets = DAORegistry.getHarvestTicketDAO().getTicketsForEvent(eventId);
		if (tickets == null) {
			return null;
		}

		ArrayList<WebTicketRow> webTickets = new ArrayList<WebTicketRow>();

		Map<Integer, TourType> tourTypeByEventId = new HashMap<Integer, TourType>();
		for(HarvestTicket ticket : tickets){
//			String normalizedRow = null;
			if (ticket.getRow() != null) {
				TourType tourType = tourTypeByEventId.get(ticket.getEventId());
				if (tourType == null) {
					HarvestEvent event = DAORegistry.getHarvestEventDAO().get(ticket.getEventId());
					if (event == null) {
						tourType = TourType.OTHER;
					} else {
						tourType = event.getEventType();
					}
					tourTypeByEventId.put(ticket.getEventId(), tourType);
				}
//				normalizedRow = SectionRowStripper.strip(tourType, ticket.getRow());
			}
			WebTicketRow webTicket = new WebTicketRow(ticket);
				       
			webTickets.add(webTicket);
		}
		webTicketsByUserEventMap.put(key, webTickets);
		return webTickets;
	}

	private List<WebTicketRow> categorizeTickets(String username, Integer eventId, boolean removeDuplicate) {		
		// apply categorizer
		
		List<WebTicketRow> webTickets = getWebTicketsFromMap(username, eventId);
		if (webTickets == null) {
			webTickets = loadWebTicketsFromMap(username, eventId);
		}
		List<CategoryMapping> mappings = categoryMappingByUserMap.get(username);
		if (mappings == null) {
			mappings = new ArrayList<CategoryMapping>();
		}
		Map<Integer, String> categorySymbolById = new HashMap<Integer, String>();
		for(CategoryMapping categoryMapping: mappings) {
			categorySymbolById.put(categoryMapping.getCategoryId(), ((WebCategoryMapping)categoryMapping).getCategoryName());
		}

		if (removeDuplicate) {
			webTickets = TicketUtil.removeDuplicateWebTickets(webTickets);
		}
		
		for(WebTicketRow ticket : webTickets) {
			Integer categoryId = Categorizer.computeCategory(mappings, ticket.getNormalizedSection(), ticket.getNormalizedRow());
			if(categoryId != null){
				ticket.setCategorySymbol(categorySymbolById.get(categoryId));
			}
		}		
		return webTickets;
	}

	private List<WebTicketRow> getWebTicketsFromMap(String username, Integer eventId) {
		String key = username + eventId;		
		return webTicketsByUserEventMap.get(key);
	}
	
	private void putWebTicketsToMap(String username, Integer eventId, List<WebTicketRow> webTickets) {
		String key = username + eventId;
		webTicketsByUserEventMap.put(key, webTickets);	
	}

	/**
	 * Invoked when the user upload his own tickets to categorize them.
	 * The uploaded tickets are not stored in the DB. 
	 * @return
	 */
	public ModelAndView loadReportDataHarvestUploadTickets(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String username = (String)request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);
		String uploadId = request.getParameter("uploadId");
		
		ArrayList<WebTicketRow> webTickets = new ArrayList<WebTicketRow>();

		if (request instanceof MultipartHttpServletRequest) {
			DateFormat formatDate = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss aa");
			DateFormat formatOtherDate = new SimpleDateFormat("MM/dd/yyyy HH:mm");

			MultipartHttpServletRequest multi = (MultipartHttpServletRequest) request;
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					multi.getFile("file").getInputStream()));
			String line;
			int lineNum = 0;
			String info = "";

			while ((line = reader.readLine()) != null) {
				lineNum++;
				if (line.trim().isEmpty()) {
					continue;
				}

				if (lineNum == 1 || lineNum == 2 || lineNum == 3) {
					continue;
				}
				String[] tokens = line.split(",");

				if (tokens.length != 8) {
					returnedMessageById.put(uploadId, "ERROR: Error Line " + lineNum + ": " + line + ": Invalid line.");
					return null;
				}
				Integer quantity = null;
				if (!tokens[0].isEmpty()) {
					quantity = Integer.valueOf(TextUtil.trimQuotes(tokens[0]));					
				}
				
				String section = TextUtil.trimQuotes(tokens[1]);
				String row = TextUtil.trimQuotes(tokens[2]);
				Double wholesale = null;
				if(!tokens[3].isEmpty()) {
					wholesale = Double.parseDouble(tokens[3]);
				}
				Double online = null;
				if(!tokens[4].isEmpty()) {
					online = Double.parseDouble(tokens[4]);
				} else {
					online = wholesale;
				}
				
				String broker = tokens[5];

				String insDate = tokens[6].trim();
				String soldDate = tokens[7].trim();

				Date startDate = null;
				Date endDate = null;

				try {
					startDate = formatDate.parse(insDate);
				} catch (ParseException e) {
					try{ 
						startDate = formatOtherDate.parse(insDate);
					} catch  (ParseException pe){
						returnedMessageById.put(uploadId, "ERROR: Error Line " + lineNum + ": " + line + ": Invalid Ins date or time.");
						pe.printStackTrace();
						return null;
					}
				}
				
				try {
					endDate = formatDate.parse(soldDate);
				} catch (ParseException e) {
					try{ 
						endDate = formatOtherDate.parse(soldDate);
					} catch  (ParseException pe){
						returnedMessageById.put(uploadId, "ERROR: Error Line " + lineNum + ": " + line + ": Invalid Sold date or time.");
						
						pe.printStackTrace();
						return null;
					}
				}
				// FIXME: get normalized section and row
				WebTicketRow ticket = new WebTicketRow(new Integer(lineNum), quantity, section, section, row, row, wholesale, online, broker, startDate,
						endDate);
				webTickets.add(ticket);
			}
		}
	
		putWebTicketsToMap(username, 0, webTickets);
		returnedMessageById.put(uploadId, "OK: " + webTickets.size() + " tickets loaded");

		return null;
	
	}
	
	/**
	 * Invoked when the user upload a category csv file.
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public ModelAndView loadReportDataHarvestUploadCategories(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String username = (String)request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);

		String uploadId = request.getParameter("uploadId");

		if (!(request instanceof MultipartHttpServletRequest)) {
			return null;
		}

		MultipartHttpServletRequest multi = (MultipartHttpServletRequest) request;

		BufferedReader reader = new BufferedReader(new InputStreamReader(
				multi.getFile("file").getInputStream()));
		String line;
		int lineCount = 0;
		List<CategoryMapping> mappings = new ArrayList<CategoryMapping>();

		while ((line = reader.readLine()) != null) {
			try {
				lineCount++;
				if (line.trim().isEmpty()) {
					continue;
				}
				
				String[] lineTokens = line.split(",");
				
				if (lineTokens.length != 8) {
					returnedMessageById.put(uploadId, "ERROR: Error Line " + lineCount + ": " + line + ": Invalid format.");
					return null;						
				}
				
				String venueStr = TextUtil.trimQuotes(lineTokens[0]);
				String section = TextUtil.trimQuotes(lineTokens[1]);
				String row = TextUtil.trimQuotes(lineTokens[2]);
				String seat = TextUtil.trimQuotes(lineTokens[3]);
				String categorySymbol = TextUtil.trimQuotes(lineTokens[4]);
				String catScheme = TextUtil.trimQuotes(lineTokens[5]);
				String catSeatQty = TextUtil.trimQuotes(lineTokens[6]);
				String equivalentCats = TextUtil.trimQuotes(lineTokens[7]);

				String startSection = "";
				String endSection = "";
				String startRow = "";
				String endRow = "";
				String startSeat = "";
				String endSeat = "";

				if (section.indexOf('-') > 0) {
					String[] tokens = section.split("-");
					startSection = tokens[0].replaceAll(CategoryMapping.MINUS_ESCAPE_MATCH_STRING, "-")
							.trim();
					endSection = tokens[1].replaceAll(CategoryMapping.MINUS_ESCAPE_MATCH_STRING, "-")
							.trim();
				} else {
					startSection = section;
					endSection = section;
				}

				if (row.indexOf('-') > 0) {
					String[] tokens = row.split("-");
					startRow = tokens[0].replaceAll(CategoryMapping.MINUS_ESCAPE_MATCH_STRING, "-").trim();
					endRow = tokens[1].replaceAll(CategoryMapping.MINUS_ESCAPE_MATCH_STRING, "-").trim();
				} else {
					startRow = row;
					endRow = row;
				}

				if (seat.indexOf('-') > 0) {
					String[] tokens = seat.split("-");
					startSeat = tokens[0].replaceAll(CategoryMapping.MINUS_ESCAPE_MATCH_STRING, "-").trim();
					endSeat = tokens[1].replaceAll(CategoryMapping.MINUS_ESCAPE_MATCH_STRING, "-").trim();
				} else {
					startSeat = seat;
					endSeat = seat;
				}

				// if start section and end section cannot be compared
				// (e.g., a number and string with letters) => error
				if (endSection.length() > 0
						&& !startSection.equals("*")
						&& startSection.matches(numberRegex) != endSection.matches(numberRegex)) {
					returnedMessageById.put(uploadId, "ERROR: Error Invalid line "
									+ lineCount
									+ ": "
									+ line
									+ ": Cannot compare number and string sections");
					return null;
				}

				if (endRow.length() > 0
						&& !startRow.equals("*")
						&& startRow.matches(numberRegex) != endRow.matches(numberRegex)) {
					returnedMessageById.put(uploadId, "ERROR: Invalid line " + lineCount
							+ ": " + line
							+ ": Cannot compare number and string rows");
					return null;
				}

				if (endSeat.length() > 0 && !startSeat.equals("*")
						&& startSeat.matches(numberRegex) != endSeat.matches(numberRegex)) {
					returnedMessageById.put(uploadId, "ERROR: Invalid line " + lineCount
							+ ": " + line
							+ ": Cannot compare number and string seats");
					return null;
				}


				WebCategoryMapping mapping = new WebCategoryMapping();
				mapping.setCategoryId(new Integer(lineCount));
				mapping.setStartSection(startSection);
				mapping.setEndSection(endSection);
				mapping.setStartRow(startRow);
				mapping.setEndRow(endRow);
				mapping.setStartSeat(startSeat);
				mapping.setEndSeat(endSeat);
				mapping.setCategoryName(categorySymbol);

				mappings.add(mapping);

				//System.out.println("Created Mapping for event: " + event + " cat: " + categorySymbol);

			} catch (Exception e) {
				e.printStackTrace();
				returnedMessageById.put(uploadId, "ERROR: Invalid line: " + lineCount + ": "
						+ line);
				return null;
			}
		}
					
		categoryMappingByUserMap.put(username, mappings);
		returnedMessageById.put(uploadId, "OK: " + mappings.size() + " category mappings loaded");		
		return null;
	}
	
	/**
	 * Get message from upload.
	 * Because we cannot get the response message directly after the upload.
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public ModelAndView loadReturnedMessage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String uploadId = request.getParameter("uploadId");
		String message = returnedMessageById.get(uploadId);
		if (message != null) {
			response.getOutputStream().print(message);
		}
		return null;
	}

	public ModelAndView loadShowDataHarvestPreview(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username = (String)request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);

		Integer eventId = Integer.parseInt(request.getParameter("eventId"));
		String removeDuplicateStr = request.getParameter("removeDuplicate");
		boolean removeDuplicate = false;
		if (removeDuplicateStr != null && removeDuplicateStr.equals("SCRUB")) {
			removeDuplicate = true;
		}
		
		String normalizeSectionAndRowsString = request.getParameter("normalizeSectionAndRows");
		boolean normalizeSectionAndRows = false;
		if (normalizeSectionAndRowsString != null && normalizeSectionAndRowsString.equals("YES")) {
			normalizeSectionAndRows = true;
		}
		
		List<WebTicketRow> webTickets = categorizeTickets(username, eventId, removeDuplicate);
		
		NumberFormat numberFormat = NumberFormat.getInstance();
		numberFormat.setGroupingUsed(false);
		numberFormat.setMinimumFractionDigits(2);
		numberFormat.setMaximumFractionDigits(2);
		
 		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		response.getOutputStream().print("[");
		int ticketCount = webTickets.size();
		int i = 0;
		for (WebTicketRow ticket: webTickets) {
			Double adjustedCurrentPrice = 0.00;
			Double buyItNowPrice = 0.00;
			
			if (ticket.getAdjustedCurrentPrice() != null) {
				adjustedCurrentPrice = ticket.getAdjustedCurrentPrice();
			}

			if (ticket.getBuyItNowPrice() != null) {
				buyItNowPrice = ticket.getBuyItNowPrice();
			}
			
			String cat = ticket.getCategorySymbol();
			if (cat == null) {
				cat = "";
			}

			String section = ticket.getSection();;
			if (normalizeSectionAndRows) {
				section = ticket.getNormalizedSection();
			} 

			String row = ticket.getRow();;
			if (normalizeSectionAndRows) {
				row = ticket.getNormalizedRow();
			} 		

			response.getOutputStream().print(
					"["
					+ ticket.getRemainingQuantity() + ","
					+ "'" + cat + "',"
					+ "'" + formatText(section) + "',"
					+ "'" + formatText(row) + "',"
					+ numberFormat.format(adjustedCurrentPrice) + ","
					+ numberFormat.format(buyItNowPrice) + ","
					+ "'" + ticket.getSeller() + "' ,"
					+ "'" + dateFormat.format(ticket.getLastUpdate()) + "', "
					+ "'" + dateFormat.format(ticket.getInsertionDate()) + "'"
					+ "]"
			);
			i++;
			
			if (i < ticketCount) {
				response.getOutputStream().print(",");
			}
		}
		response.getOutputStream().print("]");
		
		return null;
	}

	private String formatText(String str) {
		if (str == null) {
			return "";
		}
		return str;
	}

	public ModelAndView loadReportDownloadDataHarvestCsvFile(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
				
		String username = (String)request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);

		Integer eventId = Integer.parseInt(request.getParameter("eventId"));
		
		String removeDuplicateStr = request.getParameter("removeDuplicate");
		String normalizeSectionAndRowsString = request.getParameter("normalizeSectionAndRows");
		
		boolean removeDuplicate = false;
		if (removeDuplicateStr != null && removeDuplicateStr.equals("SCRUB")) {
			removeDuplicate = true;
		}
		
		Boolean normalizeSectionAndRows = false;
		if (normalizeSectionAndRowsString != null && normalizeSectionAndRowsString.equals("YES")) {
			normalizeSectionAndRows = true;
		}
		
		Date today = new Date();

 		DateFormat format = new SimpleDateFormat("MM/dd/yyyy,hh:mm,aa");
		DateFormat todayFormat = new SimpleDateFormat("MM/dd/yyyy,hh:mm,aa");
		StringBuffer line = new StringBuffer();
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=historical_tickets_" + URLEncoder.encode(username) + "_"
				+ todayFormat.format(today) + ".csv");
		
		List<WebTicketRow> webTickets = categorizeTickets(username, eventId, removeDuplicate);
		if (webTickets == null) {
			return null;
		}
		
		NumberFormat numberFormat = NumberFormat.getInstance();
		numberFormat.setGroupingUsed(false);
		numberFormat.setMinimumFractionDigits(2);
		numberFormat.setMaximumFractionDigits(2);
				
		//response.getOutputStream().print("quantity, category, section, row, wholesale, online, inserted, updated, seller\n");
		line.append("quantity, category, section, row, wholesale, online, inserted, updated, seller\n");
		
		for(WebTicketRow ticket : webTickets) {			
			String cat = ticket.getCategorySymbol();
			if(cat == null){
				cat = "";
			}
			
			Double wPrice = ticket.getAdjustedCurrentPrice();
			Double oPrice = ticket.getBuyItNowPrice();
			if(wPrice == null){
				wPrice = oPrice;
			}
			if(oPrice == null){
				oPrice = wPrice;
			}
			
			if (oPrice == null) {
				oPrice = 0.0;
			}
			if (wPrice == null) {
				oPrice = 0.0;
			}
			
			String section = ticket.getSection();
			String row = ticket.getRow();
			if (normalizeSectionAndRows) {
				section = ticket.getNormalizedSection();
				row = ticket.getNormalizedRow();
			} 
				System.out.println(ticket.getInsertionDate());
			 line.append(ticket.getQuantity() + ", " + csvFormat(cat.trim()) + ", "
					+ csvFormat(formatText(section)) + ", " + csvFormat(formatText(row)) + ", "
					+ csvFormat(numberFormat.format(wPrice)) + ", "
					+ csvFormat(numberFormat.format(oPrice)) + ", "
					+ csvFormat(format.format(ticket.getInsertionDate())) + ", "
					+ csvFormat(format.format(ticket.getLastUpdate())) + ", "
					+ csvFormat(ticket.getSeller()) + "\n");			
		}
		response.getOutputStream().write(line.toString().getBytes());
		return null;
	}
	private String csvFormat(String str){
		if(str!=null){
			str= str.replaceAll(",", " ").replaceAll("\n", " ").replaceAll("\r", " ");			
		}
		return str;
	}
	private Sheet createSheet(WritableWorkbook workbook, String name, List<WebTicketRow> webTickets, Boolean normalizeSectionAndRows, int page) throws RowsExceededException, WriteException {
 		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

		NumberFormat numberFormat = NumberFormat.getInstance();
		numberFormat.setGroupingUsed(false);
		numberFormat.setMinimumFractionDigits(2);
		numberFormat.setMaximumFractionDigits(2);
		WritableSheet sheet = workbook.createSheet(name, page);
	
		if (webTickets == null) {
			return null;
		}

		sheet.addCell(new Label(0, 0, "quantity"));
		sheet.addCell(new Label(1, 0, "category"));
		sheet.addCell(new Label(2, 0, "section"));
		sheet.addCell(new Label(3, 0, "row"));
		sheet.addCell(new Label(4, 0, "wholesale"));
		sheet.addCell(new Label(5, 0, "online"));
		sheet.addCell(new Label(6, 0, "inserted"));
		sheet.addCell(new Label(7, 0, "updated"));
		sheet.addCell(new Label(8, 0, "seller"));

		int rowNum = 1;
		for(WebTicketRow ticket : webTickets) {		
			rowNum++;
			
			String cat = ticket.getCategorySymbol();
			if(cat == null){
				cat = "";
			}
			
			Double wPrice = ticket.getAdjustedCurrentPrice();
			Double oPrice = ticket.getBuyItNowPrice();
			if(wPrice == null){
				wPrice = oPrice;
			}
			if(oPrice == null){
				oPrice = wPrice;
			}
			
			if (oPrice == null) {
				oPrice = 0.0;
			}
			if (wPrice == null) {
				oPrice = 0.0;
			}

			sheet.addCell(new Label(0, rowNum, ticket.getRemainingQuantity() + ""));
			sheet.addCell(new Label(1, rowNum, cat));

			String ticketSection = ticket.getSection();
			String ticketRow = ticket.getRow();
			if (normalizeSectionAndRows) {
				ticketSection = ticket.getNormalizedSection();
				ticketRow = ticket.getNormalizedRow();
			}
			
			sheet.addCell(new Label(2, rowNum, ticketSection));
			sheet.addCell(new Label(3, rowNum, ticketRow));
			sheet.addCell(new Label(4, rowNum, numberFormat.format(wPrice)));
			sheet.addCell(new Label(5, rowNum, numberFormat.format(oPrice)));
			sheet.addCell(new Label(6, rowNum, dateFormat.format(ticket.getInsertionDate())));
			sheet.addCell(new Label(7, rowNum, dateFormat.format(ticket.getLastUpdate())));
			sheet.addCell(new Label(8, rowNum, ticket.getSeller()));
		}
		return sheet;
	}
	
	public ModelAndView loadReportDownloadDataHarvestXlsFile(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String removeDuplicateStr = request.getParameter("removeDuplicate");
		String normalizeSectionAndRowsString = request.getParameter("normalizeSectionAndRows");
				
		String username = (String)request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);

		boolean removeDuplicate = false;
		if (removeDuplicateStr != null && removeDuplicateStr.equals("SCRUB")) {
			removeDuplicate = true;
		}
		
		Boolean normalizeSectionAndRows = false;
		if (normalizeSectionAndRowsString != null && normalizeSectionAndRowsString.equals("YES")) {
			normalizeSectionAndRows = true;
		}
		
		String[] eventIdStrings = request.getParameter("eventIds").split(",");
				
		Date today = new Date();

 		DateFormat sheetNameDateFormat = new SimpleDateFormat("MM/dd/yyyy");
 		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

		NumberFormat numberFormat = NumberFormat.getInstance();
		numberFormat.setGroupingUsed(false);
		numberFormat.setMinimumFractionDigits(2);
		numberFormat.setMaximumFractionDigits(2);

		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=historical_tickets_" + URLEncoder.encode(username) + "_"
				+ dateFormat.format(today) + ".xls");

		WritableWorkbook workbook = Workbook.createWorkbook(response.getOutputStream());
		Set<String> usedSheetNames = new HashSet<String>();

		for(String eventIdString: eventIdStrings) {
			Integer eventId = Integer.parseInt(eventIdString);
			
			HarvestEvent event = DAORegistry.getHarvestEventDAO().get(eventId);
			
			// sheet name cannot contains / and .
			String eventName = event.getName().replaceAll("[:\\\\/?*\\[\\]]", "");
			if (eventName.length() > 18) {
				eventName = eventName.substring(18);
			}
			
			
			List<WebTicketRow> webTickets = categorizeTickets(username, eventId, removeDuplicate);
			
			// split the sheets in packets of MAX_ROWS_PER_SHEET
			int page = 0;
			for (int offset = 0; offset < webTickets.size(); offset+= MAX_ROWS_PER_SHEET) {
				page++;
				List<WebTicketRow> rows = webTickets.subList(offset, offset + Math.min(webTickets.size() - offset, MAX_ROWS_PER_SHEET));
				String sheetDateStr="TBD";
				if(event.getDate()!=null){
					sheetDateStr=sheetNameDateFormat.format(event.getDate()).replace("/", "-");
				}
				String sheetName = eventName + "-" + sheetDateStr;
				int i = 2;
				while(usedSheetNames.contains(sheetName)) {
					sheetName = eventName + "-" + sheetDateStr + "-" + i++ + "-page" + page;
				}
				usedSheetNames.add(sheetName);
				try {
					createSheet(workbook, sheetName, rows, normalizeSectionAndRows, page);
				} catch (Exception e) {
					throw new ServletException(e);
				}
			}
		}
		if(usedSheetNames.size() <1)
		{
			List<WebTicketRow> rows = new ArrayList<WebTicketRow>();
			try {
				createSheet(workbook, "Dummy",rows , normalizeSectionAndRows, 1);
			} catch(Exception ex){
				throw new ServletException();
			}
		}
	try {
			workbook.write();
			workbook.close();
		} catch (WriteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new ServletException(e);
		}
		return null;
	}


	public AnnotationSessionFactoryBean getSessionFactoryBean() {
		return sessionFactoryBean;
	}

	public void setSessionFactoryBean(AnnotationSessionFactoryBean sessionFactoryBean) {
		this.sessionFactoryBean = sessionFactoryBean;
	}

	public static class WebCategoryMapping extends CategoryMapping {
		private String categoryName;

		public String getCategoryName() {
			return categoryName;
		}

		public void setCategoryName(String categoryName) {
			this.categoryName = categoryName;
		}
		
		
	}
}
