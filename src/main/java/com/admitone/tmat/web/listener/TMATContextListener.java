package com.admitone.tmat.web.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.commons.logging.impl.SLF4JLogFactory;
import org.hibernate.Hibernate;
import org.slf4j.LoggerFactory;
import org.springframework.orm.hibernate3.HibernateAccessor;

import com.tangosol.net.CacheFactory;

public class TMATContextListener implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		System.out.println("Servlet Destroyed");
		
		// shutdown logger
		SLF4JLogFactory.releaseAll();
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
