--
-- order is important here
--

ALTER TABLE artist ALTER COLUMN id INT NOT NULL;
ALTER TABLE artist ADD CONSTRAINT pk_artist PRIMARY KEY (id);

ALTER TABLE site ADD CONSTRAINT pk_site PRIMARY KEY (id);

ALTER TABLE artist_price_adjustment ADD PRIMARY KEY (artist_id, site_id);
ALTER TABLE artist_price_adjustment ADD FOREIGN KEY (artist_id) REFERENCES artist(id);
ALTER TABLE artist_price_adjustment ADD FOREIGN KEY (site_id) REFERENCES site(id);
CREATE INDEX index_artist_price_adjustment_artist_id ON artist_price_adjustment (artist_id);
CREATE INDEX index_artist_price_adjustment_site_id ON artist_price_adjustment (site_id);

-- *** rename manually table user to admitone_user ***
ALTER TABLE admitone_user ADD CONSTRAINT pk_admitone_user PRIMARY KEY (username);
ALTER TABLE role ADD CONSTRAINT pk_role PRIMARY KEY (id);
ALTER TABLE user_role ADD CONSTRAINT pk_user_role PRIMARY KEY (username, role_id);
ALTER TABLE user_role ADD FOREIGN KEY (username) REFERENCES admitone_user(username);
ALTER TABLE user_role ADD FOREIGN KEY (role_id) REFERENCES role(id);
CREATE INDEX index_user_role_index_username ON user_role (username);
CREATE INDEX index_user_role_index_role_id ON user_role (role_id);

ALTER TABLE bookmark ADD CONSTRAINT pk_bookmark PRIMARY KEY (username, type, object_id);
ALTER TABLE bookmark ADD FOREIGN KEY (username) REFERENCES admitone_user(username);
CREATE INDEX index_bookmark_username ON bookmark (username);

ALTER TABLE tour ALTER COLUMN id INT NOT NULL;
ALTER TABLE tour ADD CONSTRAINT pk_tour PRIMARY KEY (id);

ALTER TABLE venue ALTER COLUMN id INT NOT NULL;
ALTER TABLE venue ADD CONSTRAINT pk_venue PRIMARY KEY (id);

ALTER TABLE event ALTER COLUMN id INT NOT NULL;
ALTER TABLE event ADD CONSTRAINT pk_event PRIMARY KEY (id);
ALTER TABLE event ADD FOREIGN KEY(tour_id) REFERENCES tour(id);
ALTER TABLE event ADD FOREIGN KEY(venue_id) REFERENCES venue(id);
CREATE INDEX index_event_status ON event (event_status);
CREATE INDEX index_event_tour_id ON event (tour_id);
CREATE INDEX index_event_venue_id ON event (venue_id);

ALTER TABLE event_synonym ALTER COLUMN id INT NOT NULL;
ALTER TABLE event_synonym ADD CONSTRAINT pk_event_synonym PRIMARY KEY (id);
ALTER TABLE event_synonym ADD FOREIGN KEY(event_id) REFERENCES event(id);
CREATE INDEX index_event_synonym_event_id ON event_synonym (event_id);

ALTER TABLE category ALTER COLUMN id INT NOT NULL;
ALTER TABLE category ADD CONSTRAINT pk_category PRIMARY KEY (id);
ALTER TABLE category ADD FOREIGN KEY (tour_id) REFERENCES tour(id);
CREATE INDEX index_category_tour_id ON category (tour_id);

ALTER TABLE category_mapping ALTER COLUMN id INT NOT NULL;
ALTER TABLE category_mapping ADD CONSTRAINT pk_category_mapping PRIMARY KEY (id);
ALTER TABLE category_mapping ADD FOREIGN KEY(event_id) REFERENCES event(id);
CREATE INDEX index_category_mapping_category_id ON category_mapping (category_id);
CREATE INDEX index_category_mapping_event_id ON category_mapping (event_id);

ALTER TABLE category_synonym ALTER COLUMN id INT NOT NULL;
ALTER TABLE category_synonym ADD CONSTRAINT pk_category_synonym PRIMARY KEY (id);
CREATE INDEX index_category_synonym_category_id ON category_synonym (category_id);

ALTER TABLE admitone_user_alert ALTER COLUMN id INT NOT NULL;
ALTER TABLE admitone_user_alert ADD CONSTRAINT pk_admitone_user_alert PRIMARY KEY (id);
ALTER TABLE admitone_user_alert ADD FOREIGN KEY(category_id) REFERENCES category(id);
CREATE INDEX index_admitone_user_alert_category_id ON admitone_user_alert (category_id);

ALTER TABLE event_price_adjustment ADD CONSTRAINT pk_event_price_adjustment PRIMARY KEY (event_id, site_id);
ALTER TABLE event_price_adjustment ADD FOREIGN KEY(event_id) REFERENCES event(id);
ALTER TABLE event_price_adjustment ADD FOREIGN KEY(site_id) REFERENCES site(id);
CREATE INDEX index_event_price_adjustment_event_id ON event_price_adjustment (event_id);
CREATE INDEX index_event_price_adjustment_site_id ON event_price_adjustment (site_id);

ALTER TABLE property ADD CONSTRAINT pk_property PRIMARY KEY (name);

ALTER TABLE short_transaction ALTER COLUMN id INT NOT NULL;
ALTER TABLE short_transaction ADD CONSTRAINT pk_short_transaction PRIMARY KEY (id);
ALTER TABLE short_transaction ADD FOREIGN KEY(event_id) REFERENCES event(id);
ALTER TABLE short_transaction ADD FOREIGN KEY(username) REFERENCES admitone_user(username);
ALTER TABLE short_transaction ADD FOREIGN KEY(venue_id) REFERENCES venue(id);
CREATE INDEX index_short_transaction_event_id ON short_transaction (event_id);
CREATE INDEX index_short_transaction_username ON short_transaction (username);
CREATE INDEX index_short_transaction_venue_id ON short_transaction (venue_id);

ALTER TABLE synonym ADD CONSTRAINT pk_synonym PRIMARY KEY (name);

ALTER TABLE ticket_listing_crawl ALTER COLUMN id INT NOT NULL;
ALTER TABLE ticket_listing_crawl ADD CONSTRAINT pk_ticket_listing_crawl PRIMARY KEY (id);
ALTER TABLE ticket_listing_crawl ADD FOREIGN KEY(site_id) REFERENCES site(id);
ALTER TABLE ticket_listing_crawl ADD FOREIGN KEY(tour_id) REFERENCES tour(id);
ALTER TABLE ticket_listing_crawl ADD FOREIGN KEY(event_id) REFERENCES event(id);
ALTER TABLE ticket_listing_crawl ADD FOREIGN KEY(creator) REFERENCES admitone_user(username);
CREATE INDEX index_ticket_listing_crawl_site_id ON ticket_listing_crawl (site_id);
CREATE INDEX index_ticket_listing_crawl_tour_id ON ticket_listing_crawl (tour_id);
CREATE INDEX index_ticket_listing_crawl_event_id ON ticket_listing_crawl (event_id);
CREATE INDEX index_ticket_listing_crawl_creator ON ticket_listing_crawl (creator);

ALTER TABLE tour_price_adjustment ADD CONSTRAINT pk_tour_price_adjustment PRIMARY KEY (tour_id, site_id);
ALTER TABLE tour_price_adjustment ADD FOREIGN KEY(tour_id) REFERENCES tour(id);
ALTER TABLE tour_price_adjustment ADD FOREIGN KEY(site_id) REFERENCES site(id);
CREATE INDEX index_tour_price_adjustment_tour_id ON tour_price_adjustment (tour_id);
CREATE INDEX index_tour_price_adjustment_site_id ON tour_price_adjustment (site_id);

ALTER TABLE short_broadcast ALTER COLUMN id INT NOT NULL;
ALTER TABLE short_broadcast ADD CONSTRAINT pk_short_broadcast PRIMARY KEY (id);
ALTER TABLE short_broadcast ADD FOREIGN KEY(event_id) REFERENCES event(id);
CREATE INDEX index_short_broadcast_event_id ON short_broadcast (event_id);

ALTER TABLE ticket ADD CONSTRAINT pk_ticket PRIMARY KEY (id);
ALTER TABLE ticket ADD FOREIGN KEY(site_id) REFERENCES site(id);
ALTER TABLE ticket ADD FOREIGN KEY(crawl_id) REFERENCES ticket_listing_crawl(id);
ALTER TABLE ticket ADD FOREIGN KEY(event_id) REFERENCES event(id);
CREATE INDEX index_ticket_ticket_status ON ticket (ticket_status);
CREATE INDEX index_ticket_site_id ON ticket (site_id);
CREATE INDEX index_ticket_crawl_id ON ticket (crawl_id);
CREATE INDEX index_ticket_event_id ON ticket (event_id);
CREATE INDEX index_ticket_remaining_quantity ON ticket (remaining_quantity);

--
-- THERE IS NO WAY OF ALTERING A COLUMN TO IDENTITY OTHER THAN DOING IT MANUALLY
-- IN SQL MANAGEMENT STUDIO (click on table, then column and set identity to YES)
-- NOTE: event.id IS NOT AUTO_INCREMENT and thus should not be defined as ENTITY 
--
-- It has to be done for the followings:
-- artist.id
-- category.id
-- category_mapping.id
-- category_synonym.id
-- event_synonym.id
-- short_broadcast.id
-- short_transaction.id
-- tour.id
-- venue.id