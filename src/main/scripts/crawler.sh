#!/bin/sh

JARFILE=tmat-1.jar

printUsage() {
  echo "Usage: $0 <start|stop|run>"
  echo "Commands:"
  echo "  start: run in background"
  echo "  stop: stop process in background"
  echo "  run: run process in foreground"
}

if [ $# != 1 ]
then
  printUsage
  exit 0
fi

case $1 in
  'start')
  java -Xmx512m -jar $JARFILE >> crawler.out 2>&1 &
  ;;

  'run')
  java -Xmx512m -jar $JARFILE start | tee -a crawler.out 2>> crawler.out
  ;;

  'stop')
  java -Xmx512m -jar $JARFILE stop | tee -a crawler.out 2>> crawler.out
  ;;

  *)
  printUsage
  ;;
esac
